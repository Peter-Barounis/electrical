
#define _RTCC    0xD0
#define _24C64   0xA0

void start(void);
void stop(void);
unsigned char read_I2C(unsigned char ack);
unsigned char send_I2C(unsigned data);
unsigned char read_i2c_RTCC(unsigned int address,unsigned char device);
void  write_i2c_RTCC(unsigned int address,unsigned char data,unsigned char device);
unsigned char read_i2c_EEPROM(unsigned int address,unsigned char device);
void  write_i2c_EEPROM(unsigned int address,unsigned char data,unsigned char device);


void Read_Date_Time(unsigned char Date);
void New_Time(void);
void New_Date(void);


