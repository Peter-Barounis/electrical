/*****************************************************
This program was produced by the
CodeWizardAVR V2.05.3 Standard

Project :  Data System CBID Controller Box
Version : 1.05
Date    : 05/01/2015


Chip type               : ATmega64A
Program type            : Application
AVR Core Clock frequency: 7.370860 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 1024
*****************************************************/

/*
Version V1.05 (02/12/2018)

-Add new MSG 7 which reporting Bin Status and connection between Vault and ID box.(Enable function with green jumper)
-Extended Bin - In response MSG time to ~1 min instead of ~40 sec.
-Add logic to control Light_2 in vault(s), following logic below:

     	     Enquiry with "Light" feature:
          	    10 BYTE ENQUIRY FORMAT:

           ENQ HH MM SS L1 L2 L3 L4 ETX BCC

          Where: Ln =>
	-------------------------------------------------------------
	|     Ln    |  Light _2          |            Light _1         |
	|-------------------------------------------------------------
	|   0x00    |	Off	|	Off	  |
	|   0x01    |	Off	|	On	  |
	|   0x02    |	Off	|	Blinking	  | 
	|   0x03    |	N/A	|	N/A	  |
	|   0x04    |	On	|	Off	  |
	|   0x05    |	On	|	On	  |
	|   0x06    |	On	|	Blinking	  |
	|   0x07    |	N/A	|	N/A	  |
	--------------------------------------------------------------
*/

//Version V1.04: Change (extend) duration of TimeOut timer.(for Vaulted but CBX ID not detected)

#include <mega64a.h>
#include <delay.h>
#include <stdio.h>
#include "I2C.h"
#include "CBID_COMP.h"


//#define _DEBUG_


#ifndef RXB8
#define RXB8 1
#endif

#ifndef TXB8
#define TXB8 0
#endif

#ifndef UPE
#define UPE 2
#endif

#ifndef DOR
#define DOR 3
#endif

#ifndef FE
#define FE 4
#endif

#ifndef UDRE
#define UDRE 5
#endif

#ifndef RXC
#define RXC 7
#endif

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)


//For Mode Jumper
volatile unsigned char ModeSelect,TM;
#define Jumper_Hi {PORTC.4=0;DDRC.3=1;PORTC.3=1;PORTC.4=0;TM=PINC.4;}
#define Jumper_Lo {PORTC.4=0;DDRC.3=1;PORTC.3=0;PORTC.4=0;TM=PINC.4;}



// USART0 Receiver buffer
#define RX_BUFFER_SIZE0 32
char rx_buffer0[RX_BUFFER_SIZE0];
unsigned int rx_wr_index0;


unsigned char Buffer[8];
static unsigned char Vault_Light[4][2];

void Poll_Vault(unsigned char Vault_Number);
void WriteEvevt(unsigned char *data);
unsigned char ReadEvent(unsigned char ack,unsigned char *data);
void Shift_RTCC_SRAM_Data();
unsigned int read_adc(unsigned char adc_input);
void debug_putchar(unsigned char dat_tx);


unsigned int R_Flash_Pointer_Begin(void);
void W_Flash_Pointer_Begin(unsigned int pointer);

unsigned int R_Flash_Pointer_End(void);
void W_Flash_Pointer_End(unsigned int pointer);



struct Timers_{
               unsigned int Timer0,Timer1,Timer2,Blink_Timer,PowerUpDelay,CBX_Error[4],BIN_In[4];
               }Timers;


extern struct Rtc_{
                    unsigned char hour,min,sec,month,date,year;
                    unsigned char New_hour,New_min,New_sec;
                  }Rtc;


struct RS232_{
               unsigned char CheckSum,Counter,Request,Expect_ACK;
              }RS232;


struct RS485_{
              unsigned char LRC,Counter,Data_Ready,LRC_Chk,Service_Led_Counter;
              }RS485;


struct Vault_{
               unsigned char Vault_No[4][8],BIN_No[4][8],CBX_No[4][8];
               unsigned char Debounce_Vault_No[4][8],Debounce_BIN_No[4][8],Debounce_CBX_No[4][8];
               unsigned char OLD_Vault_No[4][8],OLD_BIN_No[4][8],OLD_CBX_No[4][8];
               unsigned char Debounce_Counter[4][3];
               unsigned char OffLine[4],OffLine_Counter;
               unsigned char Counter,Data_Ready[4],CBX_Status[4],StatusCounter,CH_Counter;
               unsigned char IgnoreACK,ChanelStatus[4][2];
               }Vault;



eeprom unsigned char PrepareEEPROM[3]={0,0,0};


// Timer1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
 unsigned char Cnt;

 // Reinitialize Timer1 value
 TCNT1H=0xFC68 >> 8;
 TCNT1L=0xFC68 & 0xff;


 if ((++Timers.Blink_Timer&0x200)) Sys_Led=!Sys_Led;
 if (Timers.PowerUpDelay>0) Timers.PowerUpDelay--;
 if (Timers.Timer0>0) Timers.Timer0--;
 if (Timers.Timer1>0) Timers.Timer1--;
 if (Timers.Timer2>0)
    {
    Timers.Timer2--;
    Link_Led1=1;
    Link_Led2=0;
    }
    else
        {
        Link_Led1=0;
        Link_Led2=1;
        }

 for (Cnt=0;Cnt<2;Cnt++)
     {
    //CBX Read Error Timers
    if (Timers.CBX_Error[Cnt]>1) Timers.CBX_Error[Cnt]--;

    //Bin In
    if (Timers.BIN_In[Cnt]) Timers.BIN_In[Cnt]--;
    }

}









// USART0 Receiver interrupt service routine
interrupt [USART0_RXC] void usart0_rx_isr(void)
{
char status,data;

status=UCSR0A;
data=UDR0;
if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
   {
   if (data==STX)
     {
     rx_wr_index0=0;
     RS485.LRC=0;
     RS485.Counter=10;
     }
     else
        {
        if (data==EOT) RS485.Counter=0;
        if (++RS485.Counter>3)
            {
            RS485.Counter=10;
            RS485.LRC=RS485.LRC^data;
            rx_buffer0[(rx_wr_index0++)&0x1F]=data;
            }
            else
                {
                if (RS485.Counter==3)
                    {
                     RS485.LRC=RS485.LRC^EOT;

                                      //  LRC_L                                     LRC_H
                      if (( (0x30|(RS485.LRC&0x0F))==data) && (RS485.LRC_Chk==(0x30|((RS485.LRC&0xF0)>>4) )))
                        {
                        if (rx_buffer0[1]=='P')
                            {
                            Vault.Counter=rx_buffer0[0]&0x03;

                            //Vault Number
                            for (status=0;status<6;status++) Vault.Vault_No[Vault.Counter][status]=rx_buffer0[status+2];
                            //Bin Number
                            for (status=0;status<6;status++) Vault.BIN_No[Vault.Counter][status]=rx_buffer0[status+8];
                            //CBX Number
                            for (status=0;status<6;status++) Vault.CBX_No[Vault.Counter][status]=rx_buffer0[status+14];

                            Vault.Data_Ready[Vault.Counter]=1;
                            Vault.CBX_Status[Vault.Counter]=rx_buffer0[20];
                            };
                        };
                    }
                    else RS485.LRC_Chk=data; // RS485.Counter==1
                }

        }
   }
}




// USART1 Receiver buffer
#define RX_BUFFER_SIZE1 16
unsigned char rx_buffer1[RX_BUFFER_SIZE1];


// USART1 Receiver interrupt service routine
interrupt [USART1_RXC] void usart1_rx_isr(void)
{
char status,data;

status=UCSR1A;
data=UDR1;

if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
   {
    if ((RS232.Expect_ACK==0)&&(data==ACK))
        {
        RS232.Expect_ACK=ACK;
        RS232.Request=1;
        Timers.Timer1=0; //Ready To Receive New Poll
        goto Exit_Uart1_Int;
        }
         else RS232.Expect_ACK=NAK;



    if ((data==ENQ)&&(Timers.Timer1==0))
        {
        Timers.Timer1=300;
        RS232.CheckSum=-ETX;
        RS232.Counter=0;
        goto Exit_Uart1_Int;
        }


    if ((RS232.CheckSum==data)&&(RS232.Counter==8))
        {
        Rtc.New_hour=rx_buffer1[0];
        Rtc.New_min=rx_buffer1[1];
        Rtc.New_sec=rx_buffer1[2];

        Vault_Light[0][0]=rx_buffer1[3];                                        
        Vault_Light[1][0]=rx_buffer1[4];
        Vault_Light[2][0]=rx_buffer1[5];
        Vault_Light[3][0]=rx_buffer1[6];

        Timers.Timer1=0;
        RS232.Request=1;
        RS232.Counter++;
        goto Exit_Uart1_Int;
        }

    if (RS232.Counter<=8)
        {
        RS232.CheckSum+=data;
        rx_buffer1[RS232.Counter++]=data;
        }
    }

  Exit_Uart1_Int:
}






// USART0 Transmitter buffer
#define TX_BUFFER_SIZE0 32
char tx_buffer0[TX_BUFFER_SIZE0];
unsigned char tx_wr_index0,tx_rd_index0,tx_counter0;


// USART0 Transmitter interrupt service routine
interrupt [USART0_TXC] void usart0_tx_isr(void)
{
if (tx_counter0)
   {
   --tx_counter0;
   UDR0=tx_buffer0[tx_rd_index0++];
   if (tx_rd_index0 == TX_BUFFER_SIZE0) tx_rd_index0=0;
   }
   else DE=0;
}





#ifndef _DEBUG_TERMINAL_IO_
// Write a character to the USART0 Transmitter buffer
#define _ALTERNATE_PUTCHAR_
#pragma used+
void putchar(char c)
{
while (tx_counter0 == TX_BUFFER_SIZE0);
#asm("cli")
if (tx_counter0 || ((UCSR0A & DATA_REGISTER_EMPTY)==0))
   {
   tx_buffer0[tx_wr_index0++]=c;
   if (tx_wr_index0 == TX_BUFFER_SIZE0) tx_wr_index0=0;
   ++tx_counter0;
   }
else
   UDR0=c;
#asm("sei")
}
#pragma used-
#endif





// USART1 Transmitter buffer
#define TX_BUFFER_SIZE1 16
char tx_buffer1[TX_BUFFER_SIZE1];
unsigned char tx_wr_index1,tx_rd_index1,tx_counter1;


// USART1 Transmitter interrupt service routine
interrupt [USART1_TXC] void usart1_tx_isr(void)
{
if (tx_counter1)
   {
   --tx_counter1;
   UDR1=tx_buffer1[tx_rd_index1++];
   if (tx_rd_index1 == TX_BUFFER_SIZE1) tx_rd_index1=0;
   }
}


// Write a character to the USART1 Transmitter buffer
#pragma used+
void putchar1(char c)
{
while (tx_counter1 == TX_BUFFER_SIZE1);
#asm("cli")
if (tx_counter1 || ((UCSR1A & DATA_REGISTER_EMPTY)==0))
   {
   tx_buffer1[tx_wr_index1++]=c;
   if (tx_wr_index1 == TX_BUFFER_SIZE1) tx_wr_index1=0;
   ++tx_counter1;
   }
else
   UDR1=c;
#asm("sei")
}
#pragma used-







//******************************************* MAIN **********************************************
void main(void)
{

 unsigned char Counter_Poll=0,Counter_Responds=0,CBX_Present[4],tmp1,tmp2,tmp3;
 unsigned int tmp_int;

// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
PORTA=0x00;
DDRA=0x01;

// Port B initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
PORTB=0x00;
DDRB=0x00;

// Port C initialization
// Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=P State3=P State2=T State1=T State0=T
PORTC=0x18;
DDRC=0xE0;


// Port D initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
PORTD=0x00;
DDRD=0xF0;

// Port E initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
PORTE=0x00;
DDRE=0x00;

// Port F initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
PORTF=0x00;
DDRF=0x00;

// Port G initialization
// Func4=In Func3=In Func2=In Func1=In Func0=In
// State4=T State3=T State2=T State1=T State0=T
PORTG=0x00;
DDRG=0x00;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=0xFF
// OC0 output: Disconnected
ASSR=0x00;
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;


// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 921.358 kHz
// Mode: Normal top=0xFFFF
// OC1A output: Discon.
// OC1B output: Discon.
// OC1C output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: On
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
// Compare C Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x02;
TCNT1H=0xFF;
TCNT1L=0xF0;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;
OCR1CH=0x00;
OCR1CL=0x00;

// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer2 Stopped
// Mode: Normal top=0xFF
// OC2 output: Disconnected
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

// Timer/Counter 3 initialization
// Clock source: System Clock
// Clock value: Timer3 Stopped
// Mode: Normal top=0xFFFF
// OC3A output: Discon.
// OC3B output: Discon.
// OC3C output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer3 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
// Compare C Match Interrupt: Off
TCCR3A=0x00;
TCCR3B=0x00;
TCNT3H=0x00;
TCNT3L=0x00;
ICR3H=0x00;
ICR3L=0x00;
OCR3AH=0x00;
OCR3AL=0x00;
OCR3BH=0x00;
OCR3BL=0x00;
OCR3CH=0x00;
OCR3CL=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
// INT3: Off
// INT4: Off
// INT5: Off
// INT6: Off
// INT7: Off
EICRA=0x00;
EICRB=0x00;
EIMSK=0x00;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x04;

ETIMSK=0x00;

// USART0 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART0 Receiver: On
// USART0 Transmitter: On
// USART0 Mode: Asynchronous
// USART0 Baud Rate: 9600
UCSR0A=0x00;
UCSR0B=0xD8;
UCSR0C=0x06;
UBRR0H=0x00;
UBRR0L=0x2F;

// USART1 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART1 Receiver: On
// USART1 Transmitter: On
// USART1 Mode: Asynchronous
// USART1 Baud Rate: 9600
UCSR1A=0x00;
UCSR1B=0xD8;
UCSR1C=0x06;
UBRR1H=0x00;
UBRR1L=0x2F;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;


// ADC initialization
// ADC Clock frequency: 921.357 kHz
// ADC Voltage Reference: AREF pin
ADMUX=ADC_VREF_TYPE & 0xff;
ADCSRA=0x83;

// SPI initialization
// SPI disabled
SPCR=0x00;

// TWI initialization
// TWI disabled
TWCR=0x00;


// Watchdog Timer initialization
// Watchdog Timer Prescaler: OSC/2048k
#pragma optsize-
WDTCR=0x1F;
WDTCR=0x0F;
#ifdef _OPTIMIZE_SIZE_
#pragma optsize+
#endif



//** Check or INIT EEPROM/RTCC Sram Pointers ***

 if ((PrepareEEPROM[0]!='G')||(PrepareEEPROM[1]!='F')||(PrepareEEPROM[2]!='I'))
     {
      PrepareEEPROM[0]='G';
      PrepareEEPROM[1]='F';
      PrepareEEPROM[2]='I';
      goto Prepare_EEPROM;
     }

     tmp1=read_i2c_RTCC(55,_RTCC);
     if ((tmp1!=123)||(read_i2c_RTCC(50,_RTCC)>8)||((PINC.3==0)&&(PINC.4==0)))
        {

Prepare_EEPROM:

        write_i2c_RTCC(55,123,_RTCC);
        write_i2c_RTCC(50,0,_RTCC);
        W_Flash_Pointer_Begin(0);
        W_Flash_Pointer_End(0);
        }


  for (tmp2=0;tmp2<4;tmp2++)  // Flush old CBX,BIN,Vault numbers
        {
        for (tmp1=0;tmp1<6;tmp1++)
            {
            Vault.OLD_BIN_No[tmp2][tmp1]='0';
            Vault.OLD_Vault_No[tmp2][tmp1]='0';
            Vault.OLD_CBX_No[tmp2][tmp1]='0';
            }

        Vault.OffLine[tmp2]=0;
        Vault.Data_Ready[tmp2]=0;
        Vault.CBX_Status[tmp2]=0;
        CBX_Present[tmp2]=0;

        Vault.OLD_BIN_No[tmp2][2]=4;
        Vault.OLD_BIN_No[tmp2][3]=0;
        Vault.OLD_BIN_No[tmp2][4]=9;
        Vault.OLD_BIN_No[tmp2][5]=5;

        //Bin In Timer
        Timers.BIN_In[tmp2]=50000;


        Vault.Debounce_Counter[tmp2][CBX]=0;
        Vault.Debounce_Counter[tmp2][BIN]=0;
        }


        #asm("wdr");

        delay_ms(4000);
        Timers.PowerUpDelay=30000;
        Timers.CBX_Error[0]=0;
        Timers.CBX_Error[1]=0;

        Vault.StatusCounter=0;
        Vault.CH_Counter=0;
        Vault.IgnoreACK=0;


// Global enable interrupts
#asm("sei")


//*********************************************** MAIN LOOP ***************************************************
while (1)
      {
      #asm("wdr");



      //OLD *****  Poling every 300ms (each wault(n) every 2 * 300ms = 600ms)
      if (Timers.Timer0==0)
            {
            ++Counter_Poll;  //next vault
            Counter_Poll&=0x01;  // OLD 0x03
            Poll_Vault(Counter_Poll);
            Timers.Timer0=300;     //poling time 300ms //OLD 200ms


            //Get Mode Jumper Status
            ModeSelect=0;
            Jumper_Hi;
            if (TM)
               {
                Jumper_Lo;
                if (!TM) ModeSelect=1;
               }


            if (Vault.OffLine[Counter_Responds]<10) Vault.OffLine[Counter_Responds]++;

            if (Vault.ChanelStatus[Counter_Poll][0]==1) Vault.ChanelStatus[Counter_Poll][1]=1;
              else Vault.ChanelStatus[Counter_Poll][1]=0;

            Vault.ChanelStatus[Counter_Poll][0]=0;

            // All Vaults Offline Service Led = RED
            if (++RS485.Service_Led_Counter<20) Service_Led=0;
             else
                {
                Service_Led=1;
                RS485.Service_Led_Counter=20;
                };
            }


      Counter_Responds++;
      Counter_Responds&=1;  //3




      //******** Data System Poll Respond *********
      if (RS232.Request==1)
        {
        #asm("wdr");

        RS232.Request=0;
        Timers.Timer2=OFFLINE_TIME;

        //Respond To Poll with Data
        if (RS232.Expect_ACK!=ACK)
            {
            //Set New Time
            if (Rtc.sec!=Rtc.New_sec) write_i2c_RTCC(0,Rtc.New_sec,_RTCC);  //New Sec
            if (Rtc.min!=Rtc.New_min) write_i2c_RTCC(1,Rtc.New_min,_RTCC);  //New Min
            if (Rtc.hour!=Rtc.New_hour) write_i2c_RTCC(2,Rtc.New_hour,_RTCC); //New Hours

Loop_Send:
            //Read Events (from memory)
            tmp1=ReadEvent(READ,Buffer);

            //****** Send Data ******
            tmp3=0;
            if (tmp1!=2)
              {
                putchar1(STX);    //STX
                for (tmp2=0;tmp2<6;tmp2++)  //Event Byte , Time (HHMMSS) ,I.D. Number
                    {
                    tmp3+=Buffer[tmp2];
                    putchar1(Buffer[tmp2]);
                    }

                putchar1(ETX);  //ETX
                putchar1(tmp3); //Check Sum = sum of data

                RS232.Expect_ACK=0; //Request for ACK
                Vault.IgnoreACK=0;
                Vault.StatusCounter=0;
              }
                else
                    {
                    //****** Send vault Status ********
                    if ((++Vault.StatusCounter>=10)&&(ModeSelect))
                        {
                        Vault.StatusCounter=0;

                        Vault.CH_Counter &=1;
                        Buffer[0]=(Vault.CH_Counter<<4) | BIN_STATUS; //Status Command And Chanel
                        Buffer[1]=Rtc.hour;    //Hours
                        Buffer[2]=Rtc.min;     //Min
                        Buffer[3]=Rtc.sec;     //Sec

                        // Respond Bin Number
                        if (Vault.ChanelStatus[Vault.CH_Counter][1])
                            {
                            Buffer[4]=((Vault.BIN_No[Vault.CH_Counter][2]&0x0F)<<4) | (Vault.BIN_No[Vault.CH_Counter][3]&0x0F);
                            Buffer[5]=((Vault.BIN_No[Vault.CH_Counter][4]&0x0F)<<4) | (Vault.BIN_No[Vault.CH_Counter][5]&0x0F);
                            }
                            else // Respond Chanel Offline
                                {
                                Buffer[4]=0x40;   // ID_H
                                Buffer[5]=0x44;   // ID_L
                                }

                        Vault.CH_Counter++;


                        //Send Status
                        putchar1(STX);    //STX

                        for (tmp2=0;tmp2<6;tmp2++)  //Event Byte , Time (HHMMSS) ,I.D. Number
                            {
                            tmp3+=Buffer[tmp2];
                            putchar1(Buffer[tmp2]);
                            }

                        putchar1(ETX);  //ETX
                        putchar1(tmp3); //Check Sum = sum of data

                        Vault.IgnoreACK=1;
                        RS232.Expect_ACK=0; //Request for ACK

                        }
                        else   //No Data in buffer
                            {
                            //If at Power Up Vault is not in, send MSG 4096
                            if ((Vault.OffLine[Vault.OffLine_Counter&0x01]==10)&&(!Timers.PowerUpDelay))
                               {
                               tmp1=(Vault.OffLine_Counter&0x01)<<4;         //0x03
                               Buffer[0]=tmp1|0x05;
                               Buffer[1]=Rtc.hour;    //Hours
                               Buffer[2]=Rtc.min;     //Min
                               Buffer[3]=Rtc.sec;     //Sec
                               Buffer[4]=0x40;        //ID_H
                               Buffer[5]=0x95;        //ID_L

                               WriteEvevt(Buffer);

                               Vault.OffLine[Vault.OffLine_Counter&0x01]++;      //0x03
                               }
                               else putchar1(EOT); // Respond to poll with no data MSG

                            Vault.OffLine_Counter++;
                            Vault.OffLine_Counter&=0x01;  //0x03
                            };
                    }
           }
           else
                {
                if (RS232.Expect_ACK==ACK)
                    {
                    if (!Vault.IgnoreACK)
                        {
                        ReadEvent(ERASE,Buffer);
                        RS232.Expect_ACK=NAK;
                        goto Loop_Send;
                        }

                    putchar1(EOT);
                    }

                Vault.IgnoreACK=0;
                RS232.Expect_ACK=NAK;
                }

        }






      //************** Vault Poll Respond *****************
      if (Vault.Data_Ready[Counter_Responds])
        {
         RS485.Service_Led_Counter=0;
         Vault.Data_Ready[Counter_Responds]=0;

         //Vault Activity
         Vault.ChanelStatus[Counter_Responds][0]=1;

         if (Vault.OffLine[Counter_Responds]>=10)
            {
            for (tmp1=0;tmp1<6;tmp1++) Vault.OLD_BIN_No[Counter_Responds][tmp1]='0';
            for (tmp1=0;tmp1<6;tmp1++) Vault.OLD_CBX_No[Counter_Responds][tmp1]='0';

            Vault.Debounce_Counter[Counter_Responds][BIN]=0;
            Vault.Debounce_Counter[Counter_Responds][CBX]=0;
            }

         Vault.OffLine[Counter_Responds]=0;

         Read_Date_Time(0);// Refresh Time



             //********************************************* VAULT NUMBER *************************************************
             #asm("wdr");
             for (tmp2=0;tmp2<6;tmp2++) if ((Vault.Vault_No[Counter_Responds][tmp2]!='0')&&(Vault.Vault_No[Counter_Responds][tmp2]!=0)) tmp1|=1;
             if (tmp1)
                {
                 tmp1=0;
                 for (tmp2=0;tmp2<6;tmp2++)
                    {
                    if ((Vault.Vault_No[Counter_Responds][tmp2]!=Vault.OLD_Vault_No[Counter_Responds][tmp2])) tmp1|=1;
                    Vault.OLD_Vault_No[Counter_Responds][tmp2]=Vault.Vault_No[Counter_Responds][tmp2];
                    }
                }





             //******************************************** BIN NUMBER ***********************************************
             #asm("wdr");
             tmp3=0;
             tmp1=0;
             for (tmp2=0;tmp2<6;tmp2++)
                        {
                         //Check and debounce BIN number
                         if (Vault.BIN_No[Counter_Responds][tmp2]==Vault.Debounce_BIN_No[Counter_Responds][tmp2]) Vault.Debounce_Counter[Counter_Responds][BIN]++;
                           else
                               {
                               Vault.Debounce_Counter[Counter_Responds][BIN]=0;
                               Timers.BIN_In[Counter_Responds]=50000;
                               }

                         //Reading BIN No
                         if ((Vault.BIN_No[Counter_Responds][tmp2]>'0')&&((Vault.BIN_No[Counter_Responds][tmp2]<='9'))) tmp3|=2;

                         //Check if number is change
                         if ((!Timers.BIN_In[Counter_Responds])&&(Vault.BIN_No[Counter_Responds][tmp2]!=Vault.OLD_BIN_No[Counter_Responds][tmp2])) tmp1|=1;
                         Vault.Debounce_BIN_No[Counter_Responds][tmp2]=Vault.BIN_No[Counter_Responds][tmp2];
                        }

                //Limit Debounce No
                if (Vault.Debounce_Counter[Counter_Responds][BIN]>=DEBOUNCE_BIN) Vault.Debounce_Counter[Counter_Responds][BIN]=DEBOUNCE_BIN;

                if (tmp3==0)
                            {
                            Vault.Debounce_Counter[Counter_Responds][BIN]+=20;
                            //BIN remove, add respond delay ~5 sec
                            if (Timers.BIN_In[Counter_Responds]>5000) Timers.BIN_In[Counter_Responds]=500;
                            }

                if (tmp1)
                        {
                        if (tmp3==2) tmp1=BIN_IN;
                          else tmp1=BIN_OUT;


                                 //Chanel Number       Event
                        Buffer[0]=(Counter_Responds<<4)|tmp1;

                        Buffer[1]=Rtc.hour;//Hours
                        Buffer[2]=Rtc.min; //Min
                        Buffer[3]=Rtc.sec; //Sec

                        if (tmp3)
                            {
                            tmp1=Vault.BIN_No[Counter_Responds][2]&0x0F;
                            tmp2=Vault.BIN_No[Counter_Responds][3]&0x0F;
                            Buffer[4]=(tmp1<<4)|tmp2;

                            tmp1=Vault.BIN_No[Counter_Responds][4]&0x0F;
                            tmp2=Vault.BIN_No[Counter_Responds][5]&0x0F;
                            Buffer[5]=(tmp1<<4)|tmp2;
                            }
                            else
                                {
                                tmp1=Vault.OLD_BIN_No[Counter_Responds][2]&0x0F;
                                tmp2=Vault.OLD_BIN_No[Counter_Responds][3]&0x0F;
                                Buffer[4]=(tmp1<<4)|tmp2;

                                tmp1=Vault.OLD_BIN_No[Counter_Responds][4]&0x0F;
                                tmp2=Vault.OLD_BIN_No[Counter_Responds][5]&0x0F;
                                Buffer[5]=(tmp1<<4)|tmp2;
                                }


                        WriteEvevt(Buffer);
                        for (tmp2=0;tmp2<6;tmp2++) Vault.OLD_BIN_No[Counter_Responds][tmp2]=Vault.BIN_No[Counter_Responds][tmp2];
                        }







             //************************************************ CBX NUMBER **********************************************
             #asm("wdr");
             tmp3=0;
             tmp1=0;
             for (tmp2=0;tmp2<6;tmp2++)
                        {
                         if (Vault.CBX_No[Counter_Responds][tmp2]==Vault.Debounce_CBX_No[Counter_Responds][tmp2]) Vault.Debounce_Counter[Counter_Responds][CBX]++;
                          else Vault.Debounce_Counter[Counter_Responds][CBX]=0;

                         if (Vault.CBX_No[Counter_Responds][tmp2]!='0') tmp3=2;
                         if ((Vault.Debounce_Counter[Counter_Responds][CBX]>=DEBOUNCE_CBX)&&(Vault.CBX_No[Counter_Responds][tmp2]!=Vault.OLD_CBX_No[Counter_Responds][tmp2])) tmp1|=1;

                         Vault.Debounce_CBX_No[Counter_Responds][tmp2]=Vault.CBX_No[Counter_Responds][tmp2];
                        }


                if (Vault.Debounce_Counter[Counter_Responds][CBX]>=230) Vault.Debounce_Counter[Counter_Responds][CBX]=230;


                //Vaulted ,ID not detected generate ERROR MSG 6
                if ((Timers.CBX_Error[Vault.Counter]==1)&&(CBX_Present[Vault.Counter]==3)&&((Vault.CBX_Status[Vault.Counter]&2)==0)&&(tmp3==0))
                    {
                    tmp1=1;
                    tmp3=4;
                    }


                //Set CBX ID not Detected Error Timer
                if ((Vault.CBX_Status[Vault.Counter]&3)==0) Timers.CBX_Error[Vault.Counter]=CBX_TIMEOUT;


                // CBX present bit + position bit
                CBX_Present[Vault.Counter]=Vault.CBX_Status[Vault.Counter]&3;

                //New
                if (tmp1)
                        {
                        //Assign event
                        if (tmp3!=4)
                            {
                            if (((Vault.CBX_Status[Vault.Counter]&2)|tmp3)==2) tmp1=CBX_IN;  //CBX Insert
                               else tmp1=CBX_OUT;  //CBX Remove
                            }
                             else tmp1=CBX_ID_ERROR;   //Vaulter no detected CBX ID


                                //  Chanel Number       Event
                        Buffer[0]=(Counter_Responds<<4)|tmp1;

                        Buffer[1]=Rtc.hour;//Hours
                        Buffer[2]=Rtc.min; //Min
                        Buffer[3]=Rtc.sec; //Sec

                        if ((Vault.CBX_Status[Vault.Counter]&2)||(tmp3))
                            {
                            tmp1=Vault.CBX_No[Counter_Responds][2]&0x0F;
                            tmp2=Vault.CBX_No[Counter_Responds][3]&0x0F;
                            Buffer[4]=(tmp1<<4)|tmp2;

                            tmp1=Vault.CBX_No[Counter_Responds][4]&0x0F;
                            tmp2=Vault.CBX_No[Counter_Responds][5]&0x0F;
                            Buffer[5]=(tmp1<<4)|tmp2;
                            }
                            else
                                {
                                tmp1=Vault.OLD_CBX_No[Counter_Responds][2]&0x0F;
                                tmp2=Vault.OLD_CBX_No[Counter_Responds][3]&0x0F;
                                Buffer[4]=(tmp1<<4)|tmp2;

                                tmp1=Vault.OLD_CBX_No[Counter_Responds][4]&0x0F;
                                tmp2=Vault.OLD_CBX_No[Counter_Responds][5]&0x0F;
                                Buffer[5]=(tmp1<<4)|tmp2;
                                }

                        WriteEvevt(Buffer);
                        Timers.CBX_Error[Vault.Counter]=0;

                        for (tmp2=0;tmp2<6;tmp2++) Vault.OLD_CBX_No[Counter_Responds][tmp2]=Vault.CBX_No[Counter_Responds][tmp2];
                        }
        }





           //******************* Check Voltages *******************
           tmp1=0;
           //12V
           tmp_int=(read_adc(1)*32)/100;
           if ((tmp_int<Power_Min)||(tmp_int>Power_Max)) tmp1|=1;
           //5V
           tmp_int=(read_adc(0)*8)/10;
           if ((tmp_int<Vcc_Min)||(tmp_int>Vcc_Max)) tmp1|=2;
            //Isolated
           tmp_int=read_adc(3);
           if (tmp_int>V_ISO_OK) tmp1|=4;

           if (!tmp1) Led_Power=0;
             else Led_Power=1;
    }

}








// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input)
{
ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
// Delay needed for the stabilization of the ADC input voltage
delay_us(10);
// Start the AD conversion
ADCSRA|=0x40;
// Wait for the AD conversion to complete
while ((ADCSRA & 0x10)==0);
ADCSRA|=0x10;
return ADCW;
}






//************************************** Poll Vault(n)*****************************************
void Poll_Vault(unsigned char Vault_Number)
{
 unsigned char tmp;

    Vault_Number&=0x03;

    DE=1;
    putchar(STX);
    putchar(0x30+Vault_Number);//node address
    putchar('P');//Poll  
  
    tmp=Vault_Light[Vault_Number][0] & 0x03;  // Get Bin light status

    if (tmp==2) //Bin blink, status light
        {
         // Toggle light
         if (Vault_Light[Vault_Number][1]==1) Vault_Light[Vault_Number][1]=0;
          else Vault_Light[Vault_Number][1]=1;

        tmp=Vault_Light[Vault_Number][1];
        }

    tmp|=((((Link_Led1<<1) & (Vault_Light[Vault_Number][0]&0x04) >> 1)) & 0x02); // add status of external light 2

    tmp|=0x30; //Convert in ASCII        

    putchar(tmp); // Set/Reset Vault Lights {0,1,2,4,5,6}
    putchar(ETX);


    // LRC=0x80 | (Node_address ^ 'P' ^ Vault_Light ^ ETX)
    putchar(0x80|((0x30+Vault_Number)^'P'^tmp^ETX));
};
















//************************************* Write Data in RTCC Sram Buffer/Eerprm *******************************
void WriteEvevt(unsigned char *data)
{
unsigned char tmp,tmp1,Data_Counter;
unsigned int tmp_int;


    Data_Counter=read_i2c_RTCC(50,_RTCC);

    #ifdef _DEBUG_
    debug_putchar(Data_Counter);
    #endif


    if (Data_Counter<=6) // if counter<6 write new event
        {
        #asm("wdr");
        write_i2c_RTCC(50,1+Data_Counter,_RTCC);//save counter and pointer

        start();
        send_I2C(_RTCC);
        send_I2C(((8+Data_Counter*6)&0xFF));
        for (tmp1=0;tmp1<6;tmp1++) send_I2C(data[tmp1]); //Write in RTCC Sram data[0]=Event ,data[1]=H ,data[2]=M, data[3]=S, data[4]=CBX H, data[5]=CBX L
        stop();
        }
        else
            {
            #asm("wdr");
            if (Data_Counter==7)  //Save RTCC Sram in EEPROM
                {

                 write_i2c_RTCC(50,0x08,_RTCC); // save Data_Counter=8 and Data_Pointer


                 // Wrate in EEPROM  last event
                 tmp_int=7+R_Flash_Pointer_End();
                 tmp_int*=8;

                 start();
                 send_I2C(_24C64);

                 send_I2C(((tmp_int>>8)&0xFF));
                 send_I2C((tmp_int&0xFF));

                 for (tmp1=0;tmp1<6;tmp1++) //Write Data In EEPROM
                        {
                        send_I2C(data[tmp1]);
                        delay_ms(5);
                        }

                  stop();
                 //End write in EEPROM


                 //Copy From RTCC Sram to EEPROM lat 7 events
                 for (tmp=0;tmp<=6;tmp++)
                    {

                    #asm("wdr");

                    //Read From RTCC Sram
                    start();
                    send_I2C(_RTCC);
                    send_I2C((8+tmp*6));
                    start();
                    send_I2C(_RTCC|1);
                    for (tmp1=0;tmp1<6;tmp1++) data[tmp1]=read_I2C(1);
                    read_I2C(0);
                    stop();
                    //End read RTCC Sram


                    // Wrate in EEPROM
                    tmp_int=R_Flash_Pointer_End();
                    W_Flash_Pointer_End((1+tmp_int));

                    tmp_int*=8;

                    start();
                    send_I2C(_24C64);

                    send_I2C(((tmp_int>>8)&0xFF));
                    send_I2C((tmp_int&0xFF));

                    for (tmp1=0;tmp1<6;tmp1++)
                        {
                        #asm("wdr");
                        send_I2C(data[tmp1]);
                        delay_ms(5);
                        }
                    stop();
                    //End write in EEPROM
                    }

                    W_Flash_Pointer_End((1+R_Flash_Pointer_End()));
                }
                else
                    {

                    tmp_int=R_Flash_Pointer_End();
                    W_Flash_Pointer_End((1+tmp_int));

                    tmp_int*=8;

                    //  Write Event in Eeprom
                    start();
                    send_I2C(_24C64);

                    send_I2C(((tmp_int>>8)&0xFF));
                    send_I2C((tmp_int&0xFF));

                    for (tmp=0;tmp<6;tmp++)
                        {
                        send_I2C(data[tmp]);
                        delay_ms(5);
                        }
                    stop();
                    }
            }
};









//************************************* Read Data from  RTCC Sram Buffer/Eeprom  *******************************
unsigned char ReadEvent(unsigned char ack,unsigned char *data)
{

unsigned char Data_Counter,tmp;
unsigned int tmp_int;


    #asm("wdr");

    Data_Counter=read_i2c_RTCC(50,_RTCC);

    if (Data_Counter<=6)
              {
              if (ack==1)
                        {
                        //Erase Data in RTCC Sram
                        if (Data_Counter>0) Data_Counter--;
                         else return(2);

                        Shift_RTCC_SRAM_Data();
                        write_i2c_RTCC(50,Data_Counter,_RTCC);

                        return (1);
                        };


              if (Data_Counter==0) return(2);

              start();
              send_I2C(_RTCC);
              send_I2C(8);
              start();
              send_I2C(_RTCC|1);
              for (tmp=0;tmp<6;tmp++) data[tmp]=read_I2C(1); //data[0]=Event,data[1]=H,data[2]=M,data[3]=S,data[4]=CBX H,data[5]=CBX L
              read_I2C(0);
              stop();

              return(0);
              }
              else
                 {
                 tmp_int=R_Flash_Pointer_Begin();
                 if (tmp_int==R_Flash_Pointer_End())
                    {
                    write_i2c_RTCC(50,0,_RTCC);
                    return(2);
                    }

                 if (ack==1)
                            {
                            //Erase Data in eeprom
                            write_i2c_EEPROM((8*tmp_int),0xFF,_24C64);
                            W_Flash_Pointer_Begin((1+tmp_int));
                            return (1);
                            }

                tmp_int*=8;

                //Read Data From Flash
                start();
                send_I2C(_24C64);
                send_I2C(((tmp_int>>8)&0x1F));
                send_I2C(tmp_int&0xFF);
                start();
                send_I2C(_24C64|1);

                for (tmp=0;tmp<6;tmp++) data[tmp]=read_I2C(1);

                read_I2C(0);
                stop();
                return(0);
                }
};









//********************************  Read Begin Pointer From RTCC_Sram *************************
unsigned int R_Flash_Pointer_Begin()
{
unsigned int Pointer;

    Pointer=read_i2c_RTCC(51,_RTCC);
    Pointer=(Pointer<<8)|read_i2c_RTCC(52,_RTCC);
    return(Pointer);
}






//********************************  Write End Pointer in RTCC_Sram *************************
void W_Flash_Pointer_Begin(unsigned int pointer)
{
    write_i2c_RTCC(51,((pointer>>8)&0xFF),_RTCC);
    write_i2c_RTCC(52,(pointer&0xFF),_RTCC);
}






//********************************  Read End Pointer From RTCC_Sram *************************
unsigned int R_Flash_Pointer_End()
{
unsigned int Pointer;

    Pointer=read_i2c_RTCC(53,_RTCC);
    Pointer=(Pointer<<8)|read_i2c_RTCC(54,_RTCC);
    return(Pointer);
}







//********************************  Write End Pointer in RTCC_Sram *************************
void W_Flash_Pointer_End(unsigned int pointer)
{
    write_i2c_RTCC(53,((pointer>>8)&0xFF),_RTCC);
    write_i2c_RTCC(54,(pointer&0xFF),_RTCC);
}










//********************************  Shift Data in RTCC_Sram (one event)*************************
void Shift_RTCC_SRAM_Data()
{
 unsigned char tmp,Tmp_Buff[36];

    //Read
    start();
    send_I2C(_RTCC);
    send_I2C(14);
    start();
    send_I2C(_RTCC|1);
    for (tmp=0;tmp<36;tmp++)
        {
        Tmp_Buff[tmp]=read_I2C(1);
        #asm("wdr");
        }
    read_I2C(0);
    stop();


    //Write
    start();
    send_I2C(_RTCC);
    send_I2C(8);
    for (tmp=0;tmp<36;tmp++)
        {
        send_I2C(Tmp_Buff[tmp]);
        #asm("wdr");
        }
    stop();
}








//******************************** DEBUG **********************************
#ifdef  _DEBUG_

#define _38400

 #asm
 .equ           PINE    =0x01             ;PINE=0x01
 .equ           RxD     =4                ;PINE.4      //eint4
 .equ           PORTE   =0x03             ;PORTE=0x03;
 .equ           Txd     =5                ;PORTE.5
 .equ           sb      =1                ;Number of stop bits (1, 2, ...)
 .def           temp    =R27
 .def           bitcnt  =R26              ;bit counter
 #endasm

#ifdef _38400
                #asm
                .equ	 b=30   ;7.3728MHz => 38840=30
                #endasm
#else
                #asm
                .equ	 b=128  ;7.3728MHz => 9.6K=128
                #endasm
#endif


register unsigned char reg_dat  @8;

//***************************** DEBUG PUTCHAR **********************************
void debug_putchar(unsigned char dat_tx)
 {
                DDRE.5=1;

                #asm("cli");
                reg_dat=dat_tx;

#asm            cli
			    ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
        		com	R8		;Inverte everything
        		sec			;Start bit

putchar0:   	brcc	putchar1	;If carry set
		        cbi	    PORTE,TxD	;send a '0'
		        rjmp	putchar2	;else

putchar1:	    sbi	    PORTE,TxD	;    send a '1'
		        nop

putchar2:	    ldi	    temp,b
UART_delay1:	dec	    temp
		        brne	UART_delay1

        	    ldi	    temp,b
UART_delay2:	dec 	temp
		        brne	UART_delay2

        		lsr	R8		;Get next bit
        		dec	    bitcnt		;If not all bit sent
        		brne	putchar0	;   send next
        		sei
 #endasm
 };

#endif
