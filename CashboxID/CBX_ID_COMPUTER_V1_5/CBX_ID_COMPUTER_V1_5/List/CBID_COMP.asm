
;CodeVisionAVR C Compiler V2.05.2 Standard
;(C) Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega64
;Program type             : Application
;Clock frequency          : 7.370860 MHz
;Memory model             : Small
;Optimize for             : Speed
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 1024 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;Global 'const' stored in FLASH     : Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions         : On
;Smart register allocation          : Off
;Automatic register allocation      : On

	#pragma AVRPART ADMIN PART_NAME ATmega64
	#pragma AVRPART MEMORY PROG_FLASH 65536
	#pragma AVRPART MEMORY EEPROM 2048
	#pragma AVRPART MEMORY INT_SRAM SIZE 4351
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU XMCRA=0x6D
	.EQU XMCRB=0x6C

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0100
	.EQU __SRAM_END=0x10FF
	.EQU __DSTACK_SIZE=0x0400
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _rx_wr_index0=R4
	.DEF _tx_wr_index0=R6
	.DEF _tx_rd_index0=R7
	.DEF _tx_counter0=R8
	.DEF _tx_wr_index1=R9
	.DEF _tx_rd_index1=R10
	.DEF _tx_counter1=R11

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _timer1_ovf_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _usart0_rx_isr
	JMP  0x00
	JMP  _usart0_tx_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _usart1_rx_isr
	JMP  0x00
	JMP  _usart1_tx_isr
	JMP  0x00
	JMP  0x00

_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  MCUCR,R31
	OUT  MCUCR,R30
	STS  XMCRB,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,LOW(__SRAM_START)
	LDI  R27,HIGH(__SRAM_START)
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x500

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.05.3 Standard
;
;Project :  Data System CBID Controller Box
;Version : 1.05
;Date    : 05/01/2015
;
;
;Chip type               : ATmega64A
;Program type            : Application
;AVR Core Clock frequency: 7.370860 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 1024
;*****************************************************/
;//Version V1.04: Change (extend) duration of TimeOut timer.(for Vaulted but CBX ID not detected)
;
;/*
;-Add new MSG 7 which reporting Bin Status and connection between Vault and ID box.(Enable function with green jumper)
;-Extended Bin - In response MSG time to ~1 min instead of ~40 sec.
;-Add logic to control Light_2 in vault(s), following logic below:
;
;     	     Enquiry with "Light" feature:
;          	    10 BYTE ENQUIRY FORMAT:
;
;           ENQ HH MM SS L1 L2 L3 L4 ETX BCC
;
;          Where: Ln =>
;	-------------------------------------------------------------
;	|     Ln    |  Light _2          |            Light _1         |
;	|-------------------------------------------------------------
;	|   0x00    |	Off	|	Off	  |
;	|   0x01    |	Off	|	On	  |
;	|   0x02    |	Off	|	Blinking	  |
;	|   0x03    |	N/A	|	N/A	  |
;	|   0x04    |	On	|	Off	  |
;	|   0x05    |	On	|	On	  |
;	|   0x06    |	On	|	Blinking	  |
;	|   0x07    |	N/A	|	N/A	  |
;	--------------------------------------------------------------
;*/
;
;
;#include <mega64a.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.EQU __sm_mask=0x1C
	.EQU __sm_powerdown=0x10
	.EQU __sm_powersave=0x18
	.EQU __sm_standby=0x14
	.EQU __sm_ext_standby=0x1C
	.EQU __sm_adc_noise_red=0x08
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;#include <stdio.h>
;#include "I2C.h"
;#include "CBID_COMP.h"
;
;
;//#define _DEBUG_
;
;
;#ifndef RXB8
;#define RXB8 1
;#endif
;
;#ifndef TXB8
;#define TXB8 0
;#endif
;
;#ifndef UPE
;#define UPE 2
;#endif
;
;#ifndef DOR
;#define DOR 3
;#endif
;
;#ifndef FE
;#define FE 4
;#endif
;
;#ifndef UDRE
;#define UDRE 5
;#endif
;
;#ifndef RXC
;#define RXC 7
;#endif
;
;#define FRAMING_ERROR (1<<FE)
;#define PARITY_ERROR (1<<UPE)
;#define DATA_OVERRUN (1<<DOR)
;#define DATA_REGISTER_EMPTY (1<<UDRE)
;#define RX_COMPLETE (1<<RXC)
;
;
;//For Mode Jumper
;volatile unsigned char ModeSelect,TM;
;#define Jumper_Hi {PORTC.4=0;DDRC.3=1;PORTC.3=1;PORTC.4=0;TM=PINC.4;}
;#define Jumper_Lo {PORTC.4=0;DDRC.3=1;PORTC.3=0;PORTC.4=0;TM=PINC.4;}
;
;
;
;// USART0 Receiver buffer
;#define RX_BUFFER_SIZE0 32
;char rx_buffer0[RX_BUFFER_SIZE0];
;unsigned int rx_wr_index0;
;
;
;unsigned char Buffer[8];
;static unsigned char Vault_Light[4][2];
;
;void Poll_Vault(unsigned char Vault_Number);
;void WriteEvevt(unsigned char *data);
;unsigned char ReadEvent(unsigned char ack,unsigned char *data);
;void Shift_RTCC_SRAM_Data();
;unsigned int read_adc(unsigned char adc_input);
;void debug_putchar(unsigned char dat_tx);
;
;
;unsigned int R_Flash_Pointer_Begin(void);
;void W_Flash_Pointer_Begin(unsigned int pointer);
;
;unsigned int R_Flash_Pointer_End(void);
;void W_Flash_Pointer_End(unsigned int pointer);
;
;
;
;struct Timers_{
;               unsigned int Timer0,Timer1,Timer2,Blink_Timer,PowerUpDelay,CBX_Error[4],BIN_In[4];
;               }Timers;
;
;
;extern struct Rtc_{
;                    unsigned char hour,min,sec,month,date,year;
;                    unsigned char New_hour,New_min,New_sec;
;                  }Rtc;
;
;
;struct RS232_{
;               unsigned char CheckSum,Counter,Request,Expect_ACK;
;              }RS232;
;
;
;struct RS485_{
;              unsigned char LRC,Counter,Data_Ready,LRC_Chk,Service_Led_Counter;
;              }RS485;
;
;
;struct Vault_{
;               unsigned char Vault_No[4][8],BIN_No[4][8],CBX_No[4][8];
;               unsigned char Debounce_Vault_No[4][8],Debounce_BIN_No[4][8],Debounce_CBX_No[4][8];
;               unsigned char OLD_Vault_No[4][8],OLD_BIN_No[4][8],OLD_CBX_No[4][8];
;               unsigned char Debounce_Counter[4][3];
;               unsigned char OffLine[4],OffLine_Counter;
;               unsigned char Counter,Data_Ready[4],CBX_Status[4],StatusCounter,CH_Counter;
;               unsigned char IgnoreACK,ChanelStatus[4][2];
;               }Vault;
;
;
;
;eeprom unsigned char PrepareEEPROM[3]={0,0,0};
;
;
;// Timer1 overflow interrupt service routine
;interrupt [TIM1_OVF] void timer1_ovf_isr(void)
; 0000 00A0 {

	.CSEG
_timer1_ovf_isr:
	ST   -Y,R0
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 00A1  unsigned char Cnt;
; 0000 00A2 
; 0000 00A3  // Reinitialize Timer1 value
; 0000 00A4  TCNT1H=0xFC68 >> 8;
	ST   -Y,R16
;	Cnt -> R16
	LDI  R30,LOW(252)
	OUT  0x2D,R30
; 0000 00A5  TCNT1L=0xFC68 & 0xff;
	LDI  R30,LOW(104)
	OUT  0x2C,R30
; 0000 00A6 
; 0000 00A7 
; 0000 00A8  if ((++Timers.Blink_Timer&0x200)) Sys_Led=!Sys_Led;
	__POINTW2MN _Timers,6
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	ANDI R31,HIGH(0x200)
	BREQ _0x3
	SBIS 0x12,4
	RJMP _0x4
	CBI  0x12,4
	RJMP _0x5
_0x4:
	SBI  0x12,4
_0x5:
; 0000 00A9  if (Timers.PowerUpDelay>0) Timers.PowerUpDelay--;
_0x3:
	__GETW2MN _Timers,8
	CALL __CPW02
	BRSH _0x6
	__POINTW2MN _Timers,8
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00AA  if (Timers.Timer0>0) Timers.Timer0--;
_0x6:
	LDS  R26,_Timers
	LDS  R27,_Timers+1
	CALL __CPW02
	BRSH _0x7
	LDI  R26,LOW(_Timers)
	LDI  R27,HIGH(_Timers)
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00AB  if (Timers.Timer1>0) Timers.Timer1--;
_0x7:
	__GETW2MN _Timers,2
	CALL __CPW02
	BRSH _0x8
	__POINTW2MN _Timers,2
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00AC  if (Timers.Timer2>0)
_0x8:
	__GETW2MN _Timers,4
	CALL __CPW02
	BRSH _0x9
; 0000 00AD     {
; 0000 00AE     Timers.Timer2--;
	__POINTW2MN _Timers,4
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00AF     Link_Led1=1;
	SBI  0x15,5
; 0000 00B0     Link_Led2=0;
	CBI  0x15,6
; 0000 00B1     }
; 0000 00B2     else
	RJMP _0xE
_0x9:
; 0000 00B3         {
; 0000 00B4         Link_Led1=0;
	CBI  0x15,5
; 0000 00B5         Link_Led2=1;
	SBI  0x15,6
; 0000 00B6         }
_0xE:
; 0000 00B7 
; 0000 00B8  for (Cnt=0;Cnt<2;Cnt++)
	LDI  R16,LOW(0)
_0x14:
	CPI  R16,2
	BRSH _0x15
; 0000 00B9      {
; 0000 00BA     //CBX Read Error Timers
; 0000 00BB     if (Timers.CBX_Error[Cnt]>1) Timers.CBX_Error[Cnt]--;
	__POINTW2MN _Timers,10
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETW1P
	SBIW R30,2
	BRLO _0x16
	__POINTW2MN _Timers,10
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00BC 
; 0000 00BD     //Bin In
; 0000 00BE     if (Timers.BIN_In[Cnt]) Timers.BIN_In[Cnt]--;
_0x16:
	__POINTW2MN _Timers,18
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETW1P
	SBIW R30,0
	BREQ _0x17
	__POINTW2MN _Timers,18
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00BF     }
_0x17:
	SUBI R16,-1
	RJMP _0x14
_0x15:
; 0000 00C0 
; 0000 00C1 }
	LD   R16,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R0,Y+
	RETI
;
;
;
;
;
;
;
;
;
;// USART0 Receiver interrupt service routine
;interrupt [USART0_RXC] void usart0_rx_isr(void)
; 0000 00CD {
_usart0_rx_isr:
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 00CE char status,data;
; 0000 00CF 
; 0000 00D0 status=UCSR0A;
	ST   -Y,R17
	ST   -Y,R16
;	status -> R16
;	data -> R17
	IN   R16,11
; 0000 00D1 data=UDR0;
	IN   R17,12
; 0000 00D2 if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	MOV  R30,R16
	ANDI R30,LOW(0x1C)
	BREQ PC+3
	JMP _0x18
; 0000 00D3    {
; 0000 00D4    if (data==STX)
	CPI  R17,2
	BRNE _0x19
; 0000 00D5      {
; 0000 00D6      rx_wr_index0=0;
	CLR  R4
	CLR  R5
; 0000 00D7      RS485.LRC=0;
	LDI  R30,LOW(0)
	STS  _RS485,R30
; 0000 00D8      RS485.Counter=10;
	LDI  R30,LOW(10)
	__PUTB1MN _RS485,1
; 0000 00D9      }
; 0000 00DA      else
	RJMP _0x1A
_0x19:
; 0000 00DB         {
; 0000 00DC         if (data==EOT) RS485.Counter=0;
	CPI  R17,4
	BRNE _0x1B
	LDI  R30,LOW(0)
	__PUTB1MN _RS485,1
; 0000 00DD         if (++RS485.Counter>3)
_0x1B:
	__GETB1MN _RS485,1
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,1
	CPI  R30,LOW(0x4)
	BRLO _0x1C
; 0000 00DE             {
; 0000 00DF             RS485.Counter=10;
	LDI  R30,LOW(10)
	__PUTB1MN _RS485,1
; 0000 00E0             RS485.LRC=RS485.LRC^data;
	MOV  R30,R17
	LDS  R26,_RS485
	EOR  R30,R26
	STS  _RS485,R30
; 0000 00E1             rx_buffer0[(rx_wr_index0++)&0x1F]=data;
	MOVW R30,R4
	ADIW R30,1
	MOVW R4,R30
	SBIW R30,1
	ANDI R30,LOW(0x1F)
	ANDI R31,HIGH(0x1F)
	SUBI R30,LOW(-_rx_buffer0)
	SBCI R31,HIGH(-_rx_buffer0)
	ST   Z,R17
; 0000 00E2             }
; 0000 00E3             else
	RJMP _0x1D
_0x1C:
; 0000 00E4                 {
; 0000 00E5                 if (RS485.Counter==3)
	__GETB2MN _RS485,1
	CPI  R26,LOW(0x3)
	BREQ PC+3
	JMP _0x1E
; 0000 00E6                     {
; 0000 00E7                      RS485.LRC=RS485.LRC^EOT;
	LDS  R26,_RS485
	LDI  R30,LOW(4)
	EOR  R30,R26
	STS  _RS485,R30
; 0000 00E8 
; 0000 00E9                                       //  LRC_L                                     LRC_H
; 0000 00EA                       if (( (0x30|(RS485.LRC&0x0F))==data) && (RS485.LRC_Chk==(0x30|((RS485.LRC&0xF0)>>4) )))
	ANDI R30,LOW(0xF)
	ORI  R30,LOW(0x30)
	CP   R17,R30
	BRNE _0x20
	__GETB2MN _RS485,3
	LDS  R30,_RS485
	ANDI R30,LOW(0xF0)
	SWAP R30
	ANDI R30,0xF
	ORI  R30,LOW(0x30)
	CP   R30,R26
	BREQ _0x21
_0x20:
	RJMP _0x1F
_0x21:
; 0000 00EB                         {
; 0000 00EC                         if (rx_buffer0[1]=='P')
	__GETB2MN _rx_buffer0,1
	CPI  R26,LOW(0x50)
	BREQ PC+3
	JMP _0x22
; 0000 00ED                             {
; 0000 00EE                             Vault.Counter=rx_buffer0[0]&0x03;
	LDS  R30,_rx_buffer0
	ANDI R30,LOW(0x3)
	__PUTB1MN _Vault,305
; 0000 00EF 
; 0000 00F0                             //Vault Number
; 0000 00F1                             for (status=0;status<6;status++) Vault.Vault_No[Vault.Counter][status]=rx_buffer0[status+2];
	LDI  R16,LOW(0)
_0x24:
	CPI  R16,6
	BRSH _0x25
	__GETB1MN _Vault,305
	LDI  R31,0
	CALL __LSLW3
	SUBI R30,LOW(-_Vault)
	SBCI R31,HIGH(-_Vault)
	MOVW R26,R30
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	MOV  R30,R16
	LDI  R31,0
	__ADDW1MN _rx_buffer0,2
	LD   R30,Z
	ST   X,R30
	SUBI R16,-1
	RJMP _0x24
_0x25:
; 0000 00F3 for (status=0;status<6;status++) Vault.BIN_No[Vault.Counter][status]=rx_buffer0[status+8];
	LDI  R16,LOW(0)
_0x27:
	CPI  R16,6
	BRSH _0x28
	__POINTW2MN _Vault,32
	__GETB1MN _Vault,305
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	MOV  R30,R16
	LDI  R31,0
	__ADDW1MN _rx_buffer0,8
	LD   R30,Z
	ST   X,R30
	SUBI R16,-1
	RJMP _0x27
_0x28:
; 0000 00F5 for (status=0;status<6;status++) Vault.CBX_No[Vault.Counter][status]=rx_buffer0[status+14];
	LDI  R16,LOW(0)
_0x2A:
	CPI  R16,6
	BRSH _0x2B
	__POINTW2MN _Vault,64
	__GETB1MN _Vault,305
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	MOV  R30,R16
	LDI  R31,0
	__ADDW1MN _rx_buffer0,14
	LD   R30,Z
	ST   X,R30
	SUBI R16,-1
	RJMP _0x2A
_0x2B:
; 0000 00F7 Vault.Data_Ready[Vault.Counter]=1;
	__POINTW2MN _Vault,306
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(1)
	ST   X,R30
; 0000 00F8                             Vault.CBX_Status[Vault.Counter]=rx_buffer0[20];
	__POINTW2MN _Vault,310
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _rx_buffer0,20
	ST   X,R30
; 0000 00F9                             };
_0x22:
; 0000 00FA                         };
_0x1F:
; 0000 00FB                     }
; 0000 00FC                     else RS485.LRC_Chk=data; // RS485.Counter==1
	RJMP _0x2C
_0x1E:
	__PUTBMRN _RS485,3,17
; 0000 00FD                 }
_0x2C:
_0x1D:
; 0000 00FE 
; 0000 00FF         }
_0x1A:
; 0000 0100    }
; 0000 0101 }
_0x18:
	RJMP _0x123
;
;
;
;
;// USART1 Receiver buffer
;#define RX_BUFFER_SIZE1 16
;unsigned char rx_buffer1[RX_BUFFER_SIZE1];
;
;
;// USART1 Receiver interrupt service routine
;interrupt [USART1_RXC] void usart1_rx_isr(void)
; 0000 010D {
_usart1_rx_isr:
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 010E char status,data;
; 0000 010F 
; 0000 0110 status=UCSR1A;
	ST   -Y,R17
	ST   -Y,R16
;	status -> R16
;	data -> R17
	LDS  R16,155
; 0000 0111 data=UDR1;
	LDS  R17,156
; 0000 0112 
; 0000 0113 if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	MOV  R30,R16
	ANDI R30,LOW(0x1C)
	BREQ PC+3
	JMP _0x2D
; 0000 0114    {
; 0000 0115     if ((RS232.Expect_ACK==0)&&(data==ACK))
	__GETB2MN _RS232,3
	CPI  R26,LOW(0x0)
	BRNE _0x2F
	CPI  R17,6
	BREQ _0x30
_0x2F:
	RJMP _0x2E
_0x30:
; 0000 0116         {
; 0000 0117         RS232.Expect_ACK=ACK;
	LDI  R30,LOW(6)
	__PUTB1MN _RS232,3
; 0000 0118         RS232.Request=1;
	LDI  R30,LOW(1)
	__PUTB1MN _RS232,2
; 0000 0119         Timers.Timer1=0; //Ready To Receive New Poll
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timers,2
; 0000 011A         goto Exit_Uart1_Int;
	RJMP _0x31
; 0000 011B         }
; 0000 011C          else RS232.Expect_ACK=NAK;
_0x2E:
	LDI  R30,LOW(21)
	__PUTB1MN _RS232,3
; 0000 011D 
; 0000 011E 
; 0000 011F 
; 0000 0120     if ((data==ENQ)&&(Timers.Timer1==0))
	CPI  R17,22
	BRNE _0x34
	__GETW2MN _Timers,2
	SBIW R26,0
	BREQ _0x35
_0x34:
	RJMP _0x33
_0x35:
; 0000 0121         {
; 0000 0122         Timers.Timer1=300;
	LDI  R30,LOW(300)
	LDI  R31,HIGH(300)
	__PUTW1MN _Timers,2
; 0000 0123         RS232.CheckSum=-ETX;
	LDI  R30,LOW(253)
	STS  _RS232,R30
; 0000 0124         RS232.Counter=0;
	LDI  R30,LOW(0)
	__PUTB1MN _RS232,1
; 0000 0125         goto Exit_Uart1_Int;
	RJMP _0x31
; 0000 0126         }
; 0000 0127 
; 0000 0128 
; 0000 0129     if ((RS232.CheckSum==data)&&(RS232.Counter==8))
_0x33:
	LDS  R26,_RS232
	CP   R17,R26
	BRNE _0x37
	__GETB2MN _RS232,1
	CPI  R26,LOW(0x8)
	BREQ _0x38
_0x37:
	RJMP _0x36
_0x38:
; 0000 012A         {
; 0000 012B         Rtc.New_hour=rx_buffer1[0];
	LDS  R30,_rx_buffer1
	__PUTB1MN _Rtc,6
; 0000 012C         Rtc.New_min=rx_buffer1[1];
	__GETB1MN _rx_buffer1,1
	__PUTB1MN _Rtc,7
; 0000 012D         Rtc.New_sec=rx_buffer1[2];
	__GETB1MN _rx_buffer1,2
	__PUTB1MN _Rtc,8
; 0000 012E 
; 0000 012F         Vault_Light[0][0]=rx_buffer1[3];
	__GETB1MN _rx_buffer1,3
	STS  _Vault_Light_G000,R30
; 0000 0130         Vault_Light[1][0]=rx_buffer1[4];
	__GETB1MN _rx_buffer1,4
	__PUTB1MN _Vault_Light_G000,2
; 0000 0131         Vault_Light[2][0]=rx_buffer1[5];
	__GETB1MN _rx_buffer1,5
	__PUTB1MN _Vault_Light_G000,4
; 0000 0132         Vault_Light[3][0]=rx_buffer1[6];
	__GETB1MN _rx_buffer1,6
	__PUTB1MN _Vault_Light_G000,6
; 0000 0133 
; 0000 0134         Timers.Timer1=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timers,2
; 0000 0135         RS232.Request=1;
	LDI  R30,LOW(1)
	__PUTB1MN _RS232,2
; 0000 0136         RS232.Counter++;
	__GETB1MN _RS232,1
	SUBI R30,-LOW(1)
	__PUTB1MN _RS232,1
	SUBI R30,LOW(1)
; 0000 0137         goto Exit_Uart1_Int;
	RJMP _0x31
; 0000 0138         }
; 0000 0139 
; 0000 013A     if (RS232.Counter<=8)
_0x36:
	__GETB2MN _RS232,1
	CPI  R26,LOW(0x9)
	BRSH _0x39
; 0000 013B         {
; 0000 013C         RS232.CheckSum+=data;
	MOV  R30,R17
	LDS  R26,_RS232
	ADD  R30,R26
	STS  _RS232,R30
; 0000 013D         rx_buffer1[RS232.Counter++]=data;
	__GETB1MN _RS232,1
	SUBI R30,-LOW(1)
	__PUTB1MN _RS232,1
	SUBI R30,LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_rx_buffer1)
	SBCI R31,HIGH(-_rx_buffer1)
	ST   Z,R17
; 0000 013E         }
; 0000 013F     }
_0x39:
; 0000 0140 
; 0000 0141   Exit_Uart1_Int:
_0x2D:
_0x31:
; 0000 0142 }
_0x123:
	LD   R16,Y+
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	RETI
;
;
;
;
;
;
;// USART0 Transmitter buffer
;#define TX_BUFFER_SIZE0 32
;char tx_buffer0[TX_BUFFER_SIZE0];
;unsigned char tx_wr_index0,tx_rd_index0,tx_counter0;
;
;
;// USART0 Transmitter interrupt service routine
;interrupt [USART0_TXC] void usart0_tx_isr(void)
; 0000 0151 {
_usart0_tx_isr:
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 0152 if (tx_counter0)
	TST  R8
	BREQ _0x3A
; 0000 0153    {
; 0000 0154    --tx_counter0;
	DEC  R8
; 0000 0155    UDR0=tx_buffer0[tx_rd_index0++];
	MOV  R30,R7
	INC  R7
	LDI  R31,0
	SUBI R30,LOW(-_tx_buffer0)
	SBCI R31,HIGH(-_tx_buffer0)
	LD   R30,Z
	OUT  0xC,R30
; 0000 0156    if (tx_rd_index0 == TX_BUFFER_SIZE0) tx_rd_index0=0;
	LDI  R30,LOW(32)
	CP   R30,R7
	BRNE _0x3B
	CLR  R7
; 0000 0157    }
_0x3B:
; 0000 0158    else DE=0;
	RJMP _0x3C
_0x3A:
	CBI  0x15,7
; 0000 0159 }
_0x3C:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	RETI
;
;
;
;
;
;#ifndef _DEBUG_TERMINAL_IO_
;// Write a character to the USART0 Transmitter buffer
;#define _ALTERNATE_PUTCHAR_
;#pragma used+
;void putchar(char c)
; 0000 0164 {
_putchar:
; 0000 0165 while (tx_counter0 == TX_BUFFER_SIZE0);
	ST   -Y,R26
;	c -> Y+0
_0x3F:
	LDI  R30,LOW(32)
	CP   R30,R8
	BREQ _0x3F
; 0000 0166 #asm("cli")
	cli
; 0000 0167 if (tx_counter0 || ((UCSR0A & DATA_REGISTER_EMPTY)==0))
	TST  R8
	BRNE _0x43
	SBIC 0xB,5
	RJMP _0x42
_0x43:
; 0000 0168    {
; 0000 0169    tx_buffer0[tx_wr_index0++]=c;
	MOV  R30,R6
	INC  R6
	LDI  R31,0
	SUBI R30,LOW(-_tx_buffer0)
	SBCI R31,HIGH(-_tx_buffer0)
	LD   R26,Y
	STD  Z+0,R26
; 0000 016A    if (tx_wr_index0 == TX_BUFFER_SIZE0) tx_wr_index0=0;
	LDI  R30,LOW(32)
	CP   R30,R6
	BRNE _0x45
	CLR  R6
; 0000 016B    ++tx_counter0;
_0x45:
	INC  R8
; 0000 016C    }
; 0000 016D else
	RJMP _0x46
_0x42:
; 0000 016E    UDR0=c;
	LD   R30,Y
	OUT  0xC,R30
; 0000 016F #asm("sei")
_0x46:
	sei
	RJMP _0x206000A
; 0000 0170 }
;#pragma used-
;#endif
;
;
;
;
;
;// USART1 Transmitter buffer
;#define TX_BUFFER_SIZE1 16
;char tx_buffer1[TX_BUFFER_SIZE1];
;unsigned char tx_wr_index1,tx_rd_index1,tx_counter1;
;
;
;// USART1 Transmitter interrupt service routine
;interrupt [USART1_TXC] void usart1_tx_isr(void)
; 0000 0180 {
_usart1_tx_isr:
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 0181 if (tx_counter1)
	TST  R11
	BREQ _0x47
; 0000 0182    {
; 0000 0183    --tx_counter1;
	DEC  R11
; 0000 0184    UDR1=tx_buffer1[tx_rd_index1++];
	MOV  R30,R10
	INC  R10
	LDI  R31,0
	SUBI R30,LOW(-_tx_buffer1)
	SBCI R31,HIGH(-_tx_buffer1)
	LD   R30,Z
	STS  156,R30
; 0000 0185    if (tx_rd_index1 == TX_BUFFER_SIZE1) tx_rd_index1=0;
	LDI  R30,LOW(16)
	CP   R30,R10
	BRNE _0x48
	CLR  R10
; 0000 0186    }
_0x48:
; 0000 0187 }
_0x47:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	RETI
;
;
;// Write a character to the USART1 Transmitter buffer
;#pragma used+
;void putchar1(char c)
; 0000 018D {
_putchar1:
; 0000 018E while (tx_counter1 == TX_BUFFER_SIZE1);
	ST   -Y,R26
;	c -> Y+0
_0x49:
	LDI  R30,LOW(16)
	CP   R30,R11
	BREQ _0x49
; 0000 018F #asm("cli")
	cli
; 0000 0190 if (tx_counter1 || ((UCSR1A & DATA_REGISTER_EMPTY)==0))
	TST  R11
	BRNE _0x4D
	LDS  R30,155
	ANDI R30,LOW(0x20)
	BRNE _0x4C
_0x4D:
; 0000 0191    {
; 0000 0192    tx_buffer1[tx_wr_index1++]=c;
	MOV  R30,R9
	INC  R9
	LDI  R31,0
	SUBI R30,LOW(-_tx_buffer1)
	SBCI R31,HIGH(-_tx_buffer1)
	LD   R26,Y
	STD  Z+0,R26
; 0000 0193    if (tx_wr_index1 == TX_BUFFER_SIZE1) tx_wr_index1=0;
	LDI  R30,LOW(16)
	CP   R30,R9
	BRNE _0x4F
	CLR  R9
; 0000 0194    ++tx_counter1;
_0x4F:
	INC  R11
; 0000 0195    }
; 0000 0196 else
	RJMP _0x50
_0x4C:
; 0000 0197    UDR1=c;
	LD   R30,Y
	STS  156,R30
; 0000 0198 #asm("sei")
_0x50:
	sei
_0x206000A:
; 0000 0199 }
	ADIW R28,1
	RET
;#pragma used-
;
;
;
;
;
;
;
;//******************************************* MAIN **********************************************
;void main(void)
; 0000 01A4 {
_main:
; 0000 01A5 
; 0000 01A6  unsigned char Counter_Poll=0,Counter_Responds=0,CBX_Present[4],tmp1,tmp2,tmp3;
; 0000 01A7  unsigned int tmp_int;
; 0000 01A8 
; 0000 01A9 // Input/Output Ports initialization
; 0000 01AA // Port A initialization
; 0000 01AB // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01AC // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01AD PORTA=0x00;
	SBIW R28,6
;	Counter_Poll -> R16
;	Counter_Responds -> R17
;	CBX_Present -> Y+2
;	tmp1 -> R18
;	tmp2 -> R19
;	tmp3 -> R20
;	tmp_int -> Y+0
	LDI  R16,0
	LDI  R17,0
	LDI  R30,LOW(0)
	OUT  0x1B,R30
; 0000 01AE DDRA=0x01;
	LDI  R30,LOW(1)
	OUT  0x1A,R30
; 0000 01AF 
; 0000 01B0 // Port B initialization
; 0000 01B1 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01B2 // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01B3 PORTB=0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 01B4 DDRB=0x00;
	OUT  0x17,R30
; 0000 01B5 
; 0000 01B6 // Port C initialization
; 0000 01B7 // Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01B8 // State7=T State6=T State5=T State4=P State3=P State2=T State1=T State0=T
; 0000 01B9 PORTC=0x18;
	LDI  R30,LOW(24)
	OUT  0x15,R30
; 0000 01BA DDRC=0xE0;
	LDI  R30,LOW(224)
	OUT  0x14,R30
; 0000 01BB 
; 0000 01BC 
; 0000 01BD // Port D initialization
; 0000 01BE // Func7=Out Func6=Out Func5=Out Func4=Out Func3=In Func2=In Func1=In Func0=In
; 0000 01BF // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01C0 PORTD=0x00;
	LDI  R30,LOW(0)
	OUT  0x12,R30
; 0000 01C1 DDRD=0xF0;
	LDI  R30,LOW(240)
	OUT  0x11,R30
; 0000 01C2 
; 0000 01C3 // Port E initialization
; 0000 01C4 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01C5 // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01C6 PORTE=0x00;
	LDI  R30,LOW(0)
	OUT  0x3,R30
; 0000 01C7 DDRE=0x00;
	OUT  0x2,R30
; 0000 01C8 
; 0000 01C9 // Port F initialization
; 0000 01CA // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01CB // State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T
; 0000 01CC PORTF=0x00;
	STS  98,R30
; 0000 01CD DDRF=0x00;
	STS  97,R30
; 0000 01CE 
; 0000 01CF // Port G initialization
; 0000 01D0 // Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 01D1 // State4=T State3=T State2=T State1=T State0=T
; 0000 01D2 PORTG=0x00;
	STS  101,R30
; 0000 01D3 DDRG=0x00;
	STS  100,R30
; 0000 01D4 
; 0000 01D5 // Timer/Counter 0 initialization
; 0000 01D6 // Clock source: System Clock
; 0000 01D7 // Clock value: Timer 0 Stopped
; 0000 01D8 // Mode: Normal top=0xFF
; 0000 01D9 // OC0 output: Disconnected
; 0000 01DA ASSR=0x00;
	OUT  0x30,R30
; 0000 01DB TCCR0=0x00;
	OUT  0x33,R30
; 0000 01DC TCNT0=0x00;
	OUT  0x32,R30
; 0000 01DD OCR0=0x00;
	OUT  0x31,R30
; 0000 01DE 
; 0000 01DF 
; 0000 01E0 // Timer/Counter 1 initialization
; 0000 01E1 // Clock source: System Clock
; 0000 01E2 // Clock value: 921.358 kHz
; 0000 01E3 // Mode: Normal top=0xFFFF
; 0000 01E4 // OC1A output: Discon.
; 0000 01E5 // OC1B output: Discon.
; 0000 01E6 // OC1C output: Discon.
; 0000 01E7 // Noise Canceler: Off
; 0000 01E8 // Input Capture on Falling Edge
; 0000 01E9 // Timer1 Overflow Interrupt: On
; 0000 01EA // Input Capture Interrupt: Off
; 0000 01EB // Compare A Match Interrupt: Off
; 0000 01EC // Compare B Match Interrupt: Off
; 0000 01ED // Compare C Match Interrupt: Off
; 0000 01EE TCCR1A=0x00;
	OUT  0x2F,R30
; 0000 01EF TCCR1B=0x02;
	LDI  R30,LOW(2)
	OUT  0x2E,R30
; 0000 01F0 TCNT1H=0xFF;
	LDI  R30,LOW(255)
	OUT  0x2D,R30
; 0000 01F1 TCNT1L=0xF0;
	LDI  R30,LOW(240)
	OUT  0x2C,R30
; 0000 01F2 ICR1H=0x00;
	LDI  R30,LOW(0)
	OUT  0x27,R30
; 0000 01F3 ICR1L=0x00;
	OUT  0x26,R30
; 0000 01F4 OCR1AH=0x00;
	OUT  0x2B,R30
; 0000 01F5 OCR1AL=0x00;
	OUT  0x2A,R30
; 0000 01F6 OCR1BH=0x00;
	OUT  0x29,R30
; 0000 01F7 OCR1BL=0x00;
	OUT  0x28,R30
; 0000 01F8 OCR1CH=0x00;
	STS  121,R30
; 0000 01F9 OCR1CL=0x00;
	STS  120,R30
; 0000 01FA 
; 0000 01FB // Timer/Counter 2 initialization
; 0000 01FC // Clock source: System Clock
; 0000 01FD // Clock value: Timer2 Stopped
; 0000 01FE // Mode: Normal top=0xFF
; 0000 01FF // OC2 output: Disconnected
; 0000 0200 TCCR2=0x00;
	OUT  0x25,R30
; 0000 0201 TCNT2=0x00;
	OUT  0x24,R30
; 0000 0202 OCR2=0x00;
	OUT  0x23,R30
; 0000 0203 
; 0000 0204 // Timer/Counter 3 initialization
; 0000 0205 // Clock source: System Clock
; 0000 0206 // Clock value: Timer3 Stopped
; 0000 0207 // Mode: Normal top=0xFFFF
; 0000 0208 // OC3A output: Discon.
; 0000 0209 // OC3B output: Discon.
; 0000 020A // OC3C output: Discon.
; 0000 020B // Noise Canceler: Off
; 0000 020C // Input Capture on Falling Edge
; 0000 020D // Timer3 Overflow Interrupt: Off
; 0000 020E // Input Capture Interrupt: Off
; 0000 020F // Compare A Match Interrupt: Off
; 0000 0210 // Compare B Match Interrupt: Off
; 0000 0211 // Compare C Match Interrupt: Off
; 0000 0212 TCCR3A=0x00;
	STS  139,R30
; 0000 0213 TCCR3B=0x00;
	STS  138,R30
; 0000 0214 TCNT3H=0x00;
	STS  137,R30
; 0000 0215 TCNT3L=0x00;
	STS  136,R30
; 0000 0216 ICR3H=0x00;
	STS  129,R30
; 0000 0217 ICR3L=0x00;
	STS  128,R30
; 0000 0218 OCR3AH=0x00;
	STS  135,R30
; 0000 0219 OCR3AL=0x00;
	STS  134,R30
; 0000 021A OCR3BH=0x00;
	STS  133,R30
; 0000 021B OCR3BL=0x00;
	STS  132,R30
; 0000 021C OCR3CH=0x00;
	STS  131,R30
; 0000 021D OCR3CL=0x00;
	STS  130,R30
; 0000 021E 
; 0000 021F // External Interrupt(s) initialization
; 0000 0220 // INT0: Off
; 0000 0221 // INT1: Off
; 0000 0222 // INT2: Off
; 0000 0223 // INT3: Off
; 0000 0224 // INT4: Off
; 0000 0225 // INT5: Off
; 0000 0226 // INT6: Off
; 0000 0227 // INT7: Off
; 0000 0228 EICRA=0x00;
	STS  106,R30
; 0000 0229 EICRB=0x00;
	OUT  0x3A,R30
; 0000 022A EIMSK=0x00;
	OUT  0x39,R30
; 0000 022B 
; 0000 022C // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 022D TIMSK=0x04;
	LDI  R30,LOW(4)
	OUT  0x37,R30
; 0000 022E 
; 0000 022F ETIMSK=0x00;
	LDI  R30,LOW(0)
	STS  125,R30
; 0000 0230 
; 0000 0231 // USART0 initialization
; 0000 0232 // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 0233 // USART0 Receiver: On
; 0000 0234 // USART0 Transmitter: On
; 0000 0235 // USART0 Mode: Asynchronous
; 0000 0236 // USART0 Baud Rate: 9600
; 0000 0237 UCSR0A=0x00;
	OUT  0xB,R30
; 0000 0238 UCSR0B=0xD8;
	LDI  R30,LOW(216)
	OUT  0xA,R30
; 0000 0239 UCSR0C=0x06;
	LDI  R30,LOW(6)
	STS  149,R30
; 0000 023A UBRR0H=0x00;
	LDI  R30,LOW(0)
	STS  144,R30
; 0000 023B UBRR0L=0x2F;
	LDI  R30,LOW(47)
	OUT  0x9,R30
; 0000 023C 
; 0000 023D // USART1 initialization
; 0000 023E // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 023F // USART1 Receiver: On
; 0000 0240 // USART1 Transmitter: On
; 0000 0241 // USART1 Mode: Asynchronous
; 0000 0242 // USART1 Baud Rate: 9600
; 0000 0243 UCSR1A=0x00;
	LDI  R30,LOW(0)
	STS  155,R30
; 0000 0244 UCSR1B=0xD8;
	LDI  R30,LOW(216)
	STS  154,R30
; 0000 0245 UCSR1C=0x06;
	LDI  R30,LOW(6)
	STS  157,R30
; 0000 0246 UBRR1H=0x00;
	LDI  R30,LOW(0)
	STS  152,R30
; 0000 0247 UBRR1L=0x2F;
	LDI  R30,LOW(47)
	STS  153,R30
; 0000 0248 
; 0000 0249 // Analog Comparator initialization
; 0000 024A // Analog Comparator: Off
; 0000 024B // Analog Comparator Input Capture by Timer/Counter 1: Off
; 0000 024C ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 024D SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x20,R30
; 0000 024E 
; 0000 024F 
; 0000 0250 // ADC initialization
; 0000 0251 // ADC Clock frequency: 921.357 kHz
; 0000 0252 // ADC Voltage Reference: AREF pin
; 0000 0253 ADMUX=ADC_VREF_TYPE & 0xff;
	OUT  0x7,R30
; 0000 0254 ADCSRA=0x83;
	LDI  R30,LOW(131)
	OUT  0x6,R30
; 0000 0255 
; 0000 0256 // SPI initialization
; 0000 0257 // SPI disabled
; 0000 0258 SPCR=0x00;
	LDI  R30,LOW(0)
	OUT  0xD,R30
; 0000 0259 
; 0000 025A // TWI initialization
; 0000 025B // TWI disabled
; 0000 025C TWCR=0x00;
	STS  116,R30
; 0000 025D 
; 0000 025E 
; 0000 025F // Watchdog Timer initialization
; 0000 0260 // Watchdog Timer Prescaler: OSC/2048k
; 0000 0261 #pragma optsize-
; 0000 0262 WDTCR=0x1F;
	LDI  R30,LOW(31)
	OUT  0x21,R30
; 0000 0263 WDTCR=0x0F;
	LDI  R30,LOW(15)
	OUT  0x21,R30
; 0000 0264 #ifdef _OPTIMIZE_SIZE_
; 0000 0265 #pragma optsize+
; 0000 0266 #endif
; 0000 0267 
; 0000 0268 
; 0000 0269 
; 0000 026A //** Check or INIT EEPROM/RTCC Sram Pointers ***
; 0000 026B 
; 0000 026C  if ((PrepareEEPROM[0]!='G')||(PrepareEEPROM[1]!='F')||(PrepareEEPROM[2]!='I'))
	LDI  R26,LOW(_PrepareEEPROM)
	LDI  R27,HIGH(_PrepareEEPROM)
	CALL __EEPROMRDB
	CPI  R30,LOW(0x47)
	BRNE _0x52
	__POINTW2MN _PrepareEEPROM,1
	CALL __EEPROMRDB
	CPI  R30,LOW(0x46)
	BRNE _0x52
	__POINTW2MN _PrepareEEPROM,2
	CALL __EEPROMRDB
	CPI  R30,LOW(0x49)
	BREQ _0x51
_0x52:
; 0000 026D      {
; 0000 026E       PrepareEEPROM[0]='G';
	LDI  R26,LOW(_PrepareEEPROM)
	LDI  R27,HIGH(_PrepareEEPROM)
	LDI  R30,LOW(71)
	CALL __EEPROMWRB
; 0000 026F       PrepareEEPROM[1]='F';
	__POINTW2MN _PrepareEEPROM,1
	LDI  R30,LOW(70)
	CALL __EEPROMWRB
; 0000 0270       PrepareEEPROM[2]='I';
	__POINTW2MN _PrepareEEPROM,2
	LDI  R30,LOW(73)
	CALL __EEPROMWRB
; 0000 0271       goto Prepare_EEPROM;
	RJMP _0x54
; 0000 0272      }
; 0000 0273 
; 0000 0274      tmp1=read_i2c_RTCC(55,_RTCC);
_0x51:
	LDI  R30,LOW(55)
	LDI  R31,HIGH(55)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _read_i2c_RTCC
	MOV  R18,R30
; 0000 0275      if ((tmp1!=123)||(read_i2c_RTCC(50,_RTCC)>8)||((PINC.3==0)&&(PINC.4==0)))
	CPI  R18,123
	BRNE _0x56
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _read_i2c_RTCC
	CPI  R30,LOW(0x9)
	BRSH _0x56
	LDI  R26,0
	SBIC 0x13,3
	LDI  R26,1
	CPI  R26,LOW(0x0)
	BRNE _0x57
	LDI  R26,0
	SBIC 0x13,4
	LDI  R26,1
	CPI  R26,LOW(0x0)
	BREQ _0x56
_0x57:
	RJMP _0x55
_0x56:
; 0000 0276         {
; 0000 0277 
; 0000 0278 Prepare_EEPROM:
_0x54:
; 0000 0279 
; 0000 027A         write_i2c_RTCC(55,123,_RTCC);
	LDI  R30,LOW(55)
	LDI  R31,HIGH(55)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(123)
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 027B         write_i2c_RTCC(50,0,_RTCC);
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 027C         W_Flash_Pointer_Begin(0);
	LDI  R26,LOW(0)
	LDI  R27,0
	CALL _W_Flash_Pointer_Begin
; 0000 027D         W_Flash_Pointer_End(0);
	LDI  R26,LOW(0)
	LDI  R27,0
	CALL _W_Flash_Pointer_End
; 0000 027E         }
; 0000 027F 
; 0000 0280 
; 0000 0281   for (tmp2=0;tmp2<4;tmp2++)  // Flush old CBX,BIN,Vault numbers
_0x55:
	LDI  R19,LOW(0)
_0x5B:
	CPI  R19,4
	BRLO PC+3
	JMP _0x5C
; 0000 0282         {
; 0000 0283         for (tmp1=0;tmp1<6;tmp1++)
	LDI  R18,LOW(0)
_0x5E:
	CPI  R18,6
	BRSH _0x5F
; 0000 0284             {
; 0000 0285             Vault.OLD_BIN_No[tmp2][tmp1]='0';
	__POINTW2MN _Vault,224
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R18
	ADC  R27,R30
	LDI  R30,LOW(48)
	ST   X,R30
; 0000 0286             Vault.OLD_Vault_No[tmp2][tmp1]='0';
	__POINTW2MN _Vault,192
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R18
	ADC  R27,R30
	LDI  R30,LOW(48)
	ST   X,R30
; 0000 0287             Vault.OLD_CBX_No[tmp2][tmp1]='0';
	__POINTW2MN _Vault,256
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R18
	ADC  R27,R30
	LDI  R30,LOW(48)
	ST   X,R30
; 0000 0288             }
	SUBI R18,-1
	RJMP _0x5E
_0x5F:
; 0000 0289 
; 0000 028A         Vault.OffLine[tmp2]=0;
	__POINTW2MN _Vault,300
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	ST   X,R30
; 0000 028B         Vault.Data_Ready[tmp2]=0;
	__POINTW2MN _Vault,306
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	ST   X,R30
; 0000 028C         Vault.CBX_Status[tmp2]=0;
	__POINTW2MN _Vault,310
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	ST   X,R30
; 0000 028D         CBX_Present[tmp2]=0;
	MOV  R30,R19
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,2
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 028E 
; 0000 028F         Vault.OLD_BIN_No[tmp2][2]=4;
	__POINTW2MN _Vault,224
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,2
	LDI  R26,LOW(4)
	STD  Z+0,R26
; 0000 0290         Vault.OLD_BIN_No[tmp2][3]=0;
	__POINTW2MN _Vault,224
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,3
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 0291         Vault.OLD_BIN_No[tmp2][4]=9;
	__POINTW2MN _Vault,224
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,4
	LDI  R26,LOW(9)
	STD  Z+0,R26
; 0000 0292         Vault.OLD_BIN_No[tmp2][5]=5;
	__POINTW2MN _Vault,224
	MOV  R30,R19
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,5
	LDI  R26,LOW(5)
	STD  Z+0,R26
; 0000 0293 
; 0000 0294         //Bin In Timer
; 0000 0295         Timers.BIN_In[tmp2]=50000;
	__POINTW2MN _Timers,18
	MOV  R30,R19
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(50000)
	LDI  R31,HIGH(50000)
	ST   X+,R30
	ST   X,R31
; 0000 0296 
; 0000 0297 
; 0000 0298         Vault.Debounce_Counter[tmp2][CBX]=0;
	__POINTW2MN _Vault,288
	MOV  R30,R19
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,2
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 0299         Vault.Debounce_Counter[tmp2][BIN]=0;
	__POINTW2MN _Vault,288
	MOV  R30,R19
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,1
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 029A         }
	SUBI R19,-1
	RJMP _0x5B
_0x5C:
; 0000 029B 
; 0000 029C 
; 0000 029D         #asm("wdr");
	wdr
; 0000 029E 
; 0000 029F         delay_ms(4000);
	LDI  R26,LOW(4000)
	LDI  R27,HIGH(4000)
	CALL _delay_ms
; 0000 02A0         Timers.PowerUpDelay=30000;
	LDI  R30,LOW(30000)
	LDI  R31,HIGH(30000)
	__PUTW1MN _Timers,8
; 0000 02A1         Timers.CBX_Error[0]=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timers,10
; 0000 02A2         Timers.CBX_Error[1]=0;
	__POINTW1MN _Timers,12
	LDI  R26,LOW(0)
	LDI  R27,HIGH(0)
	STD  Z+0,R26
	STD  Z+1,R27
; 0000 02A3 
; 0000 02A4         Vault.StatusCounter=0;
	LDI  R30,LOW(0)
	__PUTB1MN _Vault,314
; 0000 02A5         Vault.CH_Counter=0;
	__PUTB1MN _Vault,315
; 0000 02A6         Vault.IgnoreACK=0;
	__PUTB1MN _Vault,316
; 0000 02A7 
; 0000 02A8 
; 0000 02A9 // Global enable interrupts
; 0000 02AA #asm("sei")
	sei
; 0000 02AB 
; 0000 02AC 
; 0000 02AD //*********************************************** MAIN LOOP ***************************************************
; 0000 02AE while (1)
_0x60:
; 0000 02AF       {
; 0000 02B0       #asm("wdr");
	wdr
; 0000 02B1 
; 0000 02B2 
; 0000 02B3 
; 0000 02B4       //OLD *****  Poling every 300ms (each wault(n) every 2 * 300ms = 600ms)
; 0000 02B5       if (Timers.Timer0==0)
	LDS  R30,_Timers
	LDS  R31,_Timers+1
	SBIW R30,0
	BREQ PC+3
	JMP _0x63
; 0000 02B6             {
; 0000 02B7             ++Counter_Poll;  //next vault
	SUBI R16,-LOW(1)
; 0000 02B8             Counter_Poll&=0x01;  // OLD 0x03
	ANDI R16,LOW(1)
; 0000 02B9             Poll_Vault(Counter_Poll);
	MOV  R26,R16
	CALL _Poll_Vault
; 0000 02BA             Timers.Timer0=300;     //poling time 300ms //OLD 200ms
	LDI  R30,LOW(300)
	LDI  R31,HIGH(300)
	STS  _Timers,R30
	STS  _Timers+1,R31
; 0000 02BB 
; 0000 02BC 
; 0000 02BD             //Get Mode Jumper Status
; 0000 02BE             ModeSelect=0;
	LDI  R30,LOW(0)
	STS  _ModeSelect,R30
; 0000 02BF             Jumper_Hi;
	CBI  0x15,4
	SBI  0x14,3
	SBI  0x15,3
	CBI  0x15,4
	LDI  R30,0
	SBIC 0x13,4
	LDI  R30,1
	STS  _TM,R30
; 0000 02C0             if (TM)
	LDS  R30,_TM
	CPI  R30,0
	BREQ _0x6C
; 0000 02C1                {
; 0000 02C2                 Jumper_Lo;
	CBI  0x15,4
	SBI  0x14,3
	CBI  0x15,3
	CBI  0x15,4
	LDI  R30,0
	SBIC 0x13,4
	LDI  R30,1
	STS  _TM,R30
; 0000 02C3                 if (!TM) ModeSelect=1;
	LDS  R30,_TM
	CPI  R30,0
	BRNE _0x75
	LDI  R30,LOW(1)
	STS  _ModeSelect,R30
; 0000 02C4                }
_0x75:
; 0000 02C5 
; 0000 02C6 
; 0000 02C7             if (Vault.OffLine[Counter_Responds]<10) Vault.OffLine[Counter_Responds]++;
_0x6C:
	__POINTW2MN _Vault,300
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0xA)
	BRSH _0x76
	__POINTW2MN _Vault,300
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R30,X
	SUBI R30,-LOW(1)
	ST   X,R30
; 0000 02C8 
; 0000 02C9             if (Vault.ChanelStatus[Counter_Poll][0]==1) Vault.ChanelStatus[Counter_Poll][1]=1;
_0x76:
	__POINTW2MN _Vault,317
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R26,X
	CPI  R26,LOW(0x1)
	BRNE _0x77
	__POINTW2MN _Vault,317
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,1
	LDI  R26,LOW(1)
	RJMP _0x11D
; 0000 02CA               else Vault.ChanelStatus[Counter_Poll][1]=0;
_0x77:
	__POINTW2MN _Vault,317
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,1
	LDI  R26,LOW(0)
_0x11D:
	STD  Z+0,R26
; 0000 02CB 
; 0000 02CC             Vault.ChanelStatus[Counter_Poll][0]=0;
	__POINTW2MN _Vault,317
	MOV  R30,R16
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(0)
	ST   X,R30
; 0000 02CD 
; 0000 02CE             // All Vaults Offline Service Led = RED
; 0000 02CF             if (++RS485.Service_Led_Counter<20) Service_Led=0;
	__GETB1MN _RS485,4
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,4
	CPI  R30,LOW(0x14)
	BRSH _0x79
	CBI  0x12,7
; 0000 02D0              else
	RJMP _0x7C
_0x79:
; 0000 02D1                 {
; 0000 02D2                 Service_Led=1;
	SBI  0x12,7
; 0000 02D3                 RS485.Service_Led_Counter=20;
	LDI  R30,LOW(20)
	__PUTB1MN _RS485,4
; 0000 02D4                 };
_0x7C:
; 0000 02D5             }
; 0000 02D6 
; 0000 02D7 
; 0000 02D8       Counter_Responds++;
_0x63:
	SUBI R17,-1
; 0000 02D9       Counter_Responds&=1;  //3
	ANDI R17,LOW(1)
; 0000 02DA 
; 0000 02DB 
; 0000 02DC 
; 0000 02DD 
; 0000 02DE       //******** Data System Poll Respond *********
; 0000 02DF       if (RS232.Request==1)
	__GETB2MN _RS232,2
	CPI  R26,LOW(0x1)
	BREQ PC+3
	JMP _0x7F
; 0000 02E0         {
; 0000 02E1         #asm("wdr");
	wdr
; 0000 02E2 
; 0000 02E3         RS232.Request=0;
	LDI  R30,LOW(0)
	__PUTB1MN _RS232,2
; 0000 02E4         Timers.Timer2=OFFLINE_TIME;
	LDI  R30,LOW(4000)
	LDI  R31,HIGH(4000)
	__PUTW1MN _Timers,4
; 0000 02E5 
; 0000 02E6         //Respond To Poll with Data
; 0000 02E7         if (RS232.Expect_ACK!=ACK)
	__GETB2MN _RS232,3
	CPI  R26,LOW(0x6)
	BRNE PC+3
	JMP _0x80
; 0000 02E8             {
; 0000 02E9             //Set New Time
; 0000 02EA             if (Rtc.sec!=Rtc.New_sec) write_i2c_RTCC(0,Rtc.New_sec,_RTCC);  //New Sec
	__GETB2MN _Rtc,2
	__GETB1MN _Rtc,8
	CP   R30,R26
	BREQ _0x81
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	ST   -Y,R31
	ST   -Y,R30
	__GETB1MN _Rtc,8
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 02EB             if (Rtc.min!=Rtc.New_min) write_i2c_RTCC(1,Rtc.New_min,_RTCC);  //New Min
_0x81:
	__GETB2MN _Rtc,1
	__GETB1MN _Rtc,7
	CP   R30,R26
	BREQ _0x82
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	ST   -Y,R31
	ST   -Y,R30
	__GETB1MN _Rtc,7
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 02EC             if (Rtc.hour!=Rtc.New_hour) write_i2c_RTCC(2,Rtc.New_hour,_RTCC); //New Hours
_0x82:
	__GETB1MN _Rtc,6
	LDS  R26,_Rtc
	CP   R30,R26
	BREQ _0x83
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	ST   -Y,R31
	ST   -Y,R30
	__GETB1MN _Rtc,6
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 02ED 
; 0000 02EE Loop_Send:
_0x83:
_0x84:
; 0000 02EF             //Read Events (from memory)
; 0000 02F0             tmp1=ReadEvent(READ,Buffer);
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(_Buffer)
	LDI  R27,HIGH(_Buffer)
	CALL _ReadEvent
	MOV  R18,R30
; 0000 02F1 
; 0000 02F2             //****** Send Data ******
; 0000 02F3             tmp3=0;
	LDI  R20,LOW(0)
; 0000 02F4             if (tmp1!=2)
	CPI  R18,2
	BREQ _0x85
; 0000 02F5               {
; 0000 02F6                 putchar1(STX);    //STX
	LDI  R26,LOW(2)
	RCALL _putchar1
; 0000 02F7                 for (tmp2=0;tmp2<6;tmp2++)  //Event Byte , Time (HHMMSS) ,I.D. Number
	LDI  R19,LOW(0)
_0x87:
	CPI  R19,6
	BRSH _0x88
; 0000 02F8                     {
; 0000 02F9                     tmp3+=Buffer[tmp2];
	MOV  R30,R19
	LDI  R31,0
	SUBI R30,LOW(-_Buffer)
	SBCI R31,HIGH(-_Buffer)
	LD   R30,Z
	ADD  R20,R30
; 0000 02FA                     putchar1(Buffer[tmp2]);
	MOV  R30,R19
	LDI  R31,0
	SUBI R30,LOW(-_Buffer)
	SBCI R31,HIGH(-_Buffer)
	LD   R26,Z
	RCALL _putchar1
; 0000 02FB                     }
	SUBI R19,-1
	RJMP _0x87
_0x88:
; 0000 02FC 
; 0000 02FD                 putchar1(ETX);  //ETX
	LDI  R26,LOW(3)
	RCALL _putchar1
; 0000 02FE                 putchar1(tmp3); //Check Sum = sum of data
	MOV  R26,R20
	RCALL _putchar1
; 0000 02FF 
; 0000 0300                 RS232.Expect_ACK=0; //Request for ACK
	LDI  R30,LOW(0)
	__PUTB1MN _RS232,3
; 0000 0301                 Vault.IgnoreACK=0;
	__PUTB1MN _Vault,316
; 0000 0302                 Vault.StatusCounter=0;
	__PUTB1MN _Vault,314
; 0000 0303               }
; 0000 0304                 else
	RJMP _0x89
_0x85:
; 0000 0305                     {
; 0000 0306                     //****** Send vault Status ********
; 0000 0307                     if ((++Vault.StatusCounter>=10)&&(ModeSelect))
	__GETB1MN _Vault,314
	SUBI R30,-LOW(1)
	__PUTB1MN _Vault,314
	CPI  R30,LOW(0xA)
	BRLO _0x8B
	LDS  R30,_ModeSelect
	CPI  R30,0
	BRNE _0x8C
_0x8B:
	RJMP _0x8A
_0x8C:
; 0000 0308                         {
; 0000 0309                         Vault.StatusCounter=0;
	LDI  R30,LOW(0)
	__PUTB1MN _Vault,314
; 0000 030A 
; 0000 030B                         Vault.CH_Counter &=1;
	__GETB1MN _Vault,315
	ANDI R30,LOW(0x1)
	__PUTB1MN _Vault,315
; 0000 030C                         Buffer[0]=(Vault.CH_Counter<<4) | BIN_STATUS; //Status Command And Chanel
	__GETB1MN _Vault,315
	SWAP R30
	ANDI R30,0xF0
	ORI  R30,LOW(0x7)
	STS  _Buffer,R30
; 0000 030D                         Buffer[1]=Rtc.hour;    //Hours
	LDS  R30,_Rtc
	__PUTB1MN _Buffer,1
; 0000 030E                         Buffer[2]=Rtc.min;     //Min
	__GETB1MN _Rtc,1
	__PUTB1MN _Buffer,2
; 0000 030F                         Buffer[3]=Rtc.sec;     //Sec
	__GETB1MN _Rtc,2
	__PUTB1MN _Buffer,3
; 0000 0310 
; 0000 0311                         // Respond Bin Number
; 0000 0312                         if (Vault.ChanelStatus[Vault.CH_Counter][1])
	__POINTW2MN _Vault,317
	__GETB1MN _Vault,315
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+1
	CPI  R30,0
	BREQ _0x8D
; 0000 0313                             {
; 0000 0314                             Buffer[4]=((Vault.BIN_No[Vault.CH_Counter][2]&0x0F)<<4) | (Vault.BIN_No[Vault.CH_Counter][3]&0x0F);
	__POINTW2MN _Vault,32
	__GETB1MN _Vault,315
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	MOVW R26,R30
	LDD  R30,Z+2
	ANDI R30,LOW(0xF)
	SWAP R30
	ANDI R30,0xF0
	MOV  R0,R30
	MOVW R30,R26
	LDD  R30,Z+3
	ANDI R30,LOW(0xF)
	OR   R30,R0
	__PUTB1MN _Buffer,4
; 0000 0315                             Buffer[5]=((Vault.BIN_No[Vault.CH_Counter][4]&0x0F)<<4) | (Vault.BIN_No[Vault.CH_Counter][5]&0x0F);
	__POINTW2MN _Vault,32
	__GETB1MN _Vault,315
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	MOVW R26,R30
	LDD  R30,Z+4
	ANDI R30,LOW(0xF)
	SWAP R30
	ANDI R30,0xF0
	MOV  R0,R30
	MOVW R30,R26
	LDD  R30,Z+5
	ANDI R30,LOW(0xF)
	OR   R30,R0
	RJMP _0x11E
; 0000 0316                             }
; 0000 0317                             else // Respond Chanel Offline
_0x8D:
; 0000 0318                                 {
; 0000 0319                                 Buffer[4]=0x40;   // ID_H
	LDI  R30,LOW(64)
	__PUTB1MN _Buffer,4
; 0000 031A                                 Buffer[5]=0x44;   // ID_L
	LDI  R30,LOW(68)
_0x11E:
	__PUTB1MN _Buffer,5
; 0000 031B                                 }
; 0000 031C 
; 0000 031D                         Vault.CH_Counter++;
	__GETB1MN _Vault,315
	SUBI R30,-LOW(1)
	__PUTB1MN _Vault,315
	SUBI R30,LOW(1)
; 0000 031E 
; 0000 031F 
; 0000 0320                         //Send Status
; 0000 0321                         putchar1(STX);    //STX
	LDI  R26,LOW(2)
	RCALL _putchar1
; 0000 0322 
; 0000 0323                         for (tmp2=0;tmp2<6;tmp2++)  //Event Byte , Time (HHMMSS) ,I.D. Number
	LDI  R19,LOW(0)
_0x90:
	CPI  R19,6
	BRSH _0x91
; 0000 0324                             {
; 0000 0325                             tmp3+=Buffer[tmp2];
	MOV  R30,R19
	LDI  R31,0
	SUBI R30,LOW(-_Buffer)
	SBCI R31,HIGH(-_Buffer)
	LD   R30,Z
	ADD  R20,R30
; 0000 0326                             putchar1(Buffer[tmp2]);
	MOV  R30,R19
	LDI  R31,0
	SUBI R30,LOW(-_Buffer)
	SBCI R31,HIGH(-_Buffer)
	LD   R26,Z
	RCALL _putchar1
; 0000 0327                             }
	SUBI R19,-1
	RJMP _0x90
_0x91:
; 0000 0328 
; 0000 0329                         putchar1(ETX);  //ETX
	LDI  R26,LOW(3)
	RCALL _putchar1
; 0000 032A                         putchar1(tmp3); //Check Sum = sum of data
	MOV  R26,R20
	RCALL _putchar1
; 0000 032B 
; 0000 032C                         Vault.IgnoreACK=1;
	LDI  R30,LOW(1)
	__PUTB1MN _Vault,316
; 0000 032D                         RS232.Expect_ACK=0; //Request for ACK
	LDI  R30,LOW(0)
	__PUTB1MN _RS232,3
; 0000 032E 
; 0000 032F                         }
; 0000 0330                         else   //No Data in buffer
	RJMP _0x92
_0x8A:
; 0000 0331                             {
; 0000 0332                             //If at Power Up Vault is not in, send MSG 4096
; 0000 0333                             if ((Vault.OffLine[Vault.OffLine_Counter&0x01]==10)&&(!Timers.PowerUpDelay))
	__POINTW2MN _Vault,300
	__GETB1MN _Vault,304
	ANDI R30,LOW(0x1)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R26,X
	CPI  R26,LOW(0xA)
	BRNE _0x94
	__GETW1MN _Timers,8
	SBIW R30,0
	BREQ _0x95
_0x94:
	RJMP _0x93
_0x95:
; 0000 0334                                {
; 0000 0335                                tmp1=(Vault.OffLine_Counter&0x01)<<4;         //0x03
	__GETB1MN _Vault,304
	ANDI R30,LOW(0x1)
	SWAP R30
	ANDI R30,0xF0
	MOV  R18,R30
; 0000 0336                                Buffer[0]=tmp1|0x05;
	MOV  R30,R18
	ORI  R30,LOW(0x5)
	STS  _Buffer,R30
; 0000 0337                                Buffer[1]=Rtc.hour;    //Hours
	LDS  R30,_Rtc
	__PUTB1MN _Buffer,1
; 0000 0338                                Buffer[2]=Rtc.min;     //Min
	__GETB1MN _Rtc,1
	__PUTB1MN _Buffer,2
; 0000 0339                                Buffer[3]=Rtc.sec;     //Sec
	__GETB1MN _Rtc,2
	__PUTB1MN _Buffer,3
; 0000 033A                                Buffer[4]=0x40;        //ID_H
	LDI  R30,LOW(64)
	__PUTB1MN _Buffer,4
; 0000 033B                                Buffer[5]=0x95;        //ID_L
	LDI  R30,LOW(149)
	__PUTB1MN _Buffer,5
; 0000 033C 
; 0000 033D                                WriteEvevt(Buffer);
	LDI  R26,LOW(_Buffer)
	LDI  R27,HIGH(_Buffer)
	RCALL _WriteEvevt
; 0000 033E 
; 0000 033F                                Vault.OffLine[Vault.OffLine_Counter&0x01]++;      //0x03
	__POINTW2MN _Vault,300
	__GETB1MN _Vault,304
	ANDI R30,LOW(0x1)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	SUBI R30,-LOW(1)
	ST   X,R30
; 0000 0340                                }
; 0000 0341                                else putchar1(EOT); // Respond to poll with no data MSG
	RJMP _0x96
_0x93:
	LDI  R26,LOW(4)
	RCALL _putchar1
; 0000 0342 
; 0000 0343                             Vault.OffLine_Counter++;
_0x96:
	__GETB1MN _Vault,304
	SUBI R30,-LOW(1)
	__PUTB1MN _Vault,304
	SUBI R30,LOW(1)
; 0000 0344                             Vault.OffLine_Counter&=0x01;  //0x03
	__GETB1MN _Vault,304
	ANDI R30,LOW(0x1)
	__PUTB1MN _Vault,304
; 0000 0345                             };
_0x92:
; 0000 0346                     }
_0x89:
; 0000 0347            }
; 0000 0348            else
	RJMP _0x97
_0x80:
; 0000 0349                 {
; 0000 034A                 if (RS232.Expect_ACK==ACK)
	__GETB2MN _RS232,3
	CPI  R26,LOW(0x6)
	BRNE _0x98
; 0000 034B                     {
; 0000 034C                     if (!Vault.IgnoreACK)
	__GETB1MN _Vault,316
	CPI  R30,0
	BRNE _0x99
; 0000 034D                         {
; 0000 034E                         ReadEvent(ERASE,Buffer);
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R26,LOW(_Buffer)
	LDI  R27,HIGH(_Buffer)
	CALL _ReadEvent
; 0000 034F                         RS232.Expect_ACK=NAK;
	LDI  R30,LOW(21)
	__PUTB1MN _RS232,3
; 0000 0350                         goto Loop_Send;
	RJMP _0x84
; 0000 0351                         }
; 0000 0352 
; 0000 0353                     putchar1(EOT);
_0x99:
	LDI  R26,LOW(4)
	RCALL _putchar1
; 0000 0354                     }
; 0000 0355 
; 0000 0356                 Vault.IgnoreACK=0;
_0x98:
	LDI  R30,LOW(0)
	__PUTB1MN _Vault,316
; 0000 0357                 RS232.Expect_ACK=NAK;
	LDI  R30,LOW(21)
	__PUTB1MN _RS232,3
; 0000 0358                 }
_0x97:
; 0000 0359 
; 0000 035A         }
; 0000 035B 
; 0000 035C 
; 0000 035D 
; 0000 035E 
; 0000 035F 
; 0000 0360 
; 0000 0361       //************** Vault Poll Respond *****************
; 0000 0362       if (Vault.Data_Ready[Counter_Responds])
_0x7F:
	__POINTW2MN _Vault,306
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R30,X
	CPI  R30,0
	BRNE PC+3
	JMP _0x9A
; 0000 0363         {
; 0000 0364          RS485.Service_Led_Counter=0;
	LDI  R30,LOW(0)
	__PUTB1MN _RS485,4
; 0000 0365          Vault.Data_Ready[Counter_Responds]=0;
	__POINTW2MN _Vault,306
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	ST   X,R30
; 0000 0366 
; 0000 0367          //Vault Activity
; 0000 0368          Vault.ChanelStatus[Counter_Responds][0]=1;
	__POINTW2MN _Vault,317
	MOV  R30,R17
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(1)
	ST   X,R30
; 0000 0369 
; 0000 036A          if (Vault.OffLine[Counter_Responds]>=10)
	__POINTW2MN _Vault,300
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0xA)
	BRSH PC+3
	JMP _0x9B
; 0000 036B             {
; 0000 036C             for (tmp1=0;tmp1<6;tmp1++) Vault.OLD_BIN_No[Counter_Responds][tmp1]='0';
	LDI  R18,LOW(0)
_0x9D:
	CPI  R18,6
	BRSH _0x9E
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R18
	ADC  R27,R30
	LDI  R30,LOW(48)
	ST   X,R30
	SUBI R18,-1
	RJMP _0x9D
_0x9E:
; 0000 036D for (tmp1=0;tmp1<6;tmp1++) Vault.OLD_CBX_No[Counter_Responds][tmp1]='0';
	LDI  R18,LOW(0)
_0xA0:
	CPI  R18,6
	BRSH _0xA1
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R18
	ADC  R27,R30
	LDI  R30,LOW(48)
	ST   X,R30
	SUBI R18,-1
	RJMP _0xA0
_0xA1:
; 0000 036F Vault.Debounce_Counter[Counter_Responds][1]=0;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,1
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 0370             Vault.Debounce_Counter[Counter_Responds][CBX]=0;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,2
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 0371             }
; 0000 0372 
; 0000 0373          Vault.OffLine[Counter_Responds]=0;
_0x9B:
	__POINTW2MN _Vault,300
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	ST   X,R30
; 0000 0374 
; 0000 0375          Read_Date_Time(0);// Refresh Time
	LDI  R26,LOW(0)
	CALL _Read_Date_Time
; 0000 0376 
; 0000 0377 
; 0000 0378 
; 0000 0379              //********************************************* VAULT NUMBER *************************************************
; 0000 037A              #asm("wdr");
	wdr
; 0000 037B              for (tmp2=0;tmp2<6;tmp2++) if ((Vault.Vault_No[Counter_Responds][tmp2]!='0')&&(Vault.Vault_No[Counter_Responds][tmp2]!=0)) tmp1|=1;
	LDI  R19,LOW(0)
_0xA3:
	CPI  R19,6
	BRSH _0xA4
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	SUBI R30,LOW(-_Vault)
	SBCI R31,HIGH(-_Vault)
	MOVW R26,R30
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x30)
	BREQ _0xA6
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	SUBI R30,LOW(-_Vault)
	SBCI R31,HIGH(-_Vault)
	MOVW R26,R30
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x0)
	BRNE _0xA7
_0xA6:
	RJMP _0xA5
_0xA7:
	ORI  R18,LOW(1)
; 0000 037C              if (tmp1)
_0xA5:
	SUBI R19,-1
	RJMP _0xA3
_0xA4:
	CPI  R18,0
	BRNE PC+3
	JMP _0xA8
; 0000 037D                 {
; 0000 037E                  tmp1=0;
	LDI  R18,LOW(0)
; 0000 037F                  for (tmp2=0;tmp2<6;tmp2++)
	LDI  R19,LOW(0)
_0xAA:
	CPI  R19,6
	BRSH _0xAB
; 0000 0380                     {
; 0000 0381                     if ((Vault.Vault_No[Counter_Responds][tmp2]!=Vault.OLD_Vault_No[Counter_Responds][tmp2])) tmp1|=1;
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	MOVW R22,R30
	SUBI R30,LOW(-_Vault)
	SBCI R31,HIGH(-_Vault)
	MOVW R26,R30
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R0,X
	__POINTW2MN _Vault,192
	ADD  R26,R22
	ADC  R27,R23
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	CP   R30,R0
	BREQ _0xAC
	ORI  R18,LOW(1)
; 0000 0382                     Vault.OLD_Vault_No[Counter_Responds][tmp2]=Vault.Vault_No[Counter_Responds][tmp2];
_0xAC:
	__POINTW2MN _Vault,192
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R19
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	SUBI R30,LOW(-_Vault)
	SBCI R31,HIGH(-_Vault)
	MOVW R26,R30
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
; 0000 0383                     }
	SUBI R19,-1
	RJMP _0xAA
_0xAB:
; 0000 0384                 }
; 0000 0385 
; 0000 0386 
; 0000 0387 
; 0000 0388 
; 0000 0389 
; 0000 038A              //******************************************** BIN NUMBER ***********************************************
; 0000 038B              #asm("wdr");
_0xA8:
	wdr
; 0000 038C              tmp3=0;
	LDI  R20,LOW(0)
; 0000 038D              tmp1=0;
	LDI  R18,LOW(0)
; 0000 038E              for (tmp2=0;tmp2<6;tmp2++)
	LDI  R19,LOW(0)
_0xAE:
	CPI  R19,6
	BRLO PC+3
	JMP _0xAF
; 0000 038F                         {
; 0000 0390                          //Check and debounce BIN number
; 0000 0391                          if (Vault.BIN_No[Counter_Responds][tmp2]==Vault.Debounce_BIN_No[Counter_Responds][tmp2]) Vault.Debounce_Counter[Counter_Responds][BIN]++;
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R0,X
	__POINTW2MN _Vault,128
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	CP   R30,R0
	BRNE _0xB0
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	MOVW R26,R22
	ADD  R26,R30
	ADC  R27,R31
	ADIW R26,1
	LD   R30,X
	SUBI R30,-LOW(1)
	ST   X,R30
; 0000 0392                            else
	RJMP _0xB1
_0xB0:
; 0000 0393                                {
; 0000 0394                                Vault.Debounce_Counter[Counter_Responds][BIN]=0;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,1
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 0395                                Timers.BIN_In[Counter_Responds]=50000;
	__POINTW2MN _Timers,18
	MOV  R30,R17
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(50000)
	LDI  R31,HIGH(50000)
	ST   X+,R30
	ST   X,R31
; 0000 0396                                }
_0xB1:
; 0000 0397 
; 0000 0398                          //Reading BIN No
; 0000 0399                          if ((Vault.BIN_No[Counter_Responds][tmp2]>'0')&&((Vault.BIN_No[Counter_Responds][tmp2]<='9'))) tmp3|=2;
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x31)
	BRLO _0xB3
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x3A)
	BRLO _0xB4
_0xB3:
	RJMP _0xB2
_0xB4:
	ORI  R20,LOW(2)
; 0000 039A 
; 0000 039B                          //Check if number is change
; 0000 039C                          if ((!Timers.BIN_In[Counter_Responds])&&(Vault.BIN_No[Counter_Responds][tmp2]!=Vault.OLD_BIN_No[Counter_Responds][tmp2])) tmp1|=1;
_0xB2:
	__POINTW2MN _Timers,18
	MOV  R30,R17
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETW1P
	SBIW R30,0
	BRNE _0xB6
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R0,X
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	CP   R30,R0
	BRNE _0xB7
_0xB6:
	RJMP _0xB5
_0xB7:
	ORI  R18,LOW(1)
; 0000 039D                          Vault.Debounce_BIN_No[Counter_Responds][tmp2]=Vault.BIN_No[Counter_Responds][tmp2];
_0xB5:
	__POINTW2MN _Vault,128
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R19
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
; 0000 039E                         }
	SUBI R19,-1
	RJMP _0xAE
_0xAF:
; 0000 039F 
; 0000 03A0                 //Limit Debounce No
; 0000 03A1                 if (Vault.Debounce_Counter[Counter_Responds][BIN]>=DEBOUNCE_BIN) Vault.Debounce_Counter[Counter_Responds][BIN]=DEBOUNCE_BIN;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	LDD  R26,Z+1
	CPI  R26,LOW(0x96)
	BRLO _0xB8
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,1
	LDI  R26,LOW(150)
	STD  Z+0,R26
; 0000 03A2 
; 0000 03A3                 if (tmp3==0)
_0xB8:
	CPI  R20,0
	BRNE _0xB9
; 0000 03A4                             {
; 0000 03A5                             Vault.Debounce_Counter[Counter_Responds][BIN]+=20;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	MOVW R26,R22
	ADD  R26,R30
	ADC  R27,R31
	ADIW R26,1
	LD   R30,X
	SUBI R30,-LOW(20)
	ST   X,R30
; 0000 03A6                             //BIN remove, add respond delay ~5 sec
; 0000 03A7                             if (Timers.BIN_In[Counter_Responds]>5000) Timers.BIN_In[Counter_Responds]=500;
	__POINTW2MN _Timers,18
	MOV  R30,R17
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETW1P
	CPI  R30,LOW(0x1389)
	LDI  R26,HIGH(0x1389)
	CPC  R31,R26
	BRLO _0xBA
	__POINTW2MN _Timers,18
	MOV  R30,R17
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(500)
	LDI  R31,HIGH(500)
	ST   X+,R30
	ST   X,R31
; 0000 03A8                             }
_0xBA:
; 0000 03A9 
; 0000 03AA                 if (tmp1)
_0xB9:
	CPI  R18,0
	BRNE PC+3
	JMP _0xBB
; 0000 03AB                         {
; 0000 03AC                         if (tmp3==2) tmp1=BIN_IN;
	CPI  R20,2
	BRNE _0xBC
	LDI  R18,LOW(4)
; 0000 03AD                           else tmp1=BIN_OUT;
	RJMP _0xBD
_0xBC:
	LDI  R18,LOW(5)
; 0000 03AE 
; 0000 03AF 
; 0000 03B0                                  //Chanel Number       Event
; 0000 03B1                         Buffer[0]=(Counter_Responds<<4)|tmp1;
_0xBD:
	MOV  R30,R17
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R18
	STS  _Buffer,R30
; 0000 03B2 
; 0000 03B3                         Buffer[1]=Rtc.hour;//Hours
	LDS  R30,_Rtc
	__PUTB1MN _Buffer,1
; 0000 03B4                         Buffer[2]=Rtc.min; //Min
	__GETB1MN _Rtc,1
	__PUTB1MN _Buffer,2
; 0000 03B5                         Buffer[3]=Rtc.sec; //Sec
	__GETB1MN _Rtc,2
	__PUTB1MN _Buffer,3
; 0000 03B6 
; 0000 03B7                         if (tmp3)
	CPI  R20,0
	BREQ _0xBE
; 0000 03B8                             {
; 0000 03B9                             tmp1=Vault.BIN_No[Counter_Responds][2]&0x0F;
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+2
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 03BA                             tmp2=Vault.BIN_No[Counter_Responds][3]&0x0F;
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+3
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 03BB                             Buffer[4]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,4
; 0000 03BC 
; 0000 03BD                             tmp1=Vault.BIN_No[Counter_Responds][4]&0x0F;
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+4
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 03BE                             tmp2=Vault.BIN_No[Counter_Responds][5]&0x0F;
	__POINTW2MN _Vault,32
	RJMP _0x11F
; 0000 03BF                             Buffer[5]=(tmp1<<4)|tmp2;
; 0000 03C0                             }
; 0000 03C1                             else
_0xBE:
; 0000 03C2                                 {
; 0000 03C3                                 tmp1=Vault.OLD_BIN_No[Counter_Responds][2]&0x0F;
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+2
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 03C4                                 tmp2=Vault.OLD_BIN_No[Counter_Responds][3]&0x0F;
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+3
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 03C5                                 Buffer[4]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,4
; 0000 03C6 
; 0000 03C7                                 tmp1=Vault.OLD_BIN_No[Counter_Responds][4]&0x0F;
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+4
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 03C8                                 tmp2=Vault.OLD_BIN_No[Counter_Responds][5]&0x0F;
	__POINTW2MN _Vault,224
_0x11F:
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+5
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 03C9                                 Buffer[5]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,5
; 0000 03CA                                 }
; 0000 03CB 
; 0000 03CC 
; 0000 03CD                         WriteEvevt(Buffer);
	LDI  R26,LOW(_Buffer)
	LDI  R27,HIGH(_Buffer)
	RCALL _WriteEvevt
; 0000 03CE                         for (tmp2=0;tmp2<6;tmp2++) Vault.OLD_BIN_No[Counter_Responds][tmp2]=Vault.BIN_No[Counter_Responds][tmp2];
	LDI  R19,LOW(0)
_0xC1:
	CPI  R19,6
	BRSH _0xC2
	__POINTW2MN _Vault,224
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R19
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _Vault,32
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
	SUBI R19,-1
	RJMP _0xC1
_0xC2:
; 0000 03CF }
; 0000 03D0 
; 0000 03D1 
; 0000 03D2 
; 0000 03D3 
; 0000 03D4 
; 0000 03D5 
; 0000 03D6 
; 0000 03D7              //************************************************ CBX NUMBER **********************************************
; 0000 03D8              #asm("wdr");
_0xBB:
	wdr
; 0000 03D9              tmp3=0;
	LDI  R20,LOW(0)
; 0000 03DA              tmp1=0;
	LDI  R18,LOW(0)
; 0000 03DB              for (tmp2=0;tmp2<6;tmp2++)
	LDI  R19,LOW(0)
_0xC4:
	CPI  R19,6
	BRLO PC+3
	JMP _0xC5
; 0000 03DC                         {
; 0000 03DD                          if (Vault.CBX_No[Counter_Responds][tmp2]==Vault.Debounce_CBX_No[Counter_Responds][tmp2]) Vault.Debounce_Counter[Counter_Responds][CBX]++;
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R0,X
	__POINTW2MN _Vault,160
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	CP   R30,R0
	BRNE _0xC6
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	MOVW R26,R22
	ADD  R26,R30
	ADC  R27,R31
	ADIW R26,2
	LD   R30,X
	SUBI R30,-LOW(1)
	ST   X,R30
; 0000 03DE                           else Vault.Debounce_Counter[Counter_Responds][CBX]=0;
	RJMP _0xC7
_0xC6:
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,2
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0000 03DF 
; 0000 03E0                          if (Vault.CBX_No[Counter_Responds][tmp2]!='0') tmp3=2;
_0xC7:
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x30)
	BREQ _0xC8
	LDI  R20,LOW(2)
; 0000 03E1                          if ((Vault.Debounce_Counter[Counter_Responds][CBX]>=DEBOUNCE_CBX)&&(Vault.CBX_No[Counter_Responds][tmp2]!=Vault.OLD_CBX_No[Counter_Responds][tmp2])) tmp1|=1;
_0xC8:
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	LDD  R26,Z+2
	CPI  R26,LOW(0x3)
	BRLO _0xCA
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R0,X
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	CP   R30,R0
	BRNE _0xCB
_0xCA:
	RJMP _0xC9
_0xCB:
	ORI  R18,LOW(1)
; 0000 03E2 
; 0000 03E3                          Vault.Debounce_CBX_No[Counter_Responds][tmp2]=Vault.CBX_No[Counter_Responds][tmp2];
_0xC9:
	__POINTW2MN _Vault,160
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R19
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
; 0000 03E4                         }
	SUBI R19,-1
	RJMP _0xC4
_0xC5:
; 0000 03E5 
; 0000 03E6 
; 0000 03E7                 if (Vault.Debounce_Counter[Counter_Responds][CBX]>=230) Vault.Debounce_Counter[Counter_Responds][CBX]=230;
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	LDD  R26,Z+2
	CPI  R26,LOW(0xE6)
	BRLO _0xCC
	__POINTW2MN _Vault,288
	MOV  R30,R17
	LDI  R31,0
	MOVW R22,R26
	LDI  R26,LOW(3)
	LDI  R27,HIGH(3)
	CALL __MULW12U
	ADD  R30,R22
	ADC  R31,R23
	ADIW R30,2
	LDI  R26,LOW(230)
	STD  Z+0,R26
; 0000 03E8 
; 0000 03E9 
; 0000 03EA                 //Vaulted ,ID not detected generate ERROR MSG 6
; 0000 03EB                 if ((Timers.CBX_Error[Vault.Counter]==1)&&(CBX_Present[Vault.Counter]==3)&&((Vault.CBX_Status[Vault.Counter]&2)==0)&&(tmp3==0))
_0xCC:
	__POINTW2MN _Timers,10
	__GETB1MN _Vault,305
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	CALL __GETW1P
	SBIW R30,1
	BRNE _0xCE
	__GETB1MN _Vault,305
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,2
	ADD  R26,R30
	ADC  R27,R31
	LD   R26,X
	CPI  R26,LOW(0x3)
	BRNE _0xCE
	__POINTW2MN _Vault,310
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x2)
	BRNE _0xCE
	CPI  R20,0
	BREQ _0xCF
_0xCE:
	RJMP _0xCD
_0xCF:
; 0000 03EC                     {
; 0000 03ED                     tmp1=1;
	LDI  R18,LOW(1)
; 0000 03EE                     tmp3=4;
	LDI  R20,LOW(4)
; 0000 03EF                     }
; 0000 03F0 
; 0000 03F1 
; 0000 03F2                 //Set CBX ID not Detected Error Timer
; 0000 03F3                 if ((Vault.CBX_Status[Vault.Counter]&3)==0) Timers.CBX_Error[Vault.Counter]=CBX_TIMEOUT;
_0xCD:
	__POINTW2MN _Vault,310
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x3)
	BRNE _0xD0
	__POINTW2MN _Timers,10
	__GETB1MN _Vault,305
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(5000)
	LDI  R31,HIGH(5000)
	ST   X+,R30
	ST   X,R31
; 0000 03F4 
; 0000 03F5 
; 0000 03F6                 // CBX present bit + position bit
; 0000 03F7                 CBX_Present[Vault.Counter]=Vault.CBX_Status[Vault.Counter]&3;
_0xD0:
	__GETB1MN _Vault,305
	LDI  R31,0
	MOVW R22,R30
	MOVW R26,R28
	ADIW R26,2
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _Vault,310
	ADD  R26,R22
	ADC  R27,R23
	LD   R30,X
	ANDI R30,LOW(0x3)
	MOVW R26,R0
	ST   X,R30
; 0000 03F8 
; 0000 03F9                 //New
; 0000 03FA                 if (tmp1)
	CPI  R18,0
	BRNE PC+3
	JMP _0xD1
; 0000 03FB                         {
; 0000 03FC                         //Assign event
; 0000 03FD                         if (tmp3!=4)
	CPI  R20,4
	BREQ _0xD2
; 0000 03FE                             {
; 0000 03FF                             if (((Vault.CBX_Status[Vault.Counter]&2)|tmp3)==2) tmp1=CBX_IN;  //CBX Insert
	__POINTW2MN _Vault,310
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x2)
	OR   R30,R20
	CPI  R30,LOW(0x2)
	BRNE _0xD3
	LDI  R18,LOW(2)
; 0000 0400                                else tmp1=CBX_OUT;  //CBX Remove
	RJMP _0xD4
_0xD3:
	LDI  R18,LOW(3)
; 0000 0401                             }
_0xD4:
; 0000 0402                              else tmp1=CBX_ID_ERROR;   //Vaulter no detected CBX ID
	RJMP _0xD5
_0xD2:
	LDI  R18,LOW(6)
; 0000 0403 
; 0000 0404 
; 0000 0405                                 //  Chanel Number       Event
; 0000 0406                         Buffer[0]=(Counter_Responds<<4)|tmp1;
_0xD5:
	MOV  R30,R17
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R18
	STS  _Buffer,R30
; 0000 0407 
; 0000 0408                         Buffer[1]=Rtc.hour;//Hours
	LDS  R30,_Rtc
	__PUTB1MN _Buffer,1
; 0000 0409                         Buffer[2]=Rtc.min; //Min
	__GETB1MN _Rtc,1
	__PUTB1MN _Buffer,2
; 0000 040A                         Buffer[3]=Rtc.sec; //Sec
	__GETB1MN _Rtc,2
	__PUTB1MN _Buffer,3
; 0000 040B 
; 0000 040C                         if ((Vault.CBX_Status[Vault.Counter]&2)||(tmp3))
	__POINTW2MN _Vault,310
	__GETB1MN _Vault,305
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x2)
	BRNE _0xD7
	CPI  R20,0
	BREQ _0xD6
_0xD7:
; 0000 040D                             {
; 0000 040E                             tmp1=Vault.CBX_No[Counter_Responds][2]&0x0F;
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+2
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 040F                             tmp2=Vault.CBX_No[Counter_Responds][3]&0x0F;
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+3
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 0410                             Buffer[4]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,4
; 0000 0411 
; 0000 0412                             tmp1=Vault.CBX_No[Counter_Responds][4]&0x0F;
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+4
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 0413                             tmp2=Vault.CBX_No[Counter_Responds][5]&0x0F;
	__POINTW2MN _Vault,64
	RJMP _0x120
; 0000 0414                             Buffer[5]=(tmp1<<4)|tmp2;
; 0000 0415                             }
; 0000 0416                             else
_0xD6:
; 0000 0417                                 {
; 0000 0418                                 tmp1=Vault.OLD_CBX_No[Counter_Responds][2]&0x0F;
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+2
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 0419                                 tmp2=Vault.OLD_CBX_No[Counter_Responds][3]&0x0F;
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+3
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 041A                                 Buffer[4]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,4
; 0000 041B 
; 0000 041C                                 tmp1=Vault.OLD_CBX_No[Counter_Responds][4]&0x0F;
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+4
	ANDI R30,LOW(0xF)
	MOV  R18,R30
; 0000 041D                                 tmp2=Vault.OLD_CBX_No[Counter_Responds][5]&0x0F;
	__POINTW2MN _Vault,256
_0x120:
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R30,R26
	ADC  R31,R27
	LDD  R30,Z+5
	ANDI R30,LOW(0xF)
	MOV  R19,R30
; 0000 041E                                 Buffer[5]=(tmp1<<4)|tmp2;
	MOV  R30,R18
	SWAP R30
	ANDI R30,0xF0
	OR   R30,R19
	__PUTB1MN _Buffer,5
; 0000 041F                                 }
; 0000 0420 
; 0000 0421                         WriteEvevt(Buffer);
	LDI  R26,LOW(_Buffer)
	LDI  R27,HIGH(_Buffer)
	RCALL _WriteEvevt
; 0000 0422                         Timers.CBX_Error[Vault.Counter]=0;
	__POINTW2MN _Timers,10
	__GETB1MN _Vault,305
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	ST   X+,R30
	ST   X,R31
; 0000 0423 
; 0000 0424                         for (tmp2=0;tmp2<6;tmp2++) Vault.OLD_CBX_No[Counter_Responds][tmp2]=Vault.CBX_No[Counter_Responds][tmp2];
	LDI  R19,LOW(0)
_0xDB:
	CPI  R19,6
	BRSH _0xDC
	__POINTW2MN _Vault,256
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R19
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _Vault,64
	MOV  R30,R17
	LDI  R31,0
	CALL __LSLW3
	ADD  R26,R30
	ADC  R27,R31
	CLR  R30
	ADD  R26,R19
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
	SUBI R19,-1
	RJMP _0xDB
_0xDC:
; 0000 0425 }
; 0000 0426         }
_0xD1:
; 0000 0427 
; 0000 0428 
; 0000 0429 
; 0000 042A 
; 0000 042B 
; 0000 042C            //******************* Check Voltages *******************
; 0000 042D            tmp1=0;
_0x9A:
	LDI  R18,LOW(0)
; 0000 042E            //12V
; 0000 042F            tmp_int=(read_adc(1)*32)/100;
	LDI  R26,LOW(1)
	RCALL _read_adc
	LSL  R30
	ROL  R31
	CALL __LSLW4
	MOVW R26,R30
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __DIVW21U
	ST   Y,R30
	STD  Y+1,R31
; 0000 0430            if ((tmp_int<Power_Min)||(tmp_int>Power_Max)) tmp1|=1;
	LD   R26,Y
	LDD  R27,Y+1
	CPI  R26,LOW(0x73)
	LDI  R30,HIGH(0x73)
	CPC  R27,R30
	BRLO _0xDE
	CPI  R26,LOW(0xFB)
	LDI  R30,HIGH(0xFB)
	CPC  R27,R30
	BRLO _0xDD
_0xDE:
	ORI  R18,LOW(1)
; 0000 0431            //5V
; 0000 0432            tmp_int=(read_adc(0)*8)/10;
_0xDD:
	LDI  R26,LOW(0)
	RCALL _read_adc
	CALL __LSLW3
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21U
	ST   Y,R30
	STD  Y+1,R31
; 0000 0433            if ((tmp_int<Vcc_Min)||(tmp_int>Vcc_Max)) tmp1|=2;
	LD   R26,Y
	LDD  R27,Y+1
	CPI  R26,LOW(0x1C2)
	LDI  R30,HIGH(0x1C2)
	CPC  R27,R30
	BRLO _0xE1
	CPI  R26,LOW(0x227)
	LDI  R30,HIGH(0x227)
	CPC  R27,R30
	BRLO _0xE0
_0xE1:
	ORI  R18,LOW(2)
; 0000 0434             //Isolated
; 0000 0435            tmp_int=read_adc(3);
_0xE0:
	LDI  R26,LOW(3)
	RCALL _read_adc
	ST   Y,R30
	STD  Y+1,R31
; 0000 0436            if (tmp_int>V_ISO_OK) tmp1|=4;
	LD   R26,Y
	LDD  R27,Y+1
	CPI  R26,LOW(0x1F5)
	LDI  R30,HIGH(0x1F5)
	CPC  R27,R30
	BRLO _0xE3
	ORI  R18,LOW(4)
; 0000 0437 
; 0000 0438            if (!tmp1) Led_Power=0;
_0xE3:
	CPI  R18,0
	BRNE _0xE4
	CBI  0x12,6
; 0000 0439              else Led_Power=1;
	RJMP _0xE7
_0xE4:
	SBI  0x12,6
; 0000 043A     }
_0xE7:
	JMP  _0x60
; 0000 043B 
; 0000 043C }
_0xEA:
	RJMP _0xEA
;
;
;
;
;
;
;
;
;// Read the AD conversion result
;unsigned int read_adc(unsigned char adc_input)
; 0000 0447 {
_read_adc:
; 0000 0448 ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
	ST   -Y,R26
;	adc_input -> Y+0
	LD   R30,Y
	OUT  0x7,R30
; 0000 0449 // Delay needed for the stabilization of the ADC input voltage
; 0000 044A delay_us(10);
	__DELAY_USB 25
; 0000 044B // Start the AD conversion
; 0000 044C ADCSRA|=0x40;
	SBI  0x6,6
; 0000 044D // Wait for the AD conversion to complete
; 0000 044E while ((ADCSRA & 0x10)==0);
_0xEB:
	SBIS 0x6,4
	RJMP _0xEB
; 0000 044F ADCSRA|=0x10;
	SBI  0x6,4
; 0000 0450 return ADCW;
	IN   R30,0x4
	IN   R31,0x4+1
	JMP  _0x2060001
; 0000 0451 }
;
;
;
;
;
;
;//************************************** Poll Vault(n)*****************************************
;void Poll_Vault(unsigned char Vault_Number)
; 0000 045A {
_Poll_Vault:
; 0000 045B  unsigned char tmp;
; 0000 045C 
; 0000 045D     Vault_Number&=0x03;
	ST   -Y,R26
	ST   -Y,R16
;	Vault_Number -> Y+1
;	tmp -> R16
	LDD  R30,Y+1
	ANDI R30,LOW(0x3)
	STD  Y+1,R30
; 0000 045E 
; 0000 045F     DE=1;
	SBI  0x15,7
; 0000 0460     putchar(STX);
	LDI  R26,LOW(2)
	CALL _putchar
; 0000 0461     putchar(0x30+Vault_Number);//node address
	LDD  R26,Y+1
	SUBI R26,-LOW(48)
	CALL _putchar
; 0000 0462     putchar('P');//Poll
	LDI  R26,LOW(80)
	CALL _putchar
; 0000 0463 
; 0000 0464     tmp=Vault_Light[Vault_Number][0] & 0x03;  // Get Bin light status
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x3)
	MOV  R16,R30
; 0000 0465 
; 0000 0466     if (tmp==2) //Bin blink, status light
	CPI  R16,2
	BRNE _0xF0
; 0000 0467         {
; 0000 0468          // Toggle light
; 0000 0469          if (Vault_Light[Vault_Number][1]==1) Vault_Light[Vault_Number][1]=0;
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	LDD  R26,Z+1
	CPI  R26,LOW(0x1)
	BRNE _0xF1
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,1
	LDI  R26,LOW(0)
	RJMP _0x121
; 0000 046A           else Vault_Light[Vault_Number][1]=1;
_0xF1:
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	ADIW R30,1
	LDI  R26,LOW(1)
_0x121:
	STD  Z+0,R26
; 0000 046B 
; 0000 046C         tmp=Vault_Light[Vault_Number][1];
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R30,R26
	ADC  R31,R27
	LDD  R16,Z+1
; 0000 046D         }
; 0000 046E 
; 0000 046F     tmp|=((((Link_Led1<<1) & (Vault_Light[Vault_Number][0]&0x04) >> 1)) & 0x02); // add status of external light 2
_0xF0:
	LDI  R26,0
	SBIC 0x15,5
	LDI  R26,1
	MOV  R30,R26
	LSL  R30
	MOV  R0,R30
	LDD  R30,Y+1
	LDI  R26,LOW(_Vault_Light_G000)
	LDI  R27,HIGH(_Vault_Light_G000)
	LDI  R31,0
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	ANDI R30,LOW(0x4)
	LSR  R30
	AND  R30,R0
	ANDI R30,LOW(0x2)
	OR   R16,R30
; 0000 0470 
; 0000 0471     tmp|=0x30; //Convert in ASCII
	ORI  R16,LOW(48)
; 0000 0472 
; 0000 0473     putchar(tmp); // Set/Reset Vault Lights {0,1,2,4,5,6}
	MOV  R26,R16
	CALL _putchar
; 0000 0474     putchar(ETX);
	LDI  R26,LOW(3)
	CALL _putchar
; 0000 0475 
; 0000 0476 
; 0000 0477     // LRC=0x80 | (Node_address ^ 'P' ^ Vault_Light ^ ETX)
; 0000 0478     putchar(0x80|((0x30+Vault_Number)^'P'^tmp^ETX));
	LDD  R26,Y+1
	SUBI R26,-LOW(48)
	LDI  R30,LOW(80)
	EOR  R30,R26
	EOR  R30,R16
	LDI  R26,LOW(3)
	EOR  R30,R26
	ORI  R30,0x80
	MOV  R26,R30
	CALL _putchar
; 0000 0479 };
	LDD  R16,Y+0
	RJMP _0x2060006
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;
;//************************************* Write Data in RTCC Sram Buffer/Eerprm *******************************
;void WriteEvevt(unsigned char *data)
; 0000 048C {
_WriteEvevt:
; 0000 048D unsigned char tmp,tmp1,Data_Counter;
; 0000 048E unsigned int tmp_int;
; 0000 048F 
; 0000 0490 
; 0000 0491     Data_Counter=read_i2c_RTCC(50,_RTCC);
	ST   -Y,R27
	ST   -Y,R26
	CALL __SAVELOCR5
;	*data -> Y+5
;	tmp -> R16
;	tmp1 -> R17
;	Data_Counter -> R18
;	tmp_int -> R19,R20
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _read_i2c_RTCC
	MOV  R18,R30
; 0000 0492 
; 0000 0493     #ifdef _DEBUG_
; 0000 0494     debug_putchar(Data_Counter);
; 0000 0495     #endif
; 0000 0496 
; 0000 0497 
; 0000 0498     if (Data_Counter<=6) // if counter<6 write new event
	CPI  R18,7
	BRSH _0xF3
; 0000 0499         {
; 0000 049A         #asm("wdr");
	wdr
; 0000 049B         write_i2c_RTCC(50,1+Data_Counter,_RTCC);//save counter and pointer
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	MOV  R30,R18
	SUBI R30,-LOW(1)
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 049C 
; 0000 049D         start();
	CALL _start
; 0000 049E         send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	CALL _send_I2C
; 0000 049F         send_I2C(((8+Data_Counter*6)&0xFF));
	LDI  R26,LOW(6)
	MUL  R18,R26
	MOVW R30,R0
	SUBI R30,-LOW(8)
	LDI  R31,0
	MOVW R26,R30
	CALL _send_I2C
; 0000 04A0         for (tmp1=0;tmp1<6;tmp1++) send_I2C(data[tmp1]); //Write in RTCC Sram data[0]=Event ,data[1]=H ,data[2]=M, data[3]=S, data[4]=CBX H, data[5]=CBX L
	LDI  R17,LOW(0)
_0xF5:
	CPI  R17,6
	BRSH _0xF6
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CLR  R27
	CALL _send_I2C
	SUBI R17,-1
	RJMP _0xF5
_0xF6:
; 0000 04A1 stop();
	RJMP _0x122
; 0000 04A2         }
; 0000 04A3         else
_0xF3:
; 0000 04A4             {
; 0000 04A5             #asm("wdr");
	wdr
; 0000 04A6             if (Data_Counter==7)  //Save RTCC Sram in EEPROM
	CPI  R18,7
	BREQ PC+3
	JMP _0xF8
; 0000 04A7                 {
; 0000 04A8 
; 0000 04A9                  write_i2c_RTCC(50,0x08,_RTCC); // save Data_Counter=8 and Data_Pointer
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(208)
	CALL _write_i2c_RTCC
; 0000 04AA 
; 0000 04AB 
; 0000 04AC                  // Wrate in EEPROM  last event
; 0000 04AD                  tmp_int=7+R_Flash_Pointer_End();
	RCALL _R_Flash_Pointer_End
	ADIW R30,7
	__PUTW1R 19,20
; 0000 04AE                  tmp_int*=8;
	__MULBNWRU 19,20,8
	__PUTW1R 19,20
; 0000 04AF 
; 0000 04B0                  start();
	CALL _start
; 0000 04B1                  send_I2C(_24C64);
	LDI  R26,LOW(160)
	LDI  R27,0
	CALL _send_I2C
; 0000 04B2 
; 0000 04B3                  send_I2C(((tmp_int>>8)&0xFF));
	MOV  R30,R20
	ANDI R31,HIGH(0x0)
	MOVW R26,R30
	CALL _send_I2C
; 0000 04B4                  send_I2C((tmp_int&0xFF));
	__GETW1R 19,20
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	CALL _send_I2C
; 0000 04B5 
; 0000 04B6                  for (tmp1=0;tmp1<6;tmp1++) //Write Data In EEPROM
	LDI  R17,LOW(0)
_0xFA:
	CPI  R17,6
	BRSH _0xFB
; 0000 04B7                         {
; 0000 04B8                         send_I2C(data[tmp1]);
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CLR  R27
	CALL _send_I2C
; 0000 04B9                         delay_ms(5);
	LDI  R26,LOW(5)
	LDI  R27,0
	CALL _delay_ms
; 0000 04BA                         }
	SUBI R17,-1
	RJMP _0xFA
_0xFB:
; 0000 04BB 
; 0000 04BC                   stop();
	CALL _stop
; 0000 04BD                  //End write in EEPROM
; 0000 04BE 
; 0000 04BF 
; 0000 04C0                  //Copy From RTCC Sram to EEPROM lat 7 events
; 0000 04C1                  for (tmp=0;tmp<=6;tmp++)
	LDI  R16,LOW(0)
_0xFD:
	CPI  R16,7
	BRLO PC+3
	JMP _0xFE
; 0000 04C2                     {
; 0000 04C3 
; 0000 04C4                     #asm("wdr");
	wdr
; 0000 04C5 
; 0000 04C6                     //Read From RTCC Sram
; 0000 04C7                     start();
	CALL _start
; 0000 04C8                     send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	CALL _send_I2C
; 0000 04C9                     send_I2C((8+tmp*6));
	LDI  R26,LOW(6)
	MUL  R16,R26
	MOVW R30,R0
	SUBI R30,-LOW(8)
	LDI  R31,0
	MOVW R26,R30
	CALL _send_I2C
; 0000 04CA                     start();
	CALL _start
; 0000 04CB                     send_I2C(_RTCC|1);
	LDI  R26,LOW(209)
	LDI  R27,0
	CALL _send_I2C
; 0000 04CC                     for (tmp1=0;tmp1<6;tmp1++) data[tmp1]=read_I2C(1);
	LDI  R17,LOW(0)
_0x100:
	CPI  R17,6
	BRSH _0x101
	MOV  R30,R17
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	LDI  R26,LOW(1)
	CALL _read_I2C
	POP  R26
	POP  R27
	ST   X,R30
	SUBI R17,-1
	RJMP _0x100
_0x101:
; 0000 04CD read_I2C(0);
	LDI  R26,LOW(0)
	CALL _read_I2C
; 0000 04CE                     stop();
	CALL _stop
; 0000 04CF                     //End read RTCC Sram
; 0000 04D0 
; 0000 04D1 
; 0000 04D2                     // Wrate in EEPROM
; 0000 04D3                     tmp_int=R_Flash_Pointer_End();
	RCALL _R_Flash_Pointer_End
	__PUTW1R 19,20
; 0000 04D4                     W_Flash_Pointer_End((1+tmp_int));
	__GETW2R 19,20
	ADIW R26,1
	RCALL _W_Flash_Pointer_End
; 0000 04D5 
; 0000 04D6                     tmp_int*=8;
	__MULBNWRU 19,20,8
	__PUTW1R 19,20
; 0000 04D7 
; 0000 04D8                     start();
	CALL _start
; 0000 04D9                     send_I2C(_24C64);
	LDI  R26,LOW(160)
	LDI  R27,0
	RCALL _send_I2C
; 0000 04DA 
; 0000 04DB                     send_I2C(((tmp_int>>8)&0xFF));
	MOV  R30,R20
	ANDI R31,HIGH(0x0)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 04DC                     send_I2C((tmp_int&0xFF));
	__GETW1R 19,20
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 04DD 
; 0000 04DE                     for (tmp1=0;tmp1<6;tmp1++)
	LDI  R17,LOW(0)
_0x103:
	CPI  R17,6
	BRSH _0x104
; 0000 04DF                         {
; 0000 04E0                         #asm("wdr");
	wdr
; 0000 04E1                         send_I2C(data[tmp1]);
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CLR  R27
	RCALL _send_I2C
; 0000 04E2                         delay_ms(5);
	LDI  R26,LOW(5)
	LDI  R27,0
	CALL _delay_ms
; 0000 04E3                         }
	SUBI R17,-1
	RJMP _0x103
_0x104:
; 0000 04E4                     stop();
	RCALL _stop
; 0000 04E5                     //End write in EEPROM
; 0000 04E6                     }
	SUBI R16,-1
	RJMP _0xFD
_0xFE:
; 0000 04E7 
; 0000 04E8                     W_Flash_Pointer_End((1+R_Flash_Pointer_End()));
	RCALL _R_Flash_Pointer_End
	ADIW R30,1
	MOVW R26,R30
	RCALL _W_Flash_Pointer_End
; 0000 04E9                 }
; 0000 04EA                 else
	RJMP _0x105
_0xF8:
; 0000 04EB                     {
; 0000 04EC 
; 0000 04ED                     tmp_int=R_Flash_Pointer_End();
	RCALL _R_Flash_Pointer_End
	__PUTW1R 19,20
; 0000 04EE                     W_Flash_Pointer_End((1+tmp_int));
	__GETW2R 19,20
	ADIW R26,1
	RCALL _W_Flash_Pointer_End
; 0000 04EF 
; 0000 04F0                     tmp_int*=8;
	__MULBNWRU 19,20,8
	__PUTW1R 19,20
; 0000 04F1 
; 0000 04F2                     //  Write Event in Eeprom
; 0000 04F3                     start();
	RCALL _start
; 0000 04F4                     send_I2C(_24C64);
	LDI  R26,LOW(160)
	LDI  R27,0
	RCALL _send_I2C
; 0000 04F5 
; 0000 04F6                     send_I2C(((tmp_int>>8)&0xFF));
	MOV  R30,R20
	ANDI R31,HIGH(0x0)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 04F7                     send_I2C((tmp_int&0xFF));
	__GETW1R 19,20
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 04F8 
; 0000 04F9                     for (tmp=0;tmp<6;tmp++)
	LDI  R16,LOW(0)
_0x107:
	CPI  R16,6
	BRSH _0x108
; 0000 04FA                         {
; 0000 04FB                         send_I2C(data[tmp]);
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	LD   R26,X
	CLR  R27
	RCALL _send_I2C
; 0000 04FC                         delay_ms(5);
	LDI  R26,LOW(5)
	LDI  R27,0
	CALL _delay_ms
; 0000 04FD                         }
	SUBI R16,-1
	RJMP _0x107
_0x108:
; 0000 04FE                     stop();
_0x122:
	RCALL _stop
; 0000 04FF                     }
_0x105:
; 0000 0500             }
; 0000 0501 };
	CALL __LOADLOCR5
	RJMP _0x2060009
;
;
;
;
;
;
;
;
;
;//************************************* Read Data from  RTCC Sram Buffer/Eeprom  *******************************
;unsigned char ReadEvent(unsigned char ack,unsigned char *data)
; 0000 050D {
_ReadEvent:
; 0000 050E 
; 0000 050F unsigned char Data_Counter,tmp;
; 0000 0510 unsigned int tmp_int;
; 0000 0511 
; 0000 0512 
; 0000 0513     #asm("wdr");
	ST   -Y,R27
	ST   -Y,R26
	CALL __SAVELOCR4
;	ack -> Y+6
;	*data -> Y+4
;	Data_Counter -> R16
;	tmp -> R17
;	tmp_int -> R18,R19
	wdr
; 0000 0514 
; 0000 0515     Data_Counter=read_i2c_RTCC(50,_RTCC);
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _read_i2c_RTCC
	MOV  R16,R30
; 0000 0516 
; 0000 0517     if (Data_Counter<=6)
	CPI  R16,7
	BRLO PC+3
	JMP _0x109
; 0000 0518               {
; 0000 0519               if (ack==1)
	LDD  R26,Y+6
	CPI  R26,LOW(0x1)
	BRNE _0x10A
; 0000 051A                         {
; 0000 051B                         //Erase Data in RTCC Sram
; 0000 051C                         if (Data_Counter>0) Data_Counter--;
	CPI  R16,1
	BRLO _0x10B
	SUBI R16,1
; 0000 051D                          else return(2);
	RJMP _0x10C
_0x10B:
	LDI  R30,LOW(2)
	RJMP _0x2060008
; 0000 051E 
; 0000 051F                         Shift_RTCC_SRAM_Data();
_0x10C:
	RCALL _Shift_RTCC_SRAM_Data
; 0000 0520                         write_i2c_RTCC(50,Data_Counter,_RTCC);
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	ST   -Y,R16
	LDI  R26,LOW(208)
	RCALL _write_i2c_RTCC
; 0000 0521 
; 0000 0522                         return (1);
	LDI  R30,LOW(1)
	RJMP _0x2060008
; 0000 0523                         };
_0x10A:
; 0000 0524 
; 0000 0525 
; 0000 0526               if (Data_Counter==0) return(2);
	CPI  R16,0
	BRNE _0x10D
	LDI  R30,LOW(2)
	RJMP _0x2060008
; 0000 0527 
; 0000 0528               start();
_0x10D:
	RCALL _start
; 0000 0529               send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	RCALL _send_I2C
; 0000 052A               send_I2C(8);
	LDI  R26,LOW(8)
	LDI  R27,0
	RCALL _send_I2C
; 0000 052B               start();
	RCALL _start
; 0000 052C               send_I2C(_RTCC|1);
	LDI  R26,LOW(209)
	LDI  R27,0
	RCALL _send_I2C
; 0000 052D               for (tmp=0;tmp<6;tmp++) data[tmp]=read_I2C(1); //data[0]=Event,data[1]=H,data[2]=M,data[3]=S,data[4]=CBX H,data[5]=CBX L
	LDI  R17,LOW(0)
_0x10F:
	CPI  R17,6
	BRSH _0x110
	MOV  R30,R17
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	LDI  R26,LOW(1)
	RCALL _read_I2C
	POP  R26
	POP  R27
	ST   X,R30
	SUBI R17,-1
	RJMP _0x10F
_0x110:
; 0000 052E read_I2C(0);
	LDI  R26,LOW(0)
	RCALL _read_I2C
; 0000 052F               stop();
	RCALL _stop
; 0000 0530 
; 0000 0531               return(0);
	LDI  R30,LOW(0)
	RJMP _0x2060008
; 0000 0532               }
; 0000 0533               else
_0x109:
; 0000 0534                  {
; 0000 0535                  tmp_int=R_Flash_Pointer_Begin();
	RCALL _R_Flash_Pointer_Begin
	MOVW R18,R30
; 0000 0536                  if (tmp_int==R_Flash_Pointer_End())
	RCALL _R_Flash_Pointer_End
	CP   R30,R18
	CPC  R31,R19
	BRNE _0x112
; 0000 0537                     {
; 0000 0538                     write_i2c_RTCC(50,0,_RTCC);
	LDI  R30,LOW(50)
	LDI  R31,HIGH(50)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _write_i2c_RTCC
; 0000 0539                     return(2);
	LDI  R30,LOW(2)
	RJMP _0x2060008
; 0000 053A                     }
; 0000 053B 
; 0000 053C                  if (ack==1)
_0x112:
	LDD  R26,Y+6
	CPI  R26,LOW(0x1)
	BRNE _0x113
; 0000 053D                             {
; 0000 053E                             //Erase Data in eeprom
; 0000 053F                             write_i2c_EEPROM((8*tmp_int),0xFF,_24C64);
	__MULBNWRU 18,19,8
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(255)
	ST   -Y,R30
	LDI  R26,LOW(160)
	RCALL _write_i2c_EEPROM
; 0000 0540                             W_Flash_Pointer_Begin((1+tmp_int));
	MOVW R26,R18
	ADIW R26,1
	RCALL _W_Flash_Pointer_Begin
; 0000 0541                             return (1);
	LDI  R30,LOW(1)
	RJMP _0x2060008
; 0000 0542                             }
; 0000 0543 
; 0000 0544                 tmp_int*=8;
_0x113:
	__MULBNWRU 18,19,8
	MOVW R18,R30
; 0000 0545 
; 0000 0546                 //Read Data From Flash
; 0000 0547                 start();
	RCALL _start
; 0000 0548                 send_I2C(_24C64);
	LDI  R26,LOW(160)
	LDI  R27,0
	RCALL _send_I2C
; 0000 0549                 send_I2C(((tmp_int>>8)&0x1F));
	MOV  R30,R19
	ANDI R31,HIGH(0x0)
	ANDI R30,LOW(0x1F)
	ANDI R31,HIGH(0x1F)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 054A                 send_I2C(tmp_int&0xFF);
	MOVW R30,R18
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0000 054B                 start();
	RCALL _start
; 0000 054C                 send_I2C(_24C64|1);
	LDI  R26,LOW(161)
	LDI  R27,0
	RCALL _send_I2C
; 0000 054D 
; 0000 054E                 for (tmp=0;tmp<6;tmp++) data[tmp]=read_I2C(1);
	LDI  R17,LOW(0)
_0x115:
	CPI  R17,6
	BRSH _0x116
	MOV  R30,R17
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	LDI  R26,LOW(1)
	RCALL _read_I2C
	POP  R26
	POP  R27
	ST   X,R30
	SUBI R17,-1
	RJMP _0x115
_0x116:
; 0000 0550 read_I2C(0);
	LDI  R26,LOW(0)
	RCALL _read_I2C
; 0000 0551                 stop();
	RCALL _stop
; 0000 0552                 return(0);
	LDI  R30,LOW(0)
; 0000 0553                 }
; 0000 0554 };
_0x2060008:
	CALL __LOADLOCR4
_0x2060009:
	ADIW R28,7
	RET
;
;
;
;
;
;
;
;
;
;//********************************  Read Begin Pointer From RTCC_Sram *************************
;unsigned int R_Flash_Pointer_Begin()
; 0000 0560 {
_R_Flash_Pointer_Begin:
; 0000 0561 unsigned int Pointer;
; 0000 0562 
; 0000 0563     Pointer=read_i2c_RTCC(51,_RTCC);
	ST   -Y,R17
	ST   -Y,R16
;	Pointer -> R16,R17
	LDI  R30,LOW(51)
	LDI  R31,HIGH(51)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _read_i2c_RTCC
	MOV  R16,R30
	CLR  R17
; 0000 0564     Pointer=(Pointer<<8)|read_i2c_RTCC(52,_RTCC);
	MOV  R31,R16
	LDI  R30,LOW(0)
	PUSH R31
	PUSH R30
	LDI  R30,LOW(52)
	LDI  R31,HIGH(52)
	RJMP _0x2060007
; 0000 0565     return(Pointer);
; 0000 0566 }
;
;
;
;
;
;
;//********************************  Write End Pointer in RTCC_Sram *************************
;void W_Flash_Pointer_Begin(unsigned int pointer)
; 0000 056F {
_W_Flash_Pointer_Begin:
; 0000 0570     write_i2c_RTCC(51,((pointer>>8)&0xFF),_RTCC);
	ST   -Y,R27
	ST   -Y,R26
;	pointer -> Y+0
	LDI  R30,LOW(51)
	LDI  R31,HIGH(51)
	ST   -Y,R31
	ST   -Y,R30
	LDD  R30,Y+3
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _write_i2c_RTCC
; 0000 0571     write_i2c_RTCC(52,(pointer&0xFF),_RTCC);
	LDI  R30,LOW(52)
	LDI  R31,HIGH(52)
	RJMP _0x2060005
; 0000 0572 }
;
;
;
;
;
;
;//********************************  Read End Pointer From RTCC_Sram *************************
;unsigned int R_Flash_Pointer_End()
; 0000 057B {
_R_Flash_Pointer_End:
; 0000 057C unsigned int Pointer;
; 0000 057D 
; 0000 057E     Pointer=read_i2c_RTCC(53,_RTCC);
	ST   -Y,R17
	ST   -Y,R16
;	Pointer -> R16,R17
	LDI  R30,LOW(53)
	LDI  R31,HIGH(53)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _read_i2c_RTCC
	MOV  R16,R30
	CLR  R17
; 0000 057F     Pointer=(Pointer<<8)|read_i2c_RTCC(54,_RTCC);
	MOV  R31,R16
	LDI  R30,LOW(0)
	PUSH R31
	PUSH R30
	LDI  R30,LOW(54)
	LDI  R31,HIGH(54)
_0x2060007:
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _read_i2c_RTCC
	POP  R26
	POP  R27
	LDI  R31,0
	OR   R30,R26
	OR   R31,R27
	MOVW R16,R30
; 0000 0580     return(Pointer);
	MOVW R30,R16
	LD   R16,Y+
	LD   R17,Y+
	RET
; 0000 0581 }
;
;
;
;
;
;
;
;//********************************  Write End Pointer in RTCC_Sram *************************
;void W_Flash_Pointer_End(unsigned int pointer)
; 0000 058B {
_W_Flash_Pointer_End:
; 0000 058C     write_i2c_RTCC(53,((pointer>>8)&0xFF),_RTCC);
	ST   -Y,R27
	ST   -Y,R26
;	pointer -> Y+0
	LDI  R30,LOW(53)
	LDI  R31,HIGH(53)
	ST   -Y,R31
	ST   -Y,R30
	LDD  R30,Y+3
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _write_i2c_RTCC
; 0000 058D     write_i2c_RTCC(54,(pointer&0xFF),_RTCC);
	LDI  R30,LOW(54)
	LDI  R31,HIGH(54)
_0x2060005:
	ST   -Y,R31
	ST   -Y,R30
	LDD  R30,Y+2
	ST   -Y,R30
	LDI  R26,LOW(208)
	RCALL _write_i2c_RTCC
; 0000 058E }
_0x2060006:
	ADIW R28,2
	RET
;
;
;
;
;
;
;
;
;
;
;//********************************  Shift Data in RTCC_Sram (one event)*************************
;void Shift_RTCC_SRAM_Data()
; 0000 059B {
_Shift_RTCC_SRAM_Data:
; 0000 059C  unsigned char tmp,Tmp_Buff[36];
; 0000 059D 
; 0000 059E     //Read
; 0000 059F     start();
	SBIW R28,36
	ST   -Y,R16
;	tmp -> R16
;	Tmp_Buff -> Y+1
	RCALL _start
; 0000 05A0     send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	RCALL _send_I2C
; 0000 05A1     send_I2C(14);
	LDI  R26,LOW(14)
	LDI  R27,0
	RCALL _send_I2C
; 0000 05A2     start();
	RCALL _start
; 0000 05A3     send_I2C(_RTCC|1);
	LDI  R26,LOW(209)
	LDI  R27,0
	RCALL _send_I2C
; 0000 05A4     for (tmp=0;tmp<36;tmp++)
	LDI  R16,LOW(0)
_0x118:
	CPI  R16,36
	BRSH _0x119
; 0000 05A5         {
; 0000 05A6         Tmp_Buff[tmp]=read_I2C(1);
	MOV  R30,R16
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,1
	ADD  R30,R26
	ADC  R31,R27
	PUSH R31
	PUSH R30
	LDI  R26,LOW(1)
	RCALL _read_I2C
	POP  R26
	POP  R27
	ST   X,R30
; 0000 05A7         #asm("wdr");
	wdr
; 0000 05A8         }
	SUBI R16,-1
	RJMP _0x118
_0x119:
; 0000 05A9     read_I2C(0);
	LDI  R26,LOW(0)
	RCALL _read_I2C
; 0000 05AA     stop();
	RCALL _stop
; 0000 05AB 
; 0000 05AC 
; 0000 05AD     //Write
; 0000 05AE     start();
	RCALL _start
; 0000 05AF     send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	RCALL _send_I2C
; 0000 05B0     send_I2C(8);
	LDI  R26,LOW(8)
	LDI  R27,0
	RCALL _send_I2C
; 0000 05B1     for (tmp=0;tmp<36;tmp++)
	LDI  R16,LOW(0)
_0x11B:
	CPI  R16,36
	BRSH _0x11C
; 0000 05B2         {
; 0000 05B3         send_I2C(Tmp_Buff[tmp]);
	MOV  R30,R16
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,1
	ADD  R26,R30
	ADC  R27,R31
	LD   R26,X
	CLR  R27
	RCALL _send_I2C
; 0000 05B4         #asm("wdr");
	wdr
; 0000 05B5         }
	SUBI R16,-1
	RJMP _0x11B
_0x11C:
; 0000 05B6     stop();
	RCALL _stop
; 0000 05B7 }
	LDD  R16,Y+0
	ADIW R28,37
	RET
;
;
;
;
;
;
;
;
;//******************************** DEBUG **********************************
;#ifdef  _DEBUG_
;
;#define _38400
;
; #asm
; .equ           PINE    =0x01             ;PINE=0x01
; .equ           RxD     =4                ;PINE.4      //eint4
; .equ           PORTE   =0x03             ;PORTE=0x03;
; .equ           Txd     =5                ;PORTE.5
; .equ           sb      =1                ;Number of stop bits (1, 2, ...)
; .def           temp    =R27
; .def           bitcnt  =R26              ;bit counter
; #endasm
;
;#ifdef _38400
;                #asm
;                .equ	 b=30   ;7.3728MHz => 38840=30
;                #endasm
;#else
;                #asm
;                .equ	 b=128  ;7.3728MHz => 9.6K=128
;                #endasm
;#endif
;
;
;register unsigned char reg_dat  @8;
;
;//***************************** DEBUG PUTCHAR **********************************
;void debug_putchar(unsigned char dat_tx)
; {
;                DDRE.5=1;
;
;                #asm("cli");
;                reg_dat=dat_tx;
;
;#asm            cli
;			    ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
;        		com	R8		;Inverte everything
;        		sec			;Start bit
;
;putchar0:   	brcc	putchar1	;If carry set
;		        cbi	    PORTE,TxD	;send a '0'
;		        rjmp	putchar2	;else
;
;putchar1:	    sbi	    PORTE,TxD	;    send a '1'
;		        nop
;
;putchar2:	    ldi	    temp,b
;UART_delay1:	dec	    temp
;		        brne	UART_delay1
;
;        	    ldi	    temp,b
;UART_delay2:	dec 	temp
;		        brne	UART_delay2
;
;        		lsr	R8		;Get next bit
;        		dec	    bitcnt		;If not all bit sent
;        		brne	putchar0	;   send next
;        		sei
; #endasm
; };
;
;#endif
;
;#include <mega64a.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.EQU __sm_mask=0x1C
	.EQU __sm_powerdown=0x10
	.EQU __sm_powersave=0x18
	.EQU __sm_standby=0x14
	.EQU __sm_ext_standby=0x1C
	.EQU __sm_adc_noise_red=0x08
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;#include "I2C.h"
;
;
;
;// ********** I2C *************
;#define SDA         DDRD.1
;#define SCL         DDRD.0
;#define iSDA        PIND.1
;
;#define i2c_time    6
;
;
;
;struct Rtc_ {
;            unsigned char hour,min,sec,month,date,year;
;            unsigned char New_hour,New_min,New_sec;
;            }Rtc;
;
; //**************************************************  I2C Write
;unsigned char send_I2C(unsigned data)
; 0001 0018  {

	.CSEG
_send_I2C:
; 0001 0019   unsigned char i;
; 0001 001A    for (i=0;i<8;i++)
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R16
;	data -> Y+1
;	i -> R16
	LDI  R16,LOW(0)
_0x20004:
	CPI  R16,8
	BRSH _0x20005
; 0001 001B    {
; 0001 001C      if (data&0x80) SDA=0;
	LDD  R30,Y+1
	ANDI R30,LOW(0x80)
	BREQ _0x20006
	CBI  0x11,1
; 0001 001D        else   SDA=1;
	RJMP _0x20009
_0x20006:
	SBI  0x11,1
; 0001 001E       SCL=0;
_0x20009:
	CBI  0x11,0
; 0001 001F       delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0020       data<<=1;
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	LSL  R30
	ROL  R31
	STD  Y+1,R30
	STD  Y+1+1,R31
; 0001 0021       SCL=1;
	SBI  0x11,0
; 0001 0022       delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0023      }
	SUBI R16,-1
	RJMP _0x20004
_0x20005:
; 0001 0024       SCL=0;
	CBI  0x11,0
; 0001 0025       SDA=0;
	CBI  0x11,1
; 0001 0026       delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0027       i=iSDA;
	LDI  R30,0
	SBIC 0x10,1
	LDI  R30,1
	MOV  R16,R30
; 0001 0028       SCL=1;
	SBI  0x11,0
; 0001 0029       return(i);
	MOV  R30,R16
	RJMP _0x2060004
; 0001 002A  };
;
;
;
;//************************************************** I2C Read
;unsigned char read_I2C(unsigned char ack)
; 0001 0030  {
_read_I2C:
; 0001 0031   unsigned char i,dat=0;
; 0001 0032       SDA=0;
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	ack -> Y+2
;	i -> R16
;	dat -> R17
	LDI  R17,0
	CBI  0x11,1
; 0001 0033    for (i=0;i<8;i++)
	LDI  R16,LOW(0)
_0x20019:
	CPI  R16,8
	BRSH _0x2001A
; 0001 0034    {
; 0001 0035       SCL=0;
	CBI  0x11,0
; 0001 0036       delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0037       dat<<=1;
	LSL  R17
; 0001 0038       if (iSDA==1) dat|=1;
	SBIC 0x10,1
	ORI  R17,LOW(1)
; 0001 0039       SCL=1;
	SBI  0x11,0
; 0001 003A       delay_us(i2c_time);
	__DELAY_USB 15
; 0001 003B      }
	SUBI R16,-1
	RJMP _0x20019
_0x2001A:
; 0001 003C 
; 0001 003D      if (ack==1) SDA=1;
	LDD  R26,Y+2
	CPI  R26,LOW(0x1)
	BRNE _0x20020
	SBI  0x11,1
; 0001 003E      else SDA=0;
	RJMP _0x20023
_0x20020:
	CBI  0x11,1
; 0001 003F      SCL=0;
_0x20023:
	CBI  0x11,0
; 0001 0040      delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0041      SCL=1;
	SBI  0x11,0
; 0001 0042      SDA=0;
	CBI  0x11,1
; 0001 0043      return(dat);
	MOV  R30,R17
	LDD  R17,Y+1
_0x2060004:
	LDD  R16,Y+0
	ADIW R28,3
	RET
; 0001 0044  }
;
;
;
;
;//***************************************** I2C Start ********************************
;void start(void)        {
; 0001 004A void start(void)        {
_start:
; 0001 004B                         SDA=0;
	CBI  0x11,1
; 0001 004C                         delay_us(i2c_time);
	__DELAY_USB 15
; 0001 004D                         SCL=0;delay_us(i2c_time);
	CBI  0x11,0
	__DELAY_USB 15
; 0001 004E                         SDA=1;
	SBI  0x11,1
; 0001 004F                         delay_us(i2c_time);
	RJMP _0x2060003
; 0001 0050                         SCL=1;
; 0001 0051                         delay_us(i2c_time);
; 0001 0052                         };
;
;
;
;//***************************************** I2C Stop ********************************
;void stop(void)         {
; 0001 0057 void stop(void)         {
_stop:
; 0001 0058                         SDA=1;
	SBI  0x11,1
; 0001 0059                         delay_us(i2c_time);
	__DELAY_USB 15
; 0001 005A                         SCL=0;
	CBI  0x11,0
; 0001 005B                         delay_us(i2c_time);
	__DELAY_USB 15
; 0001 005C                         SDA=0;
	CBI  0x11,1
; 0001 005D                         delay_us(i2c_time);
_0x2060003:
	__DELAY_USB 15
; 0001 005E                         SCL=1;
	SBI  0x11,0
; 0001 005F                         delay_us(i2c_time);
	__DELAY_USB 15
; 0001 0060                         };
	RET
;
;
;
;//************************************* WRITE BYTE I2C *************************************
;void write_i2c_RTCC(unsigned int address,unsigned char data,unsigned char device)
; 0001 0066 {
_write_i2c_RTCC:
; 0001 0067         start();
	ST   -Y,R26
;	address -> Y+2
;	data -> Y+1
;	device -> Y+0
	RCALL _start
; 0001 0068         send_I2C(device);
	LD   R26,Y
	CLR  R27
	RCALL _send_I2C
; 0001 0069         send_I2C((address&0xFF));
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0001 006A         send_I2C(data);
	LDD  R26,Y+1
	CLR  R27
	RCALL _send_I2C
; 0001 006B         stop();
	RCALL _stop
; 0001 006C };
	RJMP _0x2060002
;
;
;
;
;//************************************* READ BYTE  I2C *************************************
;unsigned char read_i2c_RTCC(unsigned int address,unsigned char device)
; 0001 0073 {
_read_i2c_RTCC:
; 0001 0074 unsigned char data;
; 0001 0075 
; 0001 0076         start();
	ST   -Y,R26
	ST   -Y,R16
;	address -> Y+2
;	device -> Y+1
;	data -> R16
	RCALL _start
; 0001 0077         send_I2C(device);
	LDD  R26,Y+1
	CLR  R27
	RCALL _send_I2C
; 0001 0078         send_I2C(address&0xFF);
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0001 0079         start();
	RCALL _start
; 0001 007A         send_I2C(device|1);
	LDD  R30,Y+1
	ORI  R30,1
	LDI  R31,0
	MOVW R26,R30
	RCALL _send_I2C
; 0001 007B         data=read_I2C(0);
	LDI  R26,LOW(0)
	RCALL _read_I2C
	MOV  R16,R30
; 0001 007C         stop();
	RCALL _stop
; 0001 007D         return (data);
	MOV  R30,R16
	LDD  R16,Y+0
	RJMP _0x2060002
; 0001 007E };
;
;
;
;//************************************* WRITE BYTE I2C *************************************
;void write_i2c_EEPROM(unsigned int address,unsigned char data,unsigned char device)
; 0001 0084 {
_write_i2c_EEPROM:
; 0001 0085         start();
	ST   -Y,R26
;	address -> Y+2
;	data -> Y+1
;	device -> Y+0
	RCALL _start
; 0001 0086         send_I2C(device);
	LD   R26,Y
	CLR  R27
	RCALL _send_I2C
; 0001 0087         send_I2C(((address>>8)&0xFF));
	LDD  R30,Y+3
	ANDI R31,HIGH(0x0)
	MOVW R26,R30
	RCALL _send_I2C
; 0001 0088         send_I2C((address&0xFF));
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	ANDI R31,HIGH(0xFF)
	MOVW R26,R30
	RCALL _send_I2C
; 0001 0089         send_I2C(data);
	LDD  R26,Y+1
	CLR  R27
	RCALL _send_I2C
; 0001 008A         stop();
	RCALL _stop
; 0001 008B         delay_ms(5);
	LDI  R26,LOW(5)
	LDI  R27,0
	CALL _delay_ms
; 0001 008C };
_0x2060002:
	ADIW R28,4
	RET
;
;
;
;
;//************************************* READ BYTE  I2C *************************************
;unsigned char read_i2c_EEPROM(unsigned int address,unsigned char device)
; 0001 0093 {
; 0001 0094 unsigned char data;
; 0001 0095 
; 0001 0096         start();
;	address -> Y+2
;	device -> Y+1
;	data -> R16
; 0001 0097         send_I2C(device);
; 0001 0098         send_I2C(((address>>8)&0xFF));
; 0001 0099         send_I2C(address&0xFF);
; 0001 009A         start();
; 0001 009B         send_I2C(device|1);
; 0001 009C         data=read_I2C(0);
; 0001 009D         stop();
; 0001 009E         return (data);
; 0001 009F };
;
;
;
;
;
;//******************* READ TIME and Date *******************
;void Read_Date_Time(unsigned char Date)
; 0001 00A7 {
_Read_Date_Time:
; 0001 00A8     start();
	ST   -Y,R26
;	Date -> Y+0
	RCALL _start
; 0001 00A9     send_I2C(_RTCC);
	LDI  R26,LOW(208)
	LDI  R27,0
	RCALL _send_I2C
; 0001 00AA     send_I2C(0);
	LDI  R26,LOW(0)
	LDI  R27,0
	RCALL _send_I2C
; 0001 00AB     start();
	RCALL _start
; 0001 00AC     send_I2C(_RTCC|1);
	LDI  R26,LOW(209)
	LDI  R27,0
	RCALL _send_I2C
; 0001 00AD 
; 0001 00AE     Rtc.sec=read_I2C(1);   //0
	LDI  R26,LOW(1)
	RCALL _read_I2C
	__PUTB1MN _Rtc,2
; 0001 00AF     Rtc.min=read_I2C(1);   //1
	LDI  R26,LOW(1)
	RCALL _read_I2C
	__PUTB1MN _Rtc,1
; 0001 00B0     Rtc.hour=read_I2C(1);  //2
	LDI  R26,LOW(1)
	RCALL _read_I2C
	STS  _Rtc,R30
; 0001 00B1 
; 0001 00B2     if (Date)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x2003C
; 0001 00B3      {
; 0001 00B4      read_I2C(1);            //3
	LDI  R26,LOW(1)
	RCALL _read_I2C
; 0001 00B5 
; 0001 00B6      Rtc.date=read_I2C(1);   //4
	LDI  R26,LOW(1)
	RCALL _read_I2C
	__PUTB1MN _Rtc,4
; 0001 00B7      Rtc.month=read_I2C(1);  //5
	LDI  R26,LOW(1)
	RCALL _read_I2C
	__PUTB1MN _Rtc,3
; 0001 00B8      Rtc.year=read_I2C(1);   //6
	LDI  R26,LOW(1)
	RCALL _read_I2C
	__PUTB1MN _Rtc,5
; 0001 00B9      }
; 0001 00BA 
; 0001 00BB     read_I2C(0);
_0x2003C:
	LDI  R26,LOW(0)
	RCALL _read_I2C
; 0001 00BC     stop();
	RCALL _stop
; 0001 00BD }
_0x2060001:
	ADIW R28,1
	RET
;
;
;
;//************************* NEW TIME *********************
;void New_Time(void)
; 0001 00C3         {
; 0001 00C4         write_i2c_RTCC(0,Rtc.sec,_RTCC);
; 0001 00C5         write_i2c_RTCC(1,Rtc.min,_RTCC);
; 0001 00C6         write_i2c_RTCC(2,Rtc.hour,_RTCC);
; 0001 00C7         };
;
;
;
;
;//************************* NEW DATE *********************
;void New_Date(void)
; 0001 00CE         {
; 0001 00CF         write_i2c_RTCC(4,Rtc.date,_RTCC);
; 0001 00D0         write_i2c_RTCC(5,Rtc.month,_RTCC);
; 0001 00D1         write_i2c_RTCC(6,Rtc.year,_RTCC);
; 0001 00D2         }
;
;
;
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.EQU __sm_mask=0x1C
	.EQU __sm_powerdown=0x10
	.EQU __sm_powersave=0x18
	.EQU __sm_standby=0x14
	.EQU __sm_ext_standby=0x1C
	.EQU __sm_adc_noise_red=0x08
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG

	.CSEG

	.CSEG

	.DSEG
_ModeSelect:
	.BYTE 0x1
_TM:
	.BYTE 0x1
_rx_buffer0:
	.BYTE 0x20
_Buffer:
	.BYTE 0x8
_Vault_Light_G000:
	.BYTE 0x8
_Timers:
	.BYTE 0x1A
_Rtc:
	.BYTE 0x9
_RS232:
	.BYTE 0x4
_RS485:
	.BYTE 0x5
_Vault:
	.BYTE 0x145

	.ESEG
_PrepareEEPROM:
	.DB  0x0,0x0
	.DB  0x0

	.DSEG
_rx_buffer1:
	.BYTE 0x10
_tx_buffer0:
	.BYTE 0x20
_tx_buffer1:
	.BYTE 0x10

	.CSEG

	.CSEG
_delay_ms:
	adiw r26,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x733
	wdr
	sbiw r26,1
	brne __delay_ms0
__delay_ms1:
	ret

__LSLW4:
	LSL  R30
	ROL  R31
__LSLW3:
	LSL  R30
	ROL  R31
__LSLW2:
	LSL  R30
	ROL  R31
	LSL  R30
	ROL  R31
	RET

__MULW12U:
	MUL  R31,R26
	MOV  R31,R0
	MUL  R30,R27
	ADD  R31,R0
	MUL  R30,R26
	MOV  R30,R0
	ADD  R31,R1
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

__CPW02:
	CLR  R0
	CP   R0,R26
	CPC  R0,R27
	RET

__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
