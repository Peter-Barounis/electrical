
#include <mega64a.h>
#include <delay.h>
#include "I2C.h"



// ********** I2C *************
#define SDA         DDRD.1
#define SCL         DDRD.0
#define iSDA        PIND.1

#define i2c_time    6



struct Rtc_ {
            unsigned char hour,min,sec,month,date,year;
            unsigned char New_hour,New_min,New_sec;
            }Rtc;

 //**************************************************  I2C Write
unsigned char send_I2C(unsigned data)
 {
  unsigned char i;
   for (i=0;i<8;i++)
   {
     if (data&0x80) SDA=0;
       else   SDA=1;
      SCL=0;
      delay_us(i2c_time);
      data<<=1;
      SCL=1;
      delay_us(i2c_time);
     }
      SCL=0;
      SDA=0;
      delay_us(i2c_time);
      i=iSDA;
      SCL=1;
      return(i);
 };



//************************************************** I2C Read
unsigned char read_I2C(unsigned char ack)
 {
  unsigned char i,dat=0;
      SDA=0;
   for (i=0;i<8;i++)
   {
      SCL=0;
      delay_us(i2c_time);
      dat<<=1;
      if (iSDA==1) dat|=1;
      SCL=1;
      delay_us(i2c_time);
     }

     if (ack==1) SDA=1;
     else SDA=0;
     SCL=0;
     delay_us(i2c_time);
     SCL=1;
     SDA=0;
     return(dat);
 }




//***************************************** I2C Start ********************************
void start(void)        {
                        SDA=0;
                        delay_us(i2c_time);
                        SCL=0;delay_us(i2c_time);
                        SDA=1;
                        delay_us(i2c_time);
                        SCL=1;
                        delay_us(i2c_time);
                        };



//***************************************** I2C Stop ********************************
void stop(void)         {
                        SDA=1;
                        delay_us(i2c_time);
                        SCL=0;
                        delay_us(i2c_time);
                        SDA=0;
                        delay_us(i2c_time);
                        SCL=1;
                        delay_us(i2c_time);
                        };



//************************************* WRITE BYTE I2C *************************************
void write_i2c_RTCC(unsigned int address,unsigned char data,unsigned char device)
{
        start();
        send_I2C(device);
        send_I2C((address&0xFF));
        send_I2C(data);
        stop();
};




//************************************* READ BYTE  I2C *************************************
unsigned char read_i2c_RTCC(unsigned int address,unsigned char device)
{
unsigned char data;

        start();
        send_I2C(device);
        send_I2C(address&0xFF);
        start();
        send_I2C(device|1);
        data=read_I2C(0);
        stop();
        return (data);
};



//************************************* WRITE BYTE I2C *************************************
void write_i2c_EEPROM(unsigned int address,unsigned char data,unsigned char device)
{
        start();
        send_I2C(device);
        send_I2C(((address>>8)&0xFF));
        send_I2C((address&0xFF));
        send_I2C(data);
        stop();
        delay_ms(5);
};




//************************************* READ BYTE  I2C *************************************
unsigned char read_i2c_EEPROM(unsigned int address,unsigned char device)
{
unsigned char data;

        start();
        send_I2C(device);
        send_I2C(((address>>8)&0xFF));
        send_I2C(address&0xFF);
        start();
        send_I2C(device|1);
        data=read_I2C(0);
        stop();
        return (data);
};





//******************* READ TIME and Date *******************
void Read_Date_Time(unsigned char Date)
{
    start();
    send_I2C(_RTCC);
    send_I2C(0);
    start();
    send_I2C(_RTCC|1);

    Rtc.sec=read_I2C(1);   //0
    Rtc.min=read_I2C(1);   //1
    Rtc.hour=read_I2C(1);  //2

    if (Date)
     {
     read_I2C(1);            //3

     Rtc.date=read_I2C(1);   //4
     Rtc.month=read_I2C(1);  //5
     Rtc.year=read_I2C(1);   //6
     }

    read_I2C(0);
    stop();
}



//************************* NEW TIME *********************
void New_Time(void)
        {
        write_i2c_RTCC(0,Rtc.sec,_RTCC);
        write_i2c_RTCC(1,Rtc.min,_RTCC);
        write_i2c_RTCC(2,Rtc.hour,_RTCC);
        };




//************************* NEW DATE *********************
void New_Date(void)
        {
        write_i2c_RTCC(4,Rtc.date,_RTCC);
        write_i2c_RTCC(5,Rtc.month,_RTCC);
        write_i2c_RTCC(6,Rtc.year,_RTCC);
        }



