
;CodeVisionAVR C Compiler V2.05.3a Standard
;(C) Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega64
;Program type             : Boot Loader
;Clock frequency          : 7.372800 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 1024 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : Yes
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;Global 'const' stored in FLASH     : Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions         : On
;Smart register allocation          : On
;Automatic register allocation      : On

	#pragma AVRPART ADMIN PART_NAME ATmega64
	#pragma AVRPART MEMORY PROG_FLASH 65536
	#pragma AVRPART MEMORY EEPROM 2048
	#pragma AVRPART MEMORY INT_SRAM SIZE 4351
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU XMCRA=0x6D
	.EQU XMCRB=0x6C

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0100
	.EQU __SRAM_END=0x10FF
	.EQU __DSTACK_SIZE=0x0400
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _j=R13
	.DEF _i=R11
	.DEF _spmcrval=R10
	.DEF _CurrentAddress=R6
	.DEF _PageAddress=R4
	.DEF _PageData=R2
	.DEF _reg_dat=R8
	.DEF _led_blink=R9

	.CSEG
	.ORG 0x7000

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  _ext_int4_isr
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  _timer1_ovf_isr
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  0x7000
	JMP  _usart0_rx_isr
	JMP  0x7000
	JMP  _usart0_tx_isr
	JMP  0x7000
	JMP  0x7000

;REGISTER BIT VARIABLES INITIALIZATION
__REG_BIT_VARS:
	.DW  0x0003

_0x3:
	.DB  0x0,0x0
_0x0:
	.DB  0x50,0x72,0x65,0x73,0x73,0x20,0x5B,0x44
	.DB  0x5D,0x2D,0x4B,0x65,0x79,0x20,0x66,0x6F
	.DB  0x72,0x20,0x44,0x69,0x61,0x67,0x6E,0x6F
	.DB  0x73,0x74,0x69,0x63,0x20,0x4D,0x65,0x6E
	.DB  0x75,0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  0x02
	.DW  __REG_BIT_VARS*2

	.DW  0x02
	.DW  _program_address
	.DW  _0x3*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF THE BOOT LOADER
	LDI  R31,1
	OUT  MCUCR,R31
	LDI  R31,2
	OUT  MCUCR,R31
	STS  XMCRB,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x500

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.03.9 Standard
;Automatic Program Generator
;� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project : 1.00
;Version :
;Date    : 09/19/2011
;Author  : Sasa Vukovic
;Company : GFi Genfare a unit of SPX Corp.
;Comments:
;
;
;Chip type               : ATmega64
;Program type            : Boot Loader - Size:4096words
;AVR Core Clock frequency: 7.372800 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 1024
;*****************************************************/
;
;#include <mega64.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.EQU __sm_mask=0x1C
	.EQU __sm_powerdown=0x10
	.EQU __sm_powersave=0x18
	.EQU __sm_standby=0x14
	.EQU __sm_ext_standby=0x1C
	.EQU __sm_adc_noise_red=0x08
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;
;
;
;#define _38400      //38400 debug port speed if is not define speed is 9600
;
;
;
;
;#define RXB8 1
;#define TXB8 0
;#define UPE 2
;#define OVR 3
;#define FE 4
;#define UDRE 5
;#define RXC 7
;
;#define STX 0x02
;#define ETX 0x03
;#define ENQ 0x05
;#define ACK 0x06
;#define NAK 0x15
;
;
;
;#define FRAMING_ERROR (1<<FE)
;#define PARITY_ERROR (1<<UPE)
;#define DATA_OVERRUN (1<<OVR)
;#define DATA_REGISTER_EMPTY (1<<UDRE)
;#define RX_COMPLETE (1<<RXC)
;
;
;
;#define boot_select         PINA.0
;#define DE                  PORTC.7
;
;#define sys_led             PORTD.4
;#define err_led             PORTD.5
;
;#define Led_Power           PORTD.6
;#define Service             PORTD.7
;
;#define link_led1           PORTC.5
;#define link_led2           PORTC.6
;
;
;
;#define clr_wdt()       #asm("wdr")
;
;
;
;#pragma warn-
;eeprom unsigned char secure[]="000";
;#pragma warn+
;
;
;#pragma regalloc-
;       //for flasher
;register unsigned int j @13;
;register unsigned int i @11;
;register unsigned char spmcrval @10;
;register unsigned int CurrentAddress @6;
;register unsigned int PageAddress  @4;
;register unsigned int PageData @2;
;register unsigned char reg_dat  @8;
;#pragma regalloc+
;
;
;
;
;
;
;bit D_RxD=1,done_page=1;
;unsigned int program_address=0;

	.DSEG
;unsigned char led_blink,data_counter,chksum,lenght,record_type,buffer_counter,bin_data,debug_RxD_buff;
;unsigned char data_buffer[66];
;
;
;
;
;
;
;
;unsigned char write_flash(unsigned char page_byte,unsigned int page_address);
;unsigned char HexToBin(unsigned char dat);
;void putchar(char dat);
;unsigned char debug_getchar(void);
;void debug_putchar(unsigned char dat_tx);
;void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color);
;
;
;
;
; #asm
 .equ           PINE    =0x01             ;PINE=0x01
 .equ           RxD     =4                ;PINE.4      //eint4
 .equ           PORTE   =0x03             ;PORTE=0x03;
 .equ           Txd     =5                ;PORTE.5
 .equ           sb      =1                ;Number of stop bits (1, 2, ...)
 .def           temp    =R27
 .def           bitcnt  =R26              ;bit counter
; 0000 007E  #endasm
;
;#ifdef _38400
;                #asm
                .equ	 b=30   ;7.3728MHz => 38840=30
; 0000 0083                 #endasm
;#else
;                #asm
;                .equ	 b=128  ;7.3728MHz => 9.6K=128
;                #endasm
;#endif
;
;
;
;
;//******************External Interrupt 4 service routine  for (software)debug serial port Rxd_debug ************************
;interrupt [EXT_INT4] void ext_int4_isr(void)
; 0000 008F {

	.CSEG
_ext_int4_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
; 0000 0090  unsigned char dat;
; 0000 0091 
; 0000 0092 
; 0000 0093                 #asm
	ST   -Y,R17
;	dat -> R17
; 0000 0094                 cli
                cli
; 0000 0095                 ldi     bitcnt,9            ;8 data bit + 1 stop bit
                ldi     bitcnt,9            ;8 data bit + 1 stop bit
; 0000 0096                 #endasm
; 0000 0097 
; 0000 0098 #ifdef _38400
; 0000 0099                 {
; 0000 009A                 #asm
; 0000 009B                 ldi	    temp,20            ;38400=10
                ldi	    temp,20            ;38400=10
; 0000 009C                 #endasm
; 0000 009D                 }
; 0000 009E #else
; 0000 009F                 {
; 0000 00A0                 #asm
; 0000 00A1                 ldi	    temp,108            ;9600=20
; 0000 00A2                 #endasm
; 0000 00A3                 }
; 0000 00A4 #endif
; 0000 00A5 
; 0000 00A6                 #asm
; 0000 00A7 UART_delay00:    dec    temp
UART_delay00:    dec    temp
; 0000 00A8                 brne    UART_delay00        ;0.5 bit delay
                brne    UART_delay00        ;0.5 bit delay
; 0000 00A9 

; 0000 00AA getchar2:       ldi     temp,b
getchar2:       ldi     temp,b
; 0000 00AB UART_delay11:   dec     temp
UART_delay11:   dec     temp
; 0000 00AC                 brne    UART_delay11
                brne    UART_delay11
; 0000 00AD 

; 0000 00AE                 ldi     temp,b
                ldi     temp,b
; 0000 00AF UART_delay22:    dec    temp
UART_delay22:    dec    temp
; 0000 00B0                 brne    UART_delay22
                brne    UART_delay22
; 0000 00B1 

; 0000 00B2                 clc                         ;clear carry
                clc                         ;clear carry
; 0000 00B3                 sbic     PINE,RxD           ;if RX pin high
                sbic     PINE,RxD           ;if RX pin high
; 0000 00B4                 sec                         ;
                sec                         ;
; 0000 00B5 

; 0000 00B6                 dec      bitcnt             ;If bit is stop bit
                dec      bitcnt             ;If bit is stop bit
; 0000 00B7                 breq     getchar3           ;   return
                breq     getchar3           ;   return
; 0000 00B8                                             ;else
                                            ;else
; 0000 00B9                 ror      R8                 ;shift bit into Rxbyte
                ror      R8                 ;shift bit into Rxbyte
; 0000 00BA                 rjmp     getchar2           ;go get next
                rjmp     getchar2           ;go get next
; 0000 00BB getchar3:
getchar3:
; 0000 00BC #endasm
; 0000 00BD 
; 0000 00BE                 dat=reg_dat;
	MOV  R17,R8
; 0000 00BF                 debug_RxD_buff=dat;
	STS  _debug_RxD_buff,R17
; 0000 00C0                 D_RxD=1;
	SET
	BLD  R2,0
; 0000 00C1                 EIFR=4;
	LDI  R30,LOW(4)
	OUT  0x38,R30
; 0000 00C2                 #asm("sei")
	sei
; 0000 00C3 };
	LD   R17,Y+
	RJMP _0x86
;
;
;
;
;
;
;
;//********************************* PUTCHAR **********************************
;//****************************************************************************
;void putchar(char dat)
; 0000 00CE {
_putchar:
; 0000 00CF     //delay_ms(10);
; 0000 00D0     //DE=1;
; 0000 00D1     while (!( UCSR1A & (1<<UDRE))) clr_wdt();
	ST   -Y,R26
;	dat -> Y+0
_0x4:
	LDS  R30,155
	ANDI R30,LOW(0x20)
	BRNE _0x6
	wdr
; 0000 00D2     UDR1=dat;
	RJMP _0x4
_0x6:
	LD   R30,Y
	STS  156,R30
; 0000 00D3 }
	JMP  _0x2000001
;
;
;
;
;
;
;
;
;//********************************************** RxD uart0 interrupt ********************************************
;//***************************************************************************************************************
; interrupt [USART1_RXC] void usart0_rx_isr(void)
; 0000 00DF {
_usart0_rx_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0000 00E0  char data;
; 0000 00E1 
; 0000 00E2  data=UDR1;
	ST   -Y,R17
;	data -> R17
	LDS  R17,156
; 0000 00E3  if ((UCSR1A & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	LDS  R30,155
	ANDI R30,LOW(0x1C)
	BREQ PC+3
	JMP _0x7
; 0000 00E4  {
; 0000 00E5  ++data_counter;
	LDS  R30,_data_counter
	SUBI R30,-LOW(1)
	STS  _data_counter,R30
; 0000 00E6        if (data==':') {done_page=0;data_counter=0,chksum=0;}
	CPI  R17,58
	BRNE _0x8
	CLT
	BLD  R2,1
	LDI  R30,LOW(0)
	STS  _data_counter,R30
	STS  _chksum,R30
; 0000 00E7         else    if (done_page==0){
	RJMP _0x9
_0x8:
	SBRC R2,1
	RJMP _0xA
; 0000 00E8                                 if (data_counter<=8) {
	LDS  R26,_data_counter
	CPI  R26,LOW(0x9)
	BRLO PC+3
	JMP _0xB
; 0000 00E9 
; 0000 00EA                                                    data=HexToBin(data);
	MOV  R26,R17
	RCALL _HexToBin
	MOV  R17,R30
; 0000 00EB                                                    if ((data&0xFF)==0xFF) putchar('E');
	MOV  R30,R17
	CPI  R30,LOW(0xFF)
	BRNE _0xC
	LDI  R26,LOW(69)
	RCALL _putchar
; 0000 00EC                                                    switch (data_counter) {
_0xC:
	LDS  R30,_data_counter
	LDI  R31,0
; 0000 00ED                                                               case 1:lenght=data;break;
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	BRNE _0x10
	STS  _lenght,R17
	RJMP _0xF
; 0000 00EE                                                               case 2:{lenght=((lenght<<4)|(data&0x0F));chksum=lenght;break;}
_0x10:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BRNE _0x11
	LDS  R30,_lenght
	CALL SUBOPT_0x0
	STS  _lenght,R30
	RJMP _0x83
; 0000 00EF                                                               case 3:program_address=data;break;
_0x11:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BRNE _0x12
	CALL SUBOPT_0x1
	CALL SUBOPT_0x2
	RJMP _0xF
; 0000 00F0                                                               case 4:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
_0x12:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x13
	CALL SUBOPT_0x3
	CALL SUBOPT_0x4
	LDS  R30,_program_address
	RJMP _0x84
; 0000 00F1                                                               case 5:program_address=((program_address<<4)|(data&0x0F));break;
_0x13:
	CPI  R30,LOW(0x5)
	LDI  R26,HIGH(0x5)
	CPC  R31,R26
	BRNE _0x14
	CALL SUBOPT_0x3
	CALL SUBOPT_0x4
	RJMP _0xF
; 0000 00F2                                                               case 6:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
_0x14:
	CPI  R30,LOW(0x6)
	LDI  R26,HIGH(0x6)
	CPC  R31,R26
	BRNE _0x15
	CALL SUBOPT_0x3
	CALL SUBOPT_0x4
	LDS  R30,_program_address
	RJMP _0x84
; 0000 00F3                                                               case 7:record_type=data;break;
_0x15:
	CPI  R30,LOW(0x7)
	LDI  R26,HIGH(0x7)
	CPC  R31,R26
	BRNE _0x16
	STS  _record_type,R17
	RJMP _0xF
; 0000 00F4                                                               case 8:{record_type=(record_type<<4)|(data&0x0F);buffer_counter=0;chksum+=record_type;break;}
_0x16:
	CPI  R30,LOW(0x8)
	LDI  R26,HIGH(0x8)
	CPC  R31,R26
	BRNE _0xF
	LDS  R30,_record_type
	CALL SUBOPT_0x0
	STS  _record_type,R30
	LDI  R30,LOW(0)
	STS  _buffer_counter,R30
	LDS  R30,_record_type
_0x84:
	LDS  R26,_chksum
	ADD  R30,R26
_0x83:
	STS  _chksum,R30
; 0000 00F5                                                                           }// end switch
_0xF:
; 0000 00F6                                                          } //end if
; 0000 00F7                                       else    if ((data_counter&1)==1)  bin_data=HexToBin(data);
	RJMP _0x18
_0xB:
	LDS  R30,_data_counter
	ANDI R30,LOW(0x1)
	CPI  R30,LOW(0x1)
	BRNE _0x19
	MOV  R26,R17
	RCALL _HexToBin
	STS  _bin_data,R30
; 0000 00F8                                                    else {data=HexToBin(data);bin_data=((bin_data<<4)|(data&0x0F));chksum+=bin_data;data_buffer[buffer_counter++]=bin_data;}
	RJMP _0x1A
_0x19:
	MOV  R26,R17
	RCALL _HexToBin
	MOV  R17,R30
	LDS  R30,_bin_data
	CALL SUBOPT_0x0
	STS  _bin_data,R30
	LDS  R26,_chksum
	ADD  R30,R26
	STS  _chksum,R30
	LDS  R30,_buffer_counter
	SUBI R30,-LOW(1)
	STS  _buffer_counter,R30
	SUBI R30,LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_data_buffer)
	SBCI R31,HIGH(-_data_buffer)
	LDS  R26,_bin_data
	STD  Z+0,R26
_0x1A:
_0x18:
; 0000 00F9                                    }// end if
; 0000 00FA 
; 0000 00FB          if (((data_counter/2)-5==lenght)&&(done_page==0))  {
_0xA:
_0x9:
	LDS  R26,_data_counter
	LDI  R27,0
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	CALL __DIVW21
	SBIW R30,5
	MOVW R26,R30
	LDS  R30,_lenght
	LDI  R31,0
	CP   R30,R26
	CPC  R31,R27
	BRNE _0x1C
	LDI  R26,0
	SBRC R2,1
	LDI  R26,1
	CPI  R26,LOW(0x0)
	BREQ _0x1D
_0x1C:
	RJMP _0x1B
_0x1D:
; 0000 00FC                                                                 data_counter=0;
	LDI  R30,LOW(0)
	STS  _data_counter,R30
; 0000 00FD                                                                  if (chksum==0)  if  (record_type==1) {
	LDS  R30,_chksum
	CPI  R30,0
	BRNE _0x1E
	LDS  R26,_record_type
	CPI  R26,LOW(0x1)
	BRNE _0x1F
; 0000 00FE                                                                                                         putchar('D');
	LDI  R26,LOW(68)
	RCALL _putchar
; 0000 00FF                                                                                                         secure[0]=0xFF;
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	LDI  R30,LOW(255)
	CALL __EEPROMWRB
; 0000 0100                                                                                                         secure[1]=0xFF;
	__POINTW2MN _secure,1
	CALL __EEPROMWRB
; 0000 0101                                                                                                         secure[2]=0xFF;
	__POINTW2MN _secure,2
	CALL __EEPROMWRB
; 0000 0102                                                                                                         #asm ("cli")
	cli
; 0000 0103                                                                                                         WDTCR=0x18;
	LDI  R30,LOW(24)
	OUT  0x21,R30
; 0000 0104                                                                                                         WDTCR=0x08;
	LDI  R30,LOW(8)
	OUT  0x21,R30
; 0000 0105                                                                                                         loop:   goto loop;   //WAIT "WDT" HARDWARE RESET
_0x20:
	RJMP _0x20
; 0000 0106 
; 0000 0107                                                                                                         }
; 0000 0108                                                                                     else {
_0x1F:
; 0000 0109                                                                                           write_flash(lenght,(program_address));
	LDS  R30,_lenght
	ST   -Y,R30
	LDS  R26,_program_address
	LDS  R27,_program_address+1
	RCALL _write_flash
; 0000 010A                                                                                           done_page=1;
	SET
	BLD  R2,1
; 0000 010B                                                                                          }
; 0000 010C 
; 0000 010D                                                                     else {
	RJMP _0x22
_0x1E:
; 0000 010E                                                                             putchar('E');
	LDI  R26,LOW(69)
	RCALL _putchar
; 0000 010F                                                                             done_page=1;
	SET
	BLD  R2,1
; 0000 0110                                                                           }
_0x22:
; 0000 0111                                          }
; 0000 0112   }
_0x1B:
; 0000 0113 };
_0x7:
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;
;
;
;//***************************************** TxD Uart1 interrupt ***********************************
;//*************************************************************************************************
; interrupt [USART1_TXC] void usart0_tx_isr(void)
; 0000 011B {
_usart0_tx_isr:
; 0000 011C  DE=0;
	CBI  0x15,7
; 0000 011D };
	RETI
;
;
;
;
;// ************************** Timer 1 overflow interrupt service routine  0.01s ***********************
;interrupt [TIM1_OVF] void timer1_ovf_isr(void)
; 0000 0124 {
_timer1_ovf_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
; 0000 0125  TCNT1H=0xFF;
	LDI  R30,LOW(255)
	OUT  0x2D,R30
; 0000 0126  TCNT1L=0xB9;
	LDI  R30,LOW(185)
	OUT  0x2C,R30
; 0000 0127 
; 0000 0128   // diagnostic mode led-s
; 0000 0129                if ((++led_blink&16)==16) {
	INC  R9
	MOV  R30,R9
	ANDI R30,LOW(0x10)
	CPI  R30,LOW(0x10)
	BRNE _0x25
; 0000 012A                             sys_led=0;
	CBI  0x12,4
; 0000 012B                             err_led=1;
	SBI  0x12,5
; 0000 012C                             Led_Power=1;
	SBI  0x12,6
; 0000 012D                             Service=0;
	CBI  0x12,7
; 0000 012E                             link_led1=1;
	SBI  0x15,5
; 0000 012F                             link_led2=0;
	CBI  0x15,6
; 0000 0130                         }
; 0000 0131                          else {
	RJMP _0x32
_0x25:
; 0000 0132                                 sys_led=1;
	SBI  0x12,4
; 0000 0133                                 err_led=0;
	CBI  0x12,5
; 0000 0134                                 Led_Power=0;
	CBI  0x12,6
; 0000 0135                                 Service=1;
	SBI  0x12,7
; 0000 0136                                 link_led1=0;
	CBI  0x15,5
; 0000 0137                                 link_led2=1;
	SBI  0x15,6
; 0000 0138                               }
_0x32:
; 0000 0139  }
_0x86:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R30,Y+
	RETI
;
;
;
;
;
;
;void main(void)                    // BOOT LOADER START AT 0x3800
; 0000 0141     {
_main:
; 0000 0142     unsigned char tmp;
; 0000 0143 #asm ("cli");
;	tmp -> R17
	cli
; 0000 0144 // Watchdog Timer initialization
; 0000 0145 // Watchdog Timer Prescaler: OSC/1024k
; 0000 0146 // Watchdog Timer interrupt: Off
; 0000 0147 #pragma optsize-
; 0000 0148 #asm("wdr")
	wdr
; 0000 0149 WDTCR=0x1F;
	LDI  R30,LOW(31)
	OUT  0x21,R30
; 0000 014A WDTCR=0x0F;
	LDI  R30,LOW(15)
	OUT  0x21,R30
; 0000 014B #ifdef _OPTIMIZE_SIZE_
; 0000 014C #pragma optsize+
; 0000 014D #endif
; 0000 014E 
; 0000 014F 
; 0000 0150 // Input/Output Ports initialization
; 0000 0151 // Port A initialization
; 0000 0152 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 0153 // State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
; 0000 0154 PORTA=0xFF;
	LDI  R30,LOW(255)
	OUT  0x1B,R30
; 0000 0155 DDRA=0x00;
	LDI  R30,LOW(0)
	OUT  0x1A,R30
; 0000 0156 
; 0000 0157 delay_ms(1);
	LDI  R26,LOW(1)
	LDI  R27,0
	CALL _delay_ms
; 0000 0158 
; 0000 0159 if  ((boot_select)&&((secure[0]!=123)||(secure[1]!=212)||(secure[2]!=233)))  {
	SBIS 0x19,0
	RJMP _0x40
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	CALL __EEPROMRDB
	CPI  R30,LOW(0x7B)
	BRNE _0x41
	__POINTW2MN _secure,1
	CALL __EEPROMRDB
	CPI  R30,LOW(0xD4)
	BRNE _0x41
	__POINTW2MN _secure,2
	CALL __EEPROMRDB
	CPI  R30,LOW(0xE9)
	BREQ _0x40
_0x41:
	RJMP _0x43
_0x40:
	RJMP _0x3F
_0x43:
; 0000 015A                                                                             MCUCR =1;
	LDI  R30,LOW(1)
	OUT  0x35,R30
; 0000 015B                                                                             MCUCR =0;
	LDI  R30,LOW(0)
	OUT  0x35,R30
; 0000 015C                                                                             #asm
; 0000 015D                                                                             ldi R30,0x00
                                                                            ldi R30,0x00
; 0000 015E                                                                             ldi R31,0x00
                                                                            ldi R31,0x00
; 0000 015F                                                                             ijmp
                                                                            ijmp
; 0000 0160                                                                             #endasm
; 0000 0161                                                                             }
; 0000 0162 
; 0000 0163 // Port B initialization
; 0000 0164 // Func7=Out Func6=Out Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 0165 // State7=0 State6=0 State5=P State4=P State3=P State2=P State1=T State0=P
; 0000 0166 PORTB=0x3D;
_0x3F:
	LDI  R30,LOW(61)
	OUT  0x18,R30
; 0000 0167 DDRB=0xC0;
	LDI  R30,LOW(192)
	OUT  0x17,R30
; 0000 0168 
; 0000 0169 
; 0000 016A // Port C initialization
; 0000 016B // Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 016C // State7=0 State6=0 State5=0 State4=P State3=P State2=P State1=P State0=P
; 0000 016D PORTC=0x1F;
	LDI  R30,LOW(31)
	OUT  0x15,R30
; 0000 016E DDRC=0xE0;
	LDI  R30,LOW(224)
	OUT  0x14,R30
; 0000 016F 
; 0000 0170 
; 0000 0171 
; 0000 0172 // Port D initialization
; 0000 0173 // Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=In Func1=Out Func0=Out
; 0000 0174 // State7=0 State6=0 State5=0 State4=0 State3=1 State2=P State1=0 State0=0
; 0000 0175 PORTD=0x0C;
	LDI  R30,LOW(12)
	OUT  0x12,R30
; 0000 0176 DDRD=0xFB;
	LDI  R30,LOW(251)
	OUT  0x11,R30
; 0000 0177 
; 0000 0178 // Port E initialization
; 0000 0179 // Func7=In Func6=In Func5=Out Func4=In Func3=In Func2=Out Func1=Out Func0=In
; 0000 017A // State7=P State6=P State5=1 State4=P State3=P State2=1 State1=1 State0=P
; 0000 017B PORTE=0xFF;
	LDI  R30,LOW(255)
	OUT  0x3,R30
; 0000 017C DDRE=0x26;
	LDI  R30,LOW(38)
	OUT  0x2,R30
; 0000 017D 
; 0000 017E 
; 0000 017F // Port F initialization
; 0000 0180 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
; 0000 0181 // State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
; 0000 0182 PORTF=0xF0;
	LDI  R30,LOW(240)
	STS  98,R30
; 0000 0183 DDRF=0x00;
	LDI  R30,LOW(0)
	STS  97,R30
; 0000 0184 
; 0000 0185 
; 0000 0186 
; 0000 0187 // Port G initialization
; 0000 0188 // Func4=Out Func3=Out Func2=In Func1=In Func0=In
; 0000 0189 // State4=1 State3=1 State2=P State1=P State0=P
; 0000 018A PORTG=0x1F;
	LDI  R30,LOW(31)
	STS  101,R30
; 0000 018B DDRG=0x18;
	LDI  R30,LOW(24)
	STS  100,R30
; 0000 018C 
; 0000 018D 
; 0000 018E 
; 0000 018F // Timer/Counter 0 initialization
; 0000 0190 // Clock source: System Clock
; 0000 0191 // Clock value: Timer 0 Stopped
; 0000 0192 // Mode: Normal top=FFh
; 0000 0193 // OC0 output: Disconnected
; 0000 0194 ASSR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 0195 TCCR0=0x00;
	OUT  0x33,R30
; 0000 0196 TCNT0=0x00;
	OUT  0x32,R30
; 0000 0197 OCR0=0x00;
	OUT  0x31,R30
; 0000 0198 
; 0000 0199 
; 0000 019A 
; 0000 019B // Timer/Counter 1 initialization
; 0000 019C // Clock source: System Clock
; 0000 019D // Clock value: 7.200 kHz
; 0000 019E // Mode: Normal top=FFFFh
; 0000 019F // OC1A output: Discon.
; 0000 01A0 // OC1B output: Discon.
; 0000 01A1 // Noise Canceler: Off
; 0000 01A2 // Input Capture on Falling Edge
; 0000 01A3 // Timer 1 Overflow Interrupt: On
; 0000 01A4 // Input Capture Interrupt: Off
; 0000 01A5 // Compare A Match Interrupt: Off
; 0000 01A6 // Compare B Match Interrupt: Off
; 0000 01A7 TCCR1A=0x00;
	OUT  0x2F,R30
; 0000 01A8 TCCR1B=0x05;
	LDI  R30,LOW(5)
	OUT  0x2E,R30
; 0000 01A9 TCNT1H=0xFF;
	LDI  R30,LOW(255)
	OUT  0x2D,R30
; 0000 01AA TCNT1L=0xB9;
	LDI  R30,LOW(185)
	OUT  0x2C,R30
; 0000 01AB //ICR1H=0x00;
; 0000 01AC //ICR1L=0x00;
; 0000 01AD //OCR1AH=0x00;
; 0000 01AE //OCR1AL=0x00;
; 0000 01AF //OCR1BH=0x00;
; 0000 01B0 //OCR1BL=0x00;
; 0000 01B1 
; 0000 01B2 
; 0000 01B3 
; 0000 01B4 // Timer/Counter 2 initialization
; 0000 01B5 // Clock source: System Clock
; 0000 01B6 // Clock value: Timer 2 Stopped
; 0000 01B7 // Mode: Normal top=FFh
; 0000 01B8 // OC2 output: Disconnected
; 0000 01B9 TCCR2=0x00;
	LDI  R30,LOW(0)
	OUT  0x25,R30
; 0000 01BA TCNT2=0x00;
	OUT  0x24,R30
; 0000 01BB OCR2=0x00;
	OUT  0x23,R30
; 0000 01BC 
; 0000 01BD 
; 0000 01BE 
; 0000 01BF // Timer/Counter 3 initialization
; 0000 01C0 // Clock source: T3 pin Falling Edge
; 0000 01C1 // Mode: Normal top=FFFFh
; 0000 01C2 // Noise Canceler: Off
; 0000 01C3 // Input Capture on Falling Edge
; 0000 01C4 // OC3A output: Discon.
; 0000 01C5 // OC3B output: Discon.
; 0000 01C6 // OC3C output: Discon.
; 0000 01C7 // Timer 3 Overflow Interrupt: Off
; 0000 01C8 // Input Capture Interrupt: Off
; 0000 01C9 // Compare A Match Interrupt: Off
; 0000 01CA // Compare B Match Interrupt: Off
; 0000 01CB // Compare C Match Interrupt: Off
; 0000 01CC TCCR3A=0x00;
	STS  139,R30
; 0000 01CD TCCR3B=0x06;
	LDI  R30,LOW(6)
	STS  138,R30
; 0000 01CE //TCNT3H=0x00;
; 0000 01CF //TCNT3L=0x00;
; 0000 01D0 //ICR3H=0x00;
; 0000 01D1 //ICR3L=0x00;
; 0000 01D2 //OCR3AH=0x00;
; 0000 01D3 //OCR3AL=0x00;
; 0000 01D4 //OCR3BH=0x00;
; 0000 01D5 //OCR3BL=0x00;
; 0000 01D6 //OCR3CH=0x00;
; 0000 01D7 //OCR3CL=0x00;
; 0000 01D8 
; 0000 01D9 
; 0000 01DA 
; 0000 01DB // External Interrupt(s) initialization
; 0000 01DC // INT0: Off
; 0000 01DD // INT1: Off
; 0000 01DE // INT2: Off
; 0000 01DF // INT3: Off
; 0000 01E0 // INT4: On
; 0000 01E1 // INT4 Mode: Low level
; 0000 01E2 // INT5: Off
; 0000 01E3 // INT6: Off
; 0000 01E4 // INT7: Off
; 0000 01E5 EICRA=0x00;
	LDI  R30,LOW(0)
	STS  106,R30
; 0000 01E6 EICRB=0x00;
	OUT  0x3A,R30
; 0000 01E7 EIMSK=0x10;
	LDI  R30,LOW(16)
	OUT  0x39,R30
; 0000 01E8 EIFR=0x10;
	OUT  0x38,R30
; 0000 01E9 
; 0000 01EA 
; 0000 01EB 
; 0000 01EC // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 01ED TIMSK=0x04;
	LDI  R30,LOW(4)
	OUT  0x37,R30
; 0000 01EE ETIMSK=0x00;
	LDI  R30,LOW(0)
	STS  125,R30
; 0000 01EF 
; 0000 01F0 
; 0000 01F1 // USART0 initialization
; 0000 01F2 // USART0 disabled
; 0000 01F3 UCSR0B=0x00;
	OUT  0xA,R30
; 0000 01F4 
; 0000 01F5 
; 0000 01F6 // USART1 initialization
; 0000 01F7 // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 01F8 // USART1 Receiver: On
; 0000 01F9 // USART1 Transmitter: On
; 0000 01FA // USART1 Mode: Asynchronous
; 0000 01FB // USART1 Baud Rate: 9600
; 0000 01FC UCSR1A=0x00;
	STS  155,R30
; 0000 01FD UCSR1B=0xD8;
	LDI  R30,LOW(216)
	STS  154,R30
; 0000 01FE UCSR1C=0x06;
	LDI  R30,LOW(6)
	STS  157,R30
; 0000 01FF UBRR1H=0x00;
	LDI  R30,LOW(0)
	STS  152,R30
; 0000 0200 UBRR1L=0x2F;
	LDI  R30,LOW(47)
	STS  153,R30
; 0000 0201 
; 0000 0202 
; 0000 0203 
; 0000 0204 // Analog Comparator initialization
; 0000 0205 // Analog Comparator: Off
; 0000 0206 // Analog Comparator Input Capture by Timer/Counter 1: Off
; 0000 0207 ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 0208 ADCSRB=0x00;
	LDI  R30,LOW(0)
	STS  142,R30
; 0000 0209 
; 0000 020A #define ADC_VREF_TYPE 0x00
; 0000 020B // ADC initialization
; 0000 020C // ADC Clock frequency: 921.600 kHz
; 0000 020D // ADC Voltage Reference: AREF pin
; 0000 020E ADMUX=ADC_VREF_TYPE & 0xff;
	OUT  0x7,R30
; 0000 020F ADCSRA=0x83;
	LDI  R30,LOW(131)
	OUT  0x6,R30
; 0000 0210 
; 0000 0211 
; 0000 0212 #asm("sei")
	sei
; 0000 0213 
; 0000 0214      putchar('^');// flasher response
	LDI  R26,LOW(94)
	RCALL _putchar
; 0000 0215 
; 0000 0216 
; 0000 0217     while (1) {
_0x44:
; 0000 0218                 if (boot_select==0)
	SBIC 0x19,0
	RJMP _0x47
; 0000 0219                                 {
; 0000 021A                                 debug_putchar(12);
	LDI  R26,LOW(12)
	CALL _debug_putchar
; 0000 021B                                 debug_putchar(0x1B);
	CALL SUBOPT_0x5
; 0000 021C                                 debug_putchar('[');
; 0000 021D                                 debug_putchar('2');
	LDI  R26,LOW(50)
	CALL _debug_putchar
; 0000 021E                                 debug_putchar('J');
	LDI  R26,LOW(74)
	CALL _debug_putchar
; 0000 021F                                 debug_putstring("Press [D]-Key for Diagnostic Menu",25,0,1);
	__POINTW1FN _0x0,0
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(25)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _debug_putstring
; 0000 0220                                 tmp=0;
	LDI  R17,LOW(0)
; 0000 0221                                 while (((tmp|32)!='d')&&(tmp!=3)) tmp=debug_getchar();  // pause for D key
_0x48:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x64)
	BREQ _0x4B
	CPI  R17,3
	BRNE _0x4C
_0x4B:
	RJMP _0x4A
_0x4C:
	CALL _debug_getchar
	MOV  R17,R30
	RJMP _0x48
_0x4A:
; 0000 0222 MCUCR =1;
	LDI  R30,LOW(1)
	OUT  0x35,R30
; 0000 0223                                 MCUCR =0;
	LDI  R30,LOW(0)
	OUT  0x35,R30
; 0000 0224                                 #asm
; 0000 0225                                 ldi R30,0x00
                                ldi R30,0x00
; 0000 0226                                 ldi R31,0x00
                                ldi R31,0x00
; 0000 0227                                 ijmp
                                ijmp
; 0000 0228                                 #endasm
; 0000 0229                                 }
; 0000 022A                                  else clr_wdt();
	RJMP _0x4D
_0x47:
	wdr
; 0000 022B              };// end while
_0x4D:
	RJMP _0x44
; 0000 022C     };
_0x4E:
	RJMP _0x4E
;
;
;
;
;
;//***************************************** Hex2Bin ***********************************
;//************************************************************************************
;unsigned char HexToBin(unsigned char dat)
; 0000 0235  {
_HexToBin:
; 0000 0236        dat|=0x20;
	ST   -Y,R26
;	dat -> Y+0
	LD   R30,Y
	ORI  R30,0x20
	ST   Y,R30
; 0000 0237        if ((dat>=0x30)&&(dat<=0x39)) return(dat-0x30);
	LD   R26,Y
	CPI  R26,LOW(0x30)
	BRLO _0x50
	CPI  R26,LOW(0x3A)
	BRLO _0x51
_0x50:
	RJMP _0x4F
_0x51:
	LD   R30,Y
	SUBI R30,LOW(48)
	JMP  _0x2000001
; 0000 0238        if ((dat>=0x61)&&(dat<=0x66)) return(dat-0x61+0x0A);
_0x4F:
	LD   R26,Y
	CPI  R26,LOW(0x61)
	BRLO _0x53
	CPI  R26,LOW(0x67)
	BRLO _0x54
_0x53:
	RJMP _0x52
_0x54:
	LD   R30,Y
	SUBI R30,LOW(97)
	SUBI R30,-LOW(10)
	JMP  _0x2000001
; 0000 0239        return (255); // error
_0x52:
	LDI  R30,LOW(255)
	JMP  _0x2000001
; 0000 023A  };
;
;
;
;
;//*************************************************************** FLASH COMMANDS ********************************************
;
;
;
;//********************************* VERIFY FLASH  ****************************
;//****************************************************************************
;unsigned char check_flesh(unsigned char page_byte,unsigned int page_address)
; 0000 0246 {
_check_flesh:
; 0000 0247  unsigned int data_w;
; 0000 0248  clr_wdt();
	ST   -Y,R27
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	page_byte -> Y+4
;	page_address -> Y+2
;	data_w -> R16,R17
	wdr
; 0000 0249   PageAddress=page_address;
	__GETWRS 4,5,2
; 0000 024A          for (j=0;j<page_byte;j+=2) {
	CLR  R13
	CLR  R14
_0x56:
	LDD  R30,Y+4
	__GETW2R 13,14
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRSH _0x57
; 0000 024B                            CurrentAddress=page_address+j;
	__GETW1R 13,14
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
; 0000 024C                                              #asm
; 0000 024D                                              movw r30,r6
                                             movw r30,r6
; 0000 024E                                              lpm  r3,Z+
                                             lpm  r3,Z+
; 0000 024F                                              lpm  r2,Z
                                             lpm  r2,Z
; 0000 0250                                              #endasm
; 0000 0251                                       data_w=data_buffer[j+1];
	__GETW1R 13,14
	__ADDW1MN _data_buffer,1
	LD   R16,Z
	CLR  R17
; 0000 0252                                       data_w=(data_w<<8)|data_buffer[j];
	MOV  R31,R16
	LDI  R30,LOW(0)
	MOVW R0,R30
	LDI  R26,LOW(_data_buffer)
	LDI  R27,HIGH(_data_buffer)
	ADD  R26,R13
	ADC  R27,R14
	CALL SUBOPT_0x6
	MOVW R16,R30
; 0000 0253                                 if (PageData!=data_w) return (1);
	__CPWRR 16,17,2,3
	BREQ _0x58
	LDI  R30,LOW(1)
	RJMP _0x2000003
; 0000 0254                                    } // end for
_0x58:
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	__ADDWRR 13,14,30,31
	RJMP _0x56
_0x57:
; 0000 0255     return (0);
	LDI  R30,LOW(0)
_0x2000003:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,5
	RET
; 0000 0256 }
;
;
;
;
;
;
;//*******************************  WRITE & ERASE FLASH  ******************************
;//************************************************************************************
;unsigned char write_flash(unsigned char page_byte,unsigned int page_address)
; 0000 0260 {
_write_flash:
; 0000 0261   if (((secure[0]==123)&&(secure[1]==212)&&(secure[2]==233))||(boot_select==0))   // check
	ST   -Y,R27
	ST   -Y,R26
;	page_byte -> Y+2
;	page_address -> Y+0
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	CALL __EEPROMRDB
	CPI  R30,LOW(0x7B)
	BRNE _0x5A
	__POINTW2MN _secure,1
	CALL __EEPROMRDB
	CPI  R30,LOW(0xD4)
	BRNE _0x5A
	__POINTW2MN _secure,2
	CALL __EEPROMRDB
	CPI  R30,LOW(0xE9)
	BREQ _0x5C
_0x5A:
	LDI  R26,0
	SBIC 0x19,0
	LDI  R26,1
	CPI  R26,LOW(0x0)
	BREQ _0x5C
	JMP  _0x59
_0x5C:
; 0000 0262    {
; 0000 0263   #asm(".EQU SpmcrAddr=0x68")//57
	.EQU SpmcrAddr=0x68
; 0000 0264   PageAddress=page_address;
	__GETWRS 4,5,0
; 0000 0265  for (i=0;i<page_byte;i+=2)
	CLR  R11
	CLR  R12
_0x5F:
	LDD  R30,Y+2
	__GETW2R 11,12
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRSH _0x60
; 0000 0266   {
; 0000 0267   PageData=data_buffer[i+1];
	__GETW1R 11,12
	__ADDW1MN _data_buffer,1
	LD   R2,Z
	CLR  R3
; 0000 0268   PageData=(PageData<<8)|data_buffer[i];
	MOV  R31,R2
	LDI  R30,LOW(0)
	MOVW R0,R30
	LDI  R26,LOW(_data_buffer)
	LDI  R27,HIGH(_data_buffer)
	ADD  R26,R11
	ADC  R27,R12
	CALL SUBOPT_0x6
	MOVW R2,R30
; 0000 0269   while(SPMCSR&1) clr_wdt();
_0x61:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x63
	wdr
; 0000 026A   CurrentAddress=page_address+i;
	RJMP _0x61
_0x63:
	__GETW1R 11,12
	LD   R26,Y
	LDD  R27,Y+1
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
; 0000 026B   spmcrval=1;
	LDI  R30,LOW(1)
	MOV  R10,R30
; 0000 026C   #asm
; 0000 026D   movw  r30,r6
  movw  r30,r6
; 0000 026E   mov   r1,r3
  mov   r1,r3
; 0000 026F   mov   r0,r2
  mov   r0,r2
; 0000 0270   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
; 0000 0271   spm
  spm
; 0000 0272   #endasm
; 0000 0273   }
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	__ADDWRR 11,12,30,31
	RJMP _0x5F
_0x60:
; 0000 0274 
; 0000 0275 
; 0000 0276   while(SPMCSR&1) clr_wdt();
_0x64:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x66
	wdr
; 0000 0277   clr_wdt();
	RJMP _0x64
_0x66:
	wdr
; 0000 0278   if ((PageAddress%256)==0)
	MOVW R30,R4
	ANDI R31,HIGH(0xFF)
	SBIW R30,0
	BRNE _0x67
; 0000 0279     {
; 0000 027A   spmcrval=3;
	LDI  R30,LOW(3)
	MOV  R10,R30
; 0000 027B   #asm
; 0000 027C   movw  r30,r4
  movw  r30,r4
; 0000 027D   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
; 0000 027E   spm
  spm
; 0000 027F   #endasm                     // erase page
; 0000 0280     }
; 0000 0281 
; 0000 0282   while(SPMCSR&1) clr_wdt();
_0x67:
_0x68:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x6A
	wdr
; 0000 0283   clr_wdt();
	RJMP _0x68
_0x6A:
	wdr
; 0000 0284   spmcrval=5;
	LDI  R30,LOW(5)
	MOV  R10,R30
; 0000 0285   #asm
; 0000 0286   movw  r30,r4
  movw  r30,r4
; 0000 0287   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
; 0000 0288   spm
  spm
; 0000 0289   #endasm
; 0000 028A 
; 0000 028B   while(SPMCSR&1) clr_wdt();
_0x6B:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x6D
	wdr
; 0000 028C   clr_wdt();
	RJMP _0x6B
_0x6D:
	wdr
; 0000 028D   spmcrval=0x11;
	LDI  R30,LOW(17)
	MOV  R10,R30
; 0000 028E   #asm
; 0000 028F   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
; 0000 0290   spm
  spm
; 0000 0291   #endasm
; 0000 0292 
; 0000 0293   if (check_flesh(page_byte,page_address))  {putchar('O');return(0);}
	LDD  R30,Y+2
	ST   -Y,R30
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	CALL _check_flesh
	CPI  R30,0
	BREQ _0x6E
	LDI  R26,LOW(79)
	CALL _putchar
	LDI  R30,LOW(0)
	RJMP _0x2000002
; 0000 0294     else putchar('F');
_0x6E:
	LDI  R26,LOW(70)
	CALL _putchar
; 0000 0295     return (1);
	LDI  R30,LOW(1)
	RJMP _0x2000002
; 0000 0296   }
; 0000 0297  return ('S');
_0x59:
	LDI  R30,LOW(83)
_0x2000002:
	ADIW R28,3
	RET
; 0000 0298 
; 0000 0299 }
;
;
;
;
;
;//***************************** DEBUG PUTCHAR **********************************
;void debug_putchar(unsigned char dat_tx)
; 0000 02A1  {
_debug_putchar:
; 0000 02A2 
; 0000 02A3                 #asm("cli");
	ST   -Y,R26
;	dat_tx -> Y+0
	cli
; 0000 02A4                 reg_dat=dat_tx;
	LDD  R8,Y+0
; 0000 02A5 
; 0000 02A6 #asm               	cli
; 0000 02A7 			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
; 0000 02A8         		com	R8		;Inverte everything
        		com	R8		;Inverte everything
; 0000 02A9         		sec			;Start bit
        		sec			;Start bit
; 0000 02AA 

; 0000 02AB putchar0:   	brcc	putchar1	;If carry set
putchar0:   	brcc	putchar1	;If carry set
; 0000 02AC 		        cbi	    PORTE,TxD	;    send a '0'
		        cbi	    PORTE,TxD	;    send a '0'
; 0000 02AD 		        rjmp	putchar2	;else
		        rjmp	putchar2	;else
; 0000 02AE 

; 0000 02AF putchar1:	    sbi	    PORTE,TxD	;    send a '1'
putchar1:	    sbi	    PORTE,TxD	;    send a '1'
; 0000 02B0 		        nop
		        nop
; 0000 02B1 

; 0000 02B2 putchar2:	    ldi	    temp,b
putchar2:	    ldi	    temp,b
; 0000 02B3 UART_delay1:	dec	    temp
UART_delay1:	dec	    temp
; 0000 02B4 		        brne	UART_delay1
		        brne	UART_delay1
; 0000 02B5 

; 0000 02B6         	    ldi	    temp,b
        	    ldi	    temp,b
; 0000 02B7 UART_delay2:	dec 	temp
UART_delay2:	dec 	temp
; 0000 02B8 		        brne	UART_delay2
		        brne	UART_delay2
; 0000 02B9 

; 0000 02BA         		lsr	R8		;Get next bit
        		lsr	R8		;Get next bit
; 0000 02BB         		dec	    bitcnt		;If not all bit sent
        		dec	    bitcnt		;If not all bit sent
; 0000 02BC         		brne	putchar0	;   send next
        		brne	putchar0	;   send next
; 0000 02BD         		sei
        		sei
; 0000 02BE  #endasm
; 0000 02BF  };
_0x2000001:
	ADIW R28,1
	RET
;
;
;
;
;
;//****************************** DEBUG GETCHAR ********************************
;unsigned char debug_getchar(void)
; 0000 02C7  {
_debug_getchar:
; 0000 02C8  D_RxD=0;
	CLT
	BLD  R2,0
; 0000 02C9  while (D_RxD==0) {
_0x70:
	SBRC R2,0
	RJMP _0x72
; 0000 02CA                     clr_wdt();
	wdr
; 0000 02CB                     while (boot_select==1);
_0x73:
	SBIC 0x19,0
	RJMP _0x73
; 0000 02CC                     }
	RJMP _0x70
_0x72:
; 0000 02CD  return (debug_RxD_buff);
	LDS  R30,_debug_RxD_buff
	RET
; 0000 02CE  };
;
;
;//********************************* DEBUG PUTSTRING **************************************
;void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
; 0000 02D3  {
_debug_putstring:
; 0000 02D4   unsigned char couter_tmp;
; 0000 02D5   clr_wdt();
	ST   -Y,R26
	ST   -Y,R17
;	*text -> Y+4
;	X -> Y+3
;	Y -> Y+2
;	color -> Y+1
;	couter_tmp -> R17
	wdr
; 0000 02D6    if (X<255) {
	LDD  R26,Y+3
	CPI  R26,LOW(0xFF)
	BRLO PC+3
	JMP _0x76
; 0000 02D7             if (color){
	LDD  R30,Y+1
	CPI  R30,0
	BREQ _0x77
; 0000 02D8                         debug_putchar(0x1B);
	RCALL SUBOPT_0x5
; 0000 02D9                         debug_putchar(0x5B);
; 0000 02DA                         debug_putchar(0x30+color);
	LDD  R26,Y+1
	SUBI R26,-LOW(48)
	CALL _debug_putchar
; 0000 02DB                         debug_putchar(0x6D);
	LDI  R26,LOW(109)
	CALL _debug_putchar
; 0000 02DC                         }
; 0000 02DD 
; 0000 02DE               debug_putchar(0x1B);
_0x77:
	RCALL SUBOPT_0x5
; 0000 02DF               debug_putchar(0x5B);
; 0000 02E0               if (Y<=10) debug_putchar(0x30+Y);
	LDD  R26,Y+2
	CPI  R26,LOW(0xB)
	BRSH _0x78
	SUBI R26,-LOW(48)
	RJMP _0x85
; 0000 02E1                 else debug_putchar(0x30+10);
_0x78:
	LDI  R26,LOW(58)
_0x85:
	CALL _debug_putchar
; 0000 02E2               debug_putchar(0x03B);
	LDI  R26,LOW(59)
	CALL _debug_putchar
; 0000 02E3               debug_putchar(0x30+(X/10));
	LDD  R26,Y+3
	LDI  R27,0
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _debug_putchar
; 0000 02E4               debug_putchar(0x30+(X%10));
	LDD  R26,Y+3
	CLR  R27
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __MODW21
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _debug_putchar
; 0000 02E5               debug_putchar(0x48);
	LDI  R26,LOW(72)
	CALL _debug_putchar
; 0000 02E6               couter_tmp=0;
	LDI  R17,LOW(0)
; 0000 02E7               if (Y>10)  while(++couter_tmp<(Y-9)) debug_putchar(10);
	LDD  R26,Y+2
	CPI  R26,LOW(0xB)
	BRLO _0x7A
_0x7B:
	SUBI R17,-LOW(1)
	LDD  R30,Y+2
	LDI  R31,0
	SBIW R30,9
	MOV  R26,R17
	LDI  R27,0
	CP   R26,R30
	CPC  R27,R31
	BRGE _0x7D
	LDI  R26,LOW(10)
	CALL _debug_putchar
	RJMP _0x7B
_0x7D:
; 0000 02E8 }
_0x7A:
; 0000 02E9         while (*text!=0) {debug_putchar(*text++);}
_0x76:
_0x7E:
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	LPM  R30,Z
	CPI  R30,0
	BREQ _0x80
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	ADIW R30,1
	STD  Y+4,R30
	STD  Y+4+1,R31
	SBIW R30,1
	LPM  R26,Z
	CALL _debug_putchar
	RJMP _0x7E
_0x80:
; 0000 02EA     if (X<255){
	LDD  R26,Y+3
	CPI  R26,LOW(0xFF)
	BRSH _0x81
; 0000 02EB                 if (color)
	LDD  R30,Y+1
	CPI  R30,0
	BREQ _0x82
; 0000 02EC                           {
; 0000 02ED                           debug_putchar(0x1B);
	RCALL SUBOPT_0x5
; 0000 02EE                           debug_putchar(0x5B);
; 0000 02EF                           debug_putchar(0x30);
	LDI  R26,LOW(48)
	CALL _debug_putchar
; 0000 02F0                           debug_putchar(0x6D);
	LDI  R26,LOW(109)
	CALL _debug_putchar
; 0000 02F1                           }
; 0000 02F2                 }
_0x82:
; 0000 02F3 
; 0000 02F4   };
_0x81:
	LDD  R17,Y+0
	ADIW R28,6
	RET

	.ESEG
_secure:
	.DB  0x30,0x30,0x30,0x0

	.DSEG
_program_address:
	.BYTE 0x2
_data_counter:
	.BYTE 0x1
_chksum:
	.BYTE 0x1
_lenght:
	.BYTE 0x1
_record_type:
	.BYTE 0x1
_buffer_counter:
	.BYTE 0x1
_bin_data:
	.BYTE 0x1
_debug_RxD_buff:
	.BYTE 0x1
_data_buffer:
	.BYTE 0x42

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x0:
	SWAP R30
	ANDI R30,0xF0
	MOV  R26,R30
	MOV  R30,R17
	ANDI R30,LOW(0xF)
	OR   R30,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x1:
	MOV  R30,R17
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2:
	STS  _program_address,R30
	STS  _program_address+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x3:
	LDS  R30,_program_address
	LDS  R31,_program_address+1
	CALL __LSLW4
	MOVW R26,R30
	RJMP SUBOPT_0x1

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x4:
	ANDI R30,LOW(0xF)
	ANDI R31,HIGH(0xF)
	OR   R30,R26
	OR   R31,R27
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x5:
	LDI  R26,LOW(27)
	CALL _debug_putchar
	LDI  R26,LOW(91)
	JMP  _debug_putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6:
	LD   R30,X
	LDI  R31,0
	OR   R30,R0
	OR   R31,R1
	RET


	.CSEG
_delay_ms:
	adiw r26,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x733
	wdr
	sbiw r26,1
	brne __delay_ms0
__delay_ms1:
	ret

__ANEGW1:
	NEG  R31
	NEG  R30
	SBCI R31,0
	RET

__LSLW4:
	LSL  R30
	ROL  R31
__LSLW3:
	LSL  R30
	ROL  R31
__LSLW2:
	LSL  R30
	ROL  R31
	LSL  R30
	ROL  R31
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__DIVW21:
	RCALL __CHKSIGNW
	RCALL __DIVW21U
	BRTC __DIVW211
	RCALL __ANEGW1
__DIVW211:
	RET

__MODW21:
	CLT
	SBRS R27,7
	RJMP __MODW211
	COM  R26
	COM  R27
	ADIW R26,1
	SET
__MODW211:
	SBRC R31,7
	RCALL __ANEGW1
	RCALL __DIVW21U
	MOVW R30,R26
	BRTC __MODW212
	RCALL __ANEGW1
__MODW212:
	RET

__CHKSIGNW:
	CLT
	SBRS R31,7
	RJMP __CHKSW1
	RCALL __ANEGW1
	SET
__CHKSW1:
	SBRS R27,7
	RJMP __CHKSW2
	COM  R26
	COM  R27
	ADIW R26,1
	BLD  R0,0
	INC  R0
	BST  R0,0
__CHKSW2:
	RET

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

;END OF CODE MARKER
__END_OF_CODE:
