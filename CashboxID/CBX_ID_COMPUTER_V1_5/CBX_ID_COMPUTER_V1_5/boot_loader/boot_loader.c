/*****************************************************
This program was produced by the
CodeWizardAVR V2.03.9 Standard
Automatic Program Generator
� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 1.00
Version :
Date    : 09/19/2011
Author  : Sasa Vukovic
Company : GFi Genfare a unit of SPX Corp.
Comments:


Chip type               : ATmega64
Program type            : Boot Loader - Size:4096words
AVR Core Clock frequency: 7.372800 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 1024
*****************************************************/

#include <mega64.h>
#include <delay.h>



#define _38400      //38400 debug port speed if is not define speed is 9600




#define RXB8 1
#define TXB8 0
#define UPE 2
#define OVR 3
#define FE 4
#define UDRE 5
#define RXC 7

#define STX 0x02
#define ETX 0x03
#define ENQ 0x05
#define ACK 0x06
#define NAK 0x15



#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<OVR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)



#define boot_select         PINA.0
#define DE                  PORTC.7

#define sys_led             PORTD.4
#define err_led             PORTD.5

#define Led_Power           PORTD.6
#define Service             PORTD.7

#define link_led1           PORTC.5
#define link_led2           PORTC.6



#define clr_wdt()       #asm("wdr")



#pragma warn-
eeprom unsigned char secure[]="000";
#pragma warn+


#pragma regalloc-
       //for flasher
register unsigned int j @13;
register unsigned int i @11;
register unsigned char spmcrval @10;
register unsigned int CurrentAddress @6;
register unsigned int PageAddress  @4;
register unsigned int PageData @2;
register unsigned char reg_dat  @8;
#pragma regalloc+






bit D_RxD=1,done_page=1;
unsigned int program_address=0;
unsigned char led_blink,data_counter,chksum,lenght,record_type,buffer_counter,bin_data,debug_RxD_buff;
unsigned char data_buffer[66];







unsigned char write_flash(unsigned char page_byte,unsigned int page_address);
unsigned char HexToBin(unsigned char dat);
void putchar(char dat);
unsigned char debug_getchar(void);
void debug_putchar(unsigned char dat_tx);
void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color);




 #asm
 .equ           PINE    =0x01             ;PINE=0x01
 .equ           RxD     =4                ;PINE.4      //eint4
 .equ           PORTE   =0x03             ;PORTE=0x03;
 .equ           Txd     =5                ;PORTE.5
 .equ           sb      =1                ;Number of stop bits (1, 2, ...)
 .def           temp    =R27
 .def           bitcnt  =R26              ;bit counter
 #endasm

#ifdef _38400
                #asm
                .equ	 b=30   ;7.3728MHz => 38840=30
                #endasm
#else
                #asm
                .equ	 b=128  ;7.3728MHz => 9.6K=128
                #endasm
#endif




//******************External Interrupt 4 service routine  for (software)debug serial port Rxd_debug ************************
interrupt [EXT_INT4] void ext_int4_isr(void)
{
 unsigned char dat;


                #asm
                cli
                ldi     bitcnt,9            ;8 data bit + 1 stop bit
                #endasm

#ifdef _38400
                {
                #asm
                ldi	    temp,20            ;38400=10
                #endasm
                }
#else
                {
                #asm
                ldi	    temp,108            ;9600=20
                #endasm
                }
#endif

                #asm
UART_delay00:    dec    temp
                brne    UART_delay00        ;0.5 bit delay

getchar2:       ldi     temp,b
UART_delay11:   dec     temp
                brne    UART_delay11

                ldi     temp,b
UART_delay22:    dec    temp
                brne    UART_delay22

                clc                         ;clear carry
                sbic     PINE,RxD           ;if RX pin high
                sec                         ;

                dec      bitcnt             ;If bit is stop bit
                breq     getchar3           ;   return
                                            ;else
                ror      R8                 ;shift bit into Rxbyte
                rjmp     getchar2           ;go get next
getchar3:
#endasm

                dat=reg_dat;
                debug_RxD_buff=dat;
                D_RxD=1;
                EIFR=4;
                #asm("sei")
};







//********************************* PUTCHAR **********************************
//****************************************************************************
void putchar(char dat)
{
    //delay_ms(10);
    //DE=1;
    while (!( UCSR1A & (1<<UDRE))) clr_wdt();
    UDR1=dat;
}








//********************************************** RxD uart0 interrupt ********************************************
//***************************************************************************************************************
 interrupt [USART1_RXC] void usart0_rx_isr(void)
{
 char data;

 data=UDR1;
 if ((UCSR1A & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
 {
 ++data_counter;
       if (data==':') {done_page=0;data_counter=0,chksum=0;}
        else    if (done_page==0){
                                if (data_counter<=8) {

                                                   data=HexToBin(data);
                                                   if ((data&0xFF)==0xFF) putchar('E');
                                                   switch (data_counter) {
                                                              case 1:lenght=data;break;
                                                              case 2:{lenght=((lenght<<4)|(data&0x0F));chksum=lenght;break;}
                                                              case 3:program_address=data;break;
                                                              case 4:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
                                                              case 5:program_address=((program_address<<4)|(data&0x0F));break;
                                                              case 6:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
                                                              case 7:record_type=data;break;
                                                              case 8:{record_type=(record_type<<4)|(data&0x0F);buffer_counter=0;chksum+=record_type;break;}
                                                                          }// end switch
                                                         } //end if
                                      else    if ((data_counter&1)==1)  bin_data=HexToBin(data);
                                                   else {data=HexToBin(data);bin_data=((bin_data<<4)|(data&0x0F));chksum+=bin_data;data_buffer[buffer_counter++]=bin_data;}
                                   }// end if

         if (((data_counter/2)-5==lenght)&&(done_page==0))  {
                                                                data_counter=0;
                                                                 if (chksum==0)  if  (record_type==1) {
                                                                                                        putchar('D');
                                                                                                        secure[0]=0xFF;
                                                                                                        secure[1]=0xFF;
                                                                                                        secure[2]=0xFF;
                                                                                                        #asm ("cli")
                                                                                                        WDTCR=0x18;
                                                                                                        WDTCR=0x08;
                                                                                                        loop:   goto loop;   //WAIT "WDT" HARDWARE RESET

                                                                                                        }
                                                                                    else {
                                                                                          write_flash(lenght,(program_address));
                                                                                          done_page=1;
                                                                                         }

                                                                    else {
                                                                            putchar('E');
                                                                            done_page=1;
                                                                          }
                                         }
  }
};




//***************************************** TxD Uart1 interrupt ***********************************
//*************************************************************************************************
 interrupt [USART1_TXC] void usart0_tx_isr(void)
{
 DE=0;
};




// ************************** Timer 1 overflow interrupt service routine  0.01s ***********************
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
 TCNT1H=0xFF;
 TCNT1L=0xB9;

  // diagnostic mode led-s
               if ((++led_blink&16)==16) {
                            sys_led=0;
                            err_led=1;
                            Led_Power=1;
                            Service=0;
                            link_led1=1;
                            link_led2=0;
                        }
                         else {
                                sys_led=1;
                                err_led=0;
                                Led_Power=0;
                                Service=1;
                                link_led1=0;
                                link_led2=1;
                              }
 }






void main(void)                    // BOOT LOADER START AT 0x3800
    {
    unsigned char tmp;
#asm ("cli");
// Watchdog Timer initialization
// Watchdog Timer Prescaler: OSC/1024k
// Watchdog Timer interrupt: Off
#pragma optsize-
#asm("wdr")
WDTCR=0x1F;
WDTCR=0x0F;
#ifdef _OPTIMIZE_SIZE_
#pragma optsize+
#endif


// Input/Output Ports initialization
// Port A initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
PORTA=0xFF;
DDRA=0x00;

delay_ms(1);

if  ((boot_select)&&((secure[0]!=123)||(secure[1]!=212)||(secure[2]!=233)))  {
                                                                            MCUCR =1;
                                                                            MCUCR =0;
                                                                            #asm
                                                                            ldi R30,0x00
                                                                            ldi R31,0x00
                                                                            ijmp
                                                                            #endasm
                                                                            }

// Port B initialization
// Func7=Out Func6=Out Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=0 State6=0 State5=P State4=P State3=P State2=P State1=T State0=P
PORTB=0x3D;
DDRB=0xC0;


// Port C initialization
// Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=0 State6=0 State5=0 State4=P State3=P State2=P State1=P State0=P
PORTC=0x1F;
DDRC=0xE0;



// Port D initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=In Func1=Out Func0=Out
// State7=0 State6=0 State5=0 State4=0 State3=1 State2=P State1=0 State0=0
PORTD=0x0C;
DDRD=0xFB;

// Port E initialization
// Func7=In Func6=In Func5=Out Func4=In Func3=In Func2=Out Func1=Out Func0=In
// State7=P State6=P State5=1 State4=P State3=P State2=1 State1=1 State0=P
PORTE=0xFF;
DDRE=0x26;


// Port F initialization
// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
// State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
PORTF=0xF0;
DDRF=0x00;



// Port G initialization
// Func4=Out Func3=Out Func2=In Func1=In Func0=In
// State4=1 State3=1 State2=P State1=P State0=P
PORTG=0x1F;
DDRG=0x18;



// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: Timer 0 Stopped
// Mode: Normal top=FFh
// OC0 output: Disconnected
ASSR=0x00;
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;



// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 7.200 kHz
// Mode: Normal top=FFFFh
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer 1 Overflow Interrupt: On
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x05;
TCNT1H=0xFF;
TCNT1L=0xB9;
//ICR1H=0x00;
//ICR1L=0x00;
//OCR1AH=0x00;
//OCR1AL=0x00;
//OCR1BH=0x00;
//OCR1BL=0x00;



// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: Timer 2 Stopped
// Mode: Normal top=FFh
// OC2 output: Disconnected
TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;



// Timer/Counter 3 initialization
// Clock source: T3 pin Falling Edge
// Mode: Normal top=FFFFh
// Noise Canceler: Off
// Input Capture on Falling Edge
// OC3A output: Discon.
// OC3B output: Discon.
// OC3C output: Discon.
// Timer 3 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
// Compare C Match Interrupt: Off
TCCR3A=0x00;
TCCR3B=0x06;
//TCNT3H=0x00;
//TCNT3L=0x00;
//ICR3H=0x00;
//ICR3L=0x00;
//OCR3AH=0x00;
//OCR3AL=0x00;
//OCR3BH=0x00;
//OCR3BL=0x00;
//OCR3CH=0x00;
//OCR3CL=0x00;



// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
// INT3: Off
// INT4: On
// INT4 Mode: Low level
// INT5: Off
// INT6: Off
// INT7: Off
EICRA=0x00;
EICRB=0x00;
EIMSK=0x10;
EIFR=0x10;



// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x04;
ETIMSK=0x00;


// USART0 initialization
// USART0 disabled
UCSR0B=0x00;


// USART1 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART1 Receiver: On
// USART1 Transmitter: On
// USART1 Mode: Asynchronous
// USART1 Baud Rate: 9600
UCSR1A=0x00;
UCSR1B=0xD8;
UCSR1C=0x06;
UBRR1H=0x00;
UBRR1L=0x2F;



// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
ADCSRB=0x00;

#define ADC_VREF_TYPE 0x00
// ADC initialization
// ADC Clock frequency: 921.600 kHz
// ADC Voltage Reference: AREF pin
ADMUX=ADC_VREF_TYPE & 0xff;
ADCSRA=0x83;


#asm("sei")

     putchar('^');// flasher response


    while (1) {
                if (boot_select==0)
                                {
                                debug_putchar(12);
                                debug_putchar(0x1B);
                                debug_putchar('[');
                                debug_putchar('2');
                                debug_putchar('J');
                                debug_putstring("Press [D]-Key for Diagnostic Menu",25,0,1);
                                tmp=0;
                                while (((tmp|32)!='d')&&(tmp!=3)) tmp=debug_getchar();  // pause for D key
                                MCUCR =1;
                                MCUCR =0;
                                #asm
                                ldi R30,0x00
                                ldi R31,0x00
                                ijmp
                                #endasm
                                }
                                 else clr_wdt();
             };// end while
    };





//***************************************** Hex2Bin ***********************************
//************************************************************************************
unsigned char HexToBin(unsigned char dat)
 {
       dat|=0x20;
       if ((dat>=0x30)&&(dat<=0x39)) return(dat-0x30);
       if ((dat>=0x61)&&(dat<=0x66)) return(dat-0x61+0x0A);
       return (255); // error
 };




//*************************************************************** FLASH COMMANDS ********************************************



//********************************* VERIFY FLASH  ****************************
//****************************************************************************
unsigned char check_flesh(unsigned char page_byte,unsigned int page_address)
{
 unsigned int data_w;
 clr_wdt();
  PageAddress=page_address;
         for (j=0;j<page_byte;j+=2) {
                           CurrentAddress=page_address+j;
                                             #asm
                                             movw r30,r6
                                             lpm  r3,Z+
                                             lpm  r2,Z
                                             #endasm
                                      data_w=data_buffer[j+1];
                                      data_w=(data_w<<8)|data_buffer[j];
                                if (PageData!=data_w) return (1);
                                   } // end for
    return (0);
}






//*******************************  WRITE & ERASE FLASH  ******************************
//************************************************************************************
unsigned char write_flash(unsigned char page_byte,unsigned int page_address)
{
  if (((secure[0]==123)&&(secure[1]==212)&&(secure[2]==233))||(boot_select==0))   // check
   {
  #asm(".EQU SpmcrAddr=0x68")//57
  PageAddress=page_address;
 for (i=0;i<page_byte;i+=2)
  {
  PageData=data_buffer[i+1];
  PageData=(PageData<<8)|data_buffer[i];
  while(SPMCSR&1) clr_wdt();
  CurrentAddress=page_address+i;
  spmcrval=1;
  #asm
  movw  r30,r6
  mov   r1,r3
  mov   r0,r2
  sts   SpmcrAddr,r10
  spm
  #endasm
  }


  while(SPMCSR&1) clr_wdt();
  clr_wdt();
  if ((PageAddress%256)==0)
    {
  spmcrval=3;
  #asm
  movw  r30,r4
  sts   SpmcrAddr,r10
  spm
  #endasm                     // erase page
    }

  while(SPMCSR&1) clr_wdt();
  clr_wdt();
  spmcrval=5;
  #asm
  movw  r30,r4
  sts   SpmcrAddr,r10
  spm
  #endasm

  while(SPMCSR&1) clr_wdt();
  clr_wdt();
  spmcrval=0x11;
  #asm
  sts   SpmcrAddr,r10
  spm
  #endasm

  if (check_flesh(page_byte,page_address))  {putchar('O');return(0);}
    else putchar('F');
    return (1);
  }
 return ('S');

}





//***************************** DEBUG PUTCHAR **********************************
void debug_putchar(unsigned char dat_tx)
 {

                #asm("cli");
                reg_dat=dat_tx;

#asm               	cli
			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
        		com	R8		;Inverte everything
        		sec			;Start bit

putchar0:   	brcc	putchar1	;If carry set
		        cbi	    PORTE,TxD	;    send a '0'
		        rjmp	putchar2	;else

putchar1:	    sbi	    PORTE,TxD	;    send a '1'
		        nop

putchar2:	    ldi	    temp,b
UART_delay1:	dec	    temp
		        brne	UART_delay1

        	    ldi	    temp,b
UART_delay2:	dec 	temp
		        brne	UART_delay2

        		lsr	R8		;Get next bit
        		dec	    bitcnt		;If not all bit sent
        		brne	putchar0	;   send next
        		sei
 #endasm
 };





//****************************** DEBUG GETCHAR ********************************
unsigned char debug_getchar(void)
 {
 D_RxD=0;
 while (D_RxD==0) {
                    clr_wdt();
                    while (boot_select==1);
                    }
 return (debug_RxD_buff);
 };


//********************************* DEBUG PUTSTRING **************************************
void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
 {
  unsigned char couter_tmp;
  clr_wdt();
   if (X<255) {
            if (color){
                        debug_putchar(0x1B);
                        debug_putchar(0x5B);
                        debug_putchar(0x30+color);
                        debug_putchar(0x6D);
                        }

              debug_putchar(0x1B);
              debug_putchar(0x5B);
              if (Y<=10) debug_putchar(0x30+Y);
                else debug_putchar(0x30+10);
              debug_putchar(0x03B);
              debug_putchar(0x30+(X/10));
              debug_putchar(0x30+(X%10));
              debug_putchar(0x48);
              couter_tmp=0;
              if (Y>10)  while(++couter_tmp<(Y-9)) debug_putchar(10);
               }
        while (*text!=0) {debug_putchar(*text++);}
    if (X<255){
                if (color)
                          {
                          debug_putchar(0x1B);
                          debug_putchar(0x5B);
                          debug_putchar(0x30);
                          debug_putchar(0x6D);
                          }
                }

  };
