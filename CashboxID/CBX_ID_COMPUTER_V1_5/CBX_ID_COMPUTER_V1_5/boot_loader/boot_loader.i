
#pragma used+
sfrb PINF=0;
sfrb PINE=1;
sfrb DDRE=2;
sfrb PORTE=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      
sfrb ADCSRA=6;
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRR0L=9;
sfrb UCSR0B=0xa;
sfrb UCSR0A=0xb;
sfrb UDR0=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   
sfrb SFIOR=0x20;
sfrb WDTCR=0x21;
sfrb OCDR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrw ICR1=0x26;   
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb ASSR=0x30;
sfrb OCR0=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TIFR=0x36;
sfrb TIMSK=0x37;
sfrb EIFR=0x38;
sfrb EIMSK=0x39;
sfrb EICRB=0x3a;
sfrb XDIV=0x3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x20
	.EQU __sm_mask=0x1C
	.EQU __sm_powerdown=0x10
	.EQU __sm_powersave=0x18
	.EQU __sm_standby=0x14
	.EQU __sm_ext_standby=0x1C
	.EQU __sm_adc_noise_red=0x08
	.SET power_ctrl_reg=mcucr
	#endif
#endasm

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

#pragma warn-
eeprom unsigned char secure[]="000";
#pragma warn+

#pragma regalloc-

register unsigned int j @13;
register unsigned int i @11;
register unsigned char spmcrval @10;
register unsigned int CurrentAddress @6;
register unsigned int PageAddress  @4;
register unsigned int PageData @2;
register unsigned char reg_dat  @8;
#pragma regalloc+

bit D_RxD=1,done_page=1;
unsigned int program_address=0;
unsigned char led_blink,data_counter,chksum,lenght,record_type,buffer_counter,bin_data,debug_RxD_buff;
unsigned char data_buffer[66];

unsigned char write_flash(unsigned char page_byte,unsigned int page_address);
unsigned char HexToBin(unsigned char dat);
void putchar(char dat);
unsigned char debug_getchar(void);
void debug_putchar(unsigned char dat_tx);
void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color);

#asm
 .equ           PINE    =0x01             ;PINE=0x01
 .equ           RxD     =4                ;PINE.4      //eint4
 .equ           PORTE   =0x03             ;PORTE=0x03;
 .equ           Txd     =5                ;PORTE.5
 .equ           sb      =1                ;Number of stop bits (1, 2, ...)
 .def           temp    =R27
 .def           bitcnt  =R26              ;bit counter
 #endasm

#asm
                .equ	 b=30   ;7.3728MHz => 38840=30
                #endasm

interrupt [6] void ext_int4_isr(void)
{
unsigned char dat;

#asm
                cli
                ldi     bitcnt,9            ;8 data bit + 1 stop bit
                #endasm

{
#asm
                ldi	    temp,20            ;38400=10
                #endasm
}

#asm
UART_delay00:    dec    temp
                brne    UART_delay00        ;0.5 bit delay

getchar2:       ldi     temp,b
UART_delay11:   dec     temp
                brne    UART_delay11

                ldi     temp,b
UART_delay22:    dec    temp
                brne    UART_delay22

                clc                         ;clear carry
                sbic     PINE,RxD           ;if RX pin high
                sec                         ;

                dec      bitcnt             ;If bit is stop bit
                breq     getchar3           ;   return
                                            ;else
                ror      R8                 ;shift bit into Rxbyte
                rjmp     getchar2           ;go get next
getchar3:
#endasm

dat=reg_dat;
debug_RxD_buff=dat;
D_RxD=1;
EIFR=4;
#asm("sei")
};

void putchar(char dat)
{

while (!( (*(unsigned char *) 0x9b) & (1<<5))) #asm("wdr");
(*(unsigned char *) 0x9c)=dat;
}

interrupt [31] void usart0_rx_isr(void)
{
char data;

data=(*(unsigned char *) 0x9c);
if (((*(unsigned char *) 0x9b) & ((1<<4) | (1<<2) | (1<<3)))==0)
{
++data_counter;
if (data==':') {done_page=0;data_counter=0,chksum=0;}
else    if (done_page==0){
if (data_counter<=8) {

data=HexToBin(data);
if ((data&0xFF)==0xFF) putchar('E');
switch (data_counter) {
case 1:lenght=data;break;
case 2:{lenght=((lenght<<4)|(data&0x0F));chksum=lenght;break;}
case 3:program_address=data;break;
case 4:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
case 5:program_address=((program_address<<4)|(data&0x0F));break;
case 6:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
case 7:record_type=data;break;
case 8:{record_type=(record_type<<4)|(data&0x0F);buffer_counter=0;chksum+=record_type;break;}
}
} 
else    if ((data_counter&1)==1)  bin_data=HexToBin(data);
else {data=HexToBin(data);bin_data=((bin_data<<4)|(data&0x0F));chksum+=bin_data;data_buffer[buffer_counter++]=bin_data;}
}

if (((data_counter/2)-5==lenght)&&(done_page==0))  {
data_counter=0;
if (chksum==0)  if  (record_type==1) {
putchar('D');
secure[0]=0xFF;
secure[1]=0xFF;
secure[2]=0xFF;
#asm ("cli")
WDTCR=0x18;
WDTCR=0x08;
loop:   goto loop;   

}
else {
write_flash(lenght,(program_address));
done_page=1;
}

else {
putchar('E');
done_page=1;
}
}
}
};

interrupt [33] void usart0_tx_isr(void)
{
PORTC.7=0;
};

interrupt [15] void timer1_ovf_isr(void)
{
TCNT1H=0xFF;
TCNT1L=0xB9;

if ((++led_blink&16)==16) {
PORTD.4=0;
PORTD.5=1;
PORTD.6=1;
PORTD.7=0;
PORTC.5=1;
PORTC.6=0;
}
else {
PORTD.4=1;
PORTD.5=0;
PORTD.6=0;
PORTD.7=1;
PORTC.5=0;
PORTC.6=1;
}
}

void main(void)                    
{
unsigned char tmp;
#asm ("cli");

#pragma optsize-
#asm("wdr")
WDTCR=0x1F;
WDTCR=0x0F;
#pragma optsize+

PORTA=0xFF;
DDRA=0x00;

delay_ms(1);

if  ((PINA.0)&&((secure[0]!=123)||(secure[1]!=212)||(secure[2]!=233)))  {
MCUCR =1;
MCUCR =0;
#asm
                                                                            ldi R30,0x00
                                                                            ldi R31,0x00
                                                                            ijmp
                                                                            #endasm
}

PORTB=0x3D;
DDRB=0xC0;

PORTC=0x1F;
DDRC=0xE0;

PORTD=0x0C;
DDRD=0xFB;

PORTE=0xFF;
DDRE=0x26;

(*(unsigned char *) 0x62)=0xF0;
(*(unsigned char *) 0x61)=0x00;

(*(unsigned char *) 0x65)=0x1F;
(*(unsigned char *) 0x64)=0x18;

ASSR=0x00;
TCCR0=0x00;
TCNT0=0x00;
OCR0=0x00;

TCCR1A=0x00;
TCCR1B=0x05;
TCNT1H=0xFF;
TCNT1L=0xB9;

TCCR2=0x00;
TCNT2=0x00;
OCR2=0x00;

(*(unsigned char *) 0x8b)=0x00;
(*(unsigned char *) 0x8a)=0x06;

(*(unsigned char *) 0x6a)=0x00;
EICRB=0x00;
EIMSK=0x10;
EIFR=0x10;

TIMSK=0x04;
(*(unsigned char *) 0x7d)=0x00;

UCSR0B=0x00;

(*(unsigned char *) 0x9b)=0x00;
(*(unsigned char *) 0x9a)=0xD8;
(*(unsigned char *) 0x9d)=0x06;
(*(unsigned char *) 0x98)=0x00;
(*(unsigned char *) 0x99)=0x2F;

ACSR=0x80;
(*(unsigned char *) 0x8e)=0x00;

ADMUX=0x00 & 0xff;
ADCSRA=0x83;

#asm("sei")

putchar('^');

while (1) {
if (PINA.0==0)
{
debug_putchar(12);
debug_putchar(0x1B);
debug_putchar('[');
debug_putchar('2');
debug_putchar('J');
debug_putstring("Press [D]-Key for Diagnostic Menu",25,0,1);
tmp=0;
while (((tmp|32)!='d')&&(tmp!=3)) tmp=debug_getchar();  
MCUCR =1;
MCUCR =0;
#asm
                                ldi R30,0x00
                                ldi R31,0x00
                                ijmp
                                #endasm
}
else #asm("wdr");
};
};

unsigned char HexToBin(unsigned char dat)
{
dat|=0x20;
if ((dat>=0x30)&&(dat<=0x39)) return(dat-0x30);
if ((dat>=0x61)&&(dat<=0x66)) return(dat-0x61+0x0A);
return (255); 
};

unsigned char check_flesh(unsigned char page_byte,unsigned int page_address)
{
unsigned int data_w;
#asm("wdr");
PageAddress=page_address;
for (j=0;j<page_byte;j+=2) {
CurrentAddress=page_address+j;
#asm
                                             movw r30,r6
                                             lpm  r3,Z+
                                             lpm  r2,Z
                                             #endasm
data_w=data_buffer[j+1];
data_w=(data_w<<8)|data_buffer[j];
if (PageData!=data_w) return (1);
} 
return (0);
}

unsigned char write_flash(unsigned char page_byte,unsigned int page_address)
{
if (((secure[0]==123)&&(secure[1]==212)&&(secure[2]==233))||(PINA.0==0))   
{
#asm(".EQU SpmcrAddr=0x68")
PageAddress=page_address;
for (i=0;i<page_byte;i+=2)
{
PageData=data_buffer[i+1];
PageData=(PageData<<8)|data_buffer[i];
while((*(unsigned char *) 0x68)&1) #asm("wdr");
CurrentAddress=page_address+i;
spmcrval=1;
#asm
  movw  r30,r6
  mov   r1,r3
  mov   r0,r2
  sts   SpmcrAddr,r10
  spm
  #endasm
}

while((*(unsigned char *) 0x68)&1) #asm("wdr");
#asm("wdr");
if ((PageAddress%256)==0)
{
spmcrval=3;
#asm
  movw  r30,r4
  sts   SpmcrAddr,r10
  spm
  #endasm                     // erase page
}

while((*(unsigned char *) 0x68)&1) #asm("wdr");
#asm("wdr");
spmcrval=5;
#asm
  movw  r30,r4
  sts   SpmcrAddr,r10
  spm
  #endasm

while((*(unsigned char *) 0x68)&1) #asm("wdr");
#asm("wdr");
spmcrval=0x11;
#asm
  sts   SpmcrAddr,r10
  spm
  #endasm

if (check_flesh(page_byte,page_address))  {putchar('O');return(0);}
else putchar('F');
return (1);
}
return ('S');

}

void debug_putchar(unsigned char dat_tx)
{

#asm("cli");
reg_dat=dat_tx;

#asm               	cli
			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
        		com	R8		;Inverte everything
        		sec			;Start bit

putchar0:   	brcc	putchar1	;If carry set
		        cbi	    PORTE,TxD	;    send a '0'
		        rjmp	putchar2	;else

putchar1:	    sbi	    PORTE,TxD	;    send a '1'
		        nop

putchar2:	    ldi	    temp,b
UART_delay1:	dec	    temp
		        brne	UART_delay1

        	    ldi	    temp,b
UART_delay2:	dec 	temp
		        brne	UART_delay2

        		lsr	R8		;Get next bit
        		dec	    bitcnt		;If not all bit sent
        		brne	putchar0	;   send next
        		sei
 #endasm
};

unsigned char debug_getchar(void)
{
D_RxD=0;
while (D_RxD==0) {
#asm("wdr");
while (PINA.0==1);
}
return (debug_RxD_buff);
};

void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
{
unsigned char couter_tmp;
#asm("wdr");
if (X<255) {
if (color){
debug_putchar(0x1B);
debug_putchar(0x5B);
debug_putchar(0x30+color);
debug_putchar(0x6D);
}

debug_putchar(0x1B);
debug_putchar(0x5B);
if (Y<=10) debug_putchar(0x30+Y);
else debug_putchar(0x30+10);
debug_putchar(0x03B);
debug_putchar(0x30+(X/10));
debug_putchar(0x30+(X%10));
debug_putchar(0x48);
couter_tmp=0;
if (Y>10)  while(++couter_tmp<(Y-9)) debug_putchar(10);
}
while (*text!=0) {debug_putchar(*text++);}
if (X<255){
if (color)
{
debug_putchar(0x1B);
debug_putchar(0x5B);
debug_putchar(0x30);
debug_putchar(0x6D);
}
}

};
