
;CodeVisionAVR C Compiler V1.25.9 Standard
;(C) Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type              : ATmega64
;Program type           : Boot Loader
;Clock frequency        : 7.372800 MHz
;Memory model           : Small
;Optimize for           : Size
;(s)printf features     : int, width
;(s)scanf features      : int, width
;External SRAM size     : 0
;Data Stack size        : 1024 byte(s)
;Heap size              : 0 byte(s)
;Promote char to int    : Yes
;char is unsigned       : Yes
;8 bit enums            : Yes
;Word align FLASH struct: No
;Enhanced core instructions    : On
;Smart register allocation : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega64
	#pragma AVRPART MEMORY PROG_FLASH 65536
	#pragma AVRPART MEMORY EEPROM 2048
	#pragma AVRPART MEMORY INT_SRAM SIZE 4096
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU XMCRA=0x6D
	.EQU XMCRB=0x6C

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM
	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM
	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM
	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM
	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM
	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM
	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ANDI R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ORI  R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __CLRD1S
	LDI  R30,0
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+@1)
	LDI  R31,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	LDI  R22,BYTE3(2*@0+@1)
	LDI  R23,BYTE4(2*@0+@1)
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+@2)
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+@3)
	LDI  R@1,HIGH(@2+@3)
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+@3)
	LDI  R@1,HIGH(@2*2+@3)
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+@1
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+@1
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	LDS  R22,@0+@1+2
	LDS  R23,@0+@1+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+@2
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+@3
	LDS  R@1,@2+@3+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+@1
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	LDS  R24,@0+@1+2
	LDS  R25,@0+@1+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+@1,R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	STS  @0+@1+2,R22
	STS  @0+@1+3,R23
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+@1,R0
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+@1,R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+@1,R@2
	STS  @0+@1+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __CLRD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R@1
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

	.CSEG
	.ORG 0x7000

	.INCLUDE "boot_loader.vec"
	.INCLUDE "boot_loader.inc"

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF THE BOOT LOADER
	LDI  R31,1
	OUT  MCUCR,R31
	LDI  R31,2
	OUT  MCUCR,R31
	STS  XMCRB,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,13
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(0x1000)
	LDI  R25,HIGH(0x1000)
	LDI  R26,LOW(0x100)
	LDI  R27,HIGH(0x100)
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;STACK POINTER INITIALIZATION
	LDI  R30,LOW(0x10FF)
	OUT  SPL,R30
	LDI  R30,HIGH(0x10FF)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(0x500)
	LDI  R29,HIGH(0x500)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x500
;       1 /*****************************************************
;       2 This program was produced by the
;       3 CodeWizardAVR V2.03.9 Standard
;       4 Automatic Program Generator
;       5 � Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;       6 http://www.hpinfotech.com
;       7 
;       8 Project :
;       9 Version :
;      10 Date    : 03/18/2009
;      11 Author  : Sasa Vukovic
;      12 Company : GFi Genfare a unit of SPX Corp.
;      13 Comments:
;      14 
;      15 
;      16 Chip type               : ATmega64
;      17 Program type            : Boot Loader - Size:4096words
;      18 AVR Core Clock frequency: 7.372800 MHz
;      19 Memory model            : Small
;      20 External RAM size       : 0
;      21 Data Stack size         : 1024
;      22 *****************************************************/
;      23 
;      24 #include <mega64.h>
;      25 	#ifndef __SLEEP_DEFINED__
	#ifndef __SLEEP_DEFINED__
;      26 	#define __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
;      27 	.EQU __se_bit=0x20
	.EQU __se_bit=0x20
;      28 	.EQU __sm_mask=0x1C
	.EQU __sm_mask=0x1C
;      29 	.EQU __sm_powerdown=0x10
	.EQU __sm_powerdown=0x10
;      30 	.EQU __sm_powersave=0x18
	.EQU __sm_powersave=0x18
;      31 	.EQU __sm_standby=0x14
	.EQU __sm_standby=0x14
;      32 	.EQU __sm_ext_standby=0x1C
	.EQU __sm_ext_standby=0x1C
;      33 	.EQU __sm_adc_noise_red=0x08
	.EQU __sm_adc_noise_red=0x08
;      34 	.SET power_ctrl_reg=mcucr
	.SET power_ctrl_reg=mcucr
;      35 	#endif
	#endif
;      36 #include <delay.h>
;      37 
;      38 
;      39 
;      40 #define _38400      //38400 debug port speed if is not define speed is 9600
;      41 
;      42 
;      43 
;      44 
;      45 #define RXB8 1
;      46 #define TXB8 0
;      47 #define UPE 2
;      48 #define OVR 3
;      49 #define FE 4
;      50 #define UDRE 5
;      51 #define RXC 7
;      52 
;      53 #define STX 0x02
;      54 #define ETX 0x03
;      55 #define ENQ 0x05
;      56 #define ACK 0x06
;      57 #define NAK 0x15
;      58 
;      59 
;      60 
;      61 #define FRAMING_ERROR (1<<FE)
;      62 #define PARITY_ERROR (1<<UPE)
;      63 #define DATA_OVERRUN (1<<OVR)
;      64 #define DATA_REGISTER_EMPTY (1<<UDRE)
;      65 #define RX_COMPLETE (1<<RXC)
;      66 
;      67 
;      68 
;      69 #define boot_select         PINA.0
;      70 #define DE                  PORTC.7
;      71 
;      72 #define sys_led             PORTD.4
;      73 #define err_led             PORTD.5
;      74 
;      75 #define Led_Power           PORTD.6
;      76 #define Service             PORTD.7
;      77 
;      78 #define link_led1           PORTC.5
;      79 #define link_led2           PORTC.6
;      80 
;      81 
;      82 
;      83 #define clr_wdt()       #asm("wdr")
;      84 
;      85 
;      86 
;      87 #pragma warn-
;      88 eeprom unsigned char secure[]="000";

	.ESEG
_secure:
	.DB  0x30
	.DB  0x30
	.DB  0x30
	.DB  0x0
;      89 #pragma warn+
;      90 
;      91 
;      92 #pragma regalloc-
;      93        //for flasher
;      94 register unsigned int j @13;
;      95 register unsigned int i @11;
;      96 register unsigned char spmcrval @10;
;      97 register unsigned int CurrentAddress @6;
;      98 register unsigned int PageAddress  @4;
;      99 register unsigned int PageData @2;
;     100 register unsigned char reg_dat  @8;
;     101 #pragma regalloc+
;     102 
;     103 
;     104 
;     105 
;     106 
;     107 
;     108 bit D_RxD=1,done_page=1;
;     109 unsigned int program_address=0;

	.DSEG
_program_address:
	.BYTE 0x2
;     110 unsigned char led_blink,data_counter,chksum,lenght,record_type,buffer_counter,bin_data,debug_RxD_buff;
_data_counter:
	.BYTE 0x1
_chksum:
	.BYTE 0x1
_lenght:
	.BYTE 0x1
_record_type:
	.BYTE 0x1
_buffer_counter:
	.BYTE 0x1
_bin_data:
	.BYTE 0x1
_debug_RxD_buff:
	.BYTE 0x1
;     111 unsigned char data_buffer[66];
_data_buffer:
	.BYTE 0x42
;     112 
;     113 
;     114 
;     115 
;     116 
;     117 
;     118 
;     119 unsigned char write_flash(unsigned char page_byte,unsigned int page_address);
;     120 unsigned char HexToBin(unsigned char dat);
;     121 void putchar(char dat);
;     122 unsigned char debug_getchar(void);
;     123 void debug_putchar(unsigned char dat_tx);
;     124 void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color);
;     125 
;     126 
;     127 
;     128 
;     129  #asm
;     130  .equ           PINE    =0x01             ;PINE=0x01
 .equ           PINE    =0x01             ;PINE=0x01
;     131  .equ           RxD     =4                ;PINE.4      //eint4
 .equ           RxD     =4                ;PINE.4      //eint4
;     132  .equ           PORTE   =0x03             ;PORTE=0x03;
 .equ           PORTE   =0x03             ;PORTE=0x03;
;     133  .equ           Txd     =5                ;PORTE.5
 .equ           Txd     =5                ;PORTE.5
;     134  .equ           sb      =1                ;Number of stop bits (1, 2, ...)
 .equ           sb      =1                ;Number of stop bits (1, 2, ...)
;     135  .def           temp    =R27
 .def           temp    =R27
;     136  .def           bitcnt  =R26              ;bit counter
 .def           bitcnt  =R26              ;bit counter
;     137  #endasm
;     138 
;     139 #ifdef _38400
;     140                 #asm
;     141                 .equ	 b=30   ;7.3728MHz => 38840=30
                .equ	 b=30   ;7.3728MHz => 38840=30
;     142                 #endasm
;     143 #else
;     144                 #asm
;     145                 .equ	 b=128  ;7.3728MHz => 9.6K=128
;     146                 #endasm
;     147 #endif
;     148 
;     149 
;     150 
;     151 
;     152 //******************External Interrupt 4 service routine  for (software)debug serial port Rxd_debug ************************
;     153 interrupt [EXT_INT4] void ext_int4_isr(void)
;     154 {

	.CSEG
_ext_int4_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
;     155  unsigned char dat;
;     156 
;     157 
;     158                 #asm
	ST   -Y,R17
;	dat -> R17
;     159                 cli
                cli
;     160                 ldi     bitcnt,9            ;8 data bit + 1 stop bit
                ldi     bitcnt,9            ;8 data bit + 1 stop bit
;     161                 #endasm
;     162 
;     163 #ifdef _38400
;     164                 {
;     165                 #asm
;     166                 ldi	    temp,20            ;38400=10
                ldi	    temp,20            ;38400=10
;     167                 #endasm
;     168                 }
;     169 #else
;     170                 {
;     171                 #asm
;     172                 ldi	    temp,108            ;9600=20
;     173                 #endasm
;     174                 }
;     175 #endif
;     176 
;     177                 #asm
;     178 UART_delay00:    dec    temp
UART_delay00:    dec    temp
;     179                 brne    UART_delay00        ;0.5 bit delay
                brne    UART_delay00        ;0.5 bit delay
;     180 

;     181 getchar2:       ldi     temp,b
getchar2:       ldi     temp,b
;     182 UART_delay11:   dec     temp
UART_delay11:   dec     temp
;     183                 brne    UART_delay11
                brne    UART_delay11
;     184 

;     185                 ldi     temp,b
                ldi     temp,b
;     186 UART_delay22:    dec    temp
UART_delay22:    dec    temp
;     187                 brne    UART_delay22
                brne    UART_delay22
;     188 

;     189                 clc                         ;clear carry
                clc                         ;clear carry
;     190                 sbic     PINE,RxD           ;if RX pin high
                sbic     PINE,RxD           ;if RX pin high
;     191                 sec                         ;
                sec                         ;
;     192 

;     193                 dec      bitcnt             ;If bit is stop bit
                dec      bitcnt             ;If bit is stop bit
;     194                 breq     getchar3           ;   return
                breq     getchar3           ;   return
;     195                                             ;else
                                            ;else
;     196                 ror      R8                 ;shift bit into Rxbyte
                ror      R8                 ;shift bit into Rxbyte
;     197                 rjmp     getchar2           ;go get next
                rjmp     getchar2           ;go get next
;     198 getchar3:
getchar3:
;     199 #endasm
;     200 
;     201                 dat=reg_dat;
	MOV  R17,R8
;     202                 debug_RxD_buff=dat;
	STS  _debug_RxD_buff,R17
;     203                 D_RxD=1;
	SET
	BLD  R2,0
;     204                 EIFR=4;
	LDI  R30,LOW(4)
	OUT  0x38,R30
;     205                 #asm("sei")
	sei
;     206 };
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R30,Y+
	RETI
;     207 
;     208 
;     209 
;     210 
;     211 
;     212 
;     213 
;     214 //********************************* PUTCHAR **********************************
;     215 //****************************************************************************
;     216 void putchar(char dat)
;     217 {
_putchar:
;     218     delay_ms(10);
;	dat -> Y+0
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
;     219     DE=1;
	SBI  0x15,7
;     220     while (!( UCSR0A & (1<<UDRE))) clr_wdt();
_0x5:
	SBIC 0xB,5
	RJMP _0x7
	wdr
	JMP  _0x5
_0x7:
;     221     UDR0=dat;
	LD   R30,Y
	OUT  0xC,R30
;     222 }
	ADIW R28,1
	RET
;     223 
;     224 
;     225 
;     226 
;     227 
;     228 
;     229 
;     230 
;     231 //********************************************** RxD uart0 interrupt ********************************************
;     232 //***************************************************************************************************************
;     233  interrupt [USART0_RXC] void usart0_rx_isr(void)
;     234 {
_usart0_rx_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
;     235  char data;
;     236 
;     237  data=UDR0;
	ST   -Y,R17
;	data -> R17
	IN   R17,12
;     238  if ((UCSR0A & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	IN   R30,0xB
	ANDI R30,LOW(0x1C)
	BREQ PC+3
	JMP _0x8
;     239  {
;     240  ++data_counter;
	LDS  R30,_data_counter
	SUBI R30,-LOW(1)
	STS  _data_counter,R30
;     241        if (data==':') {done_page=0;data_counter=0,chksum=0;}
	CPI  R17,58
	BRNE _0x9
	CLT
	BLD  R2,1
	LDI  R30,LOW(0)
	STS  _data_counter,R30
	STS  _chksum,R30
;     242         else    if (done_page==0){
	RJMP _0xA
_0x9:
	SBRC R2,1
	RJMP _0xB
;     243                                 if (data_counter<=8) {
	LDS  R26,_data_counter
	CPI  R26,LOW(0x9)
	BRLO PC+3
	JMP _0xC
;     244 
;     245                                                    data=HexToBin(data);
	ST   -Y,R17
	CALL _HexToBin
	MOV  R17,R30
;     246                                                    if ((data&0xFF)==0xFF) putchar('E');
	MOV  R30,R17
	CPI  R30,LOW(0xFF)
	BRNE _0xD
	LDI  R30,LOW(69)
	ST   -Y,R30
	CALL _putchar
;     247                                                    switch (data_counter) {
_0xD:
	LDS  R30,_data_counter
	LDI  R31,0
;     248                                                               case 1:lenght=data;break;
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	BRNE _0x11
	STS  _lenght,R17
	RJMP _0x10
;     249                                                               case 2:{lenght=((lenght<<4)|(data&0x0F));chksum=lenght;break;}
_0x11:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BRNE _0x12
	LDS  R30,_lenght
	CALL SUBOPT_0x0
	STS  _lenght,R30
	RJMP _0x87
;     250                                                               case 3:program_address=data;break;
_0x12:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BRNE _0x13
	MOV  R30,R17
	LDI  R31,0
	CALL SUBOPT_0x1
	RJMP _0x10
;     251                                                               case 4:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
_0x13:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x14
	CALL SUBOPT_0x2
	CALL SUBOPT_0x3
	CALL SUBOPT_0x2
	ANDI R31,HIGH(0xFF)
	RJMP _0x88
;     252                                                               case 5:program_address=((program_address<<4)|(data&0x0F));break;
_0x14:
	CPI  R30,LOW(0x5)
	LDI  R26,HIGH(0x5)
	CPC  R31,R26
	BRNE _0x15
	CALL SUBOPT_0x2
	CALL SUBOPT_0x3
	RJMP _0x10
;     253                                                               case 6:{program_address=((program_address<<4)|(data&0x0F));chksum+=(program_address&0xFF);break;}
_0x15:
	CPI  R30,LOW(0x6)
	LDI  R26,HIGH(0x6)
	CPC  R31,R26
	BRNE _0x16
	CALL SUBOPT_0x2
	CALL SUBOPT_0x3
	CALL SUBOPT_0x2
	ANDI R31,HIGH(0xFF)
	RJMP _0x88
;     254                                                               case 7:record_type=data;break;
_0x16:
	CPI  R30,LOW(0x7)
	LDI  R26,HIGH(0x7)
	CPC  R31,R26
	BRNE _0x17
	STS  _record_type,R17
	RJMP _0x10
;     255                                                               case 8:{record_type=(record_type<<4)|(data&0x0F);buffer_counter=0;chksum+=record_type;break;}
_0x17:
	CPI  R30,LOW(0x8)
	LDI  R26,HIGH(0x8)
	CPC  R31,R26
	BRNE _0x10
	LDS  R30,_record_type
	CALL SUBOPT_0x0
	STS  _record_type,R30
	LDI  R30,LOW(0)
	STS  _buffer_counter,R30
	LDS  R30,_record_type
	LDI  R31,0
_0x88:
	LDS  R26,_chksum
	ADD  R30,R26
_0x87:
	STS  _chksum,R30
;     256                                                                           }// end switch
_0x10:
;     257                                                          } //end if
;     258                                       else    if ((data_counter&1)==1)  bin_data=HexToBin(data);
	RJMP _0x19
_0xC:
	LDS  R30,_data_counter
	ANDI R30,LOW(0x1)
	CPI  R30,LOW(0x1)
	BRNE _0x1A
	ST   -Y,R17
	CALL _HexToBin
	STS  _bin_data,R30
;     259                                                    else {data=HexToBin(data);bin_data=((bin_data<<4)|(data&0x0F));chksum+=bin_data;data_buffer[buffer_counter++]=bin_data;}
	RJMP _0x1B
_0x1A:
	ST   -Y,R17
	CALL _HexToBin
	MOV  R17,R30
	LDS  R30,_bin_data
	CALL SUBOPT_0x0
	STS  _bin_data,R30
	LDI  R31,0
	LDS  R26,_chksum
	ADD  R30,R26
	STS  _chksum,R30
	LDS  R30,_buffer_counter
	SUBI R30,-LOW(1)
	STS  _buffer_counter,R30
	SUBI R30,LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_data_buffer)
	SBCI R31,HIGH(-_data_buffer)
	LDS  R26,_bin_data
	STD  Z+0,R26
_0x1B:
_0x19:
;     260                                    }// end if
;     261 
;     262          if (((data_counter/2)-5==lenght)&&(done_page==0))  {
_0xB:
_0xA:
	LDS  R30,_data_counter
	LSR  R30
	SUBI R30,LOW(5)
	MOV  R26,R30
	LDS  R30,_lenght
	LDI  R31,0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	BRNE _0x1D
	SBRS R2,1
	RJMP _0x1E
_0x1D:
	JMP  _0x1C
_0x1E:
;     263                                                                 data_counter=0;
	LDI  R30,LOW(0)
	STS  _data_counter,R30
;     264                                                                  if (chksum==0)  if  (record_type==1) {
	LDS  R30,_chksum
	CPI  R30,0
	BRNE _0x1F
	LDS  R26,_record_type
	CPI  R26,LOW(0x1)
	BRNE _0x20
;     265                                                                                                         putchar('D');
	LDI  R30,LOW(68)
	ST   -Y,R30
	CALL _putchar
;     266                                                                                                         secure[0]=0xFF;
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	LDI  R30,LOW(255)
	CALL __EEPROMWRB
;     267                                                                                                         secure[1]=0xFF;
	__POINTW2MN _secure,1
	CALL __EEPROMWRB
;     268                                                                                                         secure[2]=0xFF;
	__POINTW2MN _secure,2
	CALL __EEPROMWRB
;     269                                                                                                         #asm ("cli")
	cli
;     270                                                                                                         WDTCR=0x18;
	LDI  R30,LOW(24)
	OUT  0x21,R30
;     271                                                                                                         WDTCR=0x08;
	LDI  R30,LOW(8)
	OUT  0x21,R30
;     272                                                                                                         loop:   goto loop;   //WAIT "WDT" HARDWARE RESET
_0x21:
	RJMP _0x21
;     273 
;     274                                                                                                         }
;     275                                                                                     else {
_0x20:
;     276                                                                                           write_flash(lenght,(program_address));done_page=1;
	LDS  R30,_lenght
	ST   -Y,R30
	CALL SUBOPT_0x2
	ST   -Y,R31
	ST   -Y,R30
	CALL _write_flash
	SET
	BLD  R2,1
;     277                                                                                          }
;     278 
;     279                                                                     else {
	RJMP _0x23
_0x1F:
;     280                                                                             putchar('E');
	LDI  R30,LOW(69)
	ST   -Y,R30
	CALL _putchar
;     281                                                                             done_page=1;
	SET
	BLD  R2,1
;     282                                                                           }
_0x23:
;     283                                          }
;     284   }
_0x1C:
;     285 };
_0x8:
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;     286 
;     287 
;     288 
;     289 
;     290 //***************************************** TxD- 485  interrupt ***********************************
;     291 //*************************************************************************************************
;     292  interrupt [USART0_TXC] void usart0_tx_isr(void)
;     293 {
_usart0_tx_isr:
;     294  DE=0;
	CBI  0x15,7
;     295 };
	RETI
;     296 
;     297 
;     298 
;     299 
;     300 // ************************** Timer 1 overflow interrupt service routine  0.01s ***********************
;     301 interrupt [TIM1_OVF] void timer1_ovf_isr(void)
;     302 {
_timer1_ovf_isr:
	ST   -Y,R30
	IN   R30,SREG
	ST   -Y,R30
;     303  TCNT1H=0xFF;
	LDI  R30,LOW(255)
	OUT  0x2D,R30
;     304  TCNT1L=0xB9;
	LDI  R30,LOW(185)
	OUT  0x2C,R30
;     305 
;     306   // diagnostic mode led-s
;     307                if ((++led_blink&16)==16) {
	INC  R9
	MOV  R30,R9
	ANDI R30,LOW(0x10)
	CPI  R30,LOW(0x10)
	BRNE _0x26
;     308                             sys_led=0;
	CBI  0x12,4
;     309                             err_led=1;
	SBI  0x12,5
;     310                             Led_Power=1;
	SBI  0x12,6
;     311                             Service=0;
	CBI  0x12,7
;     312                             link_led1=1;
	SBI  0x15,5
;     313                             link_led2=0;
	CBI  0x15,6
;     314                         }
;     315                          else {
	RJMP _0x33
_0x26:
;     316                                 sys_led=1;
	SBI  0x12,4
;     317                                 err_led=0;
	CBI  0x12,5
;     318                                 Led_Power=0;
	CBI  0x12,6
;     319                                 Service=1;
	SBI  0x12,7
;     320                                 link_led1=0;
	CBI  0x15,5
;     321                                 link_led2=1;
	SBI  0x15,6
;     322                               }
_0x33:
;     323  }
	LD   R30,Y+
	OUT  SREG,R30
	LD   R30,Y+
	RETI
;     324 
;     325 
;     326 
;     327 
;     328 
;     329 
;     330 
;     331 
;     332 
;     333 void main(void)                    // BOOT LOADER START AT 0x3800
;     334     {
_main:
;     335     unsigned char tmp;
;     336 #asm ("cli");
;	tmp -> R17
	cli
;     337 // Watchdog Timer initialization
;     338 // Watchdog Timer Prescaler: OSC/1024k
;     339 // Watchdog Timer interrupt: Off
;     340 #pragma optsize-
;     341 #asm("wdr")
	wdr
;     342 WDTCR=0x1F;
	LDI  R30,LOW(31)
	OUT  0x21,R30
;     343 WDTCR=0x0F;
	LDI  R30,LOW(15)
	OUT  0x21,R30
;     344 #ifdef _OPTIMIZE_SIZE_
;     345 #pragma optsize+
;     346 #endif
;     347 
;     348 
;     349 // Input/Output Ports initialization
;     350 // Port A initialization
;     351 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
;     352 // State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
;     353 PORTA=0xFF;
	LDI  R30,LOW(255)
	OUT  0x1B,R30
;     354 DDRA=0x00;
	LDI  R30,LOW(0)
	OUT  0x1A,R30
;     355 
;     356 delay_ms(1);
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
;     357 
;     358 if  ((boot_select)&&((secure[0]!=123)||(secure[1]!=212)||(secure[2]!=233)))  {
	SBIS 0x19,0
	RJMP _0x41
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	CALL __EEPROMRDB
	CPI  R30,LOW(0x7B)
	BRNE _0x42
	__POINTW2MN _secure,1
	CALL __EEPROMRDB
	CPI  R30,LOW(0xD4)
	BRNE _0x42
	__POINTW2MN _secure,2
	CALL __EEPROMRDB
	CPI  R30,LOW(0xE9)
	BREQ _0x41
_0x42:
	RJMP _0x44
_0x41:
	JMP  _0x40
_0x44:
;     359                                                                             MCUCR =1;
	LDI  R30,LOW(1)
	OUT  0x35,R30
;     360                                                                             MCUCR =0;
	LDI  R30,LOW(0)
	OUT  0x35,R30
;     361                                                                             #asm
;     362                                                                             ldi R30,0x00
                                                                            ldi R30,0x00
;     363                                                                             ldi R31,0x00
                                                                            ldi R31,0x00
;     364                                                                             ijmp
                                                                            ijmp
;     365                                                                             #endasm
;     366                                                                             }
;     367 
;     368 
;     369 // Port B initialization
;     370 // Func7=Out Func6=Out Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
;     371 // State7=0 State6=0 State5=P State4=P State3=P State2=P State1=T State0=P
;     372 PORTB=0x3D;
_0x40:
	LDI  R30,LOW(61)
	OUT  0x18,R30
;     373 DDRB=0xC0;
	LDI  R30,LOW(192)
	OUT  0x17,R30
;     374 
;     375 
;     376 // Port C initialization
;     377 // Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=In
;     378 // State7=0 State6=0 State5=0 State4=P State3=P State2=P State1=P State0=P
;     379 PORTC=0x1F;
	LDI  R30,LOW(31)
	OUT  0x15,R30
;     380 DDRC=0xE0;
	LDI  R30,LOW(224)
	OUT  0x14,R30
;     381 
;     382 
;     383 
;     384 // Port D initialization
;     385 // Func7=Out Func6=Out Func5=Out Func4=Out Func3=Out Func2=In Func1=Out Func0=Out
;     386 // State7=0 State6=0 State5=0 State4=0 State3=1 State2=P State1=0 State0=0
;     387 PORTD=0x0C;
	LDI  R30,LOW(12)
	OUT  0x12,R30
;     388 DDRD=0xFB;
	LDI  R30,LOW(251)
	OUT  0x11,R30
;     389 
;     390 // Port E initialization
;     391 // Func7=In Func6=In Func5=Out Func4=In Func3=In Func2=Out Func1=Out Func0=In
;     392 // State7=P State6=P State5=1 State4=P State3=P State2=1 State1=1 State0=P
;     393 PORTE=0xFF;
	LDI  R30,LOW(255)
	OUT  0x3,R30
;     394 DDRE=0x26;
	LDI  R30,LOW(38)
	OUT  0x2,R30
;     395 
;     396 
;     397 // Port F initialization
;     398 // Func7=In Func6=In Func5=In Func4=In Func3=In Func2=In Func1=In Func0=In
;     399 // State7=P State6=P State5=P State4=P State3=P State2=P State1=P State0=P
;     400 PORTF=0xF0;
	LDI  R30,LOW(240)
	STS  98,R30
;     401 DDRF=0x00;
	LDI  R30,LOW(0)
	STS  97,R30
;     402 
;     403 
;     404 
;     405 // Port G initialization
;     406 // Func4=Out Func3=Out Func2=In Func1=In Func0=In
;     407 // State4=1 State3=1 State2=P State1=P State0=P
;     408 PORTG=0x1F;
	LDI  R30,LOW(31)
	STS  101,R30
;     409 DDRG=0x18;
	LDI  R30,LOW(24)
	STS  100,R30
;     410 
;     411 
;     412 
;     413 // Timer/Counter 0 initialization
;     414 // Clock source: System Clock
;     415 // Clock value: Timer 0 Stopped
;     416 // Mode: Normal top=FFh
;     417 // OC0 output: Disconnected
;     418 ASSR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
;     419 TCCR0=0x00;
	OUT  0x33,R30
;     420 TCNT0=0x00;
	OUT  0x32,R30
;     421 OCR0=0x00;
	OUT  0x31,R30
;     422 
;     423 
;     424 
;     425 // Timer/Counter 1 initialization
;     426 // Clock source: System Clock
;     427 // Clock value: 7.200 kHz
;     428 // Mode: Normal top=FFFFh
;     429 // OC1A output: Discon.
;     430 // OC1B output: Discon.
;     431 // Noise Canceler: Off
;     432 // Input Capture on Falling Edge
;     433 // Timer 1 Overflow Interrupt: On
;     434 // Input Capture Interrupt: Off
;     435 // Compare A Match Interrupt: Off
;     436 // Compare B Match Interrupt: Off
;     437 TCCR1A=0x00;
	OUT  0x2F,R30
;     438 TCCR1B=0x05;
	LDI  R30,LOW(5)
	OUT  0x2E,R30
;     439 TCNT1H=0xFF;
	LDI  R30,LOW(255)
	OUT  0x2D,R30
;     440 TCNT1L=0xB9;
	LDI  R30,LOW(185)
	OUT  0x2C,R30
;     441 //ICR1H=0x00;
;     442 //ICR1L=0x00;
;     443 //OCR1AH=0x00;
;     444 //OCR1AL=0x00;
;     445 //OCR1BH=0x00;
;     446 //OCR1BL=0x00;
;     447 
;     448 
;     449 
;     450 // Timer/Counter 2 initialization
;     451 // Clock source: System Clock
;     452 // Clock value: Timer 2 Stopped
;     453 // Mode: Normal top=FFh
;     454 // OC2 output: Disconnected
;     455 TCCR2=0x00;
	LDI  R30,LOW(0)
	OUT  0x25,R30
;     456 TCNT2=0x00;
	OUT  0x24,R30
;     457 OCR2=0x00;
	OUT  0x23,R30
;     458 
;     459 
;     460 
;     461 // Timer/Counter 3 initialization
;     462 // Clock source: T3 pin Falling Edge
;     463 // Mode: Normal top=FFFFh
;     464 // Noise Canceler: Off
;     465 // Input Capture on Falling Edge
;     466 // OC3A output: Discon.
;     467 // OC3B output: Discon.
;     468 // OC3C output: Discon.
;     469 // Timer 3 Overflow Interrupt: Off
;     470 // Input Capture Interrupt: Off
;     471 // Compare A Match Interrupt: Off
;     472 // Compare B Match Interrupt: Off
;     473 // Compare C Match Interrupt: Off
;     474 TCCR3A=0x00;
	STS  139,R30
;     475 TCCR3B=0x06;
	LDI  R30,LOW(6)
	STS  138,R30
;     476 //TCNT3H=0x00;
;     477 //TCNT3L=0x00;
;     478 //ICR3H=0x00;
;     479 //ICR3L=0x00;
;     480 //OCR3AH=0x00;
;     481 //OCR3AL=0x00;
;     482 //OCR3BH=0x00;
;     483 //OCR3BL=0x00;
;     484 //OCR3CH=0x00;
;     485 //OCR3CL=0x00;
;     486 
;     487 
;     488 
;     489 // External Interrupt(s) initialization
;     490 // INT0: Off
;     491 // INT1: Off
;     492 // INT2: Off
;     493 // INT3: Off
;     494 // INT4: On
;     495 // INT4 Mode: Low level
;     496 // INT5: Off
;     497 // INT6: Off
;     498 // INT7: Off
;     499 EICRA=0x00;
	LDI  R30,LOW(0)
	STS  106,R30
;     500 EICRB=0x00;
	OUT  0x3A,R30
;     501 EIMSK=0x10;
	LDI  R30,LOW(16)
	OUT  0x39,R30
;     502 EIFR=0x10;
	OUT  0x38,R30
;     503 
;     504 
;     505 
;     506 // Timer(s)/Counter(s) Interrupt(s) initialization
;     507 TIMSK=0x04;
	LDI  R30,LOW(4)
	OUT  0x37,R30
;     508 ETIMSK=0x00;
	LDI  R30,LOW(0)
	STS  125,R30
;     509 
;     510 
;     511 // USART0 initialization
;     512 // Communication Parameters: 8 Data, 1 Stop, No Parity
;     513 // USART0 Receiver: On
;     514 // USART0 Transmitter: On
;     515 // USART0 Mode: Asynchronous
;     516 // USART0 Baud Rate: 57600
;     517 UCSR0A=0x00;
	OUT  0xB,R30
;     518 UCSR0B=0xD8;
	LDI  R30,LOW(216)
	OUT  0xA,R30
;     519 UCSR0C=0x06;
	LDI  R30,LOW(6)
	STS  149,R30
;     520 UBRR0H=0x00;
	LDI  R30,LOW(0)
	STS  144,R30
;     521 UBRR0L=0x07;
	LDI  R30,LOW(7)
	OUT  0x9,R30
;     522 
;     523 
;     524 
;     525 // Analog Comparator initialization
;     526 // Analog Comparator: Off
;     527 // Analog Comparator Input Capture by Timer/Counter 1: Off
;     528 ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
;     529 ADCSRB=0x00;
	LDI  R30,LOW(0)
	STS  142,R30
;     530 
;     531 #define ADC_VREF_TYPE 0x00
;     532 // ADC initialization
;     533 // ADC Clock frequency: 921.600 kHz
;     534 // ADC Voltage Reference: AREF pin
;     535 ADMUX=ADC_VREF_TYPE & 0xff;
	OUT  0x7,R30
;     536 ADCSRA=0x83;
	LDI  R30,LOW(131)
	OUT  0x6,R30
;     537 
;     538 
;     539 #asm("sei")
	sei
;     540 
;     541      putchar('^');// flasher response
	LDI  R30,LOW(94)
	ST   -Y,R30
	CALL _putchar
;     542 
;     543 
;     544     while (1) {
_0x45:
;     545                 if (boot_select==0)
	SBIC 0x19,0
	RJMP _0x48
;     546                                 {
;     547                                 debug_putchar(12);
	LDI  R30,LOW(12)
	ST   -Y,R30
	CALL _debug_putchar
;     548                                 debug_putchar(0x1B);
	CALL SUBOPT_0x4
;     549                                 debug_putchar('[');
;     550                                 debug_putchar('2');
	LDI  R30,LOW(50)
	ST   -Y,R30
	CALL _debug_putchar
;     551                                 debug_putchar('J');
	LDI  R30,LOW(74)
	ST   -Y,R30
	CALL _debug_putchar
;     552                                 debug_putstring("Press [D]-Key for Diagnostic Menu",25,0,1);
	__POINTW1FN _0,0
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(25)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _debug_putstring
;     553                                 tmp=0;
	LDI  R17,LOW(0)
;     554                                 while (((tmp|32)!='d')&&(tmp!=3)) tmp=debug_getchar();  // pause for D key
_0x49:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x64)
	BREQ _0x4C
	CPI  R17,3
	BRNE _0x4D
_0x4C:
	RJMP _0x4B
_0x4D:
	CALL _debug_getchar
	MOV  R17,R30
;     555                                 MCUCR =1;
	RJMP _0x49
_0x4B:
	LDI  R30,LOW(1)
	OUT  0x35,R30
;     556                                 MCUCR =0;
	LDI  R30,LOW(0)
	OUT  0x35,R30
;     557                                 #asm
;     558                                 ldi R30,0x00
                                ldi R30,0x00
;     559                                 ldi R31,0x00
                                ldi R31,0x00
;     560                                 ijmp
                                ijmp
;     561                                 #endasm
;     562                                 }
;     563                                  else clr_wdt();
	JMP  _0x4E
_0x48:
	wdr
_0x4E:
;     564              };// end while
	JMP  _0x45
;     565     };
_0x4F:
	RJMP _0x4F
;     566 
;     567 
;     568 
;     569 
;     570 
;     571 //***************************************** Hex2Bin ***********************************
;     572 //************************************************************************************
;     573 unsigned char HexToBin(unsigned char dat)
;     574  {
_HexToBin:
;     575        dat|=0x20;
;	dat -> Y+0
	LD   R30,Y
	ORI  R30,0x20
	ST   Y,R30
;     576        if ((dat>=0x30)&&(dat<=0x39)) return(dat-0x30);
	LD   R26,Y
	CPI  R26,LOW(0x30)
	BRLO _0x51
	CPI  R26,LOW(0x3A)
	BRLO _0x52
_0x51:
	RJMP _0x50
_0x52:
	LD   R30,Y
	SUBI R30,LOW(48)
	RJMP _0x86
;     577        if ((dat>=0x61)&&(dat<=0x66)) return(dat-0x61+0x0A);
_0x50:
	LD   R26,Y
	CPI  R26,LOW(0x61)
	BRLO _0x54
	CPI  R26,LOW(0x67)
	BRLO _0x55
_0x54:
	RJMP _0x53
_0x55:
	LD   R30,Y
	SUBI R30,LOW(97)
	SUBI R30,-LOW(10)
	RJMP _0x86
;     578        return (255); // error
_0x53:
	LDI  R30,LOW(255)
_0x86:
	ADIW R28,1
	RET
;     579  };
;     580 
;     581 
;     582 
;     583 
;     584 //*************************************************************** FLASH COMMANDS ********************************************
;     585 
;     586 
;     587 
;     588 //********************************* VERIFY FLASH  ****************************
;     589 //****************************************************************************
;     590 unsigned char check_flesh(unsigned char page_byte,unsigned int page_address)
;     591 {
_check_flesh:
;     592  unsigned int data_w;
;     593  clr_wdt();
	ST   -Y,R17
	ST   -Y,R16
;	page_byte -> Y+4
;	page_address -> Y+2
;	data_w -> R16,R17
	wdr
;     594   PageAddress=page_address;
	__GETWRS 4,5,2
;     595          for (j=0;j<page_byte;j+=2) {
	CLR  R13
	CLR  R14
_0x57:
	LDD  R30,Y+4
	__GETW2R 13,14
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRLO PC+3
	JMP _0x58
;     596                            CurrentAddress=page_address+j;
	__GETW1R 13,14
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
;     597                                              #asm
;     598                                              movw r30,r6
                                             movw r30,r6
;     599                                              lpm  r3,Z+
                                             lpm  r3,Z+
;     600                                              lpm  r2,Z
                                             lpm  r2,Z
;     601                                              #endasm
;     602                                       data_w=data_buffer[j+1];
	__GETW1R 13,14
	__ADDW1MN _data_buffer,1
	LD   R16,Z
	CLR  R17
;     603                                       data_w=(data_w<<8)|data_buffer[j];
	MOV  R31,R16
	LDI  R30,LOW(0)
	MOVW R0,R30
	LDI  R26,LOW(_data_buffer)
	LDI  R27,HIGH(_data_buffer)
	ADD  R26,R13
	ADC  R27,R14
	CALL SUBOPT_0x5
	MOVW R16,R30
;     604                                 if (PageData!=data_w) return (1);
	__CPWRR 16,17,2,3
	BREQ _0x59
	LDI  R30,LOW(1)
	RJMP _0x85
;     605                                    } // end for
_0x59:
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	__ADDWRR 13,14,30,31
	JMP  _0x57
_0x58:
;     606     return (0);
	LDI  R30,LOW(0)
_0x85:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,5
	RET
;     607 }
;     608 
;     609 
;     610 
;     611 
;     612 
;     613 
;     614 //*******************************  WRITE & ERASE FLASH  ******************************
;     615 //************************************************************************************
;     616 unsigned char write_flash(unsigned char page_byte,unsigned int page_address)
;     617 {
_write_flash:
;     618   if (((secure[0]==123)&&(secure[1]==212)&&(secure[2]==233))||(boot_select==0))   // check
;	page_byte -> Y+2
;	page_address -> Y+0
	LDI  R26,LOW(_secure)
	LDI  R27,HIGH(_secure)
	CALL __EEPROMRDB
	CPI  R30,LOW(0x7B)
	BRNE _0x5B
	__POINTW2MN _secure,1
	CALL __EEPROMRDB
	CPI  R30,LOW(0xD4)
	BRNE _0x5B
	__POINTW2MN _secure,2
	CALL __EEPROMRDB
	CPI  R30,LOW(0xE9)
	BREQ _0x5D
_0x5B:
	SBIC 0x19,0
	JMP  _0x5A
_0x5D:
;     619    {
;     620   #asm(".EQU SpmcrAddr=0x68")//57
	.EQU SpmcrAddr=0x68
;     621   PageAddress=page_address;
	__GETWRS 4,5,0
;     622  for (i=0;i<page_byte;i+=2)
	CLR  R11
	CLR  R12
_0x60:
	LDD  R30,Y+2
	__GETW2R 11,12
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRLO PC+3
	JMP _0x61
;     623   {
;     624   PageData=data_buffer[i+1];
	__GETW1R 11,12
	__ADDW1MN _data_buffer,1
	LD   R2,Z
	CLR  R3
;     625   PageData=(PageData<<8)|data_buffer[i];
	MOV  R31,R2
	LDI  R30,LOW(0)
	MOVW R0,R30
	LDI  R26,LOW(_data_buffer)
	LDI  R27,HIGH(_data_buffer)
	ADD  R26,R11
	ADC  R27,R12
	CALL SUBOPT_0x5
	MOVW R2,R30
;     626   while(SPMCSR&1) clr_wdt();
_0x62:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x64
	wdr
	JMP  _0x62
_0x64:
;     627   CurrentAddress=page_address+i;
	__GETW1R 11,12
	LD   R26,Y
	LDD  R27,Y+1
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
;     628   spmcrval=1;
	LDI  R30,LOW(1)
	MOV  R10,R30
;     629   #asm
;     630   movw  r30,r6
  movw  r30,r6
;     631   mov   r1,r3
  mov   r1,r3
;     632   mov   r0,r2
  mov   r0,r2
;     633   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
;     634   spm
  spm
;     635   #endasm
;     636   }
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	__ADDWRR 11,12,30,31
	JMP  _0x60
_0x61:
;     637 
;     638 
;     639   while(SPMCSR&1) clr_wdt();
_0x65:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x67
	wdr
	JMP  _0x65
_0x67:
;     640   clr_wdt();
	wdr
;     641   if ((PageAddress%256)==0)
	MOVW R30,R4
	ANDI R31,HIGH(0xFF)
	SBIW R30,0
	BREQ PC+3
	JMP _0x68
;     642     {
;     643   spmcrval=3;
	LDI  R30,LOW(3)
	MOV  R10,R30
;     644   #asm
;     645   movw  r30,r4
  movw  r30,r4
;     646   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
;     647   spm
  spm
;     648   #endasm                     // erase page
;     649     }
;     650 
;     651   while(SPMCSR&1) clr_wdt();
_0x68:
_0x69:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x6B
	wdr
	JMP  _0x69
_0x6B:
;     652   clr_wdt();
	wdr
;     653   spmcrval=5;
	LDI  R30,LOW(5)
	MOV  R10,R30
;     654   #asm
;     655   movw  r30,r4
  movw  r30,r4
;     656   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
;     657   spm
  spm
;     658   #endasm
;     659 
;     660   while(SPMCSR&1) clr_wdt();
_0x6C:
	LDS  R30,104
	ANDI R30,LOW(0x1)
	BREQ _0x6E
	wdr
	JMP  _0x6C
_0x6E:
;     661   clr_wdt();
	wdr
;     662   spmcrval=0x11;
	LDI  R30,LOW(17)
	MOV  R10,R30
;     663   #asm
;     664   sts   SpmcrAddr,r10
  sts   SpmcrAddr,r10
;     665   spm
  spm
;     666   #endasm
;     667 
;     668   if (check_flesh(page_byte,page_address))  {putchar('O');return(0);}
	LDD  R30,Y+2
	ST   -Y,R30
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _check_flesh
	CPI  R30,0
	BREQ _0x6F
	LDI  R30,LOW(79)
	ST   -Y,R30
	CALL _putchar
	LDI  R30,LOW(0)
	RJMP _0x84
;     669     else putchar('F');
_0x6F:
	LDI  R30,LOW(70)
	ST   -Y,R30
	CALL _putchar
;     670     return (1);
	LDI  R30,LOW(1)
	RJMP _0x84
;     671   }
;     672  return ('S');
_0x5A:
	LDI  R30,LOW(83)
_0x84:
	ADIW R28,3
	RET
;     673 
;     674 }
;     675 
;     676 
;     677 
;     678 
;     679 
;     680 //***************************** DEBUG PUTCHAR **********************************
;     681 void debug_putchar(unsigned char dat_tx)
;     682  {
_debug_putchar:
;     683 
;     684                 #asm("cli");
;	dat_tx -> Y+0
	cli
;     685                 reg_dat=dat_tx;
	LDD  R8,Y+0
;     686 
;     687 #asm               	cli
;     688 			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
			ldi	bitcnt,9+sb	;1+8+sb (sb is # of stop bits)
;     689         		com	R8		;Inverte everything
        		com	R8		;Inverte everything
;     690         		sec			;Start bit
        		sec			;Start bit
;     691 

;     692 putchar0:   	brcc	putchar1	;If carry set
putchar0:   	brcc	putchar1	;If carry set
;     693 		        cbi	    PORTE,TxD	;    send a '0'
		        cbi	    PORTE,TxD	;    send a '0'
;     694 		        rjmp	putchar2	;else
		        rjmp	putchar2	;else
;     695 

;     696 putchar1:	    sbi	    PORTE,TxD	;    send a '1'
putchar1:	    sbi	    PORTE,TxD	;    send a '1'
;     697 		        nop
		        nop
;     698 

;     699 putchar2:	    ldi	    temp,b
putchar2:	    ldi	    temp,b
;     700 UART_delay1:	dec	    temp
UART_delay1:	dec	    temp
;     701 		        brne	UART_delay1
		        brne	UART_delay1
;     702 

;     703         	    ldi	    temp,b
        	    ldi	    temp,b
;     704 UART_delay2:	dec 	temp
UART_delay2:	dec 	temp
;     705 		        brne	UART_delay2
		        brne	UART_delay2
;     706 

;     707         		lsr	R8		;Get next bit
        		lsr	R8		;Get next bit
;     708         		dec	    bitcnt		;If not all bit sent
        		dec	    bitcnt		;If not all bit sent
;     709         		brne	putchar0	;   send next
        		brne	putchar0	;   send next
;     710         		sei
        		sei
;     711  #endasm
;     712  };
	ADIW R28,1
	RET
;     713 
;     714 
;     715 
;     716 
;     717 
;     718 //****************************** DEBUG GETCHAR ********************************
;     719 unsigned char debug_getchar(void)
;     720  {
_debug_getchar:
;     721  D_RxD=0;
	CLT
	BLD  R2,0
;     722  while (D_RxD==0) {
_0x71:
	SBRC R2,0
	RJMP _0x73
;     723                     clr_wdt();
	wdr
;     724                     while (boot_select==1);
_0x74:
	SBIC 0x19,0
	RJMP _0x74
;     725                     }
	JMP  _0x71
_0x73:
;     726  return (debug_RxD_buff);
	LDS  R30,_debug_RxD_buff
	RET
;     727  };
;     728 
;     729 
;     730 //********************************* DEBUG PUTSTRING **************************************
;     731 void debug_putstring(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
;     732  {
_debug_putstring:
;     733   unsigned char couter_tmp;
;     734   clr_wdt();
	ST   -Y,R17
;	*text -> Y+4
;	X -> Y+3
;	Y -> Y+2
;	color -> Y+1
;	couter_tmp -> R17
	wdr
;     735    if (X<255) {
	LDD  R26,Y+3
	CPI  R26,LOW(0xFF)
	BRSH _0x77
;     736             if (color){
	LDD  R30,Y+1
	CPI  R30,0
	BREQ _0x78
;     737                         debug_putchar(0x1B);
	RCALL SUBOPT_0x4
;     738                         debug_putchar(0x5B);
;     739                         debug_putchar(0x30+color);
	LDD  R30,Y+1
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
;     740                         debug_putchar(0x6D);
;     741                         }
;     742 
;     743               debug_putchar(0x1B);
_0x78:
	RCALL SUBOPT_0x4
;     744               debug_putchar(0x5B);
;     745               if (Y<=10) debug_putchar(0x30+Y);
	LDD  R26,Y+2
	CPI  R26,LOW(0xB)
	BRSH _0x79
	LDD  R30,Y+2
	RCALL SUBOPT_0x6
	RJMP _0x89
;     746                 else debug_putchar(0x30+10);
_0x79:
	LDI  R30,LOW(58)
_0x89:
	ST   -Y,R30
	CALL _debug_putchar
;     747               debug_putchar(0x03B);
	LDI  R30,LOW(59)
	ST   -Y,R30
	CALL _debug_putchar
;     748               debug_putchar(0x30+(X/10));
	LDD  R26,Y+3
	LDI  R30,LOW(10)
	CALL __DIVB21U
	RCALL SUBOPT_0x6
	ST   -Y,R30
	CALL _debug_putchar
;     749               debug_putchar(0x30+(X%10));
	LDD  R26,Y+3
	LDI  R30,LOW(10)
	CALL __MODB21U
	RCALL SUBOPT_0x6
	ST   -Y,R30
	CALL _debug_putchar
;     750               debug_putchar(0x48);
	LDI  R30,LOW(72)
	ST   -Y,R30
	CALL _debug_putchar
;     751               couter_tmp=0;
	LDI  R17,LOW(0)
;     752               if (Y>10)  while(++couter_tmp<(Y-9)) debug_putchar(10);
	LDD  R26,Y+2
	CPI  R26,LOW(0xB)
	BRLO _0x7B
_0x7C:
	SUBI R17,-LOW(1)
	LDD  R30,Y+2
	SUBI R30,LOW(9)
	CP   R17,R30
	BRSH _0x7E
	LDI  R30,LOW(10)
	ST   -Y,R30
	CALL _debug_putchar
;     753                }
	RJMP _0x7C
_0x7E:
_0x7B:
;     754         while (*text!=0) {debug_putchar(*text++);}
_0x77:
_0x7F:
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	LPM  R30,Z
	CPI  R30,0
	BREQ _0x81
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	ADIW R30,1
	STD  Y+4,R30
	STD  Y+4+1,R31
	SBIW R30,1
	LPM  R30,Z
	ST   -Y,R30
	CALL _debug_putchar
	RJMP _0x7F
_0x81:
;     755     if (X<255){
	LDD  R26,Y+3
	CPI  R26,LOW(0xFF)
	BRSH _0x82
;     756                 if (color)
	LDD  R30,Y+1
	CPI  R30,0
	BREQ _0x83
;     757                           {
;     758                           debug_putchar(0x1B);
	RCALL SUBOPT_0x4
;     759                           debug_putchar(0x5B);
;     760                           debug_putchar(0x30);
	LDI  R30,LOW(48)
	RCALL SUBOPT_0x7
;     761                           debug_putchar(0x6D);
;     762                           }
;     763                 }
_0x83:
;     764 
;     765   };
_0x82:
	LDD  R17,Y+0
	ADIW R28,6
	RET


;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x0:
	SWAP R30
	ANDI R30,0xF0
	MOV  R26,R30
	MOV  R30,R17
	ANDI R30,LOW(0xF)
	LDI  R31,0
	OR   R30,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x1:
	STS  _program_address,R30
	STS  _program_address+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x2:
	LDS  R30,_program_address
	LDS  R31,_program_address+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:23 WORDS
SUBOPT_0x3:
	CALL __LSLW4
	MOVW R0,R30
	MOV  R26,R17
	LDI  R27,0
	LDI  R30,LOW(15)
	LDI  R31,HIGH(15)
	AND  R30,R26
	AND  R31,R27
	OR   R30,R0
	OR   R31,R1
	RJMP SUBOPT_0x1

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x4:
	LDI  R30,LOW(27)
	ST   -Y,R30
	CALL _debug_putchar
	LDI  R30,LOW(91)
	ST   -Y,R30
	JMP  _debug_putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x5:
	LD   R30,X
	LDI  R31,0
	OR   R30,R0
	OR   R31,R1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x6:
	LDI  R31,0
	ADIW R30,48
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x7:
	ST   -Y,R30
	CALL _debug_putchar
	LDI  R30,LOW(109)
	ST   -Y,R30
	JMP  _debug_putchar

_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x733
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__LSLW4:
	LSL  R30
	ROL  R31
__LSLW3:
	LSL  R30
	ROL  R31
__LSLW2:
	LSL  R30
	ROL  R31
	LSL  R30
	ROL  R31
	RET

__DIVB21U:
	CLR  R0
	LDI  R25,8
__DIVB21U1:
	LSL  R26
	ROL  R0
	SUB  R0,R30
	BRCC __DIVB21U2
	ADD  R0,R30
	RJMP __DIVB21U3
__DIVB21U2:
	SBR  R26,1
__DIVB21U3:
	DEC  R25
	BRNE __DIVB21U1
	MOV  R30,R26
	MOV  R26,R0
	RET

__MODB21U:
	RCALL __DIVB21U
	MOV  R30,R26
	RET

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

;END OF CODE MARKER
__END_OF_CODE:
