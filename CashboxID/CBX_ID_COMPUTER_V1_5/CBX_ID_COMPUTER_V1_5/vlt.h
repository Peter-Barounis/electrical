/*----------------------------------------------------------------------------\
|  Src File:   vlt.h                                                          |
|  Authored:   10/13/97, tgh                                                  |
|  Function:   Defines & prototypes for to interface to vault.                |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   10/13/97, tgh  -  Initial release.                             |
|      1.01:   09/22/98, tgh  -  Added VS_ flag defines.                      |
|      1.02:   02/17/99, tgh  -  Added CBN typedef in order to process        |
|                                version 4/6 ID lookup files.                 |
|      1.03:   01/20/09, tgh  -  Added flags to support lock-out.             |
|                                                                             |
|           Copyright (c) 1993-1999  GFI/USPS All Rights Reserved             |
\----------------------------------------------------------------------------*/
#ifndef  _VLT_H
#define  _VLT_H

#include <windows.h>

#include "gen.h"

/* Typedefs
*/
typedef union bcn_map
{
  struct
  {
    unsigned long id:12        ; /* cashbox/bin i.d. number   */
    unsigned long sn:18        ; /* cashbox/bin serial number */
    unsigned long nb: 1        ; /* no bin at start-up        */
    unsigned long sp: 1        ; /* spare                     */
  }               m            ;
  unsigned long   w            ;
} CBN;


/* Defines
*/
   /* Vault action types stored in
      table name "vlt" and "vs"
      column name "type".
      These also corrispond to the
      message types received from
      the vault interface computer.
   */
#define  FBXPRB            1                    /* farebox probing */
#define  CBXIN             2                    /* cashbox inserted */
#define  CBXOUT            3                    /* cashbox removed */
#define  BININ             4                    /* BIN inserted */
#define  BINOUT            5                    /* BIN removed */
#define  CBXSTAT           6                    /* CBX status */
#define  DOOR_OPEN         12                   /* door open */
#define  DOOR_CLOSED       13                   /* door closed */

   /* Vault status flags stored in
      table name "vs"
      column name "flags".
      This field type is a signed 2-byte
      int so bit-15 should not be used.
   */
#define  VS_BININ          (word)0x0001         /* BIN in vault */
#define  VS_CBXIN          (word)0x0002         /* cashbox in */
#define  VS_BINNEARFULL    (word)0x0004         /* BIN is near full */
#define  VS_BINFULL        (word)0x0008         /* BIN is full */
#define  VS_EMPLOGON       (word)0x0010         /* employee logged on */
#define  VS_VLTONLINE      (word)0x0020         /* cbxid  interface on-line */
#define  VS_VLTLCKONLINE   (word)0x0040         /* vltlck interface on-line */
#define  VS_0080           (word)0x0080         /*  */
#define  VS_LOCKSTATE      (word)0x0100         /* lock state */
#define  VS_LOCKSENSOR     (word)0x0200         /* lock sensor */
#define  VS_LOCKOVERRIDE   (word)0x0400         /* lock override */
#define  VS_LOCK           (word)0x0800         /* lock */
#define  VS_DOOR           (word)0x1000         /* door is open */
#define  VS_SHROUD         (word)0x2000         /* shroud disabled */
#define  VS_4000           (word)0x4000         /* */
#define  VS_8000           (word)0x8000         /* */

#if 0

/* example macros for the flags above. */
#define  SetVltFlag( f, s )   ((f) |= (s))
#define  ClrVltFlag( f, s )   ((f) &=~(s))
#define  TstVltFlag( f, s )   ((f) &  (s))

#endif

/* Prototypes
*/
int   InitializeVltProtocol   ( HWND, HANDLE );
int   InitializeVltData       ( HWND );
int   ParseVlt                ( byte*, int );

#endif /*_VLT_H*/

