/*----------------------------------------------------------------------------\
|  Src File:   comm_v03.c                                                     |
|  Authored:   10/13/97, tgh                                                  |
|  Function:   Communication module for vault.                                |
|  Comments:                                                                  |
|                                                                             |
|  Revision:                                                                  |
|      1.00:   10/13/97, tgh  -  Initial release.                             |
|      2.00:   04/06/00, tgh  -  Added protocol to support lock-out if enabled|
|      3.00:   11/14/05, tgh  -  Added vltflags.                              |
|                                                                             |
|              Copyright (c) 1993-2005  GFI All Rights Reserved               |
\----------------------------------------------------------------------------*/
/* system headers
*/
#include <stdio.h>
#include <time.h>

/* windows headers
*/
#include <windows.h>

/* database headers
*/
#include <sqlerr.h>

EXEC SQL include sqlca;

/* project headers
*/
#include <init.h>
#include <tlog.h>
#include <gen.h>
#include <vlt.h>
#include <vltflags.h>
#include <timers.h>

/* Defines and typedefs
*/
#define  _DEBUG_COMM
#define  SECONDS( t )      ((t)*10)



   /* Protocol defines */
#define  STX                  0x02              /* start of text */
#define  ETX                  0x03              /* end of text */
#define  EOT                  0x04              /* end of transmission */
#define  ENQ                  0x05              /* enquiry */
#define  ACK                  0x06              /* acknowllege */
#define  NAK                  0x15              /* negative acknowllege */
#define  SYN                  0x16              /* start of msg. from D.S. */
#define  MAX_RETRIES          4                 /* protocol retries */
#define  RSPLEN               6                 /* response data length */
#define  T1                   5                 /* response time .5 sec. */
#define  T3                   SECONDS(2)        /* offline time (2 sec.) */
#define  POLLTIME             SECONDS(1)

#define  RX_BUF_SZ         256                  /* read buffer size */
#define  TX_BUF_SZ         256                  /* transmit buffer size */
#define  Q_BUF_SZ          256                  /* queue buffer size */
#define  MAXMSG            16                   /* max. queued messages */
#define  TXBUF_SIZE        MAXMSG*TX_BUF_SZ

/* Flags contained in "Flags" and macros to manipulate them.
*/
#define  F_OFFLINE         0x0001               /* vault computer offline */
#define  F_V1_FULL         0x0100               /* vault 1 full */
#define  F_V2_FULL         0x0200               /* vault 2 full */
#define  F_V3_FULL         0x0400               /* vault 3 full */
#define  F_V4_FULL         0x0800               /* vault 4 full */

typedef  struct
{
   byte        b[Q_BUF_SZ];                     /* message buffer */
   int         sz;                              /* message size */
}  BUF;

typedef  struct
{
   BUF         q[MAXMSG];                       /* message queue */
   int         next;                            /* index to next */
   int         last;                            /* index to last */
   int         cnt;                             /* count of messages queued */
}  QUE;


/* Macros
*/
#define  RstNdx()          {Ndx = 0; RxBuf=RxQ.q[RxQ.next].b;}
#define  SeedChkSum( c )   (ChkSum  = (c))
#define  AppendChkSum( c ) (ChkSum += (c))
#define  NextRx( fptr )    (RxState = (fptr))
#define  SetFlag( s )      (Flags |= (s))
#define  ClrFlag( s )      (Flags &=~(s))
#define  TstFlag( s )      (Flags &  (s))
#define  uncharToBCD(x)    ( (unsigned char) (((x) / 10) << 4) + ((x) % 10 ) )

/* Prototypes
*/
void  swabl                ( char*, char*, int );
void  IdleState            ( void );
byte  CalcChkSum           ( byte*, int );
byte  IntToBCDunchar       ( word );
int   Nq                   ( QUE* );
int   Dq                   ( QUE* );
int   Putc                 ( byte );
int   Puts                 ( byte* );
int   Nputs                ( byte*, int );
int   ProcFrame            ( void );
int   Frame                ( byte, byte, BUF*, byte );
void  Poll                 ( void );
void  RtoFnct              ( void );
void  RxIdle               ( byte );            /* receive state prototypes */
void  GetETX               ( byte );
void  GetData              ( byte );
void  GetChkSum            ( byte );
int   SetVSFlag            ( int, word );
int   ClrVSFlag            ( int, word );


/* external data
*/
extern   char  szProgName[];
extern   int   LocationNumber;
extern   int   VltNo;
extern   int   LockOut;

/* module data
*/
static   HANDLE      CommDev;                   /* communication device ID */
static   HWND        hWnd;                      /* window handle */
static   OVERLAPPED  ol;
static   int         Rto;                       /* response timer */
static   int         Sto;                       /* session timer inter-char. */
static   int         PollTmr;                   /* poll timer               */

static   int         NackCnt        =  0;
static   int         PollTimeoutCnt =  0;
static   int         Retry;

static   void      (*RxState)();                /* receive state */
static   QUE         TxQ;                       /* tx message queue */
static   QUE         RxQ;                       /* rx message queue */

static   int         RxCnt;                     /* No. of data bytes to get */
static   byte        ChkSum;                    /* Current calc. checksum */

static   byte        TxBuf[ TXBUF_SIZE ];       /* Transmit Buffer */
static   int         TxNdx =  0;                /* Index into the tx buffer */
static   byte       *RxBuf;                     /* Pointer to receive buffer */
static   int         Ndx   =  0;                /* index into receive buffer */
static   word        Flags =  0;                /* maintained by macros below*/

/*----------------------------------------------------------------------------\
|                                                                             |
|              USER CALLABLE FUNCTIONS                                        |
|                                                                             |
\----------------------------------------------------------------------------*/

int   InitializeVltProtocol( w, dev )
HWND                         w;
HANDLE                          dev;
{
   int   i;

   hWnd     =  w;
   CommDev  =  dev;
   memset( (char*)&ol, 0, sizeof(OVERLAPPED) );
   ol.hEvent=  CreateEvent( NULL, TRUE, FALSE, NULL );
   Rto      =  MkTmr();
   Sto      =  MkTmr();
   PollTmr  =  MkTmr();
   memset( (char*)&TxQ, 0, sizeof(TxQ) );
   memset( (char*)&RxQ, 0, sizeof(RxQ) );
   Flags =  F_OFFLINE;
   SetFlag( F_OFFLINE );
   for( i=0; i<4; i++ ) ClrVSFlag( (VltNo*4)+i+1, VS_VLTONLINE );
   strcpy( _Shm->dsstat.vlt[VltNo].text, "Offline" );
   SetDSFlag( (VltNo==0) ? DSF_VLT1:DSF_VLT2 );
   IdleState();
   InitTmr( PollTmr, Poll, POLLTIME );
   return 0;
}


/*----------------------------------------------\
|  Function:   QFrame();                        |
|  Purpose :   Queue up data frame.             |
|  Synopsis:   void  QFrame( t, bn, bs, p, n ); |
|  Input   :   int     t - frame type           |
|              int     bn- block count/number   |
|              long    bs- block/total size     |
|              unchar *p - data pointer         |
|              int     n - data bytes           |
|  Output  :   int -1 - errors                  |
|              int >=0 - no. of frames pending  |
|  Comments:   High level function to queue up  |
|              frames of data using the HDR.    |
|              Note that the bn and bs fields   |
|              have different meanings depending|
|              on context of frame type.  The   |
|              word and long fields in bn & bs  |
|              are converted to motorola format.|
\----------------------------------------------*/
int   QFrame( t, bn, bs, p, n )
int           t, bn,        n;
long                 bs;
byte                    *p;
{
   return 0;
}

/*----------------------------------------------\
|  Function:   QFrameRaw();                     |
|  Purpose :   Queue up data frame.             |
|  Synopsis:   void  QFrameRaw( p, n );         |
|  Input   :   unchar *p - data pointer         |
|              int     n - data bytes           |
|  Output  :   int -1 - errors                  |
|              int >=0 - no. of frames pending  |
|  Comments:   Queue up a block of data of any  |
|              type not dependant on the use of |
|              HDR struct.                      |
\----------------------------------------------*/
int   QFrameRaw( p, n )
byte            *p;
int                 n;
{
   return 0;
}

int   DqFrame()
{
   return( Dq( &RxQ ) );
}

int   TxQCnt()
{
   return TxQ.cnt;
}

int   RxQCnt()
{
   return RxQ.cnt;
}

int   GetFrame( p )
byte          **p;
{
   int   n;

   if ( (n=RxQ.cnt) )
   {
      if ( p )
         *p = RxQ.q[RxQ.last].b;
      n  = RxQ.q[RxQ.last].sz;
   }

   return n;
}

int   ParseVlt( s, n )
byte           *s;
int                n;
{
   int   i;

   #ifdef _DEBUG_COMM
   if ( TstVltFlag( VLTF_DEBUG_PROTOCOL ) )
   {
      Tlog( "RX : " );
      TlogHexDump( s, n );
      Tlograw( "\n" );
   }
   #endif

   for( i=0; i<n; i++ )
   {
      if ( Ndx >= RX_BUF_SZ )
         IdleState();
      if ( RxState )
         (*RxState)( s[i] );
   }

   return 0;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|              LOCAL FUNCTIONS                                                |
|                                                                             |
\----------------------------------------------------------------------------*/

static
void  OnLine()
{
   int   i;

   if ( TstFlag( F_OFFLINE ) )
   {
      ClrFlag( F_OFFLINE );
      for( i=0; i<4; i++ ) SetVSFlag( (VltNo*4)+i+1, VS_VLTONLINE );
      strcpy( _Shm->dsstat.vlt[VltNo].text, "Online " );
      SetDSFlag( (VltNo==0) ? DSF_VLT1:DSF_VLT2 );
      Tlog( "%s: online\n", szProgName );
   }
}


static
int   BumpQndx( p )
int            *p;
{
   if ( *p < MAXMSG-1 )
      (*p)++;
   else
      *p = 0;

   return *p;
}

static
int   Nq( p )
QUE      *p;
{
   BumpQndx( &p->next );

   if ( p->cnt < MAXMSG )                       /* bump message count */
      p->cnt++;
   else
      BumpQndx( &p->last );

   return 0;
}

static
int   Dq( p )
QUE      *p;
{
   int   E = -1;

   p->q[p->last].sz = 0;                        /* set the size to zero */

   if ( p->cnt <= 0 )
   {
      p->cnt = 0;
      goto function_end;
   }

   p->cnt--;
   BumpQndx( &p->last );

   if ( p == &TxQ )                             /* bump S if transmit queue */
   {
      //IncSeq( S );
   }

   function_end:
   return p->cnt;
}

int   TxDq()
{
   int   E = -1;

   TxQ.q[TxQ.last].sz = 0;                      /* set the size to zero */

   if ( TxQ.cnt <= 0 )
   {
      TxQ.cnt = 0;
      goto function_end;
   }

   TxQ.cnt--;
   BumpQndx( &TxQ.last );

   function_end:
   return TxQ.cnt;
}

static
int   Putc( byte c )
{
   DWORD bw;

   #ifdef _DEBUG_COMM
   if ( TstVltFlag( VLTF_DEBUG_PROTOCOL ) )
   {
      Tlog( "TX : %02x\n", c );
   }
   #endif

   return WriteFile( CommDev, &c, 1, &bw, &ol );
}

static
int   Puts( s )
byte       *s;
{
   int   bw, i;
   int   n = lstrlen( s );

   #ifdef _DEBUG_COMM
   if ( TstVltFlag( VLTF_DEBUG_PROTOCOL ) )
   {
      Tlog( "TX : " );
      TlogHexDump( s, n );
      Tlograw( "\n" );
   }
   #endif

   return WriteFile( CommDev, s, n, &bw, &ol );
}

static
int   Nputs( s, n )
byte        *s;
int             n;
{
   int   bw,i;

   #ifdef _DEBUG_COMM
   if ( TstVltFlag( VLTF_DEBUG_PROTOCOL ) )
   {
      Tlog( "TX : " );
      TlogHexDump( s, n );
      Tlograw( "\n" );
   }
   #endif

   return WriteFile( CommDev, s, n, &bw, &ol );
}

static
void  IdleState()
{
   NextRx( RxIdle );
   RstNdx();
   LoadTmr( PollTmr, POLLTIME );
}

static
int   ProcFrame()
{
   if ( TstVltFlag( VLTF_DEBUG_FRAMES ) )
   {
      Tlog( "%s: ProcFrame :\n", szProgName );
      TlogHexDump( RxBuf, Ndx );
      Tlograw( "\n" );
   }

   RxQ.q[RxQ.next].sz = Ndx;                    /* save no. of bytes received */
   Nq( &RxQ );                                  /* queue up frame received */

   return 0;
}

/*----------------------------------------------\
|  Function:   Poll()                           |
|  Purpose :   poll vault computer.             |
|  Synopsis:   void  Poll( void );              |
|  Input   :   None.                            |
|  Output  :   None.                            |
|  Comments:                                    |
|     Enquiry without "lock-out" feature:       |
|        6-BYTE ENQUIRY  FORMAT:                |
|                                               |
|        ENQ HH MM SS ETX BCC                   |
|                                               |
|                                               |
|     Enquiry with "lock-out" feature:          |
|        10-BYTE ENQUIRY  FORMAT:               |
|                                               |
|        ENQ HH MM SS L1 L2 L3 L4 ETX BCC       |
|                                               |
|           where:                              |
|              Ln - (0-open, 1-locked)          |
|                                               |
\----------------------------------------------*/
static
void  Poll()
{
   static   word     pollcnt  =  0;
   static   time_t   lasttime =  0;
   byte        s[32];
   time_t      t;
   struct tm  *tm;

   time( &t );

   if ( labs( t - lasttime ) >= 5 )             /* force update every 5 sec. */
   {
      lasttime = t;
      SetDSFlag( (VltNo==0) ? DSF_VLT1:DSF_VLT2 );
   }

   tm    =  localtime( &t );

   switch( LockOut )
   {
      case  2:                                  /* Detroit shroud interface lock-out enabled */
         s[0]  =  SYN;
         s[1]  =  IntToBCDunchar( (word)tm->tm_hour );
         s[2]  =  IntToBCDunchar( (word)tm->tm_min );
         s[3]  =  IntToBCDunchar( (word)tm->tm_sec );
         s[4]  =  TstFlag( F_V1_FULL ) ? 1:0;   /* vault 1 lock flag */
         s[5]  =  TstFlag( F_V2_FULL ) ? 1:0;   /* vault 2 lock flag */
         s[6]  =  TstFlag( F_V3_FULL ) ? 1:0;   /* vault 3 lock flag */
         s[7]  =  TstFlag( F_V4_FULL ) ? 1:0;   /* vault 4 lock flag */
         s[8]  =  ETX;
         s[9]  =  CalcChkSum( s+1, 7 );
         Nputs( s, 10 );
         break;
      case  3:                                  /* CTA shroud interface enabled */
         s[0]  =  SYN;
         s[1]  =  IntToBCDunchar( (word)tm->tm_hour );
         s[2]  =  IntToBCDunchar( (word)tm->tm_min );
         s[3]  =  IntToBCDunchar( (word)tm->tm_sec );
         s[4]  =  TstVSFlag( 1, VS_SHROUD );    /* shroud #1 enable(0)/disable(1) */
         s[5]  =  TstVSFlag( 2, VS_SHROUD );    /* shroud #2 enable(0)/disable(1) */
         s[6]  =  TstVSFlag( 3, VS_SHROUD );    /* shroud #3 enable(0)/disable(1) */
         s[7]  =  TstVSFlag( 4, VS_SHROUD );    /* shroud #4 enable(0)/disable(1) */
         s[8]  =  0;
         s[9]  =  0;
         s[10] =  0;
         s[11] =  0;
         s[12] =  ETX;
         s[13] =  CalcChkSum( s+1, 11 );
         Nputs( s, 14 );
         break;
      default:
         s[0]  =  SYN;
         s[1]  =  IntToBCDunchar( (word)tm->tm_hour );
         s[2]  =  IntToBCDunchar( (word)tm->tm_min );
         s[3]  =  IntToBCDunchar( (word)tm->tm_sec );
         s[4]  =  ETX;
         s[5]  =  CalcChkSum( s+1, 3 );
         Nputs( s, 6 );
         break;
   }

   InitTmr( Rto, RtoFnct, T1 );
}


/*-------------------------------------------------------------\
|  Function:   Frame( S, R, p, f );                            |
|  Purpose :   Put queued up frames into the transmit buffer.  |
|  Synopsis:   int error = frame( S, R, p, f );                |
|              p - (BUF *) points to the data                  |
|              S - (byte) send sequence number (N(s))          |
|              R - (byte) receive sequence number (N(r))       |
|              f - (byte) final frame flag                     |
|  Return  :                                                   |
|                                                              |
|  Comments:   The "lrc" will be calculated as well as the     |
|              mapping of the N(s), N(r) sequence numbers.     |
\-------------------------------------------------------------*/
static
int   Frame( S, R, p, f )
byte         S, R,    f;
BUF               *p;
{
   return 0;
}

static
int   SendFrames()
{
   return 0;
}

static
byte  CalcChkSum( p, n )
byte             *p;
int                  n;
{
   int   i;
   byte  cs=0;

   for( i=0; i<n; i++ )
     cs += *(p+i);

   return( cs );
}

static
byte  IntToBCDunchar( w )
word                  w;
{
   byte  result   =  0,
         n        =  (byte)w;

   if ( n > 99 ) goto function_end;

   result   =  n % 10;
   n       /=  10;
   result  |=  (n%10) << 4;

   function_end:
   return result;
}

/*----------------------------------------------------------------------------\
|                                                                             |
|                             TIMER FUNCTIONS                                 |
|                                                                             |
\----------------------------------------------------------------------------*/
void  StoFnct()
{
   IdleState();
}

void  RtoFnct()
{
   int   i;

   if ( ++PollTimeoutCnt > MAX_RETRIES )
   {
      if ( !TstFlag( F_OFFLINE ) )
      {
         SetFlag( F_OFFLINE );
         for( i=0; i<4; i++ ) ClrVSFlag( (VltNo*4)+i+1, VS_VLTONLINE );
         strcpy( _Shm->dsstat.vlt[VltNo].text, "Offline" );
         SetDSFlag( (VltNo==0) ? DSF_VLT1:DSF_VLT2 );
         Tlog( "%s: offline\n", szProgName );
      }
      PollTimeoutCnt = 0;
   }
   IdleState();
}


/*
         E )   VAULT INTERFACE I/O PROTOCOL

               6-BYTE ENQUIRY  FORMAT:

                 ENQ HH MM SS ETX BCC

               9-BYTE RESPONSE FORMAT:

                 STX CT HH MM SS HI LO ETX BCC , where

                 1)  The high order nibble of CT is the channel number
                 2)  The low  order nibble of CT is message type
                 3)  HI / LO  refers to a packed bcd integer ID number

               In both cases, BCC is an 8-bit checksum of the data, i.e.,
               excluding ENQ/STX and ETX

               All data is formatted in packed bcd.

*/

/*----------------------------------------------------------------------------\
|                                                                             |
|                             RECEIVE STATES                                  |
|                                                                             |
\----------------------------------------------------------------------------*/

static
void  RxIdle( c )
byte          c;
{
   StopTmr( Rto );
   PollTimeoutCnt = 0;
   LoadTmr( PollTmr, POLLTIME );

   switch( c )
   {
      case  STX:
         SeedChkSum( 0 );
         RstNdx();
         switch( LockOut )
         {
            case  3:
               RxCnt = RSPLEN+2;
               break;
            default:
               RxCnt = RSPLEN;
               break;
         }
         NextRx( GetData );
         InitTmr( Sto, StoFnct, T3 );
         break;
      case  ENQ:
         Putc( ACK );
         break;
      case  ACK:
      case  EOT:
         OnLine();
         break;
      default:
         break;
   }
}

static
void  GetData( c )
byte           c;
{
   AppendChkSum( c );
   RxBuf[Ndx++] = c;

   if ( --RxCnt <= 0 )
      NextRx( GetETX );
}

static
void  GetETX( c )
byte          c;
{
   switch( c )
   {
      case  ETX:
         NextRx( GetChkSum );
         break;
      default:
         Tlog( "vault received %02x while expecting ETX\n", c );
         Putc( NAK );
         IdleState();
         break;
   }
}

static
void  GetChkSum( c )
byte             c;
{
   static   word  respcnt=1;

   StopTmr( Sto );

   if ( c == ChkSum )
   {
      Putc( ACK );
      OnLine();
      ProcFrame();
   }
   else
   {
      Tlog( "%02x != %02x\n", c, ChkSum );
      Putc( NAK );
   }
   IdleState();
}

