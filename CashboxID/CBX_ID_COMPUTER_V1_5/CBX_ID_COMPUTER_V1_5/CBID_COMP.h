


#define DE                  PORTC.7
#define Sys_Led             PORTD.4
#define Err_Led             PORTD.5
#define Led_Power           PORTD.6
#define Service_Led         PORTD.7
#define Link_Led1           PORTC.5
#define Link_Led2           PORTC.6



#define ENQ     0x16
#define STX     0x02
#define ETX     0x03
#define EOT     0x04
#define ACK     0x06
#define NAK     0x15


#define VAULT   0
#define BIN     1
#define CBX     2


#define CBX_IN          2
#define CBX_OUT         3
#define BIN_IN          4
#define BIN_OUT         5
#define CBX_ID_ERROR    6
#define BIN_STATUS      7



#define DEBOUNCE_VAULT   1
#define DEBOUNCE_BIN     150
#define DEBOUNCE_CBX     3
#define CBX_TIMEOUT      5000

#define OFFLINE_TIME     4000

#define DATA_IN_EEPROM     0x01
#define NO_DATA_IN_EEPROM  0x00

#define READ      0x00
#define ERASE     0x01

#define ADC_VREF_TYPE 0x00

#define Power_Min 115
#define Power_Max 250

#define Vcc_Min   450
#define Vcc_Max   550

#define V_ISO_OK  500

