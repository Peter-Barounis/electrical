
;CodeVisionAVR C Compiler V2.05.3a Standard
;(C) Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega16M1
;Program type             : Application
;Clock frequency          : 14.745600 MHz
;Memory model             : Small
;Optimize for             : Speed
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 384 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : No
;Global 'const' stored in FLASH     : Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions         : On
;Smart register allocation          : On
;Automatic register allocation      : On

	#pragma AVRPART ADMIN PART_NAME ATmega16M1
	#pragma AVRPART MEMORY PROG_FLASH 16384
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1279
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1F
	.EQU EEDR=0x20
	.EQU EEARL=0x21
	.EQU EEARH=0x22
	.EQU SMCR=0x33
	.EQU MCUSR=0x34
	.EQU MCUCR=0x35
	.EQU WDTCSR=0x60
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU GPIOR0=0x1E
	.EQU GPIOR1=0x19
	.EQU GPIOR2=0x1A

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0100
	.EQU __SRAM_END=0x04FF
	.EQU __DSTACK_SIZE=0x0180
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _Rx1_Counter=R3
	.DEF _Rx1_Pointer_Buff=R2
	.DEF _Tmp_Rx1=R5
	.DEF _Time_Out_1=R4
	.DEF _Rx2_Counter=R7
	.DEF _Rx2_Pointer_Buff=R6
	.DEF _Tmp_Rx2=R9
	.DEF _Time_Out_2=R8
	.DEF _Key_Press=R11
	.DEF _CB_Tx_Counter1=R10
	.DEF _CB_Tx_Counter2=R13
	.DEF _CB1_Time=R12

;GPIOR0-GPIOR2 INITIALIZATION VALUES
	.EQU __GPIOR0_INIT=0x00
	.EQU __GPIOR1_INIT=0x00
	.EQU __GPIOR2_INIT=0x00

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _timer1_compa_isr
	JMP  0x00
	JMP  0x00
	JMP  _timer0_compa_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00

_Firmware_Ver:
	.DB  0x30,0x31,0x30,0x30,0x0
_Password:
	.DB  0x47,0x46,0x49,0x38,0x34,0x37,0x0
_Bit_Table:
	.DB  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFE,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFD,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFB,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xF7,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xEF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xDF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xBF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0x7F,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF

_0x0:
	.DB  0x20,0x3D,0x3E,0x20,0x4E,0x65,0x77,0x20
	.DB  0x0,0x20,0x3D,0x3E,0x20,0x4F,0x6C,0x64
	.DB  0x20,0x0,0x4D,0x6F,0x64,0x65,0x20,0x0
_0x20003:
	.DB  0x0
_0x20004:
	.DB  0x0
_0x20005:
	.DB  0x0
_0x20006:
	.DB  0x30
_0x20007:
	.DB  0x78,0x0
_0x20008:
	.DB  0xF4,0x1
_0x20185:
	.DB  0x0,0x0,0x0,0x0
_0x20000:
	.DB  0x20,0x56,0x61,0x75,0x6C,0x74,0x20,0x44
	.DB  0x65,0x62,0x75,0x67,0x20,0x4D,0x65,0x6E
	.DB  0x75,0x20,0x56,0x31,0x2E,0x30,0x30,0x20
	.DB  0x0,0x4E,0x6F,0x64,0x65,0x20,0x41,0x64
	.DB  0x64,0x72,0x65,0x73,0x73,0x3A,0x0,0x3B
	.DB  0x20,0x46,0x69,0x72,0x6D,0x77,0x61,0x72
	.DB  0x65,0x20,0x56,0x65,0x72,0x73,0x69,0x6F
	.DB  0x6E,0x3A,0x0,0x3B,0x20,0x0,0x20,0x49
	.DB  0x4E,0x50,0x55,0x54,0x53,0x3A,0x0,0x43
	.DB  0x42,0x58,0x20,0x53,0x65,0x6E,0x73,0x6F
	.DB  0x72,0x3D,0x0,0x3B,0x20,0x50,0x6F,0x73
	.DB  0x69,0x74,0x69,0x6F,0x6E,0x20,0x53,0x65
	.DB  0x6E,0x73,0x6F,0x72,0x3D,0x0,0x3B,0x20
	.DB  0x4D,0x6F,0x74,0x6F,0x72,0x20,0x4C,0x6F
	.DB  0x63,0x6B,0x20,0x53,0x65,0x6E,0x73,0x6F
	.DB  0x72,0x3D,0x0,0x3B,0x20,0x4D,0x6F,0x74
	.DB  0x6F,0x72,0x20,0x46,0x61,0x75,0x6C,0x74
	.DB  0x3D,0x0,0x4F,0x55,0x54,0x50,0x55,0x54
	.DB  0x53,0x3A,0x0,0x5B,0x53,0x5D,0x6F,0x6C
	.DB  0x65,0x6E,0x6F,0x69,0x64,0x3D,0x0,0x3B
	.DB  0x20,0x4C,0x61,0x6D,0x70,0x5B,0x31,0x5D
	.DB  0x3D,0x0,0x3B,0x20,0x4C,0x61,0x6D,0x70
	.DB  0x5B,0x32,0x5D,0x3D,0x0,0x3B,0x20,0x4D
	.DB  0x6F,0x74,0x6F,0x72,0x20,0x5B,0x50,0x5D
	.DB  0x68,0x61,0x73,0x65,0x3D,0x0,0x3B,0x20
	.DB  0x4D,0x6F,0x74,0x6F,0x72,0x20,0x5B,0x52
	.DB  0x5D,0x75,0x6E,0x3D,0x0,0x56,0x61,0x75
	.DB  0x6C,0x74,0x20,0x48,0x6F,0x6C,0x64,0x69
	.DB  0x6E,0x67,0x20,0x54,0x69,0x6D,0x65,0x3D
	.DB  0x0,0x20,0x20,0x20,0x0,0x55,0x28,0x31
	.DB  0x32,0x56,0x29,0x3D,0x0,0x55,0x28,0x35
	.DB  0x56,0x29,0x3D,0x0,0x42,0x69,0x6E,0x20
	.DB  0x4E,0x75,0x6D,0x62,0x65,0x72,0x3A,0x0
	.DB  0x43,0x61,0x73,0x68,0x20,0x42,0x6F,0x78
	.DB  0x20,0x4E,0x75,0x6D,0x62,0x65,0x72,0x3A
	.DB  0x0,0x20,0x2E,0x2E,0x2E,0x20,0x20,0x20
	.DB  0x0,0x54,0x72,0x61,0x6E,0x73,0x6D,0x69
	.DB  0x73,0x73,0x69,0x6F,0x6E,0x20,0x44,0x61
	.DB  0x74,0x61,0x20,0x46,0x6F,0x72,0x6D,0x61
	.DB  0x74,0x0,0x28,0x4F,0x6E,0x6C,0x69,0x6E
	.DB  0x65,0x29,0x20,0x0,0x28,0x4F,0x66,0x66
	.DB  0x6C,0x69,0x6E,0x65,0x29,0x0,0x20,0x5B
	.DB  0x53,0x5D,0x65,0x72,0x69,0x61,0x6C,0x20
	.DB  0x4E,0x75,0x6D,0x62,0x65,0x72,0x7C,0x5B
	.DB  0x56,0x5D,0x61,0x75,0x6C,0x74,0x20,0x4E
	.DB  0x75,0x6D,0x62,0x65,0x72,0x7C,0x48,0x6F
	.DB  0x6C,0x64,0x69,0x6E,0x67,0x20,0x5B,0x54
	.DB  0x5D,0x69,0x6D,0x65,0x7C,0x5B,0x4E,0x5D
	.DB  0x2D,0x4E,0x6F,0x64,0x65,0x20,0x41,0x64
	.DB  0x64,0x72,0x65,0x73,0x73,0x7C,0x5B,0x45
	.DB  0x73,0x63,0x5D,0x2D,0x4D,0x61,0x69,0x6E
	.DB  0x20,0x4D,0x65,0x6E,0x75,0x20,0x0,0x56
	.DB  0x0,0x53,0x0,0x54,0x69,0x6D,0x65,0x20
	.DB  0x56,0x61,0x75,0x6C,0x74,0x20,0x48,0x6F
	.DB  0x6C,0x64,0x69,0x6E,0x67,0x3D,0x0,0x54
	.DB  0x0,0x5B,0x73,0x5D,0x0,0x4E,0x6F,0x64
	.DB  0x65,0x20,0x41,0x64,0x64,0x72,0x65,0x73
	.DB  0x73,0x3D,0x0,0x4E,0x0,0x4E,0x65,0x77
	.DB  0x20,0x53,0x65,0x72,0x69,0x61,0x6C,0x20
	.DB  0x4E,0x75,0x6D,0x62,0x65,0x72,0x3A,0x0
	.DB  0x4E,0x65,0x77,0x20,0x4E,0x6F,0x64,0x65
	.DB  0x20,0x41,0x64,0x64,0x72,0x65,0x73,0x73
	.DB  0x3A,0x0,0x4E,0x65,0x77,0x20,0x56,0x61
	.DB  0x75,0x6C,0x74,0x20,0x4E,0x75,0x6D,0x62
	.DB  0x65,0x72,0x3A,0x0,0x4E,0x65,0x77,0x20
	.DB  0x56,0x61,0x75,0x6C,0x74,0x20,0x48,0x6F
	.DB  0x6C,0x64,0x69,0x6E,0x67,0x20,0x54,0x69
	.DB  0x6D,0x65,0x3A,0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  _CB1_Pointer
	.DW  _0x20003*2

	.DW  0x01
	.DW  _CB2_Pointer
	.DW  _0x20004*2

	.DW  0x01
	.DW  _Status_Lights
	.DW  _0x20005*2

	.DW  0x01
	.DW  _Alarm_Status
	.DW  _0x20006*2

	.DW  0x02
	.DW  _U_12V
	.DW  _0x20007*2

	.DW  0x02
	.DW  _U_5V
	.DW  _0x20008*2

	.DW  0x04
	.DW  0x0A
	.DW  _0x20185*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  MCUCR,R31
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	WDR
	IN   R26,MCUSR
	CBR  R26,8
	OUT  MCUSR,R26
	STS  WDTCSR,R31
	STS  WDTCSR,R30

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;GPIOR0-GPIOR2 INITIALIZATION
	LDI  R30,__GPIOR0_INIT
	OUT  GPIOR0,R30
	;__GPIOR1_INIT = __GPIOR0_INIT
	OUT  GPIOR1,R30

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x280

	.CSEG
;
;#include "debug.h"
;
;
;
;extern unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8];
;extern unsigned char Tx1_Counter,TxD_1;
;
;
;#pragma optsize+
;
;//*********************************** Debug putchar *******************************
;void Debug_Putchar(unsigned char Data)
; 0000 000E {

	.CSEG
_Debug_Putchar:
; 0000 000F  TxD_1=Data;
	ST   -Y,R26
;	Data -> Y+0
	LD   R30,Y
	STS  _TxD_1,R30
; 0000 0010  Tx1_Counter=0;
	LDI  R30,LOW(0)
	STS  _Tx1_Counter,R30
; 0000 0011  while (Tx1_Counter<80);
_0x3:
	LDS  R26,_Tx1_Counter
	CPI  R26,LOW(0x50)
	BRLO _0x3
; 0000 0012 }
	JMP  _0x2000001
;
;
;
;
;//*********************************** Debug Getchar *******************************
;unsigned char Debug_Getchar(void)
; 0000 0019 {
_Debug_Getchar:
; 0000 001A  Rx3_Pointer_Buff=0;
	LDI  R30,LOW(0)
	STS  _Rx3_Pointer_Buff,R30
; 0000 001B  while (Rx3_Pointer_Buff==0) #asm("wdr");
_0x6:
	LDS  R30,_Rx3_Pointer_Buff
	CPI  R30,0
	BRNE _0x8
	wdr
; 0000 001C  return(Rx3_Buff[0]);
	RJMP _0x6
_0x8:
	LDS  R30,_Rx3_Buff
	RET
; 0000 001D }
;
;
;
;
;//********************************* DEBUG PUTSTRING **************************************
;void Print(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
; 0000 0024  {
_Print:
; 0000 0025 
; 0000 0026             #asm ("wdr");
	ST   -Y,R26
;	*text -> Y+3
;	X -> Y+2
;	Y -> Y+1
;	color -> Y+0
	wdr
; 0000 0027 
; 0000 0028 
; 0000 0029     if (color)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x9
; 0000 002A              {
; 0000 002B               Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 002C               Debug_Putchar(0x5B);
; 0000 002D               Debug_Putchar(0x30+color);
	LD   R26,Y
	SUBI R26,-LOW(48)
	RCALL _Debug_Putchar
; 0000 002E               Debug_Putchar(0x6D);
	LDI  R26,LOW(109)
	RCALL _Debug_Putchar
; 0000 002F              };
_0x9:
; 0000 0030 
; 0000 0031    if (X<200)
	LDD  R26,Y+2
	CPI  R26,LOW(0xC8)
	BRSH _0xA
; 0000 0032             {
; 0000 0033               Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 0034               Debug_Putchar(0x5B);
; 0000 0035               if (Y<10) Debug_Putchar(0x30+Y);
	LDD  R26,Y+1
	CPI  R26,LOW(0xA)
	BRSH _0xB
	SUBI R26,-LOW(48)
	RJMP _0x1D
; 0000 0036                else
_0xB:
; 0000 0037                     {
; 0000 0038                     Debug_Putchar(0x30+Y/10);
	LDD  R26,Y+1
	CALL SUBOPT_0x1
; 0000 0039                     Debug_Putchar(0x30+(Y%10));
	LDD  R26,Y+1
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
_0x1D:
	RCALL _Debug_Putchar
; 0000 003A                     };
; 0000 003B 
; 0000 003C               Debug_Putchar(0x03B);
	LDI  R26,LOW(59)
	RCALL _Debug_Putchar
; 0000 003D               Debug_Putchar(0x30+(X/10));
	LDD  R26,Y+2
	CALL SUBOPT_0x1
; 0000 003E               Debug_Putchar(0x30+(X%10));
	LDD  R26,Y+2
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	RCALL _Debug_Putchar
; 0000 003F               Debug_Putchar(0x48);
	LDI  R26,LOW(72)
	RCALL _Debug_Putchar
; 0000 0040 
; 0000 0041             }
; 0000 0042 
; 0000 0043               while (*text!=0) Debug_Putchar(*text++);
_0xA:
_0xD:
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	LPM  R30,Z
	CPI  R30,0
	BREQ _0xF
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	ADIW R30,1
	STD  Y+3,R30
	STD  Y+3+1,R31
	SBIW R30,1
	LPM  R26,Z
	RCALL _Debug_Putchar
	RJMP _0xD
_0xF:
; 0000 0045 if (color)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x10
; 0000 0046                           {
; 0000 0047                            Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 0048                            Debug_Putchar(0x5B);
; 0000 0049                            Debug_Putchar(0x30);
	LDI  R26,LOW(48)
	RCALL _Debug_Putchar
; 0000 004A                            Debug_Putchar(0x6D);
	LDI  R26,LOW(109)
	RCALL _Debug_Putchar
; 0000 004B                           }
; 0000 004C 
; 0000 004D   };
_0x10:
	ADIW R28,5
	RET
;
;
;
;
;
;
;//*************************************** Input Data ****************************************
;unsigned char Input(unsigned char *Out_Pointer,unsigned char Size,unsigned char Password)
; 0000 0056  {
_Input:
; 0000 0057   unsigned char Tmp,Tmp2;
; 0000 0058 
; 0000 0059     Debug_Putchar(' ');
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	*Out_Pointer -> Y+4
;	Size -> Y+3
;	Password -> Y+2
;	Tmp -> R17
;	Tmp2 -> R16
	LDI  R26,LOW(32)
	RCALL _Debug_Putchar
; 0000 005A     Cursor_Show();
	RCALL _Cursor_Show
; 0000 005B     Tmp=0;
	LDI  R17,LOW(0)
; 0000 005C 
; 0000 005D Loop_input:
_0x11:
; 0000 005E 
; 0000 005F     Tmp2=Debug_Getchar();
	RCALL _Debug_Getchar
	MOV  R16,R30
; 0000 0060 
; 0000 0061     if (Tmp2==27)
	CPI  R16,27
	BRNE _0x12
; 0000 0062                 {
; 0000 0063                 Tmp=0;
	LDI  R17,LOW(0)
; 0000 0064                 goto Exit_Input;
	RJMP _0x13
; 0000 0065                 };
_0x12:
; 0000 0066 
; 0000 0067 
; 0000 0068     if (Tmp2==13)
	CPI  R16,13
	BRNE _0x14
; 0000 0069                 {
; 0000 006A                  if (Tmp==Size) goto Exit_Input;
	LDD  R30,Y+3
	CP   R30,R17
	BREQ _0x13
; 0000 006B 
; 0000 006C                  Tmp=0;
	LDI  R17,LOW(0)
; 0000 006D                  goto Exit_Input;
	RJMP _0x13
; 0000 006E                 };
_0x14:
; 0000 006F 
; 0000 0070     if (Tmp2==8)
	CPI  R16,8
	BRNE _0x16
; 0000 0071                {
; 0000 0072                if (Tmp>0)
	CPI  R17,1
	BRLO _0x17
; 0000 0073                         {
; 0000 0074                         Tmp--;
	SUBI R17,1
; 0000 0075                         Debug_Putchar(8);
	LDI  R26,LOW(8)
	RCALL _Debug_Putchar
; 0000 0076                         Debug_Putchar(' ');
	LDI  R26,LOW(32)
	RCALL _Debug_Putchar
; 0000 0077                         Debug_Putchar(8);
	LDI  R26,LOW(8)
	RCALL _Debug_Putchar
; 0000 0078                         }
; 0000 0079                         goto Loop_input;
_0x17:
	RJMP _0x11
; 0000 007A                }
; 0000 007B 
; 0000 007C 
; 0000 007D     if (Tmp<Size)
_0x16:
	LDD  R30,Y+3
	CP   R17,R30
	BRSH _0x18
; 0000 007E                 {
; 0000 007F                 Out_Pointer[Tmp]=Tmp2;
	MOV  R30,R17
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R16
; 0000 0080 
; 0000 0081                 if (Password) Debug_Putchar('*');
	LDD  R30,Y+2
	CPI  R30,0
	BREQ _0x19
	LDI  R26,LOW(42)
	RJMP _0x1E
; 0000 0082                  else Debug_Putchar(Tmp2);
_0x19:
	MOV  R26,R16
_0x1E:
	RCALL _Debug_Putchar
; 0000 0083                 Tmp++;
	SUBI R17,-1
; 0000 0084                 };
_0x18:
; 0000 0085 
; 0000 0086                 goto Loop_input;
	RJMP _0x11
; 0000 0087   Exit_Input:
_0x13:
; 0000 0088   Cursor_Hide();
	RCALL _Cursor_Hide
; 0000 0089   return(Tmp);
	MOV  R30,R17
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,6
	RET
; 0000 008A  }
;
;
;
;
;
;
;//************************************************** Print Mode New/Old *****************************************
;void Mode_Print(unsigned char mode)
; 0000 0093 {
_Mode_Print:
; 0000 0094 
; 0000 0095  if (mode) Print(" => New ",255,0,0);
	ST   -Y,R26
;	mode -> Y+0
	LD   R30,Y
	CPI  R30,0
	BREQ _0x1B
	__POINTW1FN _0x0,0
	RJMP _0x1F
; 0000 0096         else Print(" => Old ",255,0,0);
_0x1B:
	__POINTW1FN _0x0,9
_0x1F:
	ST   -Y,R31
	ST   -Y,R30
	CALL SUBOPT_0x2
; 0000 0097 
; 0000 0098  Print("Mode ",255,0,0);
	__POINTW1FN _0x0,18
	ST   -Y,R31
	ST   -Y,R30
	CALL SUBOPT_0x2
; 0000 0099 };
	JMP  _0x2000001
;
;
;
;
;     //********************************************** Clear Screen ***********************************************************
;void ClrScr(void)
; 0000 00A0 {
_ClrScr:
; 0000 00A1  Debug_Putchar(12);
	LDI  R26,LOW(12)
	RCALL _Debug_Putchar
; 0000 00A2  Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00A3  Debug_Putchar('[');
; 0000 00A4  Debug_Putchar('2');
	LDI  R26,LOW(50)
	RCALL _Debug_Putchar
; 0000 00A5  Debug_Putchar('J');
	LDI  R26,LOW(74)
	RJMP _0x2000002
; 0000 00A6 }
;
;
;
;//************************************************************ Cursor Hide ********************************************************
;void Cursor_Hide()
; 0000 00AC {
_Cursor_Hide:
; 0000 00AD Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00AE Debug_Putchar('[');
; 0000 00AF Debug_Putchar('?');
	CALL SUBOPT_0x3
; 0000 00B0 Debug_Putchar('2');
; 0000 00B1 Debug_Putchar('5');
; 0000 00B2 Debug_Putchar('l');
	LDI  R26,LOW(108)
	RJMP _0x2000002
; 0000 00B3 }
;
;
;
;
;//************************************************************ Cursor Show ********************************************************
;void Cursor_Show()
; 0000 00BA {
_Cursor_Show:
; 0000 00BB Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00BC Debug_Putchar('[');
; 0000 00BD Debug_Putchar('?');
	CALL SUBOPT_0x3
; 0000 00BE Debug_Putchar('2');
; 0000 00BF Debug_Putchar('5');
; 0000 00C0 Debug_Putchar('h');
	LDI  R26,LOW(104)
_0x2000002:
	RCALL _Debug_Putchar
; 0000 00C1 }
	RET
;/******f*********
;This program was produced by the
;CodeWizardAVR V2.04.9 Standard
;Automatic Program Generator
;� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 11/29/2010
;Author  : SVUKOVIC
;Company : Microsoft
;Comments:
;
;
;Chip type               : ATtiny4313
;AVR Core Clock frequency: 14.745600 MHz
;Memory model            : Tiny
;External RAM size       : 0
;Data Stack size         : 64
;*****************************************************/
;
;#include <mega16m1.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_standby=0x0C
	.SET power_ctrl_reg=smcr
	#endif
;#include <delay.h>
;#include "debug.h"
;
;
;
;
;
;
;#define STX    2
;#define ETX    3
;#define EOT    4
;
;
;#define Min_5V   450
;#define Max_5V   550
;
;#define Min_12V   110
;#define Max_12V   130
;
;
;//#define _Test_
;
;#define Time_Base_CBXID   20
;
;
;
;//******** INPUTS *************
;#define CbxPS_Sensor      PINB.3
;#define RecPS_Sensor      PINC.0
;#define LockPS_Sensor     PINB.2
;#define Motor_Fault       PINB.7
;
;
;#define In_1            PINB.6
;#define In_2            PINC.7
;#define In_3            PINC.3
;
;
;#define Send_Mode       PIND.6
;
;
;
;
;//***** debug txd port *******
;#define Soft_TxD    PORTC.2
;
;#asm
.EQU _Soft_TxD=2
.EQU _PORTC=0x08
; 0001 0049 #endasm
;
;
;
;//**** old mode outputs ******
;#define Out_ID      PORTD.3
;#define Out_ID2     PORTB.4
;
;
;#define RS485_Dir   PORTB.5
;
;
;
;
;#define Led_Power     PORTD.5
;#define Led_Link      PORTB.1
;#define Led_Sys       PORTC.6
;#define Led_Service   PORTB.0
;
;
;
;
;//***** OUTPUTS *********
;#define Out_Lig1   PORTD.7
;#define Out_Lig2   PORTD.2
;#define Out_Valve  PORTC.1
;
;#define Out_Motor_Ph   PORTD.1
;#define Out_Motor_Run  PORTD.0
;
;#define Power_Light DDRC.7
;
;
;
;eeprom unsigned char Serial_No[]="SN123456";
;eeprom unsigned char Vault[]="VAULT0";
;eeprom unsigned char Solenoid_Time=5;
;eeprom unsigned char Node_Address_=0x30;
;
;flash unsigned char Firmware_Ver[]="0100";
;flash unsigned char Password[]="GFI847";
;
;
;
;bit Mode_1,Mode_2,Sinc_Bit,Refresh_Bit;
;bit CBX1_Done_bit,CBX2_Done_bit,CBX1_bit=0,Old_CBX1,CBX2_bit=0,Old_CBX2;
;
;unsigned char Rx1_Counter,Rx1_Pointer_Buff,Tmp_Rx1,Rx1_Buff[8],Time_Out_1;
;unsigned char Rx2_Counter,Rx2_Pointer_Buff,Tmp_Rx2,Rx2_Buff[8],Time_Out_2;
;unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8],Key_Press;
;
;unsigned char Compare_Buffer_1[6],Compare_Buffer_2[6];
;
;
;unsigned char Rx1_ID_Buff[6],Rx2_ID_Buff[6];
;
;unsigned char CB_Tx_Counter1=0,CB_Tx_Counter2=0;
;
;unsigned char Tx1_Counter,TxD_1;
;
;unsigned char CB1_Time,CB1_Pointer=0,CB2_Time,CB2_Pointer=0;

	.DSEG
;
;unsigned int CB1_Tmp,CB2_Tmp,Timer_Sinc,Solenoid_Td;
;
;unsigned char Buff_Temp[10];
;
;unsigned char Node_Address,Led_Blink,Status_Lights=0,Alarm_Status=0x30;
;
;unsigned int U_12V=120,U_5V=500;
;
;
;
;struct _Timer{
;		  	 unsigned char Time_P,Link_Timer,Counting,Time_Old;
;			 unsigned int Time;
;		  	 }Timer;
;
;
;struct _RS485{
;		  	 unsigned char Rx_Counter,Tx_Size,LRC;
;             signed char Tx_Counter;
;			 unsigned char Tx_Buffer[32],Rx_Buffer[32],Get_All_Command[32];
;		  	 }RS485;
;
;
;
;void Print_Serial_No(unsigned char position);
;void Print_Vault_No(unsigned char position);
;
;
;
;//************************************* Timer 0 output compare A interrupt service routine *****************************
;interrupt [TIM0_COMPA] void timer0_compa_isr(void)
; 0001 00A6 {

	.CSEG
_timer0_compa_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R22
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0001 00A7 
; 0001 00A8 #pragma optsize-
; 0001 00A9 
; 0001 00AA unsigned char Dat,Tmp1,Tmp2,Tmp3,Tmp4;
; 0001 00AB 
; 0001 00AC #asm("sei");
	CALL __SAVELOCR6
;	Dat -> R17
;	Tmp1 -> R16
;	Tmp2 -> R19
;	Tmp3 -> R18
;	Tmp4 -> R21
	sei
; 0001 00AD 
; 0001 00AE if(Mode_1)
	SBIS 0x1E,0
	RJMP _0x20009
; 0001 00AF           {
; 0001 00B0            if (++CB1_Time<250)
	INC  R12
	LDI  R30,LOW(250)
	CP   R12,R30
	BRLO PC+3
	JMP _0x2000A
; 0001 00B1             {
; 0001 00B2             if (CBX1_bit)
	SBIS 0x1E,6
	RJMP _0x2000B
; 0001 00B3               {
; 0001 00B4                CBX1_bit=0;
	CBI  0x1E,6
; 0001 00B5 
; 0001 00B6                if (CB1_Time>(Time_Base_CBXID*6))
	LDI  R30,LOW(120)
	CP   R30,R12
	BRSH _0x2000E
; 0001 00B7                                   {
; 0001 00B8                                    CB1_Time=0;
	CLR  R12
; 0001 00B9                                    CB1_Pointer=0;
	LDI  R30,LOW(0)
	STS  _CB1_Pointer,R30
; 0001 00BA                                    CB1_Tmp=0;
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R30
; 0001 00BB                                    goto Exit_CB1;
	JMP  _0x2000F
; 0001 00BC                                   }
; 0001 00BD 
; 0001 00BE 
; 0001 00BF                if (CB1_Time>(Time_Base_CBXID*3))
_0x2000E:
	LDI  R30,LOW(60)
	CP   R30,R12
	BRSH _0x20010
; 0001 00C0                  {
; 0001 00C1                  //CB1_Tmp<<=4;
; 0001 00C2                  #asm
; 0001 00C3                  LDS       R30,_CB1_Tmp   ;Load direct from data space
                 LDS       R30,_CB1_Tmp   ;Load direct from data space
; 0001 00C4                  LDS       R31,_CB1_Tmp+1  ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1  ;Load direct from data space
; 0001 00C5                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00C6                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00C7                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00C8                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00C9                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00CA                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00CB                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00CC                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00CD 

; 0001 00CE 

; 0001 00CF                  ;CB1_Tmp|=7
                 ;CB1_Tmp|=7
; 0001 00D0                  ORI       R30,0x07
                 ORI       R30,0x07
; 0001 00D1                  STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp,R30
; 0001 00D2                  STS       _CB1_Tmp+1,R31
                 STS       _CB1_Tmp+1,R31
; 0001 00D3 

; 0001 00D4                  #endasm
; 0001 00D5 
; 0001 00D6                   CB1_Pointer+=4;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(4)
	JMP  _0x2017D
; 0001 00D7 
; 0001 00D8                   goto Exit_CB1;
; 0001 00D9                   };
_0x20010:
; 0001 00DA 
; 0001 00DB 
; 0001 00DC                if (CB1_Time>(Time_Base_CBXID*2))
	LDI  R30,LOW(40)
	CP   R30,R12
	BRSH _0x20011
; 0001 00DD                  {
; 0001 00DE                  //CB1_Tmp<<=3;
; 0001 00DF                  #asm
; 0001 00E0                  LDS       R30,_CB1_Tmp     ;Load direct from data space
                 LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 00E1                  LDS       R31,_CB1_Tmp+1    ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1    ;Load direct from data space
; 0001 00E2                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E3                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E4                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E5                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E6                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E7                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E8 

; 0001 00E9                  ;CB1_Tmp|=3;
                 ;CB1_Tmp|=3;
; 0001 00EA                  ORI       R30,0x03
                 ORI       R30,0x03
; 0001 00EB                  STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp,R30
; 0001 00EC                  STS       _CB1_Tmp+1,R31
                 STS       _CB1_Tmp+1,R31
; 0001 00ED                  #endasm
; 0001 00EE 
; 0001 00EF                  CB1_Pointer+=3;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(3)
	RJMP _0x2017D
; 0001 00F0                  goto Exit_CB1;
; 0001 00F1                  };
_0x20011:
; 0001 00F2 
; 0001 00F3 
; 0001 00F4                if (CB1_Time<(Time_Base_CBXID))
	LDI  R30,LOW(20)
	CP   R12,R30
	BRSH _0x20012
; 0001 00F5                  {
; 0001 00F6                  CB1_Tmp<<=1;
	LDS  R30,_CB1_Tmp
	LDS  R31,_CB1_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R31
; 0001 00F7                  CB1_Pointer++;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(1)
	RJMP _0x2017D
; 0001 00F8                  goto Exit_CB1;
; 0001 00F9                  };
_0x20012:
; 0001 00FA 
; 0001 00FB 
; 0001 00FC                //CB1_Tmp<<=2;
; 0001 00FD                #asm
; 0001 00FE                LDS       R30,_CB1_Tmp     ;Load direct from data space
               LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 00FF                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 0100                LSL       R30              ;Logical Shift Left
               LSL       R30              ;Logical Shift Left
; 0001 0101                ROL       R31              ;Rotate Left Through Carry
               ROL       R31              ;Rotate Left Through Carry
; 0001 0102                LSL       R30              ;Logical Shift Left
               LSL       R30              ;Logical Shift Left
; 0001 0103                ROL       R31              ;Rotate Left Through Carry
               ROL       R31              ;Rotate Left Through Carry
; 0001 0104 

; 0001 0105                ;CB1_Tmp|=1;
               ;CB1_Tmp|=1;
; 0001 0106 

; 0001 0107                ORI       R30,1
               ORI       R30,1
; 0001 0108                STS       _CB1_Tmp,R30
               STS       _CB1_Tmp,R30
; 0001 0109                STS       _CB1_Tmp+1,R31
               STS       _CB1_Tmp+1,R31
; 0001 010A                #endasm
; 0001 010B 
; 0001 010C                CB1_Pointer+=2;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(2)
_0x2017D:
	STS  _CB1_Pointer,R30
; 0001 010D 
; 0001 010E Exit_CB1:      CB1_Time=0;
_0x2000F:
	CLR  R12
; 0001 010F                 };
_0x2000B:
; 0001 0110             }
; 0001 0111              else
	JMP  _0x20013
_0x2000A:
; 0001 0112                 {
; 0001 0113                  if (CB1_Time==250)
	LDI  R30,LOW(250)
	CP   R30,R12
	BREQ PC+3
	JMP _0x20014
; 0001 0114                         {
; 0001 0115                          if (CB1_Pointer==13) {
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xD)
	BRNE _0x20015
; 0001 0116                                                //CB1_Tmp<<=3;
; 0001 0117                                                #asm
; 0001 0118                                                LDS       R30,_CB1_Tmp     ;Load direct from data space
                                               LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 0119                                                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 011A                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 011B                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 011C                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 011D                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 011E                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 011F                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0120 

; 0001 0121                                                ;CB1_Tmp|=3;
                                               ;CB1_Tmp|=3;
; 0001 0122                                                ORI       R30,3
                                               ORI       R30,3
; 0001 0123                                                STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp,R30
; 0001 0124                                                STS       _CB1_Tmp+1,R31
                                               STS       _CB1_Tmp+1,R31
; 0001 0125                                                #endasm
; 0001 0126                                                }
; 0001 0127 
; 0001 0128 
; 0001 0129                          if (CB1_Pointer==14) {
_0x20015:
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xE)
	BRNE _0x20016
; 0001 012A                                               //CB1_Tmp<<=2;
; 0001 012B                                               #asm
; 0001 012C                                                LDS       R30,_CB1_Tmp    ;Load direct from data space
                                               LDS       R30,_CB1_Tmp    ;Load direct from data space
; 0001 012D                                                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 012E                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 012F                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0130                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0131                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0132 

; 0001 0133                                                ;CB1_Tmp|=1;
                                               ;CB1_Tmp|=1;
; 0001 0134                                                ORI       R30,1
                                               ORI       R30,1
; 0001 0135                                                STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp,R30
; 0001 0136                                                STS       _CB1_Tmp+1,R31
                                               STS       _CB1_Tmp+1,R31
; 0001 0137                                                #endasm
; 0001 0138                                               }
; 0001 0139 
; 0001 013A                          if (CB1_Pointer==15) CB1_Tmp<<=1;
_0x20016:
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xF)
	BRNE _0x20017
	LDS  R30,_CB1_Tmp
	LDS  R31,_CB1_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R31
; 0001 013B 
; 0001 013C                          CB1_Pointer=0;
_0x20017:
	LDI  R30,LOW(0)
	STS  _CB1_Pointer,R30
; 0001 013D 
; 0001 013E                          Rx1_Buff[0]='0';
	LDI  R30,LOW(48)
	STS  _Rx1_Buff,R30
; 0001 013F                          Rx1_Buff[1]='0';
	__PUTB1MN _Rx1_Buff,1
; 0001 0140 
; 0001 0141                          //Rx1_Buff[1]=(CB1_Tmp>>12)&0x0F;
; 0001 0142                          #asm
; 0001 0143                          LDS       R30,_CB1_Tmp+1 ;       ;Load immediate
                         LDS       R30,_CB1_Tmp+1 ;       ;Load immediate
; 0001 0144                          SWAP      R30
                         SWAP      R30
; 0001 0145                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0146                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0147                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0148                          BRNE      OK1
                         BRNE      OK1
; 0001 0149                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 014A                     OK1: STS       _Rx1_Buff+2,R30
                    OK1: STS       _Rx1_Buff+2,R30
; 0001 014B                          #endasm
; 0001 014C 
; 0001 014D                          //Rx1_Buff[2]=(CB1_Tmp>>8)&0x0F;
; 0001 014E                          #asm
; 0001 014F                          LDS       R30,_CB1_Tmp+1       ;Load immediate
                         LDS       R30,_CB1_Tmp+1       ;Load immediate
; 0001 0150                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0151                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0152                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0153                          BRNE      OK2
                         BRNE      OK2
; 0001 0154                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0155                     OK2: STS       _Rx1_Buff+3,R30
                    OK2: STS       _Rx1_Buff+3,R30
; 0001 0156                          #endasm
; 0001 0157 
; 0001 0158                          //Rx1_Buff[3]=(CB1_Tmp>>4)&0x0F;
; 0001 0159                          #asm
; 0001 015A                          LDS       R30,_CB1_Tmp       ;Load immediate
                         LDS       R30,_CB1_Tmp       ;Load immediate
; 0001 015B                          SWAP      R30
                         SWAP      R30
; 0001 015C                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 015D                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 015E                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 015F                          BRNE      OK3
                         BRNE      OK3
; 0001 0160                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0161                     OK3: STS       _Rx1_Buff+4,R30
                    OK3: STS       _Rx1_Buff+4,R30
; 0001 0162                          #endasm
; 0001 0163 
; 0001 0164                          //Rx1_Buff[4]=CB1_Tmp&0x0F;
; 0001 0165                          #asm
; 0001 0166                          LDS       R30,_CB1_Tmp       ;Load immediate
                         LDS       R30,_CB1_Tmp       ;Load immediate
; 0001 0167                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0168                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0169                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 016A                          BRNE      OK4
                         BRNE      OK4
; 0001 016B                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 016C                     OK4: STS       _Rx1_Buff+5,R30
                    OK4: STS       _Rx1_Buff+5,R30
; 0001 016D                          #endasm
; 0001 016E 
; 0001 016F                          CBX1_Done_bit=1;
	SBI  0x1E,4
; 0001 0170                          }
; 0001 0171                           else CB1_Time=250;
	RJMP _0x2001A
_0x20014:
	LDI  R30,LOW(250)
	MOV  R12,R30
; 0001 0172                 };
_0x2001A:
_0x20013:
; 0001 0173 
; 0001 0174           };
_0x20009:
; 0001 0175 
; 0001 0176 
; 0001 0177 
; 0001 0178 if(Mode_2)
	SBIS 0x1E,1
	RJMP _0x2001B
; 0001 0179           {
; 0001 017A            if (++CB2_Time<250)
	LDS  R26,_CB2_Time
	SUBI R26,-LOW(1)
	STS  _CB2_Time,R26
	CPI  R26,LOW(0xFA)
	BRLO PC+3
	JMP _0x2001C
; 0001 017B             {
; 0001 017C             if (CBX2_bit)
	SBIS 0x19,0
	RJMP _0x2001D
; 0001 017D               {
; 0001 017E                CBX2_bit=0;
	CBI  0x19,0
; 0001 017F 
; 0001 0180                if (CB2_Time>(Time_Base_CBXID*8))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xA1)
	BRLO _0x20020
; 0001 0181                                   {
; 0001 0182                                    CB2_Time=0;
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 0183                                    CB2_Pointer=0;
	STS  _CB2_Pointer,R30
; 0001 0184                                    CB2_Tmp=0;
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R30
; 0001 0185                                    goto Exit_CB2;
	JMP  _0x20021
; 0001 0186                                   }
; 0001 0187 
; 0001 0188 
; 0001 0189 
; 0001 018A                if (CB2_Time>(Time_Base_CBXID*3))
_0x20020:
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x3D)
	BRLO _0x20022
; 0001 018B                  {
; 0001 018C 
; 0001 018D                  //CB2_Tmp<<=4;
; 0001 018E                  #asm
; 0001 018F                  LDS       R30,_CB2_Tmp   ;Load direct from data space
                 LDS       R30,_CB2_Tmp   ;Load direct from data space
; 0001 0190                  LDS       R31,_CB2_Tmp+1  ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1  ;Load direct from data space
; 0001 0191                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0192                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0193                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0194                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0195                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0196                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0197                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0198                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0199 

; 0001 019A                  ;CB2_Tmp|=7
                 ;CB2_Tmp|=7
; 0001 019B                  ORI       R30,0x07
                 ORI       R30,0x07
; 0001 019C                  STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp,R30
; 0001 019D                  STS       _CB2_Tmp+1,R31
                 STS       _CB2_Tmp+1,R31
; 0001 019E 

; 0001 019F                  #endasm
; 0001 01A0 
; 0001 01A1                   CB2_Pointer+=4;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(4)
	JMP  _0x2017E
; 0001 01A2 
; 0001 01A3                   goto Exit_CB2;
; 0001 01A4                   };
_0x20022:
; 0001 01A5 
; 0001 01A6 
; 0001 01A7                if (CB2_Time>(Time_Base_CBXID*2))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x29)
	BRLO _0x20023
; 0001 01A8                  {
; 0001 01A9                  //CB2_Tmp<<=3;
; 0001 01AA                  #asm
; 0001 01AB                  LDS       R30,_CB2_Tmp     ;Load direct from data space
                 LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01AC                  LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01AD                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01AE                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01AF                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01B0                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01B1                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01B2                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01B3 

; 0001 01B4                  ;CB2_Tmp|=3;
                 ;CB2_Tmp|=3;
; 0001 01B5                  ORI       R30,0x03
                 ORI       R30,0x03
; 0001 01B6                  STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp,R30
; 0001 01B7                  STS       _CB2_Tmp+1,R31
                 STS       _CB2_Tmp+1,R31
; 0001 01B8                  #endasm
; 0001 01B9 
; 0001 01BA                  CB2_Pointer+=3;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(3)
	RJMP _0x2017E
; 0001 01BB                  goto Exit_CB2;
; 0001 01BC                  };
_0x20023:
; 0001 01BD 
; 0001 01BE 
; 0001 01BF                if (CB2_Time<(Time_Base_CBXID))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x14)
	BRSH _0x20024
; 0001 01C0                  {
; 0001 01C1                  CB2_Tmp<<=1;
	LDS  R30,_CB2_Tmp
	LDS  R31,_CB2_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R31
; 0001 01C2                  CB2_Pointer++;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(1)
	RJMP _0x2017E
; 0001 01C3                  goto Exit_CB2;
; 0001 01C4                  };
_0x20024:
; 0001 01C5 
; 0001 01C6 
; 0001 01C7                //CB2_Tmp<<=2;
; 0001 01C8                #asm
; 0001 01C9                LDS       R30,_CB2_Tmp     ;Load direct from data space
               LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01CA                LDS       R31,_CB2_Tmp+1    ;Load direct from data space
               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01CB                LSL       R30            ;Logical Shift Left
               LSL       R30            ;Logical Shift Left
; 0001 01CC                ROL       R31            ;Rotate Left Through Carry
               ROL       R31            ;Rotate Left Through Carry
; 0001 01CD                LSL       R30            ;Logical Shift Left
               LSL       R30            ;Logical Shift Left
; 0001 01CE                ROL       R31            ;Rotate Left Through Carry
               ROL       R31            ;Rotate Left Through Carry
; 0001 01CF 

; 0001 01D0                ;CB2_Tmp|=1;
               ;CB2_Tmp|=1;
; 0001 01D1 

; 0001 01D2                ORI       R30,1
               ORI       R30,1
; 0001 01D3                STS       _CB2_Tmp,R30
               STS       _CB2_Tmp,R30
; 0001 01D4                STS       _CB2_Tmp+1,R31
               STS       _CB2_Tmp+1,R31
; 0001 01D5                #endasm
; 0001 01D6 
; 0001 01D7                CB2_Pointer+=2;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(2)
_0x2017E:
	STS  _CB2_Pointer,R30
; 0001 01D8 
; 0001 01D9 Exit_CB2:      CB2_Time=0;
_0x20021:
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 01DA                 };
_0x2001D:
; 0001 01DB             }
; 0001 01DC              else
	JMP  _0x20025
_0x2001C:
; 0001 01DD                 {
; 0001 01DE                  if (CB2_Time==250)
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xFA)
	BREQ PC+3
	JMP _0x20026
; 0001 01DF                         {
; 0001 01E0                          if (CB2_Pointer==13) {
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xD)
	BRNE _0x20027
; 0001 01E1                                                //CB2_Tmp<<=3;
; 0001 01E2                                                #asm
; 0001 01E3                                                LDS       R30,_CB2_Tmp     ;Load direct from data space
                                               LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01E4                                                LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01E5                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01E6                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01E7                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01E8                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01E9                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01EA                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01EB 

; 0001 01EC                                                ;CB2_Tmp|=3;
                                               ;CB2_Tmp|=3;
; 0001 01ED                                                ORI       R30,3
                                               ORI       R30,3
; 0001 01EE                                                STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp,R30
; 0001 01EF                                                STS       _CB2_Tmp+1,R31
                                               STS       _CB2_Tmp+1,R31
; 0001 01F0                                                #endasm
; 0001 01F1                                                }
; 0001 01F2 
; 0001 01F3 
; 0001 01F4                          if (CB2_Pointer==14) {
_0x20027:
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xE)
	BRNE _0x20028
; 0001 01F5                                               //CB2_Tmp<<=2;
; 0001 01F6                                               #asm
; 0001 01F7                                                LDS       R30,_CB2_Tmp    ;Load direct from data space
                                               LDS       R30,_CB2_Tmp    ;Load direct from data space
; 0001 01F8                                                LDS       R31,_CB2_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1   ;Load direct from data space
; 0001 01F9                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01FA                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01FB                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01FC                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01FD 

; 0001 01FE                                                ;CB2_Tmp|=1;
                                               ;CB2_Tmp|=1;
; 0001 01FF                                                ORI       R30,1
                                               ORI       R30,1
; 0001 0200                                                STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp,R30
; 0001 0201                                                STS       _CB2_Tmp+1,R31
                                               STS       _CB2_Tmp+1,R31
; 0001 0202                                                #endasm
; 0001 0203                                               }
; 0001 0204 
; 0001 0205                          if (CB2_Pointer==15) CB2_Tmp<<=1;
_0x20028:
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xF)
	BRNE _0x20029
	LDS  R30,_CB2_Tmp
	LDS  R31,_CB2_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R31
; 0001 0206 
; 0001 0207                          CB2_Pointer=0;
_0x20029:
	LDI  R30,LOW(0)
	STS  _CB2_Pointer,R30
; 0001 0208 
; 0001 0209                          Rx2_Buff[0]='0';
	LDI  R30,LOW(48)
	STS  _Rx2_Buff,R30
; 0001 020A                          Rx2_Buff[1]='0';
	__PUTB1MN _Rx2_Buff,1
; 0001 020B 
; 0001 020C                          //Rx2_Buff[1]=(CB2_Tmp>>12)&0x0F;
; 0001 020D                          #asm
; 0001 020E                          LDS       R30,_CB2_Tmp+1 ;       ;Load immediate
                         LDS       R30,_CB2_Tmp+1 ;       ;Load immediate
; 0001 020F                          SWAP      R30
                         SWAP      R30
; 0001 0210                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0211                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0212                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0213                          BRNE      OK11
                         BRNE      OK11
; 0001 0214                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0215                  OK11:   STS       _Rx2_Buff+2,R30
                 OK11:   STS       _Rx2_Buff+2,R30
; 0001 0216                          #endasm
; 0001 0217 
; 0001 0218                          //Rx2_Buff[2]=(CB2_Tmp>>8)&0x0F;
; 0001 0219                          #asm
; 0001 021A                          LDS       R30,_CB2_Tmp+1       ;Load immediate
                         LDS       R30,_CB2_Tmp+1       ;Load immediate
; 0001 021B                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 021C                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 021D                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 021E                          BRNE      OK22
                         BRNE      OK22
; 0001 021F                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0220                  OK22:   STS       _Rx2_Buff+3,R30
                 OK22:   STS       _Rx2_Buff+3,R30
; 0001 0221                          #endasm
; 0001 0222 
; 0001 0223                          //Rx2_Buff[3]=(CB2_Tmp>>4)&0x0F;
; 0001 0224                          #asm
; 0001 0225                          LDS       R30,_CB2_Tmp       ;Load immediate
                         LDS       R30,_CB2_Tmp       ;Load immediate
; 0001 0226                          SWAP      R30
                         SWAP      R30
; 0001 0227                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0228                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0229                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 022A                          BRNE      OK33
                         BRNE      OK33
; 0001 022B                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 022C                  OK33:   STS       _Rx2_Buff+4,R30
                 OK33:   STS       _Rx2_Buff+4,R30
; 0001 022D 

; 0001 022E                          OR        R8,R26
                         OR        R8,R26
; 0001 022F                          #endasm
; 0001 0230 
; 0001 0231                          //Rx2_Buff[4]=CB2_Tmp&0x0F;
; 0001 0232                          #asm
; 0001 0233                          LDS       R30,_CB2_Tmp       ;Load immediate
                         LDS       R30,_CB2_Tmp       ;Load immediate
; 0001 0234                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0235                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0236                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0237                          BRNE      OK44
                         BRNE      OK44
; 0001 0238                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0239                  OK44:   STS       _Rx2_Buff+5,R30
                 OK44:   STS       _Rx2_Buff+5,R30
; 0001 023A                          #endasm
; 0001 023B 
; 0001 023C                          CBX2_Done_bit=1;
	SBI  0x1E,5
; 0001 023D                          }
; 0001 023E                           else CB2_Time=250;
	RJMP _0x2002C
_0x20026:
	LDI  R30,LOW(250)
	STS  _CB2_Time,R30
; 0001 023F                 };
_0x2002C:
_0x20025:
; 0001 0240 
; 0001 0241           };
_0x2001B:
; 0001 0242 
; 0001 0243 
; 0001 0244   if (Time_Out_1<250)
	LDI  R30,LOW(250)
	CP   R4,R30
	BRSH _0x2002D
; 0001 0245                      {
; 0001 0246                       Time_Out_1++;
	INC  R4
; 0001 0247                       if (Time_Out_1==250) {
	CP   R30,R4
	BRNE _0x2002E
; 0001 0248                                             Rx1_Pointer_Buff=8;
	LDI  R30,LOW(8)
	MOV  R2,R30
; 0001 0249                                             Mode_1=0;
	CBI  0x1E,0
; 0001 024A                                             }
; 0001 024B                      };
_0x2002E:
_0x2002D:
; 0001 024C 
; 0001 024D 
; 0001 024E   if (Time_Out_2<250)
	LDI  R30,LOW(250)
	CP   R8,R30
	BRSH _0x20031
; 0001 024F                      {
; 0001 0250                       Time_Out_2++;
	INC  R8
; 0001 0251                       if (Time_Out_2==250) {
	CP   R30,R8
	BRNE _0x20032
; 0001 0252                                             Rx2_Pointer_Buff=8;
	LDI  R30,LOW(8)
	MOV  R6,R30
; 0001 0253                                             Mode_2=0;
	CBI  0x1E,1
; 0001 0254                                             }
; 0001 0255                      };
_0x20032:
_0x20031:
; 0001 0256 
; 0001 0257 
; 0001 0258   if (++Timer_Sinc>7250)       //~1.5[s]
	LDI  R26,LOW(_Timer_Sinc)
	LDI  R27,HIGH(_Timer_Sinc)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	CPI  R30,LOW(0x1C53)
	LDI  R26,HIGH(0x1C53)
	CPC  R31,R26
	BRSH PC+3
	JMP _0x20035
; 0001 0259                     {
; 0001 025A                     #asm("wdr");
	wdr
; 0001 025B 
; 0001 025C                     Sinc_Bit=1;
	SBI  0x1E,2
; 0001 025D                     Timer_Sinc=0; //reset 1.5[s] timer
	LDI  R30,LOW(0)
	STS  _Timer_Sinc,R30
	STS  _Timer_Sinc+1,R30
; 0001 025E 
; 0001 025F                     CBX1_Done_bit=0;
	CBI  0x1E,4
; 0001 0260                     CBX2_Done_bit=0;
	CBI  0x1E,5
; 0001 0261 
; 0001 0262                     if (++Timer.Link_Timer>3) Led_Link=1;
	__GETB1MN _Timer,1
	SUBI R30,-LOW(1)
	__PUTB1MN _Timer,1
	CPI  R30,LOW(0x4)
	BRLO _0x2003C
	SBI  0x5,1
; 0001 0263                      else Led_Link=0;
	RJMP _0x2003F
_0x2003C:
	CBI  0x5,1
; 0001 0264 
; 0001 0265 
; 0001 0266                     Tmp2=0;
_0x2003F:
	LDI  R19,LOW(0)
; 0001 0267                     Tmp3=0;
	LDI  R18,LOW(0)
; 0001 0268                     for (Dat=0;Dat<6;Dat++) //Compare Old & New Data Rec. Data
	LDI  R17,LOW(0)
_0x20043:
	CPI  R17,6
	BRSH _0x20044
; 0001 0269                                 {
; 0001 026A                                 Tmp1=Rx1_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R16,Z
; 0001 026B                                 if (Compare_Buffer_1[Dat]!=Tmp1) Tmp2=1;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_1)
	SBCI R31,HIGH(-_Compare_Buffer_1)
	LD   R26,Z
	CP   R16,R26
	BREQ _0x20045
	LDI  R19,LOW(1)
; 0001 026C                                 Compare_Buffer_1[Dat]=Tmp1;
_0x20045:
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_1)
	SBCI R31,HIGH(-_Compare_Buffer_1)
	ST   Z,R16
; 0001 026D 
; 0001 026E                                 Tmp1=Rx2_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R16,Z
; 0001 026F                                 if (Compare_Buffer_2[Dat]!=Tmp1) Tmp3=1;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_2)
	SBCI R31,HIGH(-_Compare_Buffer_2)
	LD   R26,Z
	CP   R16,R26
	BREQ _0x20046
	LDI  R18,LOW(1)
; 0001 0270                                 Compare_Buffer_2[Dat]=Tmp1;
_0x20046:
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_2)
	SBCI R31,HIGH(-_Compare_Buffer_2)
	ST   Z,R16
; 0001 0271                                 }
	SUBI R17,-1
	RJMP _0x20043
_0x20044:
; 0001 0272 
; 0001 0273                     Tmp4=0;
	LDI  R21,LOW(0)
; 0001 0274                     for (Dat=0;Dat<6;Dat++)
	LDI  R17,LOW(0)
_0x20048:
	CPI  R17,6
	BRLO PC+3
	JMP _0x20049
; 0001 0275                                 {
; 0001 0276                                 RS485.Tx_Buffer[3+Dat]=Vault[Dat]; //Vault Number
	__POINTW2MN _RS485,4
	MOV  R30,R17
	SUBI R30,-LOW(3)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Vault)
	SBCI R27,HIGH(-_Vault)
	CALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
; 0001 0277 
; 0001 0278                                 if (Tmp2) Rx1_Buff[Dat]='0';
	CPI  R19,0
	BREQ _0x2004A
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 0279                                 if (Tmp3) Rx2_Buff[Dat]='0';
_0x2004A:
	CPI  R18,0
	BREQ _0x2004B
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 027A                                 RS485.Tx_Buffer[9+Dat]=Rx1_Buff[Dat]; //BIN Number
_0x2004B:
	__POINTW2MN _RS485,4
	MOV  R30,R17
	SUBI R30,-LOW(9)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R30,Z
	ST   X,R30
; 0001 027B                                 Tmp1=Rx2_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R16,Z
; 0001 027C                                 if (Tmp1!='0') Tmp4++;
	CPI  R16,48
	BREQ _0x2004C
	SUBI R21,-1
; 0001 027D                                 RS485.Tx_Buffer[15+Dat]=Tmp1;   //CBX Number
_0x2004C:
	__POINTW2MN _RS485,4
	MOV  R30,R17
	SUBI R30,-LOW(15)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R16
; 0001 027E                                 }
	SUBI R17,-1
	RJMP _0x20048
_0x20049:
; 0001 027F 
; 0001 0280 
; 0001 0281                     if (Send_Mode)
	SBIS 0x9,6
	RJMP _0x2004D
; 0001 0282                         {
; 0001 0283                             // ********************* Send Old Mode *********************
; 0001 0284 
; 0001 0285                             //Bin Number
; 0001 0286                             for (Dat=0;Dat<5;Dat++)
	LDI  R17,LOW(0)
_0x2004F:
	CPI  R17,5
	BRSH _0x20050
; 0001 0287                                 {
; 0001 0288                                 if (Rx1_Buff[Dat]==0x37) Rx1_ID_Buff[Dat+1]=0x0A;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x37)
	BRNE _0x20051
	MOV  R30,R17
	LDI  R31,0
	__ADDW1MN _Rx1_ID_Buff,1
	LDI  R26,LOW(10)
	STD  Z+0,R26
; 0001 0289                                  else Rx1_ID_Buff[Dat]=Rx1_Buff[Dat+1];
	RJMP _0x20052
_0x20051:
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Rx1_ID_Buff)
	SBCI R27,HIGH(-_Rx1_ID_Buff)
	MOV  R30,R17
	LDI  R31,0
	__ADDW1MN _Rx1_Buff,1
	LD   R30,Z
	ST   X,R30
; 0001 028A                                 };
_0x20052:
	SUBI R17,-1
	RJMP _0x2004F
_0x20050:
; 0001 028B 
; 0001 028C 
; 0001 028D                             //CBX Number
; 0001 028E                             for (Dat=0;Dat<5;Dat++)
	LDI  R17,LOW(0)
_0x20054:
	CPI  R17,5
	BRSH _0x20055
; 0001 028F                                 {
; 0001 0290                                 if (Rx2_Buff[Dat]==0x37) Rx2_ID_Buff[Dat+1]=0x0A;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x37)
	BRNE _0x20056
	MOV  R30,R17
	LDI  R31,0
	__ADDW1MN _Rx2_ID_Buff,1
	LDI  R26,LOW(10)
	STD  Z+0,R26
; 0001 0291                                  else Rx2_ID_Buff[Dat]=Rx2_Buff[Dat+1];
	RJMP _0x20057
_0x20056:
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Rx2_ID_Buff)
	SBCI R27,HIGH(-_Rx2_ID_Buff)
	MOV  R30,R17
	LDI  R31,0
	__ADDW1MN _Rx2_Buff,1
	LD   R30,Z
	ST   X,R30
; 0001 0292                                 }
_0x20057:
	SUBI R17,-1
	RJMP _0x20054
_0x20055:
; 0001 0293 
; 0001 0294 
; 0001 0295                             // if data >3999
; 0001 0296                             if ((!CBX1_Done_bit)&&((Rx1_ID_Buff[0]=!'0')||((Rx1_ID_Buff[1]&0x0F)>3)))
	SBIC 0x1E,4
	RJMP _0x20059
	LDI  R30,LOW(0)
	STS  _Rx1_ID_Buff,R30
	CPI  R30,0
	BRNE _0x2005A
	__GETB1MN _Rx1_ID_Buff,1
	ANDI R30,LOW(0xF)
	CPI  R30,LOW(0x4)
	BRLO _0x20059
_0x2005A:
	RJMP _0x2005C
_0x20059:
	RJMP _0x20058
_0x2005C:
; 0001 0297                                 {
; 0001 0298                                 Rx1_ID_Buff[1]='3';
	LDI  R30,LOW(51)
	__PUTB1MN _Rx1_ID_Buff,1
; 0001 0299                                 Rx1_ID_Buff[2]='9';
	LDI  R30,LOW(57)
	__PUTB1MN _Rx1_ID_Buff,2
; 0001 029A                                 Rx1_ID_Buff[3]='9';
	__PUTB1MN _Rx1_ID_Buff,3
; 0001 029B                                 Rx1_ID_Buff[4]='9';
	__PUTB1MN _Rx1_ID_Buff,4
; 0001 029C                                 };
_0x20058:
; 0001 029D 
; 0001 029E                             // if data >3999
; 0001 029F                             if ((Rx2_ID_Buff[0]=!'0')||((Rx2_ID_Buff[1]&0x0F)>3)) goto Bad_Value;
	LDI  R30,LOW(0)
	STS  _Rx2_ID_Buff,R30
	CPI  R30,0
	BRNE _0x2005E
	__GETB1MN _Rx2_ID_Buff,1
	ANDI R30,LOW(0xF)
	CPI  R30,LOW(0x4)
	BRLO _0x2005D
_0x2005E:
	RJMP _0x20060
; 0001 02A0 
; 0001 02A1                             if (!CBX2_Done_bit)
_0x2005D:
	SBIC 0x1E,5
	RJMP _0x20061
; 0001 02A2                                 {
; 0001 02A3                                 if (!CbxPS_Sensor)
	SBIC 0x3,3
	RJMP _0x20062
; 0001 02A4                                     {
; 0001 02A5                                     Bad_Value:
_0x20060:
; 0001 02A6                                             Rx2_ID_Buff[1]='3';
	LDI  R30,LOW(51)
	__PUTB1MN _Rx2_ID_Buff,1
; 0001 02A7                                             Rx2_ID_Buff[2]='9';
	LDI  R30,LOW(57)
	__PUTB1MN _Rx2_ID_Buff,2
; 0001 02A8                                             Rx2_ID_Buff[3]='9';
	__PUTB1MN _Rx2_ID_Buff,3
; 0001 02A9                                             Rx2_ID_Buff[4]='9';
	__PUTB1MN _Rx2_ID_Buff,4
; 0001 02AA                                     };
_0x20062:
; 0001 02AB                                 };
_0x20061:
; 0001 02AC 
; 0001 02AD                             CB_Tx_Counter1=8;
	LDI  R30,LOW(8)
	MOV  R10,R30
; 0001 02AE                             CB_Tx_Counter2=8;
	MOV  R13,R30
; 0001 02AF 
; 0001 02B0                         goto Exit_InT0;
	RJMP _0x20063
; 0001 02B1                         } //End send Old Mode
; 0001 02B2 
; 0001 02B3 
; 0001 02B4 
; 0001 02B5 
; 0001 02B6 
; 0001 02B7                     //Flush  Buffers CBX & BIN
; 0001 02B8                     for (Dat=0;Dat<6;Dat++)
_0x2004D:
	LDI  R17,LOW(0)
_0x20065:
	CPI  R17,6
	BRSH _0x20066
; 0001 02B9                             {
; 0001 02BA                             Rx1_Buff[Dat]='0';
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 02BB                             Rx2_Buff[Dat]='0';
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	STD  Z+0,R26
; 0001 02BC                             };
	SUBI R17,-1
	RJMP _0x20065
_0x20066:
; 0001 02BD                     }
; 0001 02BE                      // *** Send New Mode Data to CBX Computer ***
; 0001 02BF                      else  if (RS485.Tx_Counter<RS485.Tx_Size)
	RJMP _0x20067
_0x20035:
	__GETB2MN _RS485,3
	__GETB1MN _RS485,1
	LDI  R27,0
	SBRC R26,7
	SER  R27
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRLT PC+3
	JMP _0x20068
; 0001 02C0                                {
; 0001 02C1                                 RS485_Dir=1;
	SBI  0x5,5
; 0001 02C2                                 if (RS485.Tx_Counter<0) RS485.Tx_Counter++;
	__GETB2MN _RS485,3
	CPI  R26,0
	BRGE _0x2006B
	__GETB1MN _RS485,3
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,3
	SUBI R30,LOW(1)
; 0001 02C3                                  else if (!(LINSIR&0x10))
	RJMP _0x2006C
_0x2006B:
	LDS  R30,201
	ANDI R30,LOW(0x10)
	BRNE _0x2006D
; 0001 02C4                                         {
; 0001 02C5                                         Dat=RS485.Tx_Buffer[RS485.Tx_Counter++];
	__POINTW2MN _RS485,4
	__GETB1MN _RS485,3
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,3
	SUBI R30,LOW(1)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R17,X
; 0001 02C6                                         if (RS485.Tx_Counter>RS485.Tx_Size-2) //Prepare LRC_H and LRC_L
	__GETB2MN _RS485,3
	__GETB1MN _RS485,1
	SUBI R30,LOW(2)
	LDI  R27,0
	SBRC R26,7
	SER  R27
	LDI  R31,0
	CP   R30,R26
	CPC  R31,R27
	BRGE _0x2006E
; 0001 02C7                                             {
; 0001 02C8                                             if (RS485.Tx_Counter==RS485.Tx_Size-1) Dat=0x30|((RS485.LRC&0xF0)>>4);
	__GETB2MN _RS485,3
	__GETB1MN _RS485,1
	SUBI R30,LOW(1)
	CP   R30,R26
	BRNE _0x2006F
	__GETB1MN _RS485,2
	ANDI R30,LOW(0xF0)
	SWAP R30
	ANDI R30,0xF
	RJMP _0x2017F
; 0001 02C9                                               else Dat=0x30|(RS485.LRC&0x0F);
_0x2006F:
	__GETB1MN _RS485,2
	ANDI R30,LOW(0xF)
_0x2017F:
	ORI  R30,LOW(0x30)
	MOV  R17,R30
; 0001 02CA                                             }
; 0001 02CB                                          else RS485.LRC=RS485.LRC^Dat; //Calculate LRC in fly
	RJMP _0x20071
_0x2006E:
	__GETB1MN _RS485,2
	EOR  R30,R17
	__PUTB1MN _RS485,2
; 0001 02CC 
; 0001 02CD                                          LINDAT=Dat;
_0x20071:
	STS  210,R17
; 0001 02CE                                         }
; 0001 02CF                                 }
_0x2006D:
_0x2006C:
; 0001 02D0                                  else
	RJMP _0x20072
_0x20068:
; 0001 02D1                                      {
; 0001 02D2                                       if (!(LINSIR&0x10))
	LDS  R30,201
	ANDI R30,LOW(0x10)
	BREQ PC+3
	JMP _0x20073
; 0001 02D3                                            {
; 0001 02D4                                             RS485_Dir=0;
	CBI  0x5,5
; 0001 02D5                                             if (LINSIR&1)
	LDS  R30,201
	ANDI R30,LOW(0x1)
	BRNE PC+3
	JMP _0x20076
; 0001 02D6                                                 {
; 0001 02D7                                                 Dat=LINDAT;
	LDS  R17,210
; 0001 02D8                                                 RS485.Rx_Counter=RS485.Rx_Counter&0x1F;
	LDS  R30,_RS485
	ANDI R30,LOW(0x1F)
	STS  _RS485,R30
; 0001 02D9 
; 0001 02DA                                                 if (Dat==STX) //Begin command
	CPI  R17,2
	BRNE _0x20077
; 0001 02DB                                                     {
; 0001 02DC                                                     RS485.Rx_Counter=0;
	LDI  R30,LOW(0)
	STS  _RS485,R30
; 0001 02DD                                                     RS485.LRC=STX;
	LDI  R30,LOW(2)
	__PUTB1MN _RS485,2
; 0001 02DE                                                     }
; 0001 02DF 
; 0001 02E0                                                 RS485.Rx_Buffer[(RS485.Rx_Counter++)&0x1F]=Dat;
_0x20077:
	LDS  R26,_RS485
	SUBI R26,-LOW(1)
	STS  _RS485,R26
	SUBI R26,LOW(1)
	LDI  R30,LOW(31)
	AND  R30,R26
	__POINTW2MN _RS485,36
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R17
; 0001 02E1                                                 RS485.Rx_Buffer[(RS485.Rx_Counter)&0x1F]=0xFF;
	__POINTW2MN _RS485,36
	LDS  R30,_RS485
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(255)
	ST   X,R30
; 0001 02E2                                                 RS485.LRC^=Dat; //Calculate LRC
	__GETB1MN _RS485,2
	EOR  R30,R17
	__PUTB1MN _RS485,2
; 0001 02E3 
; 0001 02E4                                                 if (((RS485.LRC&0x7F)==0x00)&&(RS485.Rx_Buffer[(RS485.Rx_Counter-2)&0x1F])==ETX) //End of transfer
	__GETB1MN _RS485,2
	ANDI R30,0x7F
	CPI  R30,0
	BRNE _0x20079
	__POINTW2MN _RS485,36
	LDS  R30,_RS485
	SUBI R30,LOW(2)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R26,X
	CPI  R26,LOW(0x3)
	BREQ _0x2007A
_0x20079:
	RJMP _0x20078
_0x2007A:
; 0001 02E5                                                     {
; 0001 02E6                                                     if ((RS485.Rx_Buffer[0]==STX)&&(RS485.Rx_Buffer[1]==Node_Address))
	__GETB2MN _RS485,36
	CPI  R26,LOW(0x2)
	BRNE _0x2007C
	__GETB2MN _RS485,37
	LDS  R30,_Node_Address
	CP   R30,R26
	BREQ _0x2007D
_0x2007C:
	RJMP _0x2007B
_0x2007D:
; 0001 02E7                                                         {
; 0001 02E8                                                         RS485.Tx_Buffer[1]=Node_Address;
	LDS  R30,_Node_Address
	__PUTB1MN _RS485,5
; 0001 02E9                                                         Dat=RS485.Rx_Buffer[2];
	__GETBRMN 17,_RS485,38
; 0001 02EA                                                         Timer.Link_Timer=0;
	LDI  R30,LOW(0)
	__PUTB1MN _Timer,1
; 0001 02EB 
; 0001 02EC                                                         //Command Poll
; 0001 02ED                                                         if ((Dat=='P')&&((RS485.Rx_Counter==5)||(RS485.Rx_Counter==6)))
	CPI  R17,80
	BRNE _0x2007F
	LDS  R26,_RS485
	CPI  R26,LOW(0x5)
	BREQ _0x20080
	CPI  R26,LOW(0x6)
	BRNE _0x2007F
_0x20080:
	RJMP _0x20082
_0x2007F:
	RJMP _0x2007E
_0x20082:
; 0001 02EE                                                             {
; 0001 02EF                                                             if (RS485.Rx_Counter==6)
	LDS  R26,_RS485
	CPI  R26,LOW(0x6)
	BRNE _0x20083
; 0001 02F0                                                                  {
; 0001 02F1                                                                   Status_Lights=RS485.Rx_Buffer[3]&0x33;
	__GETB1MN _RS485,39
	ANDI R30,LOW(0x33)
	STS  _Status_Lights,R30
; 0001 02F2                                                                   Out_Lig1=Status_Lights&1;
	ANDI R30,LOW(0x1)
	BRNE _0x20084
	CBI  0xB,7
	RJMP _0x20085
_0x20084:
	SBI  0xB,7
_0x20085:
; 0001 02F3                                                                   Out_Lig2=(Status_Lights>>1)&1;
	LDS  R30,_Status_Lights
	LSR  R30
	ANDI R30,LOW(0x1)
	BRNE _0x20086
	CBI  0xB,2
	RJMP _0x20087
_0x20086:
	SBI  0xB,2
_0x20087:
; 0001 02F4                                                                  }
; 0001 02F5 
; 0001 02F6                                                                 RS485.Tx_Buffer[22]=0x30|Status_Lights; //Status Light
_0x20083:
	LDS  R30,_Status_Lights
	ORI  R30,LOW(0x30)
	__PUTB1MN _RS485,26
; 0001 02F7                                                                 RS485.Tx_Buffer[0]=STX;
	LDI  R30,LOW(2)
	__PUTB1MN _RS485,4
; 0001 02F8                                                                 RS485.Tx_Buffer[2]='P';
	LDI  R30,LOW(80)
	__PUTB1MN _RS485,6
; 0001 02F9                                                                 RS485.Tx_Buffer[21]=(0x30+!RecPS_Sensor|(!CbxPS_Sensor<<1)|(Out_Valve<<2)); //Sensor Status
	LDI  R30,0
	SBIS 0x6,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R0,R30
	LDI  R26,0
	SBIS 0x3,3
	LDI  R26,1
	MOV  R30,R26
	LSL  R30
	OR   R0,R30
	LDI  R26,0
	SBIC 0x8,1
	LDI  R26,1
	MOV  R30,R26
	LSL  R30
	LSL  R30
	OR   R30,R0
	__PUTB1MN _RS485,25
; 0001 02FA                                                                 RS485.Tx_Buffer[23]=Alarm_Status; //Alarm Status
	LDS  R30,_Alarm_Status
	__PUTB1MN _RS485,27
; 0001 02FB                                                                 RS485.Tx_Buffer[24]=ETX;
	LDI  R30,LOW(3)
	__PUTB1MN _RS485,28
; 0001 02FC                                                                 RS485.Tx_Buffer[25]=EOT;
	LDI  R30,LOW(4)
	__PUTB1MN _RS485,29
; 0001 02FD                                                                 RS485.Tx_Counter=-5;
	LDI  R30,LOW(251)
	__PUTB1MN _RS485,3
; 0001 02FE                                                                 //RS485.Tx_Buffer[26]=LRC_H;
; 0001 02FF                                                                 //RS485.Tx_Buffer[27]=LRC_L;
; 0001 0300                                                                 RS485.Tx_Size=28;
	LDI  R30,LOW(28)
	__PUTB1MN _RS485,1
; 0001 0301                                                                 RS485.LRC=STX;
	LDI  R30,LOW(2)
	__PUTB1MN _RS485,2
; 0001 0302                                                                 goto Exit_InT00;
	RJMP _0x20088
; 0001 0303                                                             }
; 0001 0304 
; 0001 0305 
; 0001 0306                                                         //Get All
; 0001 0307                                                         if ((Dat=='A')&&((RS485.Rx_Counter==5)||(RS485.Rx_Counter==6)))
_0x2007E:
	CPI  R17,65
	BRNE _0x2008A
	LDS  R26,_RS485
	CPI  R26,LOW(0x5)
	BREQ _0x2008B
	CPI  R26,LOW(0x6)
	BRNE _0x2008A
_0x2008B:
	RJMP _0x2008D
_0x2008A:
	RJMP _0x20089
_0x2008D:
; 0001 0308                                                             {
; 0001 0309                                                             Tmp1=(RS485.Rx_Buffer[3]-0x30);
	__GETB1MN _RS485,39
	SUBI R30,LOW(48)
	MOV  R16,R30
; 0001 030A                                                             if ((RS485.Rx_Counter==6)&&(Tmp1>=5)&&(Tmp1<=25))
	LDS  R26,_RS485
	CPI  R26,LOW(0x6)
	BRNE _0x2008F
	CPI  R16,5
	BRLO _0x2008F
	CPI  R16,26
	BRLO _0x20090
_0x2008F:
	RJMP _0x2008E
_0x20090:
; 0001 030B                                                               {
; 0001 030C                                                                Solenoid_Time=Tmp1;
	MOV  R30,R16
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMWRB
; 0001 030D                                                                RS485.Get_All_Command[0]=(Tmp1/10)+0x30;
	MOV  R26,R16
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	__PUTB1MN _RS485,68
; 0001 030E                                                                RS485.Get_All_Command[1]=(Tmp1%10)+0x30;
	MOV  R26,R16
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	__PUTB1MN _RS485,69
; 0001 030F                                                                Refresh_Bit=1;
	SBI  0x1E,3
; 0001 0310                                                               }
; 0001 0311 
; 0001 0312                                                             RS485.Tx_Buffer[0]=STX;
_0x2008E:
	LDI  R30,LOW(2)
	__PUTB1MN _RS485,4
; 0001 0313                                                             RS485.Tx_Buffer[2]='A';
	LDI  R30,LOW(65)
	__PUTB1MN _RS485,6
; 0001 0314                                                             RS485.Tx_Buffer[25]=ETX;
	LDI  R30,LOW(3)
	__PUTB1MN _RS485,29
; 0001 0315                                                             RS485.Tx_Buffer[26]=EOT;
	LDI  R30,LOW(4)
	__PUTB1MN _RS485,30
; 0001 0316                                                             for (Dat=0;Dat<22;Dat++) RS485.Tx_Buffer[Dat+3]=RS485.Get_All_Command[Dat];
	LDI  R17,LOW(0)
_0x20094:
	CPI  R17,22
	BRSH _0x20095
	__POINTW2MN _RS485,4
	MOV  R30,R17
	SUBI R30,-LOW(3)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	__POINTW2MN _RS485,68
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R30,X
	MOVW R26,R0
	ST   X,R30
	SUBI R17,-1
	RJMP _0x20094
_0x20095:
; 0001 0317 RS485.Tx_Counter=-5;
	LDI  R30,LOW(251)
	__PUTB1MN _RS485,3
; 0001 0318                                                             RS485.Tx_Size=27;
	LDI  R30,LOW(27)
	__PUTB1MN _RS485,1
; 0001 0319 
; 0001 031A                                                             goto Exit_InT0;
	RJMP _0x20063
; 0001 031B                                                             }
; 0001 031C                                                         }
_0x20089:
; 0001 031D                                                     }
_0x2007B:
; 0001 031E                                                 }
_0x20078:
; 0001 031F                                            }
_0x20076:
; 0001 0320                                      };//End Send New Mode Data Format
_0x20073:
_0x20072:
_0x20067:
; 0001 0321 
; 0001 0322         if (Send_Mode)
	SBIS 0x9,6
	RJMP _0x20096
; 0001 0323                 {
; 0001 0324 
; 0001 0325                 LINCR=0x00; //Disable Uart
	LDI  R30,LOW(0)
	STS  200,R30
; 0001 0326 
; 0001 0327                 if (CB_Tx_Counter1<255)   //Send CBX1 Old Mode
	LDI  R30,LOW(255)
	CP   R10,R30
	BRSH _0x20097
; 0001 0328                   {
; 0001 0329                     if (!(CB_Tx_Counter1&8))
	SBRC R10,3
	RJMP _0x20098
; 0001 032A                       {
; 0001 032B                        Dat=Rx2_ID_Buff[1+((CB_Tx_Counter1>>6)&3)];
	MOV  R30,R10
	SWAP R30
	ANDI R30,0xF
	LSR  R30
	LSR  R30
	ANDI R30,LOW(0x3)
	SUBI R30,-LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_ID_Buff)
	SBCI R31,HIGH(-_Rx2_ID_Buff)
	LD   R17,Z
; 0001 032C                        if (!((Dat<<((CB_Tx_Counter1>>4)&3))&8)) Out_ID=1;
	MOV  R30,R10
	SWAP R30
	ANDI R30,LOW(0x3)
	MOV  R26,R17
	CALL __LSLB12
	ANDI R30,LOW(0x8)
	BRNE _0x20099
	SBI  0xB,3
; 0001 032D                       }
_0x20099:
; 0001 032E                        else Out_ID=0;
	RJMP _0x2009C
_0x20098:
	CBI  0xB,3
; 0001 032F 
; 0001 0330                   CB_Tx_Counter1++;
_0x2009C:
	INC  R10
; 0001 0331                   }
; 0001 0332                    else Out_ID=0;
	RJMP _0x2009F
_0x20097:
	CBI  0xB,3
; 0001 0333 
; 0001 0334 
; 0001 0335 
; 0001 0336                 if (CB_Tx_Counter2<255)   //Send CBX2 Old Mode
_0x2009F:
	LDI  R30,LOW(255)
	CP   R13,R30
	BRSH _0x200A2
; 0001 0337                   {
; 0001 0338                    if (!(CB_Tx_Counter2&8))
	SBRC R13,3
	RJMP _0x200A3
; 0001 0339                      {
; 0001 033A                      Dat=Rx1_ID_Buff[1+((CB_Tx_Counter2>>6)&3)];
	MOV  R30,R13
	SWAP R30
	ANDI R30,0xF
	LSR  R30
	LSR  R30
	ANDI R30,LOW(0x3)
	SUBI R30,-LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_ID_Buff)
	SBCI R31,HIGH(-_Rx1_ID_Buff)
	LD   R17,Z
; 0001 033B                      if (!((Dat<<((CB_Tx_Counter2>>4)&3))&8)) Out_ID2=1;
	MOV  R30,R13
	SWAP R30
	ANDI R30,LOW(0x3)
	MOV  R26,R17
	CALL __LSLB12
	ANDI R30,LOW(0x8)
	BRNE _0x200A4
	SBI  0x5,4
; 0001 033C                      }
_0x200A4:
; 0001 033D                       else Out_ID2=0;
	RJMP _0x200A7
_0x200A3:
	CBI  0x5,4
; 0001 033E 
; 0001 033F                   CB_Tx_Counter2++;
_0x200A7:
	INC  R13
; 0001 0340                   }
; 0001 0341                    else Out_ID2=0;
	RJMP _0x200AA
_0x200A2:
	CBI  0x5,4
; 0001 0342                 }//End Send  Old Mode Data Format
_0x200AA:
; 0001 0343 
; 0001 0344 
; 0001 0345 Exit_InT00:
_0x20096:
_0x20088:
; 0001 0346 		 if (!Timer.Time_P--)
	LDS  R30,_Timer
	SUBI R30,LOW(1)
	STS  _Timer,R30
	SUBI R30,-LOW(1)
	BREQ PC+3
	JMP _0x200AD
; 0001 0347                 {
; 0001 0348                 Timer.Time_P=239;     //50ms
	LDI  R30,LOW(239)
	STS  _Timer,R30
; 0001 0349 
; 0001 034A                 //Sys LED Blink
; 0001 034B                 if (++Led_Blink&0x08) Led_Sys=0;
	LDS  R26,_Led_Blink
	SUBI R26,-LOW(1)
	STS  _Led_Blink,R26
	ANDI R26,LOW(0x8)
	BREQ _0x200AE
	CBI  0x8,6
; 0001 034C                  else Led_Sys=1;
	RJMP _0x200B1
_0x200AE:
	SBI  0x8,6
; 0001 034D 
; 0001 034E                 if (RecPS_Sensor&CbxPS_Sensor)
_0x200B1:
	LDI  R26,0
	SBIC 0x6,0
	LDI  R26,1
	LDI  R30,0
	SBIC 0x3,3
	LDI  R30,1
	AND  R30,R26
	BREQ _0x200B4
; 0001 034F                 {
; 0001 0350                 Solenoid_Td=0;
	LDI  R30,LOW(0)
	STS  _Solenoid_Td,R30
	STS  _Solenoid_Td+1,R30
; 0001 0351                 Timer.Time=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timer,4
; 0001 0352                 }
; 0001 0353 
; 0001 0354                 //***** Vault Holding Timer *********
; 0001 0355                 if (Timer.Time>30) Timer.Time--;
_0x200B4:
	__GETW2MN _Timer,4
	SBIW R26,31
	BRLO _0x200B5
	__POINTW2MN _Timer,4
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0001 0356                  else
	RJMP _0x200B6
_0x200B5:
; 0001 0357                     {
; 0001 0358                      if (Timer.Time>1)
	__GETW2MN _Timer,4
	SBIW R26,2
	BRLO _0x200B7
; 0001 0359                         {
; 0001 035A                         Timer.Time--;
	__POINTW2MN _Timer,4
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0001 035B                         if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Out_Valve=1;      //Solenoid On
	SBIS 0x6,0
	RJMP _0x200B9
	SBIC 0x3,3
	RJMP _0x200B8
_0x200B9:
	SBI  0x8,1
; 0001 035C                           else {
	RJMP _0x200BD
_0x200B8:
; 0001 035D                                Out_Valve=0; //Solenoid Off
	CBI  0x8,1
; 0001 035E                                }
_0x200BD:
; 0001 035F                         }
; 0001 0360                         else
	RJMP _0x200C0
_0x200B7:
; 0001 0361                             {
; 0001 0362                               Out_Valve=0; //Solenoid Off  , Time Out
	CBI  0x8,1
; 0001 0363                               if ((!RecPS_Sensor)||(!CbxPS_Sensor))
	SBIS 0x6,0
	RJMP _0x200C4
	SBIC 0x3,3
	RJMP _0x200C3
_0x200C4:
; 0001 0364                                 {
; 0001 0365                                  if (++Solenoid_Td>=30)
	LDI  R26,LOW(_Solenoid_Td)
	LDI  R27,HIGH(_Solenoid_Td)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,30
	BRLO _0x200C6
; 0001 0366                                     {
; 0001 0367                                      Timer.Time=Solenoid_Time;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	__POINTW2MN _Timer,4
	LDI  R31,0
	ST   X+,R30
	ST   X,R31
; 0001 0368                                      Timer.Time*=20;
	__GETW2MN _Timer,4
	LDI  R30,LOW(20)
	CALL __MULB1W2U
	__PUTW1MN _Timer,4
; 0001 0369                                      Timer.Counting=1;
	LDI  R30,LOW(1)
	__PUTB1MN _Timer,2
; 0001 036A                                      }
; 0001 036B                                 }
_0x200C6:
; 0001 036C                             }
_0x200C3:
_0x200C0:
; 0001 036D                     }
_0x200B6:
; 0001 036E                 }
; 0001 036F 
; 0001 0370 Exit_InT0:
_0x200AD:
_0x20063:
; 0001 0371 
; 0001 0372   if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Power_Light=0; //Power Up Led Light
	SBIS 0x6,0
	RJMP _0x200C8
	SBIC 0x3,3
	RJMP _0x200C7
_0x200C8:
	CBI  0x7,7
; 0001 0373    else Power_Light=1;
	RJMP _0x200CC
_0x200C7:
	SBI  0x7,7
; 0001 0374 
; 0001 0375   #asm("cli");
_0x200CC:
	cli
; 0001 0376 
; 0001 0377 #pragma optsize+
; 0001 0378 };
	CALL __LOADLOCR6
	ADIW R28,6
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R22,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;
;
;
;
;
;
;#include "look_up_tbl.h"
;
;#pragma optsize-
;//***********************  Timer1 output compare A interrupt service routine ***********************
;interrupt [TIM1_COMPA] void timer1_compa_isr(void)
; 0001 0385 {
_timer1_compa_isr:
	ST   -Y,R26
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0001 0386 
; 0001 0387 unsigned char Tmp_;
; 0001 0388 
; 0001 0389 
; 0001 038A 
; 0001 038B   //******** Software Trasmit for Debuging TxD ***************
; 0001 038C 
; 0001 038D                     #asm
	ST   -Y,R17
;	Tmp_ -> R17
; 0001 038E             ;if (++Tx1_Counter<72)
            ;if (++Tx1_Counter<72)
; 0001 038F                     LDS     R31,_Tx1_Counter
                    LDS     R31,_Tx1_Counter
; 0001 0390                     INC     R31
                    INC     R31
; 0001 0391                     CPI     R31,72
                    CPI     R31,72
; 0001 0392                     BRCC    Exit_Else_Tx1
                    BRCC    Exit_Else_Tx1
; 0001 0393 

; 0001 0394             ;if (Tx1_Counter<8) _Soft_TxD=0;
            ;if (Tx1_Counter<8) _Soft_TxD=0;
; 0001 0395                     CPI     R31,8
                    CPI     R31,8
; 0001 0396                     BRCC    Else_Tx1
                    BRCC    Else_Tx1
; 0001 0397                     CBI     _PORTC,_Soft_TxD      ;_Soft_TxD=0
                    CBI     _PORTC,_Soft_TxD      ;_Soft_TxD=0
; 0001 0398                     RJMP    Exit_Tx1
                    RJMP    Exit_Tx1
; 0001 0399 

; 0001 039A             ;if (!(Tx1_Counter&7))
            ;if (!(Tx1_Counter&7))
; 0001 039B Else_Tx1:           MOV     R30,R31
Else_Tx1:           MOV     R30,R31
; 0001 039C                     ANDI    R30,0x07
                    ANDI    R30,0x07
; 0001 039D                     BRNE    Exit_Tx1
                    BRNE    Exit_Tx1
; 0001 039E 

; 0001 039F             ;if (TxD_1&0x80) _TxD_Dbg=0;
            ;if (TxD_1&0x80) _TxD_Dbg=0;
; 0001 03A0                     LDS     R30,_TxD_1
                    LDS     R30,_TxD_1
; 0001 03A1 

; 0001 03A2                     SBRS    R30,0
                    SBRS    R30,0
; 0001 03A3                     CBI     _PORTC,_Soft_TxD     ;_Soft_TxD=0
                    CBI     _PORTC,_Soft_TxD     ;_Soft_TxD=0
; 0001 03A4                     SBRC    R30,0
                    SBRC    R30,0
; 0001 03A5 

; 0001 03A6             ;else
            ;else
; 0001 03A7                     SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
                    SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
; 0001 03A8                     LSR     R30                   ;TxD_1>>=1;
                    LSR     R30                   ;TxD_1>>=1;
; 0001 03A9                     STS     _TxD_1,R30
                    STS     _TxD_1,R30
; 0001 03AA                     RJMP    Exit_Tx1
                    RJMP    Exit_Tx1
; 0001 03AB 

; 0001 03AC Exit_Else_Tx1:      SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
Exit_Else_Tx1:      SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
; 0001 03AD                     CPI     R31,80
                    CPI     R31,80
; 0001 03AE                     BRCS    Exit_Tx1
                    BRCS    Exit_Tx1
; 0001 03AF                     LDI     R31,80
                    LDI     R31,80
; 0001 03B0 

; 0001 03B1 Exit_Tx1:           STS     _Tx1_Counter,R31
Exit_Tx1:           STS     _Tx1_Counter,R31
; 0001 03B2                     #endasm
; 0001 03B3 
; 0001 03B4 
; 0001 03B5 
; 0001 03B6 //if(!Mode_1)
; 0001 03B7            {
; 0001 03B8             if (++Rx1_Counter<78) //Software RxD_1  for CBX_1
	INC  R3
	LDI  R30,LOW(78)
	CP   R3,R30
	BRSH _0x200CF
; 0001 03B9                     {
; 0001 03BA                     if (!In_1) Tmp_Rx1&=Bit_Table[Rx1_Counter];
	SBIC 0x3,6
	RJMP _0x200D0
	MOV  R30,R3
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	AND  R5,R30
; 0001 03BB                     }
_0x200D0:
; 0001 03BC                     else
	RJMP _0x200D1
_0x200CF:
; 0001 03BD                         {
; 0001 03BE                          if (Rx1_Counter==78)
	LDI  R30,LOW(78)
	CP   R30,R3
	BRNE _0x200D2
; 0001 03BF                             {
; 0001 03C0                             if (!Tmp_Rx1) Mode_1=1;
	TST  R5
	BRNE _0x200D3
	SBI  0x1E,0
; 0001 03C1                             if (!Mode_1) Rx1_Buff[(Rx1_Pointer_Buff++)&0x07]=Tmp_Rx1;
_0x200D3:
	SBIC 0x1E,0
	RJMP _0x200D6
	MOV  R30,R2
	INC  R2
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	ST   Z,R5
; 0001 03C2                             Time_Out_1=0;
_0x200D6:
	CLR  R4
; 0001 03C3                             CBX1_Done_bit=1;
	SBI  0x1E,4
; 0001 03C4                             }
; 0001 03C5                             else
	RJMP _0x200D9
_0x200D2:
; 0001 03C6                                 {
; 0001 03C7                                 Rx1_Counter=78;
	LDI  R30,LOW(78)
	MOV  R3,R30
; 0001 03C8                                 Tmp_Rx1=0xFF;
	LDI  R30,LOW(255)
	MOV  R5,R30
; 0001 03C9                                 if (In_1==0) Rx1_Counter=0;
	SBIS 0x3,6
	CLR  R3
; 0001 03CA                                 };
_0x200D9:
; 0001 03CB                         };
_0x200D1:
; 0001 03CC             }
; 0001 03CD //             else
; 0001 03CE                  {
; 0001 03CF                   Tmp_=In_1;
	LDI  R30,0
	SBIC 0x3,6
	LDI  R30,1
	MOV  R17,R30
; 0001 03D0                   if ((Tmp_)&&(!Old_CBX1)) {
	CPI  R17,0
	BREQ _0x200DC
	SBIS 0x1E,7
	RJMP _0x200DD
_0x200DC:
	RJMP _0x200DB
_0x200DD:
; 0001 03D1                                               CBX1_bit=1;
	SBI  0x1E,6
; 0001 03D2                                               if (CB1_Time>=250) CB1_Time=0;
	LDI  R30,LOW(250)
	CP   R12,R30
	BRLO _0x200E0
	CLR  R12
; 0001 03D3                                               }
_0x200E0:
; 0001 03D4                   Old_CBX1=Tmp_;
_0x200DB:
	CPI  R17,0
	BRNE _0x200E1
	CBI  0x1E,7
	RJMP _0x200E2
_0x200E1:
	SBI  0x1E,7
_0x200E2:
; 0001 03D5                  };
; 0001 03D6 
; 0001 03D7 
; 0001 03D8 
; 0001 03D9 //if(!Mode_2)
; 0001 03DA           {
; 0001 03DB             if (++Rx2_Counter<78) //Software RxD_2  for CBX_2
	INC  R7
	LDI  R30,LOW(78)
	CP   R7,R30
	BRSH _0x200E3
; 0001 03DC                     {
; 0001 03DD                     if (!In_2) Tmp_Rx2=Tmp_Rx2&Bit_Table[Rx2_Counter];
	SBIC 0x6,7
	RJMP _0x200E4
	MOV  R30,R7
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	AND  R9,R30
; 0001 03DE                     }
_0x200E4:
; 0001 03DF                     else
	RJMP _0x200E5
_0x200E3:
; 0001 03E0                         {
; 0001 03E1                          if (Rx2_Counter==78)
	LDI  R30,LOW(78)
	CP   R30,R7
	BRNE _0x200E6
; 0001 03E2                             {
; 0001 03E3                             if (!Tmp_Rx2) Mode_2=1;
	TST  R9
	BRNE _0x200E7
	SBI  0x1E,1
; 0001 03E4                             if (!Mode_2) Rx2_Buff[(Rx2_Pointer_Buff++)&0x07]=Tmp_Rx2;
_0x200E7:
	SBIC 0x1E,1
	RJMP _0x200EA
	MOV  R30,R6
	INC  R6
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	ST   Z,R9
; 0001 03E5                              Time_Out_2=0;
_0x200EA:
	CLR  R8
; 0001 03E6                              CBX2_Done_bit=1;
	SBI  0x1E,5
; 0001 03E7                              }
; 0001 03E8                              else
	RJMP _0x200ED
_0x200E6:
; 0001 03E9                                 {
; 0001 03EA                                 Rx2_Counter=78;
	LDI  R30,LOW(78)
	MOV  R7,R30
; 0001 03EB                                 Tmp_Rx2=0xFF;
	LDI  R30,LOW(255)
	MOV  R9,R30
; 0001 03EC                                 if (!In_2) Rx2_Counter=0;
	SBIS 0x6,7
	CLR  R7
; 0001 03ED                                 };
_0x200ED:
; 0001 03EE                         }
_0x200E5:
; 0001 03EF             }
; 0001 03F0   //            else
; 0001 03F1                   {
; 0001 03F2                    Tmp_=In_2;
	LDI  R30,0
	SBIC 0x6,7
	LDI  R30,1
	MOV  R17,R30
; 0001 03F3                    if ((Tmp_)&&(!Old_CBX2))
	CPI  R17,0
	BREQ _0x200F0
	SBIS 0x19,1
	RJMP _0x200F1
_0x200F0:
	RJMP _0x200EF
_0x200F1:
; 0001 03F4                                            {
; 0001 03F5                                             CBX2_bit=1;
	SBI  0x19,0
; 0001 03F6                                             if (CB2_Time>=250) CB2_Time=0;
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xFA)
	BRLO _0x200F4
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 03F7                                             }
_0x200F4:
; 0001 03F8                     Old_CBX2=Tmp_;
_0x200EF:
	CPI  R17,0
	BRNE _0x200F5
	CBI  0x19,1
	RJMP _0x200F6
_0x200F5:
	SBI  0x19,1
_0x200F6:
; 0001 03F9                    };
; 0001 03FA 
; 0001 03FB 
; 0001 03FC 
; 0001 03FD  if (++Rx3_Counter<78)   //Software Receive for Debuging RxD
	LDS  R26,_Rx3_Counter
	SUBI R26,-LOW(1)
	STS  _Rx3_Counter,R26
	CPI  R26,LOW(0x4E)
	BRSH _0x200F7
; 0001 03FE                     {
; 0001 03FF                     if (!In_3) Tmp_Rx3=Tmp_Rx3&Bit_Table[Rx3_Counter];
	SBIC 0x6,3
	RJMP _0x200F8
	LDS  R30,_Rx3_Counter
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	LDS  R26,_Tmp_Rx3
	AND  R30,R26
	STS  _Tmp_Rx3,R30
; 0001 0400                     }
_0x200F8:
; 0001 0401                     else
	RJMP _0x200F9
_0x200F7:
; 0001 0402                         {
; 0001 0403                          if (Rx3_Counter==78) Rx3_Buff[(Rx3_Pointer_Buff++)&0x07]=Tmp_Rx3;
	LDS  R26,_Rx3_Counter
	CPI  R26,LOW(0x4E)
	BRNE _0x200FA
	LDS  R26,_Rx3_Pointer_Buff
	SUBI R26,-LOW(1)
	STS  _Rx3_Pointer_Buff,R26
	SUBI R26,LOW(1)
	LDI  R30,LOW(7)
	AND  R30,R26
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDS  R26,_Tmp_Rx3
	STD  Z+0,R26
; 0001 0404                              else
	RJMP _0x200FB
_0x200FA:
; 0001 0405                                 {
; 0001 0406                                 Rx3_Counter=78;
	LDI  R30,LOW(78)
	STS  _Rx3_Counter,R30
; 0001 0407                                 Tmp_Rx3=0xFF;
	LDI  R30,LOW(255)
	STS  _Tmp_Rx3,R30
; 0001 0408                                 if (!In_3) Rx3_Counter=0;
	SBIC 0x6,3
	RJMP _0x200FC
	LDI  R30,LOW(0)
	STS  _Rx3_Counter,R30
; 0001 0409                                 };
_0x200FC:
_0x200FB:
; 0001 040A                         };
_0x200F9:
; 0001 040B 
; 0001 040C #pragma optsize+
; 0001 040D };
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R26,Y+
	RETI
;
;
;
;
;#define ADC_VREF_TYPE 0x00
;// Read the AD conversion result
;
;unsigned int read_adc(unsigned char adc_input)
; 0001 0416 {
_read_adc:
; 0001 0417 ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
	ST   -Y,R26
;	adc_input -> Y+0
	LD   R30,Y
	STS  124,R30
; 0001 0418 // Delay needed for the stabilization of the ADC input voltage
; 0001 0419 delay_us(10);
	__DELAY_USB 49
; 0001 041A // Start the AD conversion
; 0001 041B ADCSRA|=0x40;
	LDS  R30,122
	ORI  R30,0x40
	STS  122,R30
; 0001 041C // Wait for the AD conversion to complete
; 0001 041D while ((ADCSRA & 0x10)==0);
_0x200FD:
	LDS  R30,122
	ANDI R30,LOW(0x10)
	BREQ _0x200FD
; 0001 041E ADCSRA|=0x10;
	LDS  R30,122
	ORI  R30,0x10
	STS  122,R30
; 0001 041F return ADCW;
	LDS  R30,120
	LDS  R31,120+1
_0x2000001:
	ADIW R28,1
	RET
; 0001 0420 }
;
;
;
;//*************************************** MAIN ***********************************************
;
;void main(void)
; 0001 0427 {
_main:
; 0001 0428 
; 0001 0429 #pragma optsize+
; 0001 042A 
; 0001 042B unsigned char Temp_Byte,Temp_Byte2;
; 0001 042C 
; 0001 042D #ifdef _Test_
; 0001 042E unsigned char Cmp1,Cmp2,Err1,Err2;
; 0001 042F #endif
; 0001 0430 
; 0001 0431 
; 0001 0432 
; 0001 0433 // Crystal Oscillator division factor: 1
; 0001 0434 #pragma optsize-
; 0001 0435 CLKPR=0x80;
;	Temp_Byte -> R17
;	Temp_Byte2 -> R16
	LDI  R30,LOW(128)
	STS  97,R30
; 0001 0436 CLKPR=0x00;
	LDI  R30,LOW(0)
	STS  97,R30
; 0001 0437 #ifdef _OPTIMIZE_SIZE_
; 0001 0438 #pragma optsize+
; 0001 0439 #endif
; 0001 043A 
; 0001 043B 
; 0001 043C 
; 0001 043D // Input/Output Ports initialization
; 0001 043E // Port B initialization
; 0001 043F // Func7=In Func6=In Func5=Out Func4=Out Func3=In Func2=In Func1=Out Func0=Out
; 0001 0440 // State7=P State6=P State5=1 State4=1 State3=P State2=P State1=1 State0=0
; 0001 0441 PORTB=0xFE;
	LDI  R30,LOW(254)
	OUT  0x5,R30
; 0001 0442 DDRB=0x33;
	LDI  R30,LOW(51)
	OUT  0x4,R30
; 0001 0443 
; 0001 0444 // Port C initialization
; 0001 0445 // Func7=In Func6=Out Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=In
; 0001 0446 // State7=T State6=0 State5=T State4=T State3=P State2=1 State1=0 State0=P
; 0001 0447 PORTC=0x0D;
	LDI  R30,LOW(13)
	OUT  0x8,R30
; 0001 0448 DDRC=0x46;
	LDI  R30,LOW(70)
	OUT  0x7,R30
; 0001 0449 
; 0001 044A // Port D initialization
; 0001 044B // Func7=Out Func6=In Func5=Out Func4=In Func3=Out Func2=Out Func1=Out Func0=Out
; 0001 044C // State7=T State6=P State5=1 State4=P State3=1 State2=0 State1=0 State0=1
; 0001 044D PORTD=0x79;
	LDI  R30,LOW(121)
	OUT  0xB,R30
; 0001 044E DDRD=0xAF;
	LDI  R30,LOW(175)
	OUT  0xA,R30
; 0001 044F 
; 0001 0450 // Port E initialization
; 0001 0451 // Func2=In Func1=In Func0=In
; 0001 0452 // State2=T State1=T State0=T
; 0001 0453 PORTE=0x00;
	LDI  R30,LOW(0)
	OUT  0xE,R30
; 0001 0454 DDRE=0x00;
	OUT  0xD,R30
; 0001 0455 
; 0001 0456 // Timer/Counter 0 initialization
; 0001 0457 // Clock source: System Clock
; 0001 0458 // Clock value: 230.400 kHz
; 0001 0459 // Mode: CTC top=OCR0A
; 0001 045A // OC0A output: Disconnected
; 0001 045B // OC0B output: Disconnected
; 0001 045C TCCR0A=0x02;
	LDI  R30,LOW(2)
	OUT  0x24,R30
; 0001 045D TCCR0B=0x03;
	LDI  R30,LOW(3)
	OUT  0x25,R30
; 0001 045E TCNT0=0x00;
	LDI  R30,LOW(0)
	OUT  0x26,R30
; 0001 045F OCR0A=0x2F;
	LDI  R30,LOW(47)
	OUT  0x27,R30
; 0001 0460 OCR0B=0x00;
	LDI  R30,LOW(0)
	OUT  0x28,R30
; 0001 0461 
; 0001 0462 // Timer/Counter 1 initialization
; 0001 0463 // Clock source: System Clock
; 0001 0464 // Clock value: 14745.600 kHz
; 0001 0465 // Mode: CTC top=OCR1A
; 0001 0466 // OC1A output: Discon.
; 0001 0467 // OC1B output: Discon.
; 0001 0468 // Noise Canceler: Off
; 0001 0469 // Input Capture on Falling Edge
; 0001 046A // Timer1 Overflow Interrupt: Off
; 0001 046B // Input Capture Interrupt: Off
; 0001 046C // Compare A Match Interrupt: On
; 0001 046D // Compare B Match Interrupt: Off
; 0001 046E TCCR1A=0x00;
	STS  128,R30
; 0001 046F TCCR1B=0x09;
	LDI  R30,LOW(9)
	STS  129,R30
; 0001 0470 TCNT1H=0x00;
	LDI  R30,LOW(0)
	STS  133,R30
; 0001 0471 TCNT1L=0x00;
	STS  132,R30
; 0001 0472 ICR1H=0x00;
	STS  135,R30
; 0001 0473 ICR1L=0x00;
	STS  134,R30
; 0001 0474 OCR1AH=0x00;
	STS  137,R30
; 0001 0475 OCR1AL=0xBF;
	LDI  R30,LOW(191)
	STS  136,R30
; 0001 0476 OCR1BH=0x00;
	LDI  R30,LOW(0)
	STS  139,R30
; 0001 0477 OCR1BL=0x00;
	STS  138,R30
; 0001 0478 
; 0001 0479 
; 0001 047A // External Interrupt(s) initialization
; 0001 047B // INT0: Off
; 0001 047C // INT1: Off
; 0001 047D // INT2: Off
; 0001 047E MCUCR=0x00;
	OUT  0x35,R30
; 0001 047F MCUSR=0x00;
	OUT  0x34,R30
; 0001 0480 
; 0001 0481 
; 0001 0482 
; 0001 0483 // Timer(s)/Counter(s) Interrupt(s) initialization
; 0001 0484 // Timer/Counter 0 Interrupt(s) initialization
; 0001 0485 TIMSK0=0x02;
	LDI  R30,LOW(2)
	STS  110,R30
; 0001 0486 TIMSK1=0x02;
	STS  111,R30
; 0001 0487 
; 0001 0488 
; 0001 0489 // CAN Controller initialization
; 0001 048A // CAN disabled
; 0001 048B CANGCON=0x00;
	LDI  R30,LOW(0)
	STS  216,R30
; 0001 048C 
; 0001 048D 
; 0001 048E 
; 0001 048F // ADC initialization
; 0001 0490 // ADC Clock frequency: 921.600 kHz
; 0001 0491 // ADC Bandgap Voltage Reference: Off
; 0001 0492 // ADC High Speed Mode: Off
; 0001 0493 // ADC Auto Trigger Source: ADC Stopped
; 0001 0494 // Digital input buffers on ADC0: On, ADC1: On, ADC2: On, ADC3: On
; 0001 0495 // ADC4: On, ADC5: On, ADC6: On, ADC7: On
; 0001 0496 DIDR0=0x00;
	STS  126,R30
; 0001 0497 ADMUX=ADC_VREF_TYPE & 0xff;
	STS  124,R30
; 0001 0498 ADCSRA=0x84;
	LDI  R30,LOW(132)
	STS  122,R30
; 0001 0499 ADCSRB=0x20;
	LDI  R30,LOW(32)
	STS  123,R30
; 0001 049A 
; 0001 049B 
; 0001 049C 
; 0001 049D // SPI initialization
; 0001 049E // SPI disabled
; 0001 049F SPCR=0x00;
	LDI  R30,LOW(0)
	OUT  0x2C,R30
; 0001 04A0 
; 0001 04A1 
; 0001 04A2 // Watchdog Timer initialization
; 0001 04A3 // Watchdog Timer Prescaler: OSC/512k
; 0001 04A4 // Watchdog Timer interrupt: Off
; 0001 04A5 #asm("wdr")
	wdr
; 0001 04A6 WDTCSR=0x38;
	LDI  R30,LOW(56)
	STS  96,R30
; 0001 04A7 WDTCSR=0x28;
	LDI  R30,LOW(40)
	STS  96,R30
; 0001 04A8 
; 0001 04A9 
; 0001 04AA RS485.Tx_Size=20;
	LDI  R30,LOW(20)
	__PUTB1MN _RS485,1
; 0001 04AB Timer.Link_Timer=4;
	LDI  R30,LOW(4)
	__PUTB1MN _Timer,1
; 0001 04AC Timer.Time=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timer,4
; 0001 04AD 
; 0001 04AE 
; 0001 04AF Reset:
_0x20100:
; 0001 04B0 
; 0001 04B1 Timer.Counting=1;
	LDI  R30,LOW(1)
	__PUTB1MN _Timer,2
; 0001 04B2 Refresh_Bit=0;
	CBI  0x1E,3
; 0001 04B3 
; 0001 04B4 if (Send_Mode) LINCR=0x00; //Disable Uart
	SBIS 0x9,6
	RJMP _0x20103
	LDI  R30,LOW(0)
	RJMP _0x20180
; 0001 04B5  else LINCR=0x0F; // Enable Uart
_0x20103:
	LDI  R30,LOW(15)
_0x20180:
	STS  200,R30
; 0001 04B6     {
; 0001 04B7     // UART initialization
; 0001 04B8     // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0001 04B9     // UART Receiver: On
; 0001 04BA     // UART Transmitter: On
; 0001 04BB     // UART Baud Rate: 9600
; 0001 04BC     LINCR=0x0F;
	LDI  R30,LOW(15)
	STS  200,R30
; 0001 04BD     LINBTR=0x18;
	LDI  R30,LOW(24)
	STS  204,R30
; 0001 04BE     LINBRRH=0x00;
	LDI  R30,LOW(0)
	STS  206,R30
; 0001 04BF     LINBRRL=47;
	LDI  R30,LOW(47)
	STS  205,R30
; 0001 04C0     LINDAT=0xFF;
	LDI  R30,LOW(255)
	STS  210,R30
; 0001 04C1     }
; 0001 04C2 
; 0001 04C3 
; 0001 04C4 #asm("sei") // Global enable interrupts
	sei
; 0001 04C5 
; 0001 04C6 #ifndef _Test_
; 0001 04C7 Sinc_Bit=0;
	CBI  0x1E,2
; 0001 04C8 Timer_Sinc=0;
	LDI  R30,LOW(0)
	STS  _Timer_Sinc,R30
	STS  _Timer_Sinc+1,R30
; 0001 04C9 #endif
; 0001 04CA 
; 0001 04CB ClrScr();
	CALL _ClrScr
; 0001 04CC 
; 0001 04CD Cursor_Hide();
	CALL _Cursor_Hide
; 0001 04CE 
; 0001 04CF Print(" Vault Debug Menu V1.00 ",30,0,7);
	__POINTW1FN _0x20000,0
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(30)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04D0 
; 0001 04D1 Print("Node Address:",1,12,1);
	__POINTW1FN _0x20000,25
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04D2 Debug_Putchar('0');
	LDI  R26,LOW(48)
	CALL _Debug_Putchar
; 0001 04D3 Debug_Putchar('x');
	LDI  R26,LOW(120)
	CALL _Debug_Putchar
; 0001 04D4 Debug_Putchar('0');
	LDI  R26,LOW(48)
	CALL _Debug_Putchar
; 0001 04D5 Debug_Putchar(Node_Address_);
	LDI  R26,LOW(_Node_Address_)
	LDI  R27,HIGH(_Node_Address_)
	CALL __EEPROMRDB
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 04D6 
; 0001 04D7 for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
	LDI  R17,LOW(0)
_0x20108:
	CPI  R17,8
	BRSH _0x20109
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDI  R26,LOW(0)
	STD  Z+0,R26
	SUBI R17,-1
	RJMP _0x20108
_0x20109:
; 0001 04D8 Print_Serial_No(20);
	LDI  R26,LOW(20)
	RCALL _Print_Serial_No
; 0001 04D9 
; 0001 04DA 
; 0001 04DB Print("; Firmware Version:",24,20,1);
	__POINTW1FN _0x20000,39
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(24)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04DC Temp_Byte=Firmware_Ver[0];
	LDI  R30,LOW(_Firmware_Ver*2)
	LDI  R31,HIGH(_Firmware_Ver*2)
	LPM  R17,Z
; 0001 04DD Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04DE RS485.Get_All_Command[16]=Temp_Byte;
	__PUTBMRN _RS485,84,17
; 0001 04DF 
; 0001 04E0 Temp_Byte=Firmware_Ver[1];
	__POINTW1FN _Firmware_Ver,1
	LPM  R17,Z
; 0001 04E1 Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04E2 RS485.Get_All_Command[17]=Temp_Byte;
	__PUTBMRN _RS485,85,17
; 0001 04E3 
; 0001 04E4 Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 04E5 Temp_Byte=Firmware_Ver[2];
	__POINTW1FN _Firmware_Ver,2
	LPM  R17,Z
; 0001 04E6 Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04E7 RS485.Get_All_Command[18]=Temp_Byte;
	__PUTBMRN _RS485,86,17
; 0001 04E8 
; 0001 04E9 Temp_Byte=Firmware_Ver[3];
	__POINTW1FN _Firmware_Ver,3
	LPM  R17,Z
; 0001 04EA Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04EB RS485.Get_All_Command[19]=Temp_Byte;
	__PUTBMRN _RS485,87,17
; 0001 04EC 
; 0001 04ED 
; 0001 04EE Print("; ",49,20,1);
	__POINTW1FN _0x20000,59
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(49)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04EF Print("; ",66,20,1);
	__POINTW1FN _0x20000,59
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(66)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04F0 
; 0001 04F1 Print(" INPUTS:",1,16,7);
	__POINTW1FN _0x20000,62
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04F2 Print("CBX Sensor=",10,16,1);
	__POINTW1FN _0x20000,71
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04F3 Print("; Position Sensor=",23,16,1);
	__POINTW1FN _0x20000,83
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04F4 Print("; Motor Lock Sensor=",43,16,1);
	__POINTW1FN _0x20000,102
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(43)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04F5 Print("; Motor Fault=",65,16,1);
	__POINTW1FN _0x20000,123
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(65)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04F6 
; 0001 04F7 
; 0001 04F8 Print("OUTPUTS:",1,14,7);
	__POINTW1FN _0x20000,138
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04F9 Print("[S]olenoid=",10,14,1);
	__POINTW1FN _0x20000,147
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04FA Print("; Lamp[1]=",23,14,1);
	__POINTW1FN _0x20000,159
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04FB Print("; Lamp[2]=",35,14,1);
	__POINTW1FN _0x20000,170
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(35)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04FC Print("; Motor [P]hase=",47,14,1);
	__POINTW1FN _0x20000,181
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(47)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04FD Print("; Motor [R]un=",65,14,1);
	__POINTW1FN _0x20000,198
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(65)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04FE 
; 0001 04FF 
; 0001 0500 Print("Vault Holding Time=",1,18,1);
	__POINTW1FN _0x20000,213
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(18)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0501 Temp_Byte=0x30+Solenoid_Time/10;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0502 Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0503 RS485.Get_All_Command[0]=Temp_Byte;
	__PUTBMRN _RS485,68,17
; 0001 0504 Temp_Byte=0x30+Solenoid_Time%10;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0505 Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0506 RS485.Get_All_Command[1]=Temp_Byte;
	__PUTBMRN _RS485,69,17
; 0001 0507 Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 0508 Debug_Putchar('s');
	LDI  R26,LOW(115)
	CALL _Debug_Putchar
; 0001 0509 Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 050A 
; 0001 050B 
; 0001 050C Key_Press=Rx3_Pointer_Buff;
	LDS  R11,_Rx3_Pointer_Buff
; 0001 050D 
; 0001 050E while (1)
_0x2010A:
; 0001 050F       {
; 0001 0510        #asm("wdr");
	wdr
; 0001 0511        Node_Address=Node_Address_;
	LDI  R26,LOW(_Node_Address_)
	LDI  R27,HIGH(_Node_Address_)
	CALL __EEPROMRDB
	STS  _Node_Address,R30
; 0001 0512 
; 0001 0513        if ((LINSIR&8)) LINSIR=0xFF;
	LDS  R30,201
	ANDI R30,LOW(0x8)
	BREQ _0x2010D
	LDI  R30,LOW(255)
	STS  201,R30
; 0001 0514 
; 0001 0515        Out_Lig1=Status_Lights&1;
_0x2010D:
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x1)
	BRNE _0x2010E
	CBI  0xB,7
	RJMP _0x2010F
_0x2010E:
	SBI  0xB,7
_0x2010F:
; 0001 0516        Out_Lig2=(Status_Lights>>1)&1;
	LDS  R30,_Status_Lights
	LSR  R30
	ANDI R30,LOW(0x1)
	BRNE _0x20110
	CBI  0xB,2
	RJMP _0x20111
_0x20110:
	SBI  0xB,2
_0x20111:
; 0001 0517 
; 0001 0518 
; 0001 0519        //*************** Holding time Progress Bar
; 0001 051A        Temp_Byte=Timer.Time/20;
	__GETW2MN _Timer,4
	LDI  R30,LOW(20)
	LDI  R31,HIGH(20)
	CALL __DIVW21U
	MOV  R17,R30
; 0001 051B        if (Timer.Time_Old!=Temp_Byte)
	__GETB2MN _Timer,3
	CP   R17,R26
	BREQ _0x20112
; 0001 051C                 {
; 0001 051D                 Timer.Time_Old=Temp_Byte;
	__PUTBMRN _Timer,3,17
; 0001 051E                 if (Timer.Counting==1)
	__GETB2MN _Timer,2
	CPI  R26,LOW(0x1)
	BRNE _0x20113
; 0001 051F                     {
; 0001 0520                     Print("",27,18,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(27)
	ST   -Y,R30
	LDI  R30,LOW(18)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0521                     for (Temp_Byte=0;Temp_Byte<Timer.Time_Old;Temp_Byte++) Debug_Putchar(176); //Show Progres Bar
	LDI  R17,LOW(0)
_0x20115:
	__GETB1MN _Timer,3
	CP   R17,R30
	BRSH _0x20116
	LDI  R26,LOW(176)
	CALL _Debug_Putchar
	SUBI R17,-1
	RJMP _0x20115
_0x20116:
; 0001 0522 Timer.Counting=0;
	LDI  R30,LOW(0)
	__PUTB1MN _Timer,2
; 0001 0523                     }
; 0001 0524                     else Print("   ",26+Solenoid_Time-(Solenoid_Time-Temp_Byte),18,1);
	RJMP _0x20117
_0x20113:
	__POINTW1FN _0x20000,233
	ST   -Y,R31
	ST   -Y,R30
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	SUBI R30,-LOW(26)
	MOV  R0,R30
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	SUB  R30,R17
	MOV  R26,R0
	SUB  R26,R30
	ST   -Y,R26
	LDI  R30,LOW(18)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0525                 }
_0x20117:
; 0001 0526 
; 0001 0527        if ((Timer.Time==0)&&(Timer.Counting!=2))
_0x20112:
	__GETW2MN _Timer,4
	SBIW R26,0
	BRNE _0x20119
	__GETB2MN _Timer,2
	CPI  R26,LOW(0x2)
	BRNE _0x2011A
_0x20119:
	RJMP _0x20118
_0x2011A:
; 0001 0528             {
; 0001 0529             Print("",27,18,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(27)
	ST   -Y,R30
	LDI  R30,LOW(18)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 052A             for (Temp_Byte=0;Temp_Byte<Solenoid_Time-1;Temp_Byte++) Debug_Putchar(' '); //Erase Progress Bar
	LDI  R17,LOW(0)
_0x2011C:
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	SUBI R30,LOW(1)
	CP   R17,R30
	BRSH _0x2011D
	LDI  R26,LOW(32)
	CALL _Debug_Putchar
	SUBI R17,-1
	RJMP _0x2011C
_0x2011D:
; 0001 052B Timer.Counting=2;
	LDI  R30,LOW(2)
	__PUTB1MN _Timer,2
; 0001 052C             }
; 0001 052D 
; 0001 052E 
; 0001 052F 
; 0001 0530 
; 0001 0531        if (Key_Press!=Rx3_Pointer_Buff)
_0x20118:
	LDS  R30,_Rx3_Pointer_Buff
	CP   R30,R11
	BRNE PC+3
	JMP _0x2011E
; 0001 0532             {
; 0001 0533             Temp_Byte=Rx3_Buff[(Rx3_Pointer_Buff-1)&0x07];
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R17,Z
; 0001 0534 
; 0001 0535             Key_Press=Rx3_Pointer_Buff;
	LDS  R11,_Rx3_Pointer_Buff
; 0001 0536 
; 0001 0537 
; 0001 0538             if (Temp_Byte=='1') //On/Off Lamp 1
	CPI  R17,49
	BRNE _0x2011F
; 0001 0539                 {
; 0001 053A                 if ((Status_Lights&1)==1) Status_Lights&=2;
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x1)
	CPI  R30,LOW(0x1)
	BRNE _0x20120
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x2)
	RJMP _0x20181
; 0001 053B                  else Status_Lights|=1;
_0x20120:
	LDS  R30,_Status_Lights
	ORI  R30,1
_0x20181:
	STS  _Status_Lights,R30
; 0001 053C                 Print("",33,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(33)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 053D                 Debug_Putchar(0x30+Out_Lig1);
	LDI  R30,0
	SBIC 0xB,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 053E                 }
; 0001 053F 
; 0001 0540 
; 0001 0541             if (Temp_Byte=='2') //On/Off Lamp 2
_0x2011F:
	CPI  R17,50
	BRNE _0x20122
; 0001 0542                 {
; 0001 0543                 if ((Status_Lights&2)==2) Status_Lights&=1;
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x2)
	CPI  R30,LOW(0x2)
	BRNE _0x20123
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x1)
	RJMP _0x20182
; 0001 0544                  else Status_Lights|=2;
_0x20123:
	LDS  R30,_Status_Lights
	ORI  R30,2
_0x20182:
	STS  _Status_Lights,R30
; 0001 0545                 Print("",45,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(45)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0546                 Debug_Putchar(0x30+Out_Lig2);
	LDI  R30,0
	SBIC 0xB,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0547                 }
; 0001 0548 
; 0001 0549 
; 0001 054A 
; 0001 054B             if ((Temp_Byte|32)=='s')  //On/Off valve solenoid On/Off
_0x20122:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x73)
	BRNE _0x20125
; 0001 054C                     {
; 0001 054D                     Out_Valve=!Out_Valve;
	SBIS 0x8,1
	RJMP _0x20126
	CBI  0x8,1
	RJMP _0x20127
_0x20126:
	SBI  0x8,1
_0x20127:
; 0001 054E                     Print("",21,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 054F                     Debug_Putchar(0x30+Out_Valve);
	LDI  R30,0
	SBIC 0x8,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0550                     }
; 0001 0551 
; 0001 0552             if ((Temp_Byte|32)=='p') // Motor Phase Left/Right
_0x20125:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x70)
	BRNE _0x20128
; 0001 0553                     {
; 0001 0554                     Out_Motor_Ph=!Out_Motor_Ph;
	SBIS 0xB,1
	RJMP _0x20129
	CBI  0xB,1
	RJMP _0x2012A
_0x20129:
	SBI  0xB,1
_0x2012A:
; 0001 0555                     Print("",63,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0556                     Debug_Putchar(0x30+Out_Motor_Ph);
	LDI  R30,0
	SBIC 0xB,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0557                     }
; 0001 0558 
; 0001 0559             if ((Temp_Byte|32)=='r') // Motor Run/Stop
_0x20128:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x72)
	BRNE _0x2012B
; 0001 055A                     {
; 0001 055B                     Out_Motor_Run=!Out_Motor_Run;
	SBIS 0xB,0
	RJMP _0x2012C
	CBI  0xB,0
	RJMP _0x2012D
_0x2012C:
	SBI  0xB,0
_0x2012D:
; 0001 055C                     Print("",79,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 055D                     Debug_Putchar(0x30+!Out_Motor_Run);
	LDI  R30,0
	SBIS 0xB,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 055E                     }
; 0001 055F 
; 0001 0560              Temp_Byte=0;
_0x2012B:
	LDI  R17,LOW(0)
; 0001 0561             }
; 0001 0562 
; 0001 0563 
; 0001 0564     if ((Sinc_Bit)&&(RS485.Tx_Buffer[2]!='A'))
_0x2011E:
	SBIS 0x1E,2
	RJMP _0x2012F
	__GETB2MN _RS485,6
	CPI  R26,LOW(0x41)
	BRNE _0x20130
_0x2012F:
	RJMP _0x2012E
_0x20130:
; 0001 0565        {
; 0001 0566        Sinc_Bit=0;
	CBI  0x1E,2
; 0001 0567        if (Refresh_Bit) goto Reset;
	SBIC 0x1E,3
	RJMP _0x20100
; 0001 0568 
; 0001 0569        U_12V=read_adc(8)/3.1;
	LDI  R26,LOW(8)
	RCALL _read_adc
	CLR  R22
	CLR  R23
	CALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x40466666
	CALL __DIVF21
	LDI  R26,LOW(_U_12V)
	LDI  R27,HIGH(_U_12V)
	CALL __CFD1U
	ST   X+,R30
	ST   X,R31
; 0001 056A        U_5V=read_adc(9)/1.25;
	LDI  R26,LOW(9)
	RCALL _read_adc
	CLR  R22
	CLR  R23
	CALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x3FA00000
	CALL __DIVF21
	LDI  R26,LOW(_U_5V)
	LDI  R27,HIGH(_U_5V)
	CALL __CFD1U
	ST   X+,R30
	ST   X,R31
; 0001 056B 
; 0001 056C 
; 0001 056D        Temp_Byte=0x30;
	LDI  R17,LOW(48)
; 0001 056E        if ((U_5V<Min_5V)||(U_5V>Max_5V)) Temp_Byte|=1;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	CPI  R26,LOW(0x1C2)
	LDI  R30,HIGH(0x1C2)
	CPC  R27,R30
	BRLO _0x20135
	CPI  R26,LOW(0x227)
	LDI  R30,HIGH(0x227)
	CPC  R27,R30
	BRLO _0x20134
_0x20135:
	ORI  R17,LOW(1)
; 0001 056F        if ((U_12V<Min_12V)||(U_12V>Max_12V)) Temp_Byte|=2;
_0x20134:
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	CPI  R26,LOW(0x6E)
	LDI  R30,HIGH(0x6E)
	CPC  R27,R30
	BRLO _0x20138
	CPI  R26,LOW(0x83)
	LDI  R30,HIGH(0x83)
	CPC  R27,R30
	BRLO _0x20137
_0x20138:
	ORI  R17,LOW(2)
; 0001 0570        if (!Motor_Fault) Temp_Byte|=4;
_0x20137:
	SBIS 0x3,7
	ORI  R17,LOW(4)
; 0001 0571        Alarm_Status=Temp_Byte;
	STS  _Alarm_Status,R17
; 0001 0572 
; 0001 0573        if ((Alarm_Status&3)) Led_Power=1; //Alarm Light On/Off
	LDS  R30,_Alarm_Status
	ANDI R30,LOW(0x3)
	BREQ _0x2013B
	SBI  0xB,5
; 0001 0574         else Led_Power=0;
	RJMP _0x2013E
_0x2013B:
	CBI  0xB,5
; 0001 0575 
; 0001 0576        //Print("_ U(12V)=",49,18,1);
; 0001 0577        Print("U(12V)=",51,20,1+6*((Alarm_Status>>1)&1));
_0x2013E:
	__POINTW1FN _0x20000,237
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(51)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDS  R30,_Alarm_Status
	LSR  R30
	ANDI R30,LOW(0x1)
	LDI  R26,LOW(6)
	MUL  R30,R26
	MOVW R30,R0
	SUBI R30,-LOW(1)
	MOV  R26,R30
	CALL _Print
; 0001 0578        Temp_Byte=0x30+U_12V/100;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0579        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 057A        RS485.Get_All_Command[5]=Temp_Byte;
	__PUTBMRN _RS485,73,17
; 0001 057B        Temp_Byte=0x30+(U_12V%100)/10;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 057C        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 057D        RS485.Get_All_Command[6]=Temp_Byte;
	__PUTBMRN _RS485,74,17
; 0001 057E        Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 057F        Temp_Byte=0x30+U_12V%10;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __MODW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0580        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0581        RS485.Get_All_Command[7]=Temp_Byte;
	__PUTBMRN _RS485,75,17
; 0001 0582        Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 0583        Debug_Putchar('V');
	LDI  R26,LOW(86)
	CALL _Debug_Putchar
; 0001 0584        Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 0585 
; 0001 0586 
; 0001 0587        //Print("_ U(5V)=",65,18,1);
; 0001 0588        Print("U(5V)=",68,20,1+6*Alarm_Status&1);
	__POINTW1FN _0x20000,245
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(68)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDS  R30,_Alarm_Status
	LDI  R26,LOW(6)
	MUL  R30,R26
	MOVW R30,R0
	SUBI R30,-LOW(1)
	ANDI R30,LOW(0x1)
	MOV  R26,R30
	CALL _Print
; 0001 0589        Temp_Byte=0x30+U_5V/100;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 058A        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 058B        RS485.Get_All_Command[2]=Temp_Byte;
	__PUTBMRN _RS485,70,17
; 0001 058C        Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 058D        Temp_Byte=0x30+(U_5V%100)/10;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 058E        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 058F        RS485.Get_All_Command[3]=Temp_Byte;
	__PUTBMRN _RS485,71,17
; 0001 0590        Temp_Byte=0x30+U_5V%10;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __MODW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0591        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0592        RS485.Get_All_Command[4]=Temp_Byte;
	__PUTBMRN _RS485,72,17
; 0001 0593        Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 0594        Debug_Putchar('V');
	LDI  R26,LOW(86)
	CALL _Debug_Putchar
; 0001 0595        Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 0596 
; 0001 0597 
; 0001 0598 
; 0001 0599        //Print("[S]olenoid=",11,14,1);
; 0001 059A        Print("",21,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 059B        Debug_Putchar(0x30+Out_Valve);
	LDI  R30,0
	SBIC 0x8,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 059C 
; 0001 059D 
; 0001 059E        //Print("; Lamp[1]=",24,14,1);
; 0001 059F        Print("",33,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(33)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05A0        Debug_Putchar(0x30+Out_Lig1);
	LDI  R30,0
	SBIC 0xB,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05A1 
; 0001 05A2 
; 0001 05A3        //Print("; Lamp[2]=",36,14,1);
; 0001 05A4        Print("",45,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(45)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05A5        Debug_Putchar(0x30+Out_Lig2);
	LDI  R30,0
	SBIC 0xB,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05A6 
; 0001 05A7 
; 0001 05A8 
; 0001 05A9        //Print("; Motor Phase=",48,14,1);
; 0001 05AA        Print("",63,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05AB        Debug_Putchar(0x30+Out_Motor_Ph);
	LDI  R30,0
	SBIC 0xB,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05AC 
; 0001 05AD 
; 0001 05AE 
; 0001 05AF        //Print("; Motor Run=",64,14,1);
; 0001 05B0        Print("",79,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05B1        Debug_Putchar(0x30+!Out_Motor_Run);
	LDI  R30,0
	SBIS 0xB,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05B2 
; 0001 05B3 
; 0001 05B4        //Print("CBX Sensor=",11,16,1);
; 0001 05B5        Print("",21,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05B6        Debug_Putchar(0x30+!CbxPS_Sensor);
	LDI  R30,0
	SBIS 0x3,3
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05B7 
; 0001 05B8 
; 0001 05B9 
; 0001 05BA        //Print("; Position Sensor=",29,16,1);
; 0001 05BB        Print("",41,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(41)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05BC        Debug_Putchar(0x30+!RecPS_Sensor);
	LDI  R30,0
	SBIS 0x6,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05BD 
; 0001 05BE 
; 0001 05BF 
; 0001 05C0        //Print("; Motor Lock Sensor=",49,16,1);
; 0001 05C1        Print("",63,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05C2        Debug_Putchar(0x30+!LockPS_Sensor);
	LDI  R30,0
	SBIS 0x3,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05C3 
; 0001 05C4 
; 0001 05C5 
; 0001 05C6        //Print("; Motor Fault=",66,16,1);
; 0001 05C7        Print("",79,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05C8        Debug_Putchar(0x30+!Motor_Fault);
	LDI  R30,0
	SBIS 0x3,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05C9 
; 0001 05CA 
; 0001 05CB        Print_Vault_No(10);
	LDI  R26,LOW(10)
	RCALL _Print_Vault_No
; 0001 05CC 
; 0001 05CD 
; 0001 05CE 
; 0001 05CF 
; 0001 05D0        Print("Bin Number:",1,8,1);
	__POINTW1FN _0x20000,252
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05D1        for (Temp_Byte=9+Mode_1;Temp_Byte<15;Temp_Byte++) Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
	LDI  R30,0
	SBIC 0x1E,0
	LDI  R30,1
	SUBI R30,-LOW(9)
	MOV  R17,R30
_0x20142:
	CPI  R17,15
	BRSH _0x20143
	__POINTW2MN _RS485,4
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CALL _Debug_Putchar
	SUBI R17,-1
	RJMP _0x20142
_0x20143:
; 0001 05D2 Mode_Print(!Mode_1);
	LDI  R26,0
	SBIS 0x1E,0
	LDI  R26,1
	CALL _Mode_Print
; 0001 05D3 
; 0001 05D4 
; 0001 05D5 
; 0001 05D6 #ifdef _Test_
; 0001 05D7        if (Cmp1!=RS485.Tx_Buffer[8]) {Cmp1=RS485.Tx_Buffer[8];Err1++;}
; 0001 05D8        Debug_Putchar(0x30+Err1);
; 0001 05D9 #endif
; 0001 05DA 
; 0001 05DB 
; 0001 05DC        Print("Cash Box Number:",1,6,1);
	__POINTW1FN _0x20000,264
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(6)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05DD        if (Power_Light==0)
	SBIC 0x7,7
	RJMP _0x20144
; 0001 05DE         {
; 0001 05DF          for (Temp_Byte=15+Mode_2;Temp_Byte<21;Temp_Byte++)
	LDI  R30,0
	SBIC 0x1E,1
	LDI  R30,1
	SUBI R30,-LOW(15)
	MOV  R17,R30
_0x20146:
	CPI  R17,21
	BRSH _0x20147
; 0001 05E0             {
; 0001 05E1              if (RS485.Tx_Buffer[Temp_Byte]!=':') Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
	__POINTW2MN _RS485,4
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x3A)
	BREQ _0x20148
	__POINTW2MN _RS485,4
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	RJMP _0x20183
; 0001 05E2               else Debug_Putchar('0');
_0x20148:
	LDI  R26,LOW(48)
_0x20183:
	CALL _Debug_Putchar
; 0001 05E3             }
	SUBI R17,-1
	RJMP _0x20146
_0x20147:
; 0001 05E4          if (Send_Mode) Mode_Print(RS485.Tx_Buffer[12]==':'?0:!Mode_2);
	SBIS 0x9,6
	RJMP _0x2014A
	__GETB2MN _RS485,16
	CPI  R26,LOW(0x3A)
	BRNE _0x2014B
	LDI  R30,LOW(0)
	RJMP _0x2014C
_0x2014B:
	LDI  R30,0
	SBIS 0x1E,1
	LDI  R30,1
_0x2014C:
	MOV  R26,R30
	CALL _Mode_Print
; 0001 05E5         }
_0x2014A:
; 0001 05E6          else Print(" ...   ",255,0,0);
	RJMP _0x2014E
_0x20144:
	__POINTW1FN _0x20000,281
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(255)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Print
; 0001 05E7 
; 0001 05E8 
; 0001 05E9 
; 0001 05EA 
; 0001 05EB #ifdef _Test_
; 0001 05EC        if (Cmp2!=RS485.Tx_Buffer[14]) {Cmp2=RS485.Tx_Buffer[14];Err2++;}
; 0001 05ED        Debug_Putchar(0x30+Err2);
; 0001 05EE #endif
; 0001 05EF 
; 0001 05F0 
; 0001 05F1 
; 0001 05F2        Print("Transmission Data Format",1,4,1);
_0x2014E:
	__POINTW1FN _0x20000,289
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(4)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05F3        Mode_Print(!Send_Mode);
	LDI  R26,0
	SBIS 0x9,6
	LDI  R26,1
	CALL _Mode_Print
; 0001 05F4        if (Led_Link==0) Print("(Online) ",255,0,0);
	SBIC 0x5,1
	RJMP _0x2014F
	__POINTW1FN _0x20000,314
	RJMP _0x20184
; 0001 05F5         else Print("(Offline)",255,0,0);
_0x2014F:
	__POINTW1FN _0x20000,324
_0x20184:
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(255)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Print
; 0001 05F6        }
; 0001 05F7 
; 0001 05F8 
; 0001 05F9 
; 0001 05FA for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++)
_0x2012E:
	LDI  R17,LOW(0)
_0x20152:
	CPI  R17,8
	BRLO PC+3
	JMP _0x20153
; 0001 05FB    if ((Rx3_Buff[Temp_Byte]==13)||(Rx3_Buff[Temp_Byte]==27))
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R26,Z
	CPI  R26,LOW(0xD)
	BREQ _0x20155
	CPI  R26,LOW(0x1B)
	BREQ _0x20155
	RJMP _0x20154
_0x20155:
; 0001 05FC             {
; 0001 05FD              if (Rx3_Buff[Temp_Byte]==27) goto Reset;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x1B)
	BRNE _0x20157
	RJMP _0x20100
; 0001 05FE 
; 0001 05FF              Temp_Byte=(Temp_Byte-6)&0x07;
_0x20157:
	MOV  R30,R17
	SUBI R30,LOW(6)
	ANDI R30,LOW(0x7)
	MOV  R17,R30
; 0001 0600 
; 0001 0601              for (Temp_Byte2=0;Temp_Byte2<6;Temp_Byte2++) if (Password[Temp_Byte2]!=Rx3_Buff[(Temp_Byte2+Temp_Byte)&0x07]) goto Bad_Psw;
	LDI  R16,LOW(0)
_0x20159:
	CPI  R16,6
	BRSH _0x2015A
	MOV  R30,R16
	LDI  R31,0
	SUBI R30,LOW(-_Password*2)
	SBCI R31,HIGH(-_Password*2)
	LPM  R26,Z
	MOV  R30,R17
	ADD  R30,R16
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R30,Z
	CP   R30,R26
	BREQ _0x2015B
	RJMP _0x2015C
; 0001 0602 
; 0001 0603 
; 0001 0604 #ifdef _Test_
; 0001 0605 
; 0001 0606             Err1=0;
; 0001 0607             Err2=0;
; 0001 0608 #endif
; 0001 0609 
; 0001 060A 
; 0001 060B              //Password Corect ,enter in Menu for changing Serial , Vault number and valt holding time
; 0001 060C 
; 0001 060D Loop_Change: ClrScr();
_0x2015B:
	SUBI R16,-1
	RJMP _0x20159
_0x2015A:
_0x2015D:
	CALL _ClrScr
; 0001 060E 
; 0001 060F              Print(" [S]erial Number|[V]ault Number|Holding [T]ime|[N]-Node Address|[Esc]-Main Menu ",1,0,7);
	__POINTW1FN _0x20000,334
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 0610 
; 0001 0611              Print_Vault_No(4);
	LDI  R26,LOW(4)
	RCALL _Print_Vault_No
; 0001 0612              Print("V",1,4,4);
	__POINTW1FN _0x20000,415
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(4)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 0613 
; 0001 0614              Print_Serial_No(6);
	LDI  R26,LOW(6)
	RCALL _Print_Serial_No
; 0001 0615              Print("S",1,6,4);
	__POINTW1FN _0x20000,417
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(6)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 0616 
; 0001 0617              Print("Time Vault Holding=",1,8,1);
	__POINTW1FN _0x20000,419
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0618              Debug_Putchar(0x30+(Solenoid_Time/10));
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0619              Debug_Putchar(0x30+Solenoid_Time%10);
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 061A              Print("T",1,8,4);
	__POINTW1FN _0x20000,439
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 061B              Print("[s]",23,8,1);
	__POINTW1FN _0x20000,441
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 061C 
; 0001 061D              Print("Node Address=",1,10,1);
	__POINTW1FN _0x20000,445
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 061E              Debug_Putchar('0');
	LDI  R26,LOW(48)
	CALL _Debug_Putchar
; 0001 061F              Debug_Putchar('x');
	LDI  R26,LOW(120)
	CALL _Debug_Putchar
; 0001 0620              Debug_Putchar('0');
	LDI  R26,LOW(48)
	CALL _Debug_Putchar
; 0001 0621              Debug_Putchar(Node_Address_);
	LDI  R26,LOW(_Node_Address_)
	LDI  R27,HIGH(_Node_Address_)
	CALL __EEPROMRDB
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0622              Print("N",1,10,4);
	__POINTW1FN _0x20000,459
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 0623 
; 0001 0624 
; 0001 0625              Temp_Byte=Debug_Getchar();
	CALL _Debug_Getchar
	MOV  R17,R30
; 0001 0626 
; 0001 0627              if (Temp_Byte==27) goto Reset;
	CPI  R17,27
	BRNE _0x2015E
	RJMP _0x20100
; 0001 0628 
; 0001 0629              if ((Temp_Byte|32)=='s')
_0x2015E:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x73)
	BRNE _0x2015F
; 0001 062A                     {
; 0001 062B                     Print("New Serial Number:",1,12,7);
	__POINTW1FN _0x20000,461
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 062C                     if (Input(Buff_Temp,8,0)==8) for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Serial_No[Temp_Byte]=Buff_Temp[Temp_Byte];
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x8)
	BRNE _0x20160
	LDI  R17,LOW(0)
_0x20162:
	CPI  R17,8
	BRSH _0x20163
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Serial_No)
	SBCI R27,HIGH(-_Serial_No)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Buff_Temp)
	SBCI R31,HIGH(-_Buff_Temp)
	LD   R30,Z
	CALL __EEPROMWRB
	SUBI R17,-1
	RJMP _0x20162
_0x20163:
; 0001 062D };
_0x20160:
_0x2015F:
; 0001 062E 
; 0001 062F             if ((Temp_Byte|32)=='n')
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x6E)
	BRNE _0x20164
; 0001 0630                     {
; 0001 0631                     Print("New Node Address:",1,12,7);
	__POINTW1FN _0x20000,480
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 0632                     if (Input(Buff_Temp,1,0)==1) if ((Buff_Temp[0]>=0x30)&&(Buff_Temp[0]<=0x38)) Node_Address_=Buff_Temp[0];
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x1)
	BRNE _0x20165
	LDS  R26,_Buff_Temp
	CPI  R26,LOW(0x30)
	BRLO _0x20167
	CPI  R26,LOW(0x39)
	BRLO _0x20168
_0x20167:
	RJMP _0x20166
_0x20168:
	LDS  R30,_Buff_Temp
	LDI  R26,LOW(_Node_Address_)
	LDI  R27,HIGH(_Node_Address_)
	CALL __EEPROMWRB
; 0001 0633                     };
_0x20166:
_0x20165:
_0x20164:
; 0001 0634 
; 0001 0635 
; 0001 0636              if ((Temp_Byte|32)=='v')
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x76)
	BRNE _0x20169
; 0001 0637                     {
; 0001 0638                     Print("New Vault Number:",1,12,7);
	__POINTW1FN _0x20000,498
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 0639                     if (Input(Buff_Temp,6,0)==6) for (Temp_Byte=0;Temp_Byte<6;Temp_Byte++) Vault[Temp_Byte]=Buff_Temp[Temp_Byte];
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(6)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x6)
	BRNE _0x2016A
	LDI  R17,LOW(0)
_0x2016C:
	CPI  R17,6
	BRSH _0x2016D
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Vault)
	SBCI R27,HIGH(-_Vault)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Buff_Temp)
	SBCI R31,HIGH(-_Buff_Temp)
	LD   R30,Z
	CALL __EEPROMWRB
	SUBI R17,-1
	RJMP _0x2016C
_0x2016D:
; 0001 063A };
_0x2016A:
_0x20169:
; 0001 063B 
; 0001 063C 
; 0001 063D              if ((Temp_Byte|32)=='t')
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x74)
	BRNE _0x2016E
; 0001 063E                     {
; 0001 063F                     Print("New Vault Holding Time:",1,12,7);
	__POINTW1FN _0x20000,516
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 0640                     if (Input(Buff_Temp,2,0)==2)
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(2)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x2)
	BRNE _0x2016F
; 0001 0641                             {
; 0001 0642                             Temp_Byte=(10*(Buff_Temp[0]-0x30))+Buff_Temp[1]-0x30;
	LDS  R30,_Buff_Temp
	SUBI R30,LOW(48)
	LDI  R26,LOW(10)
	MUL  R30,R26
	MOVW R30,R0
	MOV  R26,R30
	__GETB1MN _Buff_Temp,1
	ADD  R26,R30
	SUBI R26,LOW(48)
	MOV  R17,R26
; 0001 0643                             if ((Temp_Byte<=25)&&(Temp_Byte>=4)) Solenoid_Time=Temp_Byte;
	CPI  R17,26
	BRSH _0x20171
	CPI  R17,4
	BRSH _0x20172
_0x20171:
	RJMP _0x20170
_0x20172:
	MOV  R30,R17
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMWRB
; 0001 0644                             Temp_Byte=0;
_0x20170:
	LDI  R17,LOW(0)
; 0001 0645                             }
; 0001 0646                     };
_0x2016F:
_0x2016E:
; 0001 0647 
; 0001 0648              goto Loop_Change;
	RJMP _0x2015D
; 0001 0649 
; 0001 064A 
; 0001 064B     Bad_Psw: for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
_0x2015C:
	LDI  R17,LOW(0)
_0x20174:
	CPI  R17,8
	BRSH _0x20175
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDI  R26,LOW(0)
	STD  Z+0,R26
	SUBI R17,-1
	RJMP _0x20174
_0x20175:
; 0001 064C break;
	RJMP _0x20153
; 0001 064D             };
_0x20154:
	SUBI R17,-1
	RJMP _0x20152
_0x20153:
; 0001 064E     };
	RJMP _0x2010A
; 0001 064F };
_0x20176:
	RJMP _0x20176
;
;
;
;
;
;
;void Print_Serial_No(unsigned char position)
; 0001 0657 {
_Print_Serial_No:
; 0001 0658  unsigned counter,Tmp;
; 0001 0659 
; 0001 065A  Print("Serial Number:",1,position,1);
	ST   -Y,R26
	CALL __SAVELOCR4
;	position -> Y+4
;	counter -> R16,R17
;	Tmp -> R18,R19
	__POINTW1FN _0x20000,465
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDD  R30,Y+7
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 065B 
; 0001 065C  for (counter=0;counter<8;counter++)
	__GETWRN 16,17,0
_0x20178:
	__CPWRN 16,17,8
	BRSH _0x20179
; 0001 065D     {
; 0001 065E     Tmp=Serial_No[counter];
	LDI  R26,LOW(_Serial_No)
	LDI  R27,HIGH(_Serial_No)
	ADD  R26,R16
	ADC  R27,R17
	CALL __EEPROMRDB
	MOV  R18,R30
	CLR  R19
; 0001 065F     RS485.Get_All_Command[counter+8]=Tmp;
	__POINTW2MN _RS485,68
	MOVW R30,R16
	ADIW R30,8
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R18
; 0001 0660     Debug_Putchar(Tmp);
	MOV  R26,R18
	CALL _Debug_Putchar
; 0001 0661     }
	__ADDWRN 16,17,1
	RJMP _0x20178
_0x20179:
; 0001 0662 }
	CALL __LOADLOCR4
	ADIW R28,5
	RET
;
;
;
;void Print_Vault_No(unsigned char position)
; 0001 0667 {
_Print_Vault_No:
; 0001 0668  unsigned counter;
; 0001 0669  Print("Vault Number:",1,position,1);
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	position -> Y+2
;	counter -> R16,R17
	__POINTW1FN _0x20000,502
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDD  R30,Y+5
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 066A  for (counter=0;counter<6;counter++) Debug_Putchar(Vault[counter]);
	__GETWRN 16,17,0
_0x2017B:
	__CPWRN 16,17,6
	BRSH _0x2017C
	LDI  R26,LOW(_Vault)
	LDI  R27,HIGH(_Vault)
	ADD  R26,R16
	ADC  R27,R17
	CALL __EEPROMRDB
	MOV  R26,R30
	CALL _Debug_Putchar
	__ADDWRN 16,17,1
	RJMP _0x2017B
_0x2017C:
; 0001 066B }
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,3
	RET
;

	.DSEG
_Rx3_Counter:
	.BYTE 0x1
_Rx3_Pointer_Buff:
	.BYTE 0x1
_Tmp_Rx3:
	.BYTE 0x1
_Rx3_Buff:
	.BYTE 0x8
_Tx1_Counter:
	.BYTE 0x1
_TxD_1:
	.BYTE 0x1

	.ESEG
_Serial_No:
	.DB  0x53,0x4E,0x31,0x32
	.DB  0x33,0x34,0x35,0x36
	.DB  0x0
_Vault:
	.DB  0x56,0x41,0x55,0x4C
	.DB  0x54,0x30
	.DB  0x0
_Solenoid_Time:
	.DB  0x5
_Node_Address_:
	.DB  0x30

	.DSEG
_Rx1_Buff:
	.BYTE 0x8
_Rx2_Buff:
	.BYTE 0x8
_Compare_Buffer_1:
	.BYTE 0x6
_Compare_Buffer_2:
	.BYTE 0x6
_Rx1_ID_Buff:
	.BYTE 0x6
_Rx2_ID_Buff:
	.BYTE 0x6
_CB1_Pointer:
	.BYTE 0x1
_CB2_Time:
	.BYTE 0x1
_CB2_Pointer:
	.BYTE 0x1
_CB1_Tmp:
	.BYTE 0x2
_CB2_Tmp:
	.BYTE 0x2
_Timer_Sinc:
	.BYTE 0x2
_Solenoid_Td:
	.BYTE 0x2
_Buff_Temp:
	.BYTE 0xA
_Node_Address:
	.BYTE 0x1
_Led_Blink:
	.BYTE 0x1
_Status_Lights:
	.BYTE 0x1
_Alarm_Status:
	.BYTE 0x1
_U_12V:
	.BYTE 0x2
_U_5V:
	.BYTE 0x2
_Timer:
	.BYTE 0x6
_RS485:
	.BYTE 0x64

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x0:
	LDI  R26,LOW(27)
	CALL _Debug_Putchar
	LDI  R26,LOW(91)
	JMP  _Debug_Putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	JMP  _Debug_Putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2:
	LDI  R30,LOW(255)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(0)
	JMP  _Print

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x3:
	LDI  R26,LOW(63)
	CALL _Debug_Putchar
	LDI  R26,LOW(50)
	CALL _Debug_Putchar
	LDI  R26,LOW(53)
	JMP  _Debug_Putchar


	.CSEG
__ROUND_REPACK:
	TST  R21
	BRPL __REPACK
	CPI  R21,0x80
	BRNE __ROUND_REPACK0
	SBRS R30,0
	RJMP __REPACK
__ROUND_REPACK0:
	ADIW R30,1
	ADC  R22,R25
	ADC  R23,R25
	BRVS __REPACK1

__REPACK:
	LDI  R21,0x80
	EOR  R21,R23
	BRNE __REPACK0
	PUSH R21
	RJMP __ZERORES
__REPACK0:
	CPI  R21,0xFF
	BREQ __REPACK1
	LSL  R22
	LSL  R0
	ROR  R21
	ROR  R22
	MOV  R23,R21
	RET
__REPACK1:
	PUSH R21
	TST  R0
	BRMI __REPACK2
	RJMP __MAXRES
__REPACK2:
	RJMP __MINRES

__UNPACK:
	LDI  R21,0x80
	MOV  R1,R25
	AND  R1,R21
	LSL  R24
	ROL  R25
	EOR  R25,R21
	LSL  R21
	ROR  R24

__UNPACK1:
	LDI  R21,0x80
	MOV  R0,R23
	AND  R0,R21
	LSL  R22
	ROL  R23
	EOR  R23,R21
	LSL  R21
	ROR  R22
	RET

__CFD1U:
	SET
	RJMP __CFD1U0
__CFD1:
	CLT
__CFD1U0:
	PUSH R21
	RCALL __UNPACK1
	CPI  R23,0x80
	BRLO __CFD10
	CPI  R23,0xFF
	BRCC __CFD10
	RJMP __ZERORES
__CFD10:
	LDI  R21,22
	SUB  R21,R23
	BRPL __CFD11
	NEG  R21
	CPI  R21,8
	BRTC __CFD19
	CPI  R21,9
__CFD19:
	BRLO __CFD17
	SER  R30
	SER  R31
	SER  R22
	LDI  R23,0x7F
	BLD  R23,7
	RJMP __CFD15
__CFD17:
	CLR  R23
	TST  R21
	BREQ __CFD15
__CFD18:
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R23
	DEC  R21
	BRNE __CFD18
	RJMP __CFD15
__CFD11:
	CLR  R23
__CFD12:
	CPI  R21,8
	BRLO __CFD13
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R23
	SUBI R21,8
	RJMP __CFD12
__CFD13:
	TST  R21
	BREQ __CFD15
__CFD14:
	LSR  R23
	ROR  R22
	ROR  R31
	ROR  R30
	DEC  R21
	BRNE __CFD14
__CFD15:
	TST  R0
	BRPL __CFD16
	RCALL __ANEGD1
__CFD16:
	POP  R21
	RET

__CDF1U:
	SET
	RJMP __CDF1U0
__CDF1:
	CLT
__CDF1U0:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __CDF10
	CLR  R0
	BRTS __CDF11
	TST  R23
	BRPL __CDF11
	COM  R0
	RCALL __ANEGD1
__CDF11:
	MOV  R1,R23
	LDI  R23,30
	TST  R1
__CDF12:
	BRMI __CDF13
	DEC  R23
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R1
	RJMP __CDF12
__CDF13:
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R1
	PUSH R21
	RCALL __REPACK
	POP  R21
__CDF10:
	RET

__ZERORES:
	CLR  R30
	CLR  R31
	CLR  R22
	CLR  R23
	POP  R21
	RET

__MINRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	SER  R23
	POP  R21
	RET

__MAXRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	LDI  R23,0x7F
	POP  R21
	RET

__DIVF21:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BRNE __DIVF210
	TST  R1
__DIVF211:
	BRPL __DIVF219
	RJMP __MINRES
__DIVF219:
	RJMP __MAXRES
__DIVF210:
	CPI  R25,0x80
	BRNE __DIVF218
__DIVF217:
	RJMP __ZERORES
__DIVF218:
	EOR  R0,R1
	SEC
	SBC  R25,R23
	BRVC __DIVF216
	BRLT __DIVF217
	TST  R0
	RJMP __DIVF211
__DIVF216:
	MOV  R23,R25
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R1
	CLR  R17
	CLR  R18
	CLR  R19
	CLR  R20
	CLR  R21
	LDI  R25,32
__DIVF212:
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	CPC  R20,R17
	BRLO __DIVF213
	SUB  R26,R30
	SBC  R27,R31
	SBC  R24,R22
	SBC  R20,R17
	SEC
	RJMP __DIVF214
__DIVF213:
	CLC
__DIVF214:
	ROL  R21
	ROL  R18
	ROL  R19
	ROL  R1
	ROL  R26
	ROL  R27
	ROL  R24
	ROL  R20
	DEC  R25
	BRNE __DIVF212
	MOVW R30,R18
	MOV  R22,R1
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	TST  R22
	BRMI __DIVF215
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	DEC  R23
	BRVS __DIVF217
__DIVF215:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__ANEGD1:
	COM  R31
	COM  R22
	COM  R23
	NEG  R30
	SBCI R31,-1
	SBCI R22,-1
	SBCI R23,-1
	RET

__LSLB12:
	TST  R30
	MOV  R0,R30
	MOV  R30,R26
	BREQ __LSLB12R
__LSLB12L:
	LSL  R30
	DEC  R0
	BRNE __LSLB12L
__LSLB12R:
	RET

__CWD1:
	MOV  R22,R31
	ADD  R22,R22
	SBC  R22,R22
	MOV  R23,R22
	RET

__MULB1W2U:
	MOV  R22,R30
	MUL  R22,R26
	MOVW R30,R0
	MUL  R22,R27
	ADD  R31,R0
	RET

__DIVB21U:
	CLR  R0
	LDI  R25,8
__DIVB21U1:
	LSL  R26
	ROL  R0
	SUB  R0,R30
	BRCC __DIVB21U2
	ADD  R0,R30
	RJMP __DIVB21U3
__DIVB21U2:
	SBR  R26,1
__DIVB21U3:
	DEC  R25
	BRNE __DIVB21U1
	MOV  R30,R26
	MOV  R26,R0
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__MODB21U:
	RCALL __DIVB21U
	MOV  R30,R26
	RET

__MODW21U:
	RCALL __DIVW21U
	MOVW R30,R26
	RET

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
