
;CodeVisionAVR C Compiler V2.05.2 Standard
;(C) Copyright 1998-2011 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega16M1
;Program type             : Application
;Clock frequency          : 14.745600 MHz
;Memory model             : Small
;Optimize for             : Speed
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 384 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : No
;Global 'const' stored in FLASH     : Yes
;Enhanced function parameter passing: Yes
;Enhanced core instructions         : On
;Smart register allocation          : On
;Automatic register allocation      : On

	#pragma AVRPART ADMIN PART_NAME ATmega16M1
	#pragma AVRPART MEMORY PROG_FLASH 16384
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1279
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1F
	.EQU EEDR=0x20
	.EQU EEARL=0x21
	.EQU EEARH=0x22
	.EQU SMCR=0x33
	.EQU MCUSR=0x34
	.EQU MCUCR=0x35
	.EQU WDTCSR=0x60
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU GPIOR0=0x1E
	.EQU GPIOR1=0x19
	.EQU GPIOR2=0x1A

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0100
	.EQU __SRAM_END=0x04FF
	.EQU __DSTACK_SIZE=0x0180
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTW2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD2FN
	LDI  R26,LOW(2*@0+(@1))
	LDI  R27,HIGH(2*@0+(@1))
	LDI  R24,BYTE3(2*@0+(@1))
	LDI  R25,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _Rx1_Counter=R3
	.DEF _Rx1_Pointer_Buff=R2
	.DEF _Tmp_Rx1=R5
	.DEF _Time_Out_1=R4
	.DEF _Rx2_Counter=R7
	.DEF _Rx2_Pointer_Buff=R6
	.DEF _Tmp_Rx2=R9
	.DEF _Time_Out_2=R8
	.DEF _Key_Press=R11
	.DEF _CB_Tx_Counter1=R10
	.DEF _CB_Tx_Counter2=R13
	.DEF _CB1_Time=R12

;GPIOR0-GPIOR2 INITIALIZATION VALUES
	.EQU __GPIOR0_INIT=0x00
	.EQU __GPIOR1_INIT=0x00
	.EQU __GPIOR2_INIT=0x00

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  _timer1_compa_isr
	JMP  0x00
	JMP  0x00
	JMP  _timer0_compa_isr
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00

_Firmware_Ver:
	.DB  0x30,0x31,0x30,0x30,0x0
_Password:
	.DB  0x47,0x46,0x49,0x38,0x34,0x37,0x0
_Bit_Table:
	.DB  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFE,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFD,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFB,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xF7,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xEF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xDF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xBF,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0x7F,0xFF,0xFF,0xFF,0xFF
	.DB  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x0:
	.DB  0x20,0x3D,0x3E,0x20,0x4E,0x65,0x77,0x20
	.DB  0x0,0x20,0x3D,0x3E,0x20,0x4F,0x6C,0x64
	.DB  0x20,0x0,0x4D,0x6F,0x64,0x65,0x20,0x0
_0x20003:
	.DB  0x0
_0x20004:
	.DB  0x0
_0x20005:
	.DB  0x0
_0x20006:
	.DB  0x30
_0x20007:
	.DB  0x78,0x0
_0x20008:
	.DB  0xF4,0x1
_0x2015B:
	.DB  0x0,0x0,0x0,0x0
_0x20000:
	.DB  0x20,0x56,0x61,0x75,0x6C,0x74,0x20,0x44
	.DB  0x65,0x62,0x75,0x67,0x20,0x4D,0x65,0x6E
	.DB  0x75,0x20,0x56,0x31,0x2E,0x30,0x30,0x20
	.DB  0x0,0x3B,0x20,0x46,0x69,0x72,0x6D,0x77
	.DB  0x61,0x72,0x65,0x20,0x56,0x65,0x72,0x73
	.DB  0x69,0x6F,0x6E,0x3A,0x0,0x3B,0x20,0x0
	.DB  0x20,0x49,0x4E,0x50,0x55,0x54,0x53,0x3A
	.DB  0x0,0x43,0x42,0x58,0x20,0x53,0x65,0x6E
	.DB  0x73,0x6F,0x72,0x3D,0x0,0x3B,0x20,0x50
	.DB  0x6F,0x73,0x69,0x74,0x69,0x6F,0x6E,0x20
	.DB  0x53,0x65,0x6E,0x73,0x6F,0x72,0x3D,0x0
	.DB  0x3B,0x20,0x4D,0x6F,0x74,0x6F,0x72,0x20
	.DB  0x4C,0x6F,0x63,0x6B,0x20,0x53,0x65,0x6E
	.DB  0x73,0x6F,0x72,0x3D,0x0,0x3B,0x20,0x4D
	.DB  0x6F,0x74,0x6F,0x72,0x20,0x46,0x61,0x75
	.DB  0x6C,0x74,0x3D,0x0,0x4F,0x55,0x54,0x50
	.DB  0x55,0x54,0x53,0x3A,0x0,0x5B,0x53,0x5D
	.DB  0x6F,0x6C,0x65,0x6E,0x6F,0x69,0x64,0x3D
	.DB  0x0,0x3B,0x20,0x4C,0x61,0x6D,0x70,0x5B
	.DB  0x31,0x5D,0x3D,0x0,0x3B,0x20,0x4C,0x61
	.DB  0x6D,0x70,0x5B,0x32,0x5D,0x3D,0x0,0x3B
	.DB  0x20,0x4D,0x6F,0x74,0x6F,0x72,0x20,0x5B
	.DB  0x50,0x5D,0x68,0x61,0x73,0x65,0x3D,0x0
	.DB  0x3B,0x20,0x4D,0x6F,0x74,0x6F,0x72,0x20
	.DB  0x5B,0x52,0x5D,0x75,0x6E,0x3D,0x0,0x56
	.DB  0x61,0x75,0x6C,0x74,0x20,0x48,0x6F,0x6C
	.DB  0x64,0x69,0x6E,0x67,0x20,0x54,0x69,0x6D
	.DB  0x65,0x3D,0x0,0x55,0x28,0x31,0x32,0x56
	.DB  0x29,0x3D,0x0,0x55,0x28,0x35,0x56,0x29
	.DB  0x3D,0x0,0x42,0x69,0x6E,0x20,0x4E,0x75
	.DB  0x6D,0x62,0x65,0x72,0x3A,0x0,0x43,0x61
	.DB  0x73,0x68,0x20,0x42,0x6F,0x78,0x20,0x4E
	.DB  0x75,0x6D,0x62,0x65,0x72,0x3A,0x0,0x54
	.DB  0x72,0x61,0x6E,0x73,0x6D,0x69,0x73,0x73
	.DB  0x69,0x6F,0x6E,0x20,0x44,0x61,0x74,0x61
	.DB  0x20,0x46,0x6F,0x72,0x6D,0x61,0x74,0x0
	.DB  0x20,0x5B,0x53,0x5D,0x65,0x72,0x69,0x61
	.DB  0x6C,0x20,0x4E,0x75,0x6D,0x62,0x65,0x72
	.DB  0x2C,0x5B,0x56,0x5D,0x61,0x75,0x6C,0x74
	.DB  0x20,0x4E,0x75,0x6D,0x62,0x65,0x72,0x2C
	.DB  0x48,0x6F,0x6C,0x64,0x69,0x6E,0x67,0x20
	.DB  0x5B,0x54,0x5D,0x69,0x6D,0x65,0x20,0x5B
	.DB  0x45,0x73,0x63,0x5D,0x2D,0x4D,0x61,0x69
	.DB  0x6E,0x20,0x4D,0x65,0x6E,0x75,0x20,0x0
	.DB  0x56,0x0,0x53,0x0,0x54,0x69,0x6D,0x65
	.DB  0x20,0x56,0x61,0x75,0x6C,0x74,0x20,0x48
	.DB  0x6F,0x6C,0x64,0x69,0x6E,0x67,0x3D,0x0
	.DB  0x54,0x0,0x5B,0x73,0x5D,0x0,0x4E,0x65
	.DB  0x77,0x20,0x53,0x65,0x72,0x69,0x61,0x6C
	.DB  0x20,0x4E,0x75,0x6D,0x62,0x65,0x72,0x3A
	.DB  0x0,0x4E,0x65,0x77,0x20,0x56,0x61,0x75
	.DB  0x6C,0x74,0x20,0x4E,0x75,0x6D,0x62,0x65
	.DB  0x72,0x3A,0x0,0x4E,0x65,0x77,0x20,0x56
	.DB  0x61,0x6C,0x74,0x20,0x48,0x6F,0x6C,0x64
	.DB  0x69,0x6E,0x67,0x20,0x54,0x69,0x6D,0x65
	.DB  0x3A,0x0
_0x2040003:
	.DB  0x0,0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  _CB1_Pointer
	.DW  _0x20003*2

	.DW  0x01
	.DW  _CB2_Pointer
	.DW  _0x20004*2

	.DW  0x01
	.DW  _Status_Lights
	.DW  _0x20005*2

	.DW  0x01
	.DW  _Alarm_Status
	.DW  _0x20006*2

	.DW  0x02
	.DW  _U_12V
	.DW  _0x20007*2

	.DW  0x02
	.DW  _U_5V
	.DW  _0x20008*2

	.DW  0x04
	.DW  0x0A
	.DW  _0x2015B*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  MCUCR,R31
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	WDR
	IN   R26,MCUSR
	CBR  R26,8
	OUT  MCUSR,R26
	STS  WDTCSR,R31
	STS  WDTCSR,R30

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;GPIOR0-GPIOR2 INITIALIZATION
	LDI  R30,__GPIOR0_INIT
	OUT  GPIOR0,R30
	;__GPIOR1_INIT = __GPIOR0_INIT
	OUT  GPIOR1,R30

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x280

	.CSEG
;
;#include "debug.h"
;
;
;
;extern unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8];
;extern unsigned char Tx1_Counter,TxD_1;
;
;
;#pragma optsize+
;
;//*********************************** Debug putchar *******************************
;void Debug_Putchar(unsigned char Data)
; 0000 000E {

	.CSEG
_Debug_Putchar:
; 0000 000F  TxD_1=Data;
	ST   -Y,R26
;	Data -> Y+0
	LD   R30,Y
	STS  _TxD_1,R30
; 0000 0010  Tx1_Counter=0;
	LDI  R30,LOW(0)
	STS  _Tx1_Counter,R30
; 0000 0011  while (Tx1_Counter<80);
_0x3:
	LDS  R26,_Tx1_Counter
	CPI  R26,LOW(0x50)
	BRLO _0x3
; 0000 0012 }
	JMP  _0x2060002
;
;
;
;
;//*********************************** Debug Getchar *******************************
;unsigned char Debug_Getchar(void)
; 0000 0019 {
_Debug_Getchar:
; 0000 001A  Rx3_Pointer_Buff=0;
	LDI  R30,LOW(0)
	STS  _Rx3_Pointer_Buff,R30
; 0000 001B  while (Rx3_Pointer_Buff==0) #asm("wdr");
_0x6:
	LDS  R30,_Rx3_Pointer_Buff
	CPI  R30,0
	BRNE _0x8
	wdr
; 0000 001C  return(Rx3_Buff[0]);
	RJMP _0x6
_0x8:
	LDS  R30,_Rx3_Buff
	RET
; 0000 001D }
;
;
;
;
;//********************************* DEBUG PUTSTRING **************************************
;void Print(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
; 0000 0024  {
_Print:
; 0000 0025 
; 0000 0026             #asm ("wdr");
	ST   -Y,R26
;	*text -> Y+3
;	X -> Y+2
;	Y -> Y+1
;	color -> Y+0
	wdr
; 0000 0027 
; 0000 0028 
; 0000 0029     if (color)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x9
; 0000 002A              {
; 0000 002B               Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 002C               Debug_Putchar(0x5B);
; 0000 002D               Debug_Putchar(0x30+color);
	LD   R26,Y
	SUBI R26,-LOW(48)
	RCALL _Debug_Putchar
; 0000 002E               Debug_Putchar(0x6D);
	LDI  R26,LOW(109)
	RCALL _Debug_Putchar
; 0000 002F              };
_0x9:
; 0000 0030 
; 0000 0031    if (X<200)
	LDD  R26,Y+2
	CPI  R26,LOW(0xC8)
	BRSH _0xA
; 0000 0032             {
; 0000 0033               Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 0034               Debug_Putchar(0x5B);
; 0000 0035               if (Y<10) Debug_Putchar(0x30+Y);
	LDD  R26,Y+1
	CPI  R26,LOW(0xA)
	BRSH _0xB
	SUBI R26,-LOW(48)
	RJMP _0x1D
; 0000 0036                else
_0xB:
; 0000 0037                     {
; 0000 0038                     Debug_Putchar(0x30+Y/10);
	LDD  R26,Y+1
	CALL SUBOPT_0x1
; 0000 0039                     Debug_Putchar(0x30+(Y%10));
	LDD  R26,Y+1
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
_0x1D:
	RCALL _Debug_Putchar
; 0000 003A                     };
; 0000 003B 
; 0000 003C               Debug_Putchar(0x03B);
	LDI  R26,LOW(59)
	RCALL _Debug_Putchar
; 0000 003D               Debug_Putchar(0x30+(X/10));
	LDD  R26,Y+2
	CALL SUBOPT_0x1
; 0000 003E               Debug_Putchar(0x30+(X%10));
	LDD  R26,Y+2
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	RCALL _Debug_Putchar
; 0000 003F               Debug_Putchar(0x48);
	LDI  R26,LOW(72)
	RCALL _Debug_Putchar
; 0000 0040 
; 0000 0041             }
; 0000 0042 
; 0000 0043               while (*text!=0) Debug_Putchar(*text++);
_0xA:
_0xD:
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	LPM  R30,Z
	CPI  R30,0
	BREQ _0xF
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	ADIW R30,1
	STD  Y+3,R30
	STD  Y+3+1,R31
	SBIW R30,1
	LPM  R26,Z
	RCALL _Debug_Putchar
	RJMP _0xD
_0xF:
; 0000 0045 if (color)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x10
; 0000 0046                           {
; 0000 0047                            Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 0048                            Debug_Putchar(0x5B);
; 0000 0049                            Debug_Putchar(0x30);
	LDI  R26,LOW(48)
	RCALL _Debug_Putchar
; 0000 004A                            Debug_Putchar(0x6D);
	LDI  R26,LOW(109)
	RCALL _Debug_Putchar
; 0000 004B                           }
; 0000 004C 
; 0000 004D   };
_0x10:
	ADIW R28,5
	RET
;
;
;
;
;
;
;//*************************************** Input Data ****************************************
;unsigned char Input(unsigned char *Out_Pointer,unsigned char Size,unsigned char Password)
; 0000 0056  {
_Input:
; 0000 0057   unsigned char Tmp,Tmp2;
; 0000 0058 
; 0000 0059     Debug_Putchar(' ');
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	*Out_Pointer -> Y+4
;	Size -> Y+3
;	Password -> Y+2
;	Tmp -> R17
;	Tmp2 -> R16
	LDI  R26,LOW(32)
	RCALL _Debug_Putchar
; 0000 005A     Cursor_Show();
	RCALL _Cursor_Show
; 0000 005B     Tmp=0;
	LDI  R17,LOW(0)
; 0000 005C 
; 0000 005D Loop_input:
_0x11:
; 0000 005E 
; 0000 005F     Tmp2=Debug_Getchar();
	RCALL _Debug_Getchar
	MOV  R16,R30
; 0000 0060 
; 0000 0061     if (Tmp2==27)
	CPI  R16,27
	BRNE _0x12
; 0000 0062                 {
; 0000 0063                 Tmp=0;
	LDI  R17,LOW(0)
; 0000 0064                 goto Exit_Input;
	RJMP _0x13
; 0000 0065                 };
_0x12:
; 0000 0066 
; 0000 0067 
; 0000 0068     if (Tmp2==13)
	CPI  R16,13
	BRNE _0x14
; 0000 0069                 {
; 0000 006A                  if (Tmp==Size) goto Exit_Input;
	LDD  R30,Y+3
	CP   R30,R17
	BREQ _0x13
; 0000 006B 
; 0000 006C                  Tmp=0;
	LDI  R17,LOW(0)
; 0000 006D                  goto Exit_Input;
	RJMP _0x13
; 0000 006E                 };
_0x14:
; 0000 006F 
; 0000 0070     if (Tmp2==8)
	CPI  R16,8
	BRNE _0x16
; 0000 0071                {
; 0000 0072                if (Tmp>0)
	CPI  R17,1
	BRLO _0x17
; 0000 0073                         {
; 0000 0074                         Tmp--;
	SUBI R17,1
; 0000 0075                         Debug_Putchar(8);
	LDI  R26,LOW(8)
	RCALL _Debug_Putchar
; 0000 0076                         Debug_Putchar(' ');
	LDI  R26,LOW(32)
	RCALL _Debug_Putchar
; 0000 0077                         Debug_Putchar(8);
	LDI  R26,LOW(8)
	RCALL _Debug_Putchar
; 0000 0078                         }
; 0000 0079                         goto Loop_input;
_0x17:
	RJMP _0x11
; 0000 007A                }
; 0000 007B 
; 0000 007C 
; 0000 007D     if (Tmp<Size)
_0x16:
	LDD  R30,Y+3
	CP   R17,R30
	BRSH _0x18
; 0000 007E                 {
; 0000 007F                 Out_Pointer[Tmp]=Tmp2;
	MOV  R30,R17
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R16
; 0000 0080 
; 0000 0081                 if (Password) Debug_Putchar('*');
	LDD  R30,Y+2
	CPI  R30,0
	BREQ _0x19
	LDI  R26,LOW(42)
	RJMP _0x1E
; 0000 0082                  else Debug_Putchar(Tmp2);
_0x19:
	MOV  R26,R16
_0x1E:
	RCALL _Debug_Putchar
; 0000 0083                 Tmp++;
	SUBI R17,-1
; 0000 0084                 };
_0x18:
; 0000 0085 
; 0000 0086                 goto Loop_input;
	RJMP _0x11
; 0000 0087   Exit_Input:
_0x13:
; 0000 0088   Cursor_Hide();
	RCALL _Cursor_Hide
; 0000 0089   return(Tmp);
	MOV  R30,R17
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,6
	RET
; 0000 008A  }
;
;
;
;
;
;
;//************************************************** Print Mode New/Old *****************************************
;void Mode_Print(unsigned char mode)
; 0000 0093 {
_Mode_Print:
; 0000 0094 
; 0000 0095  if (mode) Print(" => New ",255,0,0);
	ST   -Y,R26
;	mode -> Y+0
	LD   R30,Y
	CPI  R30,0
	BREQ _0x1B
	__POINTW1FN _0x0,0
	RJMP _0x1F
; 0000 0096         else Print(" => Old ",255,0,0);
_0x1B:
	__POINTW1FN _0x0,9
_0x1F:
	ST   -Y,R31
	ST   -Y,R30
	CALL SUBOPT_0x2
; 0000 0097 
; 0000 0098  Print("Mode ",255,0,0);
	__POINTW1FN _0x0,18
	ST   -Y,R31
	ST   -Y,R30
	CALL SUBOPT_0x2
; 0000 0099 };
	JMP  _0x2060002
;
;
;
;
;     //********************************************** Clear Screen ***********************************************************
;void ClrScr(void)
; 0000 00A0 {
_ClrScr:
; 0000 00A1  Debug_Putchar(12);
	LDI  R26,LOW(12)
	RCALL _Debug_Putchar
; 0000 00A2  Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00A3  Debug_Putchar('[');
; 0000 00A4  Debug_Putchar('2');
	LDI  R26,LOW(50)
	RCALL _Debug_Putchar
; 0000 00A5  Debug_Putchar('J');
	LDI  R26,LOW(74)
	RJMP _0x2060003
; 0000 00A6 }
;
;
;
;//************************************************************ Cursor Hide ********************************************************
;void Cursor_Hide()
; 0000 00AC {
_Cursor_Hide:
; 0000 00AD Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00AE Debug_Putchar('[');
; 0000 00AF Debug_Putchar('?');
	CALL SUBOPT_0x3
; 0000 00B0 Debug_Putchar('2');
; 0000 00B1 Debug_Putchar('5');
; 0000 00B2 Debug_Putchar('l');
	LDI  R26,LOW(108)
	RJMP _0x2060003
; 0000 00B3 }
;
;
;
;
;//************************************************************ Cursor Show ********************************************************
;void Cursor_Show()
; 0000 00BA {
_Cursor_Show:
; 0000 00BB Debug_Putchar(0x1B);
	CALL SUBOPT_0x0
; 0000 00BC Debug_Putchar('[');
; 0000 00BD Debug_Putchar('?');
	CALL SUBOPT_0x3
; 0000 00BE Debug_Putchar('2');
; 0000 00BF Debug_Putchar('5');
; 0000 00C0 Debug_Putchar('h');
	LDI  R26,LOW(104)
_0x2060003:
	RCALL _Debug_Putchar
; 0000 00C1 }
	RET
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.04.9 Standard
;Automatic Program Generator
;� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 11/29/2010
;Author  : SVUKOVIC
;Company : Microsoft
;Comments:
;
;
;Chip type               : ATtiny4313
;AVR Core Clock frequency: 14.745600 MHz
;Memory model            : Tiny
;External RAM size       : 0
;Data Stack size         : 64
;*****************************************************/
;
;#include <mega16m1.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_standby=0x0C
	.SET power_ctrl_reg=smcr
	#endif
;#include <stdio.h>
;#include <delay.h>
;
;#include "debug.h"
;
;
;#define STX    'S'
;#define ETX    'E'
;
;#define NORMAL 'N'
;
;
;
;
;#define Min_5V   450
;#define Max_5V   550
;
;#define Min_12V   110
;#define Max_12V   130
;
;
;//#define _Test_
;
;#define Time_Base_CBXID   20
;
;
;
;//******** INPUTS *************
;#define CbxPS_Sensor      PINB.3
;#define RecPS_Sensor      PINC.0
;#define LockPS_Sensor     PINB.2
;#define Motor_Fault       PINB.7
;
;
;#define In_1            PINB.6
;#define In_2            PINC.7
;#define In_3            PINC.3
;
;
;#define Send_Mode       PIND.6
;//bit Send_Mode=0;
;
;
;#asm
.EQU _Soft_TxD=2
.EQU _PORTC=0x08
; 0001 0046 #endasm
;
;
;#define Soft_TxD    PORTC.2
;
;
;#define Out_ID      PORTD.3
;#define Out_ID2     PORTB.4
;
;
;#define RS485_Dir   PORTB.5
;
;
;
;
;#define Led_Power     PORTD.5
;#define Led_Link      PORTB.1
;#define Led_Sys       PORTC.6
;#define Led_Service   PORTB.0
;
;
;
;
;//***** OUTPUTS *********
;#define Out_Lig1   PORTD.7
;#define Out_Lig2   PORTD.2
;#define Out_Valve  PORTC.1
;
;#define Out_Motor_Ph   PORTD.1
;#define Out_Motor_Run  PORTD.0
;
;#define Power_Light DDRC.7
;
;
;
;eeprom unsigned char Serial_No[]="SN123456";
;eeprom unsigned char Vault[]="VAULT";
;eeprom unsigned char Solenoid_Time=5;
;
;flash unsigned char Firmware_Ver[]="0100";
;flash unsigned char Password[]="GFI847";
;
;
;
;bit Mode_1,Mode_2,Sinc_Bit,Refresh_Bit;
;bit CBX1_Done_bit,CBX2_Done_bit,CBX1_bit=0,Old_CBX1,CBX2_bit=0,Old_CBX2;
;
;unsigned char Rx1_Counter,Rx1_Pointer_Buff,Tmp_Rx1,Rx1_Buff[8],Time_Out_1;
;unsigned char Rx2_Counter,Rx2_Pointer_Buff,Tmp_Rx2,Rx2_Buff[8],Time_Out_2;
;unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8],Key_Press;
;
;unsigned char Compare_Buffer_1[5],Compare_Buffer_2[5];
;
;
;unsigned char Rx1_ID_Buff[5],Rx2_ID_Buff[5];
;
;unsigned char CB_Tx_Counter1=0,CB_Tx_Counter2=0;
;
;unsigned char Tx1_Counter,TxD_1;
;
;unsigned char CB1_Time,CB1_Pointer=0,CB2_Time,CB2_Pointer=0;

	.DSEG
;
;unsigned int CB1_Tmp,CB2_Tmp,Timer_Sinc,Solenoid_Td;
;
;unsigned char Buff_Temp[10];;
;
;unsigned char Light_Counter,Led_Blink,Status_Counter,Status_Lights=0,Alarm_Status=0x30;
;
;unsigned int U_12V=120,U_5V=500;
;
;
;
;struct _Timer{
;		  	 unsigned char Time_P;
;			 unsigned int Time;
;		  	 }Timer;
;
;
;struct _RS485{
;		  	 unsigned char Tx_Counter,Rx_Counter,Tx_Size;
;			 unsigned char Tx_Buffer[32],Rx_Buffer[32];
;		  	 }RS485;
;
;struct _Calculated{
;			       unsigned char U5V[3],U12V[3],Th[2];
;		  	      }Calculated;
;
;
;
;void Print_Serial_No(unsigned char position);
;void Print_Vault_No(unsigned char position);
;
;
;
;//************************************* Timer 0 output compare A interrupt service routine *****************************
;interrupt [TIM0_COMPA] void timer0_compa_isr(void)
; 0001 00A6 {

	.CSEG
_timer0_compa_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R22
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0001 00A7 
; 0001 00A8 #pragma optsize-
; 0001 00A9 
; 0001 00AA unsigned char Dat,Tmp1,Tmp2,Tmp3,Tmp4;
; 0001 00AB 
; 0001 00AC #asm("sei");
	CALL __SAVELOCR6
;	Dat -> R17
;	Tmp1 -> R16
;	Tmp2 -> R19
;	Tmp3 -> R18
;	Tmp4 -> R21
	sei
; 0001 00AD 
; 0001 00AE if(Mode_1)
	SBIS 0x1E,0
	RJMP _0x20009
; 0001 00AF           {
; 0001 00B0            if (++CB1_Time<250)
	INC  R12
	LDI  R30,LOW(250)
	CP   R12,R30
	BRLO PC+3
	JMP _0x2000A
; 0001 00B1             {
; 0001 00B2             if (CBX1_bit)
	SBIS 0x1E,6
	RJMP _0x2000B
; 0001 00B3               {
; 0001 00B4                CBX1_bit=0;
	CBI  0x1E,6
; 0001 00B5 
; 0001 00B6                if (CB1_Time>(Time_Base_CBXID*6))
	LDI  R30,LOW(120)
	CP   R30,R12
	BRSH _0x2000E
; 0001 00B7                                   {
; 0001 00B8                                    CB1_Time=0;
	CLR  R12
; 0001 00B9                                    CB1_Pointer=0;
	LDI  R30,LOW(0)
	STS  _CB1_Pointer,R30
; 0001 00BA                                    CB1_Tmp=0;
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R30
; 0001 00BB                                    goto Exit_CB1;
	JMP  _0x2000F
; 0001 00BC                                   }
; 0001 00BD 
; 0001 00BE 
; 0001 00BF                if (CB1_Time>(Time_Base_CBXID*3))
_0x2000E:
	LDI  R30,LOW(60)
	CP   R30,R12
	BRSH _0x20010
; 0001 00C0                  {
; 0001 00C1                  //CB1_Tmp<<=4;
; 0001 00C2                  #asm
; 0001 00C3                  LDS       R30,_CB1_Tmp   ;Load direct from data space
                 LDS       R30,_CB1_Tmp   ;Load direct from data space
; 0001 00C4                  LDS       R31,_CB1_Tmp+1  ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1  ;Load direct from data space
; 0001 00C5                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00C6                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00C7                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00C8                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00C9                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00CA                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00CB                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00CC                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00CD 

; 0001 00CE 

; 0001 00CF                  ;CB1_Tmp|=7
                 ;CB1_Tmp|=7
; 0001 00D0                  ORI       R30,0x07
                 ORI       R30,0x07
; 0001 00D1 

; 0001 00D2                  STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp,R30
; 0001 00D3                  STS       _CB1_Tmp+1,R31
                 STS       _CB1_Tmp+1,R31
; 0001 00D4 

; 0001 00D5                  #endasm
; 0001 00D6 
; 0001 00D7                   CB1_Pointer+=4;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(4)
	JMP  _0x20157
; 0001 00D8 
; 0001 00D9                   goto Exit_CB1;
; 0001 00DA                   };
_0x20010:
; 0001 00DB 
; 0001 00DC 
; 0001 00DD                if (CB1_Time>(Time_Base_CBXID*2))
	LDI  R30,LOW(40)
	CP   R30,R12
	BRSH _0x20011
; 0001 00DE                  {
; 0001 00DF                  //CB1_Tmp<<=3;
; 0001 00E0                  #asm
; 0001 00E1                  LDS       R30,_CB1_Tmp     ;Load direct from data space
                 LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 00E2                  LDS       R31,_CB1_Tmp+1    ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1    ;Load direct from data space
; 0001 00E3                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E4                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E5                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E6                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E7                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 00E8                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 00E9 

; 0001 00EA                  ;CB1_Tmp|=3;
                 ;CB1_Tmp|=3;
; 0001 00EB                  ORI       R30,0x03
                 ORI       R30,0x03
; 0001 00EC 

; 0001 00ED                  STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp,R30
; 0001 00EE                  STS       _CB1_Tmp+1,R31
                 STS       _CB1_Tmp+1,R31
; 0001 00EF                  #endasm
; 0001 00F0 
; 0001 00F1                  CB1_Pointer+=3;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(3)
	RJMP _0x20157
; 0001 00F2                  goto Exit_CB1;
; 0001 00F3                  };
_0x20011:
; 0001 00F4 
; 0001 00F5 
; 0001 00F6                if (CB1_Time<(Time_Base_CBXID))
	LDI  R30,LOW(20)
	CP   R12,R30
	BRSH _0x20012
; 0001 00F7                  {
; 0001 00F8                  CB1_Tmp<<=1;
	LDS  R30,_CB1_Tmp
	LDS  R31,_CB1_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R31
; 0001 00F9                  CB1_Pointer++;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(1)
	RJMP _0x20157
; 0001 00FA                  goto Exit_CB1;
; 0001 00FB                  };
_0x20012:
; 0001 00FC 
; 0001 00FD 
; 0001 00FE                //CB1_Tmp<<=2;
; 0001 00FF                #asm
; 0001 0100                LDS       R30,_CB1_Tmp     ;Load direct from data space
               LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 0101                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 0102                LSL       R30              ;Logical Shift Left
               LSL       R30              ;Logical Shift Left
; 0001 0103                ROL       R31              ;Rotate Left Through Carry
               ROL       R31              ;Rotate Left Through Carry
; 0001 0104                LSL       R30              ;Logical Shift Left
               LSL       R30              ;Logical Shift Left
; 0001 0105                ROL       R31              ;Rotate Left Through Carry
               ROL       R31              ;Rotate Left Through Carry
; 0001 0106 

; 0001 0107                ;CB1_Tmp|=1;
               ;CB1_Tmp|=1;
; 0001 0108 

; 0001 0109                ORI       R30,1
               ORI       R30,1
; 0001 010A                STS       _CB1_Tmp,R30
               STS       _CB1_Tmp,R30
; 0001 010B                STS       _CB1_Tmp+1,R31
               STS       _CB1_Tmp+1,R31
; 0001 010C                #endasm
; 0001 010D 
; 0001 010E                CB1_Pointer+=2;
	LDS  R30,_CB1_Pointer
	SUBI R30,-LOW(2)
_0x20157:
	STS  _CB1_Pointer,R30
; 0001 010F 
; 0001 0110 Exit_CB1:      CB1_Time=0;
_0x2000F:
	CLR  R12
; 0001 0111                 };
_0x2000B:
; 0001 0112             }
; 0001 0113              else
	JMP  _0x20013
_0x2000A:
; 0001 0114                 {
; 0001 0115                  if (CB1_Time==250)
	LDI  R30,LOW(250)
	CP   R30,R12
	BREQ PC+3
	JMP _0x20014
; 0001 0116                         {
; 0001 0117                          if (CB1_Pointer==13) {
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xD)
	BRNE _0x20015
; 0001 0118                                                //CB1_Tmp<<=3;
; 0001 0119                                                #asm
; 0001 011A                                                LDS       R30,_CB1_Tmp     ;Load direct from data space
                                               LDS       R30,_CB1_Tmp     ;Load direct from data space
; 0001 011B                                                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 011C                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 011D                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 011E                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 011F                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0120                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0121                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0122 

; 0001 0123                                                ;CB1_Tmp|=3;
                                               ;CB1_Tmp|=3;
; 0001 0124                                                ORI       R30,3
                                               ORI       R30,3
; 0001 0125 

; 0001 0126                                                STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp,R30
; 0001 0127                                                STS       _CB1_Tmp+1,R31
                                               STS       _CB1_Tmp+1,R31
; 0001 0128                                                #endasm
; 0001 0129                                                }
; 0001 012A 
; 0001 012B 
; 0001 012C                          if (CB1_Pointer==14) {
_0x20015:
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xE)
	BRNE _0x20016
; 0001 012D                                               //CB1_Tmp<<=2;
; 0001 012E                                               #asm
; 0001 012F                                                LDS       R30,_CB1_Tmp    ;Load direct from data space
                                               LDS       R30,_CB1_Tmp    ;Load direct from data space
; 0001 0130                                                LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
; 0001 0131                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0132                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0133                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0134                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0135 

; 0001 0136                                                ;CB1_Tmp|=1;
                                               ;CB1_Tmp|=1;
; 0001 0137                                                ORI       R30,1
                                               ORI       R30,1
; 0001 0138 

; 0001 0139                                                STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp,R30
; 0001 013A                                                STS       _CB1_Tmp+1,R31
                                               STS       _CB1_Tmp+1,R31
; 0001 013B                                                #endasm
; 0001 013C                                               }
; 0001 013D 
; 0001 013E                          if (CB1_Pointer==15) CB1_Tmp<<=1;
_0x20016:
	LDS  R26,_CB1_Pointer
	CPI  R26,LOW(0xF)
	BRNE _0x20017
	LDS  R30,_CB1_Tmp
	LDS  R31,_CB1_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB1_Tmp,R30
	STS  _CB1_Tmp+1,R31
; 0001 013F 
; 0001 0140                          CB1_Pointer=0;
_0x20017:
	LDI  R30,LOW(0)
	STS  _CB1_Pointer,R30
; 0001 0141 
; 0001 0142                          Rx1_Buff[0]='0';
	LDI  R30,LOW(48)
	STS  _Rx1_Buff,R30
; 0001 0143 
; 0001 0144                          //Rx1_Buff[1]=(CB1_Tmp>>12)&0x0F;
; 0001 0145                          #asm
; 0001 0146                          LDS       R30,_CB1_Tmp+1 ;       ;Load immediate
                         LDS       R30,_CB1_Tmp+1 ;       ;Load immediate
; 0001 0147                          SWAP      R30
                         SWAP      R30
; 0001 0148                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0149                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 014A                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 014B                          BRNE      OK1
                         BRNE      OK1
; 0001 014C                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 014D                     OK1: STS       _Rx1_Buff+1,R30
                    OK1: STS       _Rx1_Buff+1,R30
; 0001 014E                          #endasm
; 0001 014F 
; 0001 0150                          //Rx1_Buff[2]=(CB1_Tmp>>8)&0x0F;
; 0001 0151                          #asm
; 0001 0152                          LDS       R30,_CB1_Tmp+1       ;Load immediate
                         LDS       R30,_CB1_Tmp+1       ;Load immediate
; 0001 0153                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0154                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0155                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0156                          BRNE      OK2
                         BRNE      OK2
; 0001 0157                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0158                     OK2: STS       _Rx1_Buff+2,R30
                    OK2: STS       _Rx1_Buff+2,R30
; 0001 0159                          #endasm
; 0001 015A 
; 0001 015B                          //Rx1_Buff[3]=(CB1_Tmp>>4)&0x0F;
; 0001 015C                          #asm
; 0001 015D                          LDS       R30,_CB1_Tmp       ;Load immediate
                         LDS       R30,_CB1_Tmp       ;Load immediate
; 0001 015E                          SWAP      R30
                         SWAP      R30
; 0001 015F                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0160                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0161                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0162                          BRNE      OK3
                         BRNE      OK3
; 0001 0163                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0164                     OK3: STS       _Rx1_Buff+3,R30
                    OK3: STS       _Rx1_Buff+3,R30
; 0001 0165                          #endasm
; 0001 0166 
; 0001 0167                          //Rx1_Buff[4]=CB1_Tmp&0x0F;
; 0001 0168                          #asm
; 0001 0169                          LDS       R30,_CB1_Tmp       ;Load immediate
                         LDS       R30,_CB1_Tmp       ;Load immediate
; 0001 016A                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 016B                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 016C                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 016D                          BRNE      OK4
                         BRNE      OK4
; 0001 016E                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 016F                     OK4: STS       _Rx1_Buff+4,R30
                    OK4: STS       _Rx1_Buff+4,R30
; 0001 0170                          #endasm
; 0001 0171 
; 0001 0172 
; 0001 0173                          CBX1_Done_bit=1;
	SBI  0x1E,4
; 0001 0174                          }
; 0001 0175                           else CB1_Time=250;
	RJMP _0x2001A
_0x20014:
	LDI  R30,LOW(250)
	MOV  R12,R30
; 0001 0176                 };
_0x2001A:
_0x20013:
; 0001 0177 
; 0001 0178           };
_0x20009:
; 0001 0179 
; 0001 017A 
; 0001 017B 
; 0001 017C if(Mode_2)
	SBIS 0x1E,1
	RJMP _0x2001B
; 0001 017D           {
; 0001 017E            if (++CB2_Time<250)
	LDS  R26,_CB2_Time
	SUBI R26,-LOW(1)
	STS  _CB2_Time,R26
	CPI  R26,LOW(0xFA)
	BRLO PC+3
	JMP _0x2001C
; 0001 017F             {
; 0001 0180             if (CBX2_bit)
	SBIS 0x19,0
	RJMP _0x2001D
; 0001 0181               {
; 0001 0182                CBX2_bit=0;
	CBI  0x19,0
; 0001 0183 
; 0001 0184                if (CB2_Time>(Time_Base_CBXID*8))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xA1)
	BRLO _0x20020
; 0001 0185                                   {
; 0001 0186                                    CB2_Time=0;
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 0187                                    CB2_Pointer=0;
	STS  _CB2_Pointer,R30
; 0001 0188                                    CB2_Tmp=0;
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R30
; 0001 0189                                    goto Exit_CB2;
	JMP  _0x20021
; 0001 018A                                   }
; 0001 018B 
; 0001 018C 
; 0001 018D 
; 0001 018E                if (CB2_Time>(Time_Base_CBXID*3))
_0x20020:
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x3D)
	BRLO _0x20022
; 0001 018F                  {
; 0001 0190 
; 0001 0191                  //CB2_Tmp<<=4;
; 0001 0192                  #asm
; 0001 0193                  LDS       R30,_CB2_Tmp   ;Load direct from data space
                 LDS       R30,_CB2_Tmp   ;Load direct from data space
; 0001 0194                  LDS       R31,_CB2_Tmp+1  ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1  ;Load direct from data space
; 0001 0195                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0196                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0197                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 0198                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 0199                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 019A                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 019B                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 019C                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 019D 

; 0001 019E                  ;CB2_Tmp|=7
                 ;CB2_Tmp|=7
; 0001 019F                  ORI       R30,0x07
                 ORI       R30,0x07
; 0001 01A0 

; 0001 01A1                  STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp,R30
; 0001 01A2                  STS       _CB2_Tmp+1,R31
                 STS       _CB2_Tmp+1,R31
; 0001 01A3 

; 0001 01A4                  #endasm
; 0001 01A5 
; 0001 01A6                   CB2_Pointer+=4;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(4)
	JMP  _0x20158
; 0001 01A7 
; 0001 01A8                   goto Exit_CB2;
; 0001 01A9                   };
_0x20022:
; 0001 01AA 
; 0001 01AB 
; 0001 01AC                if (CB2_Time>(Time_Base_CBXID*2))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x29)
	BRLO _0x20023
; 0001 01AD                  {
; 0001 01AE                  //CB2_Tmp<<=3;
; 0001 01AF                  #asm
; 0001 01B0                  LDS       R30,_CB2_Tmp     ;Load direct from data space
                 LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01B1                  LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01B2                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01B3                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01B4                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01B5                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01B6                  LSL       R30            ;Logical Shift Left
                 LSL       R30            ;Logical Shift Left
; 0001 01B7                  ROL       R31            ;Rotate Left Through Carry
                 ROL       R31            ;Rotate Left Through Carry
; 0001 01B8 

; 0001 01B9                  ;CB2_Tmp|=3;
                 ;CB2_Tmp|=3;
; 0001 01BA                  ORI       R30,0x03
                 ORI       R30,0x03
; 0001 01BB 

; 0001 01BC                  STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp,R30
; 0001 01BD                  STS       _CB2_Tmp+1,R31
                 STS       _CB2_Tmp+1,R31
; 0001 01BE                  #endasm
; 0001 01BF 
; 0001 01C0                  CB2_Pointer+=3;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(3)
	RJMP _0x20158
; 0001 01C1                  goto Exit_CB2;
; 0001 01C2                  };
_0x20023:
; 0001 01C3 
; 0001 01C4 
; 0001 01C5                if (CB2_Time<(Time_Base_CBXID))
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0x14)
	BRSH _0x20024
; 0001 01C6                  {
; 0001 01C7                  CB2_Tmp<<=1;
	LDS  R30,_CB2_Tmp
	LDS  R31,_CB2_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R31
; 0001 01C8                  CB2_Pointer++;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(1)
	RJMP _0x20158
; 0001 01C9                  goto Exit_CB2;
; 0001 01CA                  };
_0x20024:
; 0001 01CB 
; 0001 01CC 
; 0001 01CD                //CB2_Tmp<<=2;
; 0001 01CE                #asm
; 0001 01CF                LDS       R30,_CB2_Tmp     ;Load direct from data space
               LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01D0                LDS       R31,_CB2_Tmp+1    ;Load direct from data space
               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01D1                LSL       R30            ;Logical Shift Left
               LSL       R30            ;Logical Shift Left
; 0001 01D2                ROL       R31            ;Rotate Left Through Carry
               ROL       R31            ;Rotate Left Through Carry
; 0001 01D3                LSL       R30            ;Logical Shift Left
               LSL       R30            ;Logical Shift Left
; 0001 01D4                ROL       R31            ;Rotate Left Through Carry
               ROL       R31            ;Rotate Left Through Carry
; 0001 01D5 

; 0001 01D6                ;CB2_Tmp|=1;
               ;CB2_Tmp|=1;
; 0001 01D7 

; 0001 01D8                ORI       R30,1
               ORI       R30,1
; 0001 01D9                STS       _CB2_Tmp,R30
               STS       _CB2_Tmp,R30
; 0001 01DA                STS       _CB2_Tmp+1,R31
               STS       _CB2_Tmp+1,R31
; 0001 01DB                #endasm
; 0001 01DC 
; 0001 01DD                CB2_Pointer+=2;
	LDS  R30,_CB2_Pointer
	SUBI R30,-LOW(2)
_0x20158:
	STS  _CB2_Pointer,R30
; 0001 01DE 
; 0001 01DF Exit_CB2:      CB2_Time=0;
_0x20021:
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 01E0                 };
_0x2001D:
; 0001 01E1             }
; 0001 01E2              else
	JMP  _0x20025
_0x2001C:
; 0001 01E3                 {
; 0001 01E4                  if (CB2_Time==250)
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xFA)
	BREQ PC+3
	JMP _0x20026
; 0001 01E5                         {
; 0001 01E6                          if (CB2_Pointer==13) {
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xD)
	BRNE _0x20027
; 0001 01E7                                                //CB2_Tmp<<=3;
; 0001 01E8                                                #asm
; 0001 01E9                                                LDS       R30,_CB2_Tmp     ;Load direct from data space
                                               LDS       R30,_CB2_Tmp     ;Load direct from data space
; 0001 01EA                                                LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
; 0001 01EB                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01EC                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01ED                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01EE                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01EF                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 01F0                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 01F1 

; 0001 01F2                                                ;CB2_Tmp|=3;
                                               ;CB2_Tmp|=3;
; 0001 01F3                                                ORI       R30,3
                                               ORI       R30,3
; 0001 01F4 

; 0001 01F5                                                STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp,R30
; 0001 01F6                                                STS       _CB2_Tmp+1,R31
                                               STS       _CB2_Tmp+1,R31
; 0001 01F7                                                #endasm
; 0001 01F8                                                }
; 0001 01F9 
; 0001 01FA 
; 0001 01FB                          if (CB2_Pointer==14) {
_0x20027:
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xE)
	BRNE _0x20028
; 0001 01FC                                               //CB2_Tmp<<=2;
; 0001 01FD                                               #asm
; 0001 01FE                                                LDS       R30,_CB2_Tmp    ;Load direct from data space
                                               LDS       R30,_CB2_Tmp    ;Load direct from data space
; 0001 01FF                                                LDS       R31,_CB2_Tmp+1   ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1   ;Load direct from data space
; 0001 0200                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0201                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0202                                                LSL       R30            ;Logical Shift Left
                                               LSL       R30            ;Logical Shift Left
; 0001 0203                                                ROL       R31            ;Rotate Left Through Carry
                                               ROL       R31            ;Rotate Left Through Carry
; 0001 0204 

; 0001 0205                                                ;CB2_Tmp|=1;
                                               ;CB2_Tmp|=1;
; 0001 0206                                                ORI       R30,1
                                               ORI       R30,1
; 0001 0207 

; 0001 0208                                                STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp,R30
; 0001 0209                                                STS       _CB2_Tmp+1,R31
                                               STS       _CB2_Tmp+1,R31
; 0001 020A                                                #endasm
; 0001 020B                                               }
; 0001 020C 
; 0001 020D                          if (CB2_Pointer==15) CB2_Tmp<<=1;
_0x20028:
	LDS  R26,_CB2_Pointer
	CPI  R26,LOW(0xF)
	BRNE _0x20029
	LDS  R30,_CB2_Tmp
	LDS  R31,_CB2_Tmp+1
	LSL  R30
	ROL  R31
	STS  _CB2_Tmp,R30
	STS  _CB2_Tmp+1,R31
; 0001 020E 
; 0001 020F                          CB2_Pointer=0;
_0x20029:
	LDI  R30,LOW(0)
	STS  _CB2_Pointer,R30
; 0001 0210 
; 0001 0211                          Rx2_Buff[0]='0';
	LDI  R30,LOW(48)
	STS  _Rx2_Buff,R30
; 0001 0212 
; 0001 0213                          //Rx2_Buff[1]=(CB2_Tmp>>12)&0x0F;
; 0001 0214                          #asm
; 0001 0215                          LDS       R30,_CB2_Tmp+1 ;       ;Load immediate
                         LDS       R30,_CB2_Tmp+1 ;       ;Load immediate
; 0001 0216                          SWAP      R30
                         SWAP      R30
; 0001 0217                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0218                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0219                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 021A                          BRNE      OK11
                         BRNE      OK11
; 0001 021B                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 021C                  OK11:   STS       _Rx2_Buff+1,R30
                 OK11:   STS       _Rx2_Buff+1,R30
; 0001 021D                          #endasm
; 0001 021E 
; 0001 021F                          //Rx2_Buff[2]=(CB2_Tmp>>8)&0x0F;
; 0001 0220                          #asm
; 0001 0221                          LDS       R30,_CB2_Tmp+1       ;Load immediate
                         LDS       R30,_CB2_Tmp+1       ;Load immediate
; 0001 0222                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 0223                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0224                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0225                          BRNE      OK22
                         BRNE      OK22
; 0001 0226                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0227                  OK22:   STS       _Rx2_Buff+2,R30
                 OK22:   STS       _Rx2_Buff+2,R30
; 0001 0228                          #endasm
; 0001 0229 
; 0001 022A                          //Rx2_Buff[3]=(CB2_Tmp>>4)&0x0F;
; 0001 022B                          #asm
; 0001 022C                          LDS       R30,_CB2_Tmp       ;Load immediate
                         LDS       R30,_CB2_Tmp       ;Load immediate
; 0001 022D                          SWAP      R30
                         SWAP      R30
; 0001 022E                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 022F                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 0230                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 0231                          BRNE      OK33
                         BRNE      OK33
; 0001 0232                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0233                  OK33:   STS       _Rx2_Buff+3,R30
                 OK33:   STS       _Rx2_Buff+3,R30
; 0001 0234 

; 0001 0235                          OR        R8,R26
                         OR        R8,R26
; 0001 0236                          #endasm
; 0001 0237 
; 0001 0238                          //Rx2_Buff[4]=CB2_Tmp&0x0F;
; 0001 0239                          #asm
; 0001 023A                          LDS       R30,_CB2_Tmp       ;Load immediate
                         LDS       R30,_CB2_Tmp       ;Load immediate
; 0001 023B                          ANDI      R30,0x0F       ;Logical AND with immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
; 0001 023C                          ORI       R30,0x30
                         ORI       R30,0x30
; 0001 023D                          CPI       R30,0x3A
                         CPI       R30,0x3A
; 0001 023E                          BRNE      OK44
                         BRNE      OK44
; 0001 023F                          LDI       R30,0x37
                         LDI       R30,0x37
; 0001 0240                  OK44:   STS       _Rx2_Buff+4,R30
                 OK44:   STS       _Rx2_Buff+4,R30
; 0001 0241                          #endasm
; 0001 0242 
; 0001 0243                          CBX2_Done_bit=1;
	SBI  0x1E,5
; 0001 0244                          }
; 0001 0245                           else CB2_Time=250;
	RJMP _0x2002C
_0x20026:
	LDI  R30,LOW(250)
	STS  _CB2_Time,R30
; 0001 0246                 };
_0x2002C:
_0x20025:
; 0001 0247 
; 0001 0248           };
_0x2001B:
; 0001 0249 
; 0001 024A 
; 0001 024B   if (Time_Out_1<250)
	LDI  R30,LOW(250)
	CP   R4,R30
	BRSH _0x2002D
; 0001 024C                      {
; 0001 024D                       Time_Out_1++;
	INC  R4
; 0001 024E                       if (Time_Out_1==250) {
	CP   R30,R4
	BRNE _0x2002E
; 0001 024F                                             Rx1_Pointer_Buff=8;
	LDI  R30,LOW(8)
	MOV  R2,R30
; 0001 0250                                             Mode_1=0;
	CBI  0x1E,0
; 0001 0251                                             }
; 0001 0252                      };
_0x2002E:
_0x2002D:
; 0001 0253 
; 0001 0254 
; 0001 0255   if (Time_Out_2<250)
	LDI  R30,LOW(250)
	CP   R8,R30
	BRSH _0x20031
; 0001 0256                      {
; 0001 0257                       Time_Out_2++;
	INC  R8
; 0001 0258                       if (Time_Out_2==250) {
	CP   R30,R8
	BRNE _0x20032
; 0001 0259                                             Rx2_Pointer_Buff=8;
	LDI  R30,LOW(8)
	MOV  R6,R30
; 0001 025A                                             Mode_2=0;
	CBI  0x1E,1
; 0001 025B                                             }
; 0001 025C                      };
_0x20032:
_0x20031:
; 0001 025D 
; 0001 025E 
; 0001 025F   if (++Timer_Sinc>7250)       //~1.5[s]
	LDI  R26,LOW(_Timer_Sinc)
	LDI  R27,HIGH(_Timer_Sinc)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	CPI  R30,LOW(0x1C53)
	LDI  R26,HIGH(0x1C53)
	CPC  R31,R26
	BRSH PC+3
	JMP _0x20035
; 0001 0260                     {
; 0001 0261 
; 0001 0262                     #asm("wdr");
	wdr
; 0001 0263 
; 0001 0264                     Sinc_Bit=1;
	SBI  0x1E,2
; 0001 0265                     Timer_Sinc=0; //reset 1.5[s] timer
	LDI  R30,LOW(0)
	STS  _Timer_Sinc,R30
	STS  _Timer_Sinc+1,R30
; 0001 0266 
; 0001 0267                     CBX1_Done_bit=0;
	CBI  0x1E,4
; 0001 0268                     CBX2_Done_bit=0;
	CBI  0x1E,5
; 0001 0269 
; 0001 026A                     Tmp2=0;
	LDI  R19,LOW(0)
; 0001 026B                     Tmp3=0;
	LDI  R18,LOW(0)
; 0001 026C                     for (Dat=0;Dat<5;Dat++) //Compare Old & New Data Rec. Data
	LDI  R17,LOW(0)
_0x2003D:
	CPI  R17,5
	BRSH _0x2003E
; 0001 026D                                 {
; 0001 026E                                 Tmp1=Rx1_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R16,Z
; 0001 026F                                 if (Compare_Buffer_1[Dat]!=Tmp1) Tmp2=1;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_1)
	SBCI R31,HIGH(-_Compare_Buffer_1)
	LD   R26,Z
	CP   R16,R26
	BREQ _0x2003F
	LDI  R19,LOW(1)
; 0001 0270                                 Compare_Buffer_1[Dat]=Tmp1;
_0x2003F:
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_1)
	SBCI R31,HIGH(-_Compare_Buffer_1)
	ST   Z,R16
; 0001 0271 
; 0001 0272                                 Tmp1=Rx2_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R16,Z
; 0001 0273                                 if (Compare_Buffer_2[Dat]!=Tmp1) Tmp3=1;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_2)
	SBCI R31,HIGH(-_Compare_Buffer_2)
	LD   R26,Z
	CP   R16,R26
	BREQ _0x20040
	LDI  R18,LOW(1)
; 0001 0274                                 Compare_Buffer_2[Dat]=Tmp1;
_0x20040:
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Compare_Buffer_2)
	SBCI R31,HIGH(-_Compare_Buffer_2)
	ST   Z,R16
; 0001 0275                                 }
	SUBI R17,-1
	RJMP _0x2003D
_0x2003E:
; 0001 0276 
; 0001 0277                     Tmp4=0;
	LDI  R21,LOW(0)
; 0001 0278                     for (Dat=0;Dat<5;Dat++)
	LDI  R17,LOW(0)
_0x20042:
	CPI  R17,5
	BRLO PC+3
	JMP _0x20043
; 0001 0279                                 {
; 0001 027A                                 RS485.Tx_Buffer[2+Dat]=Vault[Dat]; //Vault Number
	__POINTW2MN _RS485,3
	MOV  R30,R17
	SUBI R30,-LOW(2)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Vault)
	SBCI R27,HIGH(-_Vault)
	CALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
; 0001 027B 
; 0001 027C                                 if (Tmp2) Rx1_Buff[Dat]='0';
	CPI  R19,0
	BREQ _0x20044
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 027D                                 if (Tmp3) Rx2_Buff[Dat]='0';
_0x20044:
	CPI  R18,0
	BREQ _0x20045
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 027E                                 RS485.Tx_Buffer[7+Dat]=Rx1_Buff[Dat]; //BIN Number
_0x20045:
	__POINTW2MN _RS485,3
	MOV  R30,R17
	SUBI R30,-LOW(7)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R30,Z
	ST   X,R30
; 0001 027F                                 Tmp1=Rx2_Buff[Dat];
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R16,Z
; 0001 0280                                 if (Tmp1!='0') Tmp4++;
	CPI  R16,48
	BREQ _0x20046
	SUBI R21,-1
; 0001 0281                                 RS485.Tx_Buffer[12+Dat]=Tmp1;   //CBX Number
_0x20046:
	__POINTW2MN _RS485,3
	MOV  R30,R17
	SUBI R30,-LOW(12)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R16
; 0001 0282                                 }
	SUBI R17,-1
	RJMP _0x20042
_0x20043:
; 0001 0283 
; 0001 0284                     if (!Send_Mode)
	SBIC 0x9,6
	RJMP _0x20047
; 0001 0285                         {
; 0001 0286                          if (RS485.Rx_Counter==0)
	__GETB1MN _RS485,1
	CPI  R30,0
	BRNE _0x20048
; 0001 0287                             {
; 0001 0288                             RS485.Tx_Size=21;
	LDI  R30,LOW(21)
	__PUTB1MN _RS485,2
; 0001 0289                             RS485.Tx_Buffer[1]=NORMAL;
	LDI  R30,LOW(78)
	__PUTB1MN _RS485,4
; 0001 028A 
; 0001 028B 
; 0001 028C 
; 0001 028D                             RS485.Tx_Buffer[17]=(0x30+!RecPS_Sensor|(!CbxPS_Sensor<<1)|(Out_Valve<<2)); //Sensor Status
	LDI  R30,0
	SBIS 0x6,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R0,R30
	LDI  R26,0
	SBIS 0x3,3
	LDI  R26,1
	MOV  R30,R26
	LSL  R30
	OR   R0,R30
	LDI  R26,0
	SBIC 0x8,1
	LDI  R26,1
	MOV  R30,R26
	LSL  R30
	LSL  R30
	OR   R30,R0
	__PUTB1MN _RS485,20
; 0001 028E                             RS485.Tx_Buffer[18]=0x30+Status_Lights; //Status Light
	LDS  R30,_Status_Lights
	SUBI R30,-LOW(48)
	__PUTB1MN _RS485,21
; 0001 028F                             RS485.Tx_Buffer[19]=Alarm_Status; //Alarm Status
	LDS  R30,_Alarm_Status
	__PUTB1MN _RS485,22
; 0001 0290                             }
; 0001 0291                             else
	RJMP _0x20049
_0x20048:
; 0001 0292                                 {
; 0001 0293                                 //*********************** Execute Command **************************
; 0001 0294 
; 0001 0295                                  RS485.Tx_Size=1;
	LDI  R30,LOW(1)
	__PUTB1MN _RS485,2
; 0001 0296 
; 0001 0297                                  if (RS485.Rx_Buffer[0]==STX)
	__GETB2MN _RS485,35
	CPI  R26,LOW(0x53)
	BREQ PC+3
	JMP _0x2004A
; 0001 0298                                    {
; 0001 0299 
; 0001 029A                                     RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=RS485.Rx_Buffer[1];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _RS485,36
	ST   X,R30
; 0001 029B 
; 0001 029C                                     // ********* Command GET All <Hold Time, 5V,12V,Serial No,Firmware Ver> ******
; 0001 029D                                     if ((RS485.Rx_Buffer[1]=='A')&&(RS485.Rx_Buffer[2]==ETX))
	__GETB2MN _RS485,36
	CPI  R26,LOW(0x41)
	BRNE _0x2004C
	__GETB2MN _RS485,37
	CPI  R26,LOW(0x45)
	BREQ _0x2004D
_0x2004C:
	RJMP _0x2004B
_0x2004D:
; 0001 029E                                       {
; 0001 029F 
; 0001 02A0                                         //Vault Holding Time
; 0001 02A1                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.Th[0];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,6
	ST   X,R30
; 0001 02A2                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.Th[1];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,7
	ST   X,R30
; 0001 02A3 
; 0001 02A4                                         // 5V
; 0001 02A5                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U5V[0];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	LDS  R26,_Calculated
	STD  Z+0,R26
; 0001 02A6                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U5V[1];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,1
	ST   X,R30
; 0001 02A7                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U5V[2];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,2
	ST   X,R30
; 0001 02A8 
; 0001 02A9 
; 0001 02AA                                         // 12V
; 0001 02AB                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U12V[0];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,3
	ST   X,R30
; 0001 02AC                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U12V[1];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,4
	ST   X,R30
; 0001 02AD                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Calculated.U12V[2];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__GETB1MN _Calculated,5
	ST   X,R30
; 0001 02AE 
; 0001 02AF 
; 0001 02B0                                         //Serial Number
; 0001 02B1                                         for (Dat=0;Dat<8;Dat++) RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Serial_No[Dat];
	LDI  R17,LOW(0)
_0x2004F:
	CPI  R17,8
	BRSH _0x20050
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	MOVW R0,R30
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Serial_No)
	SBCI R27,HIGH(-_Serial_No)
	CALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
	SUBI R17,-1
	RJMP _0x2004F
_0x20050:
; 0001 02B4 RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Firmware_Ver[0];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(_Firmware_Ver*2)
	LDI  R31,HIGH(_Firmware_Ver*2)
	LPM  R30,Z
	ST   X,R30
; 0001 02B5                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Firmware_Ver[1];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__POINTW1FN _Firmware_Ver,1
	LPM  R30,Z
	ST   X,R30
; 0001 02B6                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Firmware_Ver[2];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__POINTW1FN _Firmware_Ver,2
	LPM  R30,Z
	ST   X,R30
; 0001 02B7                                         RS485.Tx_Buffer[(RS485.Tx_Size++)&0x1F]=Firmware_Ver[3];
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	__POINTW1FN _Firmware_Ver,3
	LPM  R30,Z
	ST   X,R30
; 0001 02B8 
; 0001 02B9 
; 0001 02BA 
; 0001 02BB                                       }// End Command Get All
; 0001 02BC 
; 0001 02BD                                    //New Vault Time
; 0001 02BE                                    if ((RS485.Rx_Buffer[1]=='T')&&(RS485.Rx_Buffer[3]==ETX))
_0x2004B:
	__GETB2MN _RS485,36
	CPI  R26,LOW(0x54)
	BRNE _0x20052
	__GETB2MN _RS485,38
	CPI  R26,LOW(0x45)
	BREQ _0x20053
_0x20052:
	RJMP _0x20051
_0x20053:
; 0001 02BF                                       {
; 0001 02C0                                        if ((RS485.Rx_Buffer[2]>=0x35)&&(RS485.Rx_Buffer[2]<=0x30+25)) Solenoid_Time=(RS485.Rx_Buffer[2]-0x30);
	__GETB2MN _RS485,37
	CPI  R26,LOW(0x35)
	BRLO _0x20055
	__GETB2MN _RS485,37
	CPI  R26,LOW(0x4A)
	BRLO _0x20056
_0x20055:
	RJMP _0x20054
_0x20056:
	__GETB1MN _RS485,37
	SUBI R30,LOW(48)
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMWRB
; 0001 02C1                                        Refresh_Bit=1;
_0x20054:
	SBI  0x1E,3
; 0001 02C2                                       }
; 0001 02C3 
; 0001 02C4 
; 0001 02C5                                       ++RS485.Tx_Size; // ETX at End
_0x20051:
	__GETB1MN _RS485,2
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,2
; 0001 02C6                                    }
; 0001 02C7 
; 0001 02C8                                 for (Dat=0;Dat<8;Dat++) RS485.Rx_Buffer[Dat]=0; //Flush Rec Buffer
_0x2004A:
	LDI  R17,LOW(0)
_0x2005A:
	CPI  R17,8
	BRSH _0x2005B
	__POINTW2MN _RS485,35
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	ST   X,R30
	SUBI R17,-1
	RJMP _0x2005A
_0x2005B:
; 0001 02C9 };
_0x20049:
; 0001 02CA 
; 0001 02CB 
; 0001 02CC                         RS485.Tx_Buffer[0]=STX; //Begin
	LDI  R30,LOW(83)
	__PUTB1MN _RS485,3
; 0001 02CD                         RS485.Tx_Buffer[(RS485.Tx_Size-1)&0x1F]=ETX; //End
	__POINTW2MN _RS485,3
	__GETB1MN _RS485,2
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LDI  R30,LOW(69)
	ST   X,R30
; 0001 02CE                         RS485.Tx_Counter=0;
	LDI  R30,LOW(0)
	STS  _RS485,R30
; 0001 02CF                         RS485.Rx_Counter=0;
	__PUTB1MN _RS485,1
; 0001 02D0 
; 0001 02D1                         RS485.Rx_Counter=0; //Reset Rxd counter
	__PUTB1MN _RS485,1
; 0001 02D2                         RS485.Rx_Buffer[0]=0; //Flush Command Buffer
	__PUTB1MN _RS485,35
; 0001 02D3 
; 0001 02D4                         goto Exit_InT0;
	RJMP _0x2005C
; 0001 02D5                         }
; 0001 02D6                         else
_0x20047:
; 0001 02D7                             {
; 0001 02D8                             // ********************* Send Old Mode *********************
; 0001 02D9 
; 0001 02DA                             //Bin Number
; 0001 02DB                             for (Dat=0;Dat<5;Dat++)
	LDI  R17,LOW(0)
_0x2005F:
	CPI  R17,5
	BRSH _0x20060
; 0001 02DC                                 {
; 0001 02DD                                 if (Rx1_Buff[Dat]==0x37) Rx1_ID_Buff[Dat]=0x0A;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x37)
	BRNE _0x20061
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_ID_Buff)
	SBCI R31,HIGH(-_Rx1_ID_Buff)
	LDI  R26,LOW(10)
	STD  Z+0,R26
; 0001 02DE                                  else Rx1_ID_Buff[Dat]=Rx1_Buff[Dat];
	RJMP _0x20062
_0x20061:
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Rx1_ID_Buff)
	SBCI R27,HIGH(-_Rx1_ID_Buff)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LD   R30,Z
	ST   X,R30
; 0001 02DF                                 };
_0x20062:
	SUBI R17,-1
	RJMP _0x2005F
_0x20060:
; 0001 02E0 
; 0001 02E1 
; 0001 02E2                             //CBX Number
; 0001 02E3                             for (Dat=0;Dat<5;Dat++)
	LDI  R17,LOW(0)
_0x20064:
	CPI  R17,5
	BRSH _0x20065
; 0001 02E4                                 {
; 0001 02E5                                 if (Rx2_Buff[Dat]==0x37) Rx2_ID_Buff[Dat]=0x0A;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x37)
	BRNE _0x20066
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_ID_Buff)
	SBCI R31,HIGH(-_Rx2_ID_Buff)
	LDI  R26,LOW(10)
	STD  Z+0,R26
; 0001 02E6                                  else Rx2_ID_Buff[Dat]=Rx2_Buff[Dat];
	RJMP _0x20067
_0x20066:
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Rx2_ID_Buff)
	SBCI R27,HIGH(-_Rx2_ID_Buff)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	LD   R30,Z
	ST   X,R30
; 0001 02E7                                 }
_0x20067:
	SUBI R17,-1
	RJMP _0x20064
_0x20065:
; 0001 02E8 
; 0001 02E9 
; 0001 02EA                             // if data >3999
; 0001 02EB                             if ((!CBX1_Done_bit)&&((Rx1_ID_Buff[0]=!'0')||((Rx1_ID_Buff[1]&0x0F)>3)))
	SBIC 0x1E,4
	RJMP _0x20069
	LDI  R30,LOW(0)
	STS  _Rx1_ID_Buff,R30
	CPI  R30,0
	BRNE _0x2006A
	__GETB1MN _Rx1_ID_Buff,1
	ANDI R30,LOW(0xF)
	CPI  R30,LOW(0x4)
	BRLO _0x20069
_0x2006A:
	RJMP _0x2006C
_0x20069:
	RJMP _0x20068
_0x2006C:
; 0001 02EC                                 {
; 0001 02ED                                 Rx1_ID_Buff[1]='3';
	LDI  R30,LOW(51)
	__PUTB1MN _Rx1_ID_Buff,1
; 0001 02EE                                 Rx1_ID_Buff[2]='9';
	LDI  R30,LOW(57)
	__PUTB1MN _Rx1_ID_Buff,2
; 0001 02EF                                 Rx1_ID_Buff[3]='9';
	__PUTB1MN _Rx1_ID_Buff,3
; 0001 02F0                                 Rx1_ID_Buff[4]='9';
	__PUTB1MN _Rx1_ID_Buff,4
; 0001 02F1                                 };
_0x20068:
; 0001 02F2 
; 0001 02F3                             // if data >3999
; 0001 02F4                             if ((Rx2_ID_Buff[0]=!'0')||((Rx2_ID_Buff[1]&0x0F)>3)) goto Bad_Value;
	LDI  R30,LOW(0)
	STS  _Rx2_ID_Buff,R30
	CPI  R30,0
	BRNE _0x2006E
	__GETB1MN _Rx2_ID_Buff,1
	ANDI R30,LOW(0xF)
	CPI  R30,LOW(0x4)
	BRLO _0x2006D
_0x2006E:
	RJMP _0x20070
; 0001 02F5 
; 0001 02F6                             if (!CBX2_Done_bit)
_0x2006D:
	SBIC 0x1E,5
	RJMP _0x20071
; 0001 02F7                                 {
; 0001 02F8                                 if (!CbxPS_Sensor)
	SBIC 0x3,3
	RJMP _0x20072
; 0001 02F9                                     {
; 0001 02FA                                     Bad_Value:
_0x20070:
; 0001 02FB                                             Rx2_ID_Buff[1]='3';
	LDI  R30,LOW(51)
	__PUTB1MN _Rx2_ID_Buff,1
; 0001 02FC                                             Rx2_ID_Buff[2]='9';
	LDI  R30,LOW(57)
	__PUTB1MN _Rx2_ID_Buff,2
; 0001 02FD                                             Rx2_ID_Buff[3]='9';
	__PUTB1MN _Rx2_ID_Buff,3
; 0001 02FE                                             Rx2_ID_Buff[4]='9';
	__PUTB1MN _Rx2_ID_Buff,4
; 0001 02FF                                     };
_0x20072:
; 0001 0300                                 };
_0x20071:
; 0001 0301 
; 0001 0302                             CB_Tx_Counter1=8;
	LDI  R30,LOW(8)
	MOV  R10,R30
; 0001 0303                             CB_Tx_Counter2=8;
	MOV  R13,R30
; 0001 0304 
; 0001 0305                             goto Exit_InT0;
	RJMP _0x2005C
; 0001 0306                             };//End send Old Mode
; 0001 0307 
; 0001 0308 
; 0001 0309                     //Flush  Buffers CBX & BIN
; 0001 030A                     for (Dat=0;Dat<5;Dat++)
_0x20074:
	CPI  R17,5
	BRSH _0x20075
; 0001 030B                             {
; 0001 030C                             Rx1_Buff[Dat]='0';
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	LDI  R26,LOW(48)
	STD  Z+0,R26
; 0001 030D                             Rx2_Buff[Dat]='0';
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	STD  Z+0,R26
; 0001 030E                             };
	SUBI R17,-1
	RJMP _0x20074
_0x20075:
; 0001 030F                     }
; 0001 0310                             // *** Send New Mode Data to CBX Computer ***
; 0001 0311                      else  if (RS485.Tx_Counter<RS485.Tx_Size)
	RJMP _0x20076
_0x20035:
	__GETB1MN _RS485,2
	LDS  R26,_RS485
	CP   R26,R30
	BRSH _0x20077
; 0001 0312                                {
; 0001 0313                                 RS485_Dir=1;
	SBI  0x5,5
; 0001 0314                                 if (LINSIR&2) LINDAT=RS485.Tx_Buffer[RS485.Tx_Counter++];
	LDS  R30,201
	ANDI R30,LOW(0x2)
	BREQ _0x2007A
	__POINTW2MN _RS485,3
	LDS  R30,_RS485
	SUBI R30,-LOW(1)
	STS  _RS485,R30
	SUBI R30,LOW(1)
	LDI  R31,0
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	STS  210,R30
; 0001 0315                                 }
_0x2007A:
; 0001 0316                                     else
	RJMP _0x2007B
_0x20077:
; 0001 0317                                         {
; 0001 0318                                          if (LINSIR&2)
	LDS  R30,201
	ANDI R30,LOW(0x2)
	BREQ _0x2007C
; 0001 0319                                            {
; 0001 031A                                             RS485_Dir=0;
	CBI  0x5,5
; 0001 031B                                             if (((LINSIR&1))&&(RS485.Rx_Counter<32))
	LDS  R30,201
	ANDI R30,LOW(0x1)
	BREQ _0x20080
	__GETB2MN _RS485,1
	CPI  R26,LOW(0x20)
	BRLO _0x20081
_0x20080:
	RJMP _0x2007F
_0x20081:
; 0001 031C                                               {
; 0001 031D                                                Dat=LINDAT;
	LDS  R17,210
; 0001 031E                                                if ((Dat>=0x30)&&(Dat<=0x33)&&(RS485.Rx_Counter==0)) Status_Lights=Dat&0x03;
	CPI  R17,48
	BRLO _0x20083
	CPI  R17,52
	BRSH _0x20083
	__GETB2MN _RS485,1
	CPI  R26,LOW(0x0)
	BREQ _0x20084
_0x20083:
	RJMP _0x20082
_0x20084:
	MOV  R30,R17
	ANDI R30,LOW(0x3)
	STS  _Status_Lights,R30
; 0001 031F                                                  else RS485.Rx_Buffer[(RS485.Rx_Counter++)&0x1F]=Dat;
	RJMP _0x20085
_0x20082:
	__POINTW2MN _RS485,35
	__GETB1MN _RS485,1
	SUBI R30,-LOW(1)
	__PUTB1MN _RS485,1
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x1F)
	LDI  R31,0
	ADD  R30,R26
	ADC  R31,R27
	ST   Z,R17
; 0001 0320                                               }
_0x20085:
; 0001 0321                                            }
_0x2007F:
; 0001 0322                                         };//End Send New Mode Data Format
_0x2007C:
_0x2007B:
_0x20076:
; 0001 0323 
; 0001 0324         if (Send_Mode)
	SBIS 0x9,6
	RJMP _0x20086
; 0001 0325                 {
; 0001 0326 
; 0001 0327                 LINCR=0x00; //Disable Uart
	LDI  R30,LOW(0)
	STS  200,R30
; 0001 0328 
; 0001 0329                 if (CB_Tx_Counter1<255)   //Send CBX1 Old Mode
	LDI  R30,LOW(255)
	CP   R10,R30
	BRSH _0x20087
; 0001 032A                   {
; 0001 032B                     if (!(CB_Tx_Counter1&8))
	SBRC R10,3
	RJMP _0x20088
; 0001 032C                       {
; 0001 032D                        Dat=Rx2_ID_Buff[1+((CB_Tx_Counter1>>6)&3)];
	MOV  R30,R10
	SWAP R30
	ANDI R30,0xF
	LSR  R30
	LSR  R30
	ANDI R30,LOW(0x3)
	SUBI R30,-LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_ID_Buff)
	SBCI R31,HIGH(-_Rx2_ID_Buff)
	LD   R17,Z
; 0001 032E                        if (!((Dat<<((CB_Tx_Counter1>>4)&3))&8)) Out_ID=1;
	MOV  R30,R10
	SWAP R30
	ANDI R30,LOW(0x3)
	MOV  R26,R17
	CALL __LSLB12
	ANDI R30,LOW(0x8)
	BRNE _0x20089
	SBI  0xB,3
; 0001 032F                       }
_0x20089:
; 0001 0330                        else Out_ID=0;
	RJMP _0x2008C
_0x20088:
	CBI  0xB,3
; 0001 0331 
; 0001 0332                   CB_Tx_Counter1++;
_0x2008C:
	INC  R10
; 0001 0333                   }
; 0001 0334                    else Out_ID=0;
	RJMP _0x2008F
_0x20087:
	CBI  0xB,3
; 0001 0335 
; 0001 0336 
; 0001 0337 
; 0001 0338                 if (CB_Tx_Counter2<255)   //Send CBX2 Old Mode
_0x2008F:
	LDI  R30,LOW(255)
	CP   R13,R30
	BRSH _0x20092
; 0001 0339                   {
; 0001 033A                    if (!(CB_Tx_Counter2&8))
	SBRC R13,3
	RJMP _0x20093
; 0001 033B                      {
; 0001 033C                      Dat=Rx1_ID_Buff[1+((CB_Tx_Counter2>>6)&3)];
	MOV  R30,R13
	SWAP R30
	ANDI R30,0xF
	LSR  R30
	LSR  R30
	ANDI R30,LOW(0x3)
	SUBI R30,-LOW(1)
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_ID_Buff)
	SBCI R31,HIGH(-_Rx1_ID_Buff)
	LD   R17,Z
; 0001 033D                      if (!((Dat<<((CB_Tx_Counter2>>4)&3))&8)) Out_ID2=1;
	MOV  R30,R13
	SWAP R30
	ANDI R30,LOW(0x3)
	MOV  R26,R17
	CALL __LSLB12
	ANDI R30,LOW(0x8)
	BRNE _0x20094
	SBI  0x5,4
; 0001 033E                      }
_0x20094:
; 0001 033F                       else Out_ID2=0;
	RJMP _0x20097
_0x20093:
	CBI  0x5,4
; 0001 0340 
; 0001 0341                   CB_Tx_Counter2++;
_0x20097:
	INC  R13
; 0001 0342                   }
; 0001 0343                    else Out_ID2=0;
	RJMP _0x2009A
_0x20092:
	CBI  0x5,4
; 0001 0344                 }//End Send  Old Mode Data Format
_0x2009A:
; 0001 0345 
; 0001 0346 
; 0001 0347 		 if (!Timer.Time_P--)
_0x20086:
	LDS  R30,_Timer
	SUBI R30,LOW(1)
	STS  _Timer,R30
	SUBI R30,-LOW(1)
	BREQ PC+3
	JMP _0x2009D
; 0001 0348                 {
; 0001 0349                 Timer.Time_P=239;     //50ms
	LDI  R30,LOW(239)
	STS  _Timer,R30
; 0001 034A 
; 0001 034B                 //Sys LED Blink
; 0001 034C                 if (++Led_Blink&0x08) Led_Sys=0;
	LDS  R26,_Led_Blink
	SUBI R26,-LOW(1)
	STS  _Led_Blink,R26
	ANDI R26,LOW(0x8)
	BREQ _0x2009E
	CBI  0x8,6
; 0001 034D                  else Led_Sys=1;
	RJMP _0x200A1
_0x2009E:
	SBI  0x8,6
; 0001 034E 
; 0001 034F 
; 0001 0350                 //***** Vault Holding Timer *********
; 0001 0351                 if (Timer.Time>30) Timer.Time--;
_0x200A1:
	__GETW2MN _Timer,1
	SBIW R26,31
	BRLO _0x200A4
	__POINTW2MN _Timer,1
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0001 0352                  else
	RJMP _0x200A5
_0x200A4:
; 0001 0353                     {
; 0001 0354                      if (Timer.Time>1)
	__GETW2MN _Timer,1
	SBIW R26,2
	BRLO _0x200A6
; 0001 0355                         {
; 0001 0356                         Timer.Time--;
	__POINTW2MN _Timer,1
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0001 0357                         if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Out_Valve=1;      //Solenoid On
	SBIS 0x6,0
	RJMP _0x200A8
	SBIC 0x3,3
	RJMP _0x200A7
_0x200A8:
	SBI  0x8,1
; 0001 0358                           else Out_Valve=0; //Solenoid Off
	RJMP _0x200AC
_0x200A7:
	CBI  0x8,1
; 0001 0359                         }
_0x200AC:
; 0001 035A                         else
	RJMP _0x200AF
_0x200A6:
; 0001 035B                             {
; 0001 035C                               Out_Valve=0; //Solenoid Off  , Time Out
	CBI  0x8,1
; 0001 035D                               if ((!RecPS_Sensor)||(!CbxPS_Sensor))
	SBIS 0x6,0
	RJMP _0x200B3
	SBIC 0x3,3
	RJMP _0x200B2
_0x200B3:
; 0001 035E                                 {
; 0001 035F                                  if (++Solenoid_Td>=30)
	LDI  R26,LOW(_Solenoid_Td)
	LDI  R27,HIGH(_Solenoid_Td)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,30
	BRLO _0x200B5
; 0001 0360                                     {
; 0001 0361                                      Timer.Time=Solenoid_Time;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	__POINTW2MN _Timer,1
	LDI  R31,0
	ST   X+,R30
	ST   X,R31
; 0001 0362                                      Timer.Time*=20;
	__GETW2MN _Timer,1
	LDI  R30,LOW(20)
	CALL __MULB1W2U
	__PUTW1MN _Timer,1
; 0001 0363                                      }
; 0001 0364                                 }
_0x200B5:
; 0001 0365                                   else {
	RJMP _0x200B6
_0x200B2:
; 0001 0366                                        Solenoid_Td=0;
	LDI  R30,LOW(0)
	STS  _Solenoid_Td,R30
	STS  _Solenoid_Td+1,R30
; 0001 0367                                        Timer.Time=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timer,1
; 0001 0368                                        };
_0x200B6:
; 0001 0369                             }
_0x200AF:
; 0001 036A 
; 0001 036B                     }
_0x200A5:
; 0001 036C                 }
; 0001 036D 
; 0001 036E Exit_InT0:
_0x2009D:
_0x2005C:
; 0001 036F 
; 0001 0370   if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Power_Light=0; //Power Up Led Light
	SBIS 0x6,0
	RJMP _0x200B8
	SBIC 0x3,3
	RJMP _0x200B7
_0x200B8:
	CBI  0x7,7
; 0001 0371    else Power_Light=1;
	RJMP _0x200BC
_0x200B7:
	SBI  0x7,7
; 0001 0372 
; 0001 0373   #asm("cli");
_0x200BC:
	cli
; 0001 0374 
; 0001 0375 #pragma optsize+
; 0001 0376 };
	CALL __LOADLOCR6
	ADIW R28,6
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R22,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;
;
;
;
;
;
;#include "look_up_tbl.h"
;
;#pragma optsize-
;//***********************  Timer1 output compare A interrupt service routine ***********************
;interrupt [TIM1_COMPA] void timer1_compa_isr(void)
; 0001 0383 {
_timer1_compa_isr:
	ST   -Y,R26
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0001 0384 
; 0001 0385 unsigned char Tmp_;
; 0001 0386 
; 0001 0387 
; 0001 0388 
; 0001 0389   //******** Software Trasmit for Debuging TxD ***************
; 0001 038A 
; 0001 038B                     #asm
	ST   -Y,R17
;	Tmp_ -> R17
; 0001 038C             ;if (++Tx1_Counter<72)
            ;if (++Tx1_Counter<72)
; 0001 038D                     LDS     R31,_Tx1_Counter
                    LDS     R31,_Tx1_Counter
; 0001 038E                     INC     R31
                    INC     R31
; 0001 038F                     CPI     R31,72
                    CPI     R31,72
; 0001 0390                     BRCC    Exit_Else_Tx1
                    BRCC    Exit_Else_Tx1
; 0001 0391 

; 0001 0392             ;if (Tx1_Counter<8) _Soft_TxD=0;
            ;if (Tx1_Counter<8) _Soft_TxD=0;
; 0001 0393                     CPI     R31,8
                    CPI     R31,8
; 0001 0394                     BRCC    Else_Tx1
                    BRCC    Else_Tx1
; 0001 0395                     CBI     _PORTC,_Soft_TxD      ;_Soft_TxD=0
                    CBI     _PORTC,_Soft_TxD      ;_Soft_TxD=0
; 0001 0396                     RJMP    Exit_Tx1
                    RJMP    Exit_Tx1
; 0001 0397 

; 0001 0398             ;if (!(Tx1_Counter&7))
            ;if (!(Tx1_Counter&7))
; 0001 0399 Else_Tx1:           MOV     R30,R31
Else_Tx1:           MOV     R30,R31
; 0001 039A                     ANDI    R30,0x07
                    ANDI    R30,0x07
; 0001 039B                     BRNE    Exit_Tx1
                    BRNE    Exit_Tx1
; 0001 039C 

; 0001 039D             ;if (TxD_1&0x80) _TxD_Dbg=0;
            ;if (TxD_1&0x80) _TxD_Dbg=0;
; 0001 039E                     LDS     R30,_TxD_1
                    LDS     R30,_TxD_1
; 0001 039F 

; 0001 03A0                     SBRS    R30,0
                    SBRS    R30,0
; 0001 03A1                     CBI     _PORTC,_Soft_TxD     ;_Soft_TxD=0
                    CBI     _PORTC,_Soft_TxD     ;_Soft_TxD=0
; 0001 03A2                     SBRC    R30,0
                    SBRC    R30,0
; 0001 03A3 

; 0001 03A4             ;else
            ;else
; 0001 03A5                     SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
                    SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
; 0001 03A6                     LSR     R30                   ;TxD_1>>=1;
                    LSR     R30                   ;TxD_1>>=1;
; 0001 03A7                     STS     _TxD_1,R30
                    STS     _TxD_1,R30
; 0001 03A8                     RJMP    Exit_Tx1
                    RJMP    Exit_Tx1
; 0001 03A9 

; 0001 03AA Exit_Else_Tx1:      SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
Exit_Else_Tx1:      SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
; 0001 03AB                     CPI     R31,80
                    CPI     R31,80
; 0001 03AC                     BRCS    Exit_Tx1
                    BRCS    Exit_Tx1
; 0001 03AD                     LDI     R31,80
                    LDI     R31,80
; 0001 03AE 

; 0001 03AF Exit_Tx1:           STS     _Tx1_Counter,R31
Exit_Tx1:           STS     _Tx1_Counter,R31
; 0001 03B0                     #endasm
; 0001 03B1 
; 0001 03B2 
; 0001 03B3 
; 0001 03B4 //if(!Mode_1)
; 0001 03B5            {
; 0001 03B6             if (++Rx1_Counter<78) //Software RxD_1  for CBX_1
	INC  R3
	LDI  R30,LOW(78)
	CP   R3,R30
	BRSH _0x200BF
; 0001 03B7                     {
; 0001 03B8                     if (!In_1) Tmp_Rx1&=Bit_Table[Rx1_Counter];
	SBIC 0x3,6
	RJMP _0x200C0
	MOV  R30,R3
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	AND  R5,R30
; 0001 03B9                     }
_0x200C0:
; 0001 03BA                     else
	RJMP _0x200C1
_0x200BF:
; 0001 03BB                         {
; 0001 03BC                          if (Rx1_Counter==78)
	LDI  R30,LOW(78)
	CP   R30,R3
	BRNE _0x200C2
; 0001 03BD                             {
; 0001 03BE                             if (!Tmp_Rx1) Mode_1=1;
	TST  R5
	BRNE _0x200C3
	SBI  0x1E,0
; 0001 03BF                             if (!Mode_1) Rx1_Buff[(Rx1_Pointer_Buff++)&0x07]=Tmp_Rx1;
_0x200C3:
	SBIC 0x1E,0
	RJMP _0x200C6
	MOV  R30,R2
	INC  R2
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx1_Buff)
	SBCI R31,HIGH(-_Rx1_Buff)
	ST   Z,R5
; 0001 03C0                             Time_Out_1=0;
_0x200C6:
	CLR  R4
; 0001 03C1                             CBX1_Done_bit=1;
	SBI  0x1E,4
; 0001 03C2                             }
; 0001 03C3                             else
	RJMP _0x200C9
_0x200C2:
; 0001 03C4                                 {
; 0001 03C5                                 Rx1_Counter=78;
	LDI  R30,LOW(78)
	MOV  R3,R30
; 0001 03C6                                 Tmp_Rx1=0xFF;
	LDI  R30,LOW(255)
	MOV  R5,R30
; 0001 03C7                                 if (In_1==0) Rx1_Counter=0;
	SBIS 0x3,6
	CLR  R3
; 0001 03C8                                 };
_0x200C9:
; 0001 03C9                         };
_0x200C1:
; 0001 03CA             }
; 0001 03CB //             else
; 0001 03CC                  {
; 0001 03CD                   Tmp_=In_1;
	LDI  R30,0
	SBIC 0x3,6
	LDI  R30,1
	MOV  R17,R30
; 0001 03CE                   if ((Tmp_)&&(!Old_CBX1)) {
	CPI  R17,0
	BREQ _0x200CC
	SBIS 0x1E,7
	RJMP _0x200CD
_0x200CC:
	RJMP _0x200CB
_0x200CD:
; 0001 03CF                                               CBX1_bit=1;
	SBI  0x1E,6
; 0001 03D0                                               if (CB1_Time>=250) CB1_Time=0;
	LDI  R30,LOW(250)
	CP   R12,R30
	BRLO _0x200D0
	CLR  R12
; 0001 03D1                                               }
_0x200D0:
; 0001 03D2                   Old_CBX1=Tmp_;
_0x200CB:
	CPI  R17,0
	BRNE _0x200D1
	CBI  0x1E,7
	RJMP _0x200D2
_0x200D1:
	SBI  0x1E,7
_0x200D2:
; 0001 03D3                  };
; 0001 03D4 
; 0001 03D5 
; 0001 03D6 
; 0001 03D7 //if(!Mode_2)
; 0001 03D8           {
; 0001 03D9             if (++Rx2_Counter<78) //Software RxD_2  for CBX_2
	INC  R7
	LDI  R30,LOW(78)
	CP   R7,R30
	BRSH _0x200D3
; 0001 03DA                     {
; 0001 03DB                     if (!In_2) Tmp_Rx2=Tmp_Rx2&Bit_Table[Rx2_Counter];
	SBIC 0x6,7
	RJMP _0x200D4
	MOV  R30,R7
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	AND  R9,R30
; 0001 03DC                     }
_0x200D4:
; 0001 03DD                     else
	RJMP _0x200D5
_0x200D3:
; 0001 03DE                         {
; 0001 03DF                          if (Rx2_Counter==78)
	LDI  R30,LOW(78)
	CP   R30,R7
	BRNE _0x200D6
; 0001 03E0                             {
; 0001 03E1                             if (!Tmp_Rx2) Mode_2=1;
	TST  R9
	BRNE _0x200D7
	SBI  0x1E,1
; 0001 03E2                             if (!Mode_2) Rx2_Buff[(Rx2_Pointer_Buff++)&0x07]=Tmp_Rx2;
_0x200D7:
	SBIC 0x1E,1
	RJMP _0x200DA
	MOV  R30,R6
	INC  R6
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx2_Buff)
	SBCI R31,HIGH(-_Rx2_Buff)
	ST   Z,R9
; 0001 03E3                              Time_Out_2=0;
_0x200DA:
	CLR  R8
; 0001 03E4                              CBX2_Done_bit=1;
	SBI  0x1E,5
; 0001 03E5                              }
; 0001 03E6                              else
	RJMP _0x200DD
_0x200D6:
; 0001 03E7                                 {
; 0001 03E8                                 Rx2_Counter=78;
	LDI  R30,LOW(78)
	MOV  R7,R30
; 0001 03E9                                 Tmp_Rx2=0xFF;
	LDI  R30,LOW(255)
	MOV  R9,R30
; 0001 03EA                                 if (!In_2) Rx2_Counter=0;
	SBIS 0x6,7
	CLR  R7
; 0001 03EB                                 };
_0x200DD:
; 0001 03EC                         }
_0x200D5:
; 0001 03ED             }
; 0001 03EE   //            else
; 0001 03EF                   {
; 0001 03F0                    Tmp_=In_2;
	LDI  R30,0
	SBIC 0x6,7
	LDI  R30,1
	MOV  R17,R30
; 0001 03F1                    if ((Tmp_)&&(!Old_CBX2))
	CPI  R17,0
	BREQ _0x200E0
	SBIS 0x19,1
	RJMP _0x200E1
_0x200E0:
	RJMP _0x200DF
_0x200E1:
; 0001 03F2                                            {
; 0001 03F3                                             CBX2_bit=1;
	SBI  0x19,0
; 0001 03F4                                             if (CB2_Time>=250) CB2_Time=0;
	LDS  R26,_CB2_Time
	CPI  R26,LOW(0xFA)
	BRLO _0x200E4
	LDI  R30,LOW(0)
	STS  _CB2_Time,R30
; 0001 03F5                                             }
_0x200E4:
; 0001 03F6                     Old_CBX2=Tmp_;
_0x200DF:
	CPI  R17,0
	BRNE _0x200E5
	CBI  0x19,1
	RJMP _0x200E6
_0x200E5:
	SBI  0x19,1
_0x200E6:
; 0001 03F7                    };
; 0001 03F8 
; 0001 03F9 
; 0001 03FA 
; 0001 03FB  if (++Rx3_Counter<78)   //Software Receive for Debuging RxD
	LDS  R26,_Rx3_Counter
	SUBI R26,-LOW(1)
	STS  _Rx3_Counter,R26
	CPI  R26,LOW(0x4E)
	BRSH _0x200E7
; 0001 03FC                     {
; 0001 03FD                     if (!In_3) Tmp_Rx3=Tmp_Rx3&Bit_Table[Rx3_Counter];
	SBIC 0x6,3
	RJMP _0x200E8
	LDS  R30,_Rx3_Counter
	LDI  R31,0
	SUBI R30,LOW(-_Bit_Table*2)
	SBCI R31,HIGH(-_Bit_Table*2)
	LPM  R30,Z
	LDS  R26,_Tmp_Rx3
	AND  R30,R26
	STS  _Tmp_Rx3,R30
; 0001 03FE                     }
_0x200E8:
; 0001 03FF                     else
	RJMP _0x200E9
_0x200E7:
; 0001 0400                         {
; 0001 0401                          if (Rx3_Counter==78) Rx3_Buff[(Rx3_Pointer_Buff++)&0x07]=Tmp_Rx3;
	LDS  R26,_Rx3_Counter
	CPI  R26,LOW(0x4E)
	BRNE _0x200EA
	LDS  R26,_Rx3_Pointer_Buff
	SUBI R26,-LOW(1)
	STS  _Rx3_Pointer_Buff,R26
	SUBI R26,LOW(1)
	LDI  R30,LOW(7)
	AND  R30,R26
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDS  R26,_Tmp_Rx3
	STD  Z+0,R26
; 0001 0402                              else
	RJMP _0x200EB
_0x200EA:
; 0001 0403                                 {
; 0001 0404                                 Rx3_Counter=78;
	LDI  R30,LOW(78)
	STS  _Rx3_Counter,R30
; 0001 0405                                 Tmp_Rx3=0xFF;
	LDI  R30,LOW(255)
	STS  _Tmp_Rx3,R30
; 0001 0406                                 if (!In_3) Rx3_Counter=0;
	SBIC 0x6,3
	RJMP _0x200EC
	LDI  R30,LOW(0)
	STS  _Rx3_Counter,R30
; 0001 0407                                 };
_0x200EC:
_0x200EB:
; 0001 0408                         };
_0x200E9:
; 0001 0409 
; 0001 040A #pragma optsize+
; 0001 040B };
	LD   R17,Y+
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R26,Y+
	RETI
;
;
;
;
;#define ADC_VREF_TYPE 0x00
;// Read the AD conversion result
;
;unsigned int read_adc(unsigned char adc_input)
; 0001 0414 {
_read_adc:
; 0001 0415 ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
	ST   -Y,R26
;	adc_input -> Y+0
	LD   R30,Y
	STS  124,R30
; 0001 0416 // Delay needed for the stabilization of the ADC input voltage
; 0001 0417 delay_us(10);
	__DELAY_USB 49
; 0001 0418 // Start the AD conversion
; 0001 0419 ADCSRA|=0x40;
	LDS  R30,122
	ORI  R30,0x40
	STS  122,R30
; 0001 041A // Wait for the AD conversion to complete
; 0001 041B while ((ADCSRA & 0x10)==0);
_0x200ED:
	LDS  R30,122
	ANDI R30,LOW(0x10)
	BREQ _0x200ED
; 0001 041C ADCSRA|=0x10;
	LDS  R30,122
	ORI  R30,0x10
	STS  122,R30
; 0001 041D return ADCW;
	LDS  R30,120
	LDS  R31,120+1
_0x2060002:
	ADIW R28,1
	RET
; 0001 041E }
;
;
;
;//*************************************** MAIN ***********************************************
;
;void main(void)
; 0001 0425 {
_main:
; 0001 0426 
; 0001 0427 #pragma optsize+
; 0001 0428 
; 0001 0429 unsigned char Temp_Byte,Temp_Byte2;
; 0001 042A 
; 0001 042B #ifdef _Test_
; 0001 042C unsigned char Cmp1,Cmp2,Err1,Err2;
; 0001 042D #endif
; 0001 042E 
; 0001 042F 
; 0001 0430 
; 0001 0431 // Crystal Oscillator division factor: 1
; 0001 0432 #pragma optsize-
; 0001 0433 CLKPR=0x80;
;	Temp_Byte -> R17
;	Temp_Byte2 -> R16
	LDI  R30,LOW(128)
	STS  97,R30
; 0001 0434 CLKPR=0x00;
	LDI  R30,LOW(0)
	STS  97,R30
; 0001 0435 #ifdef _OPTIMIZE_SIZE_
; 0001 0436 #pragma optsize+
; 0001 0437 #endif
; 0001 0438 
; 0001 0439 // Input/Output Ports initialization
; 0001 043A // Port B initialization
; 0001 043B // Func7=In Func6=In Func5=Out Func4=Out Func3=In Func2=In Func1=Out Func0=Out
; 0001 043C // State7=P State6=P State5=1 State4=1 State3=P State2=P State1=1 State0=0
; 0001 043D PORTB=0xFE;
	LDI  R30,LOW(254)
	OUT  0x5,R30
; 0001 043E DDRB=0x33;
	LDI  R30,LOW(51)
	OUT  0x4,R30
; 0001 043F 
; 0001 0440 // Port C initialization
; 0001 0441 // Func7=In Func6=Out Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=In
; 0001 0442 // State7=T State6=0 State5=T State4=T State3=P State2=1 State1=0 State0=P
; 0001 0443 PORTC=0x0D;
	LDI  R30,LOW(13)
	OUT  0x8,R30
; 0001 0444 DDRC=0x46;
	LDI  R30,LOW(70)
	OUT  0x7,R30
; 0001 0445 
; 0001 0446 // Port D initialization
; 0001 0447 // Func7=Out Func6=In Func5=Out Func4=In Func3=Out Func2=Out Func1=Out Func0=Out
; 0001 0448 // State7=T State6=P State5=1 State4=P State3=1 State2=0 State1=0 State0=1
; 0001 0449 PORTD=0x79;
	LDI  R30,LOW(121)
	OUT  0xB,R30
; 0001 044A DDRD=0xAF;
	LDI  R30,LOW(175)
	OUT  0xA,R30
; 0001 044B 
; 0001 044C // Port E initialization
; 0001 044D // Func2=In Func1=In Func0=In
; 0001 044E // State2=T State1=T State0=T
; 0001 044F PORTE=0x00;
	LDI  R30,LOW(0)
	OUT  0xE,R30
; 0001 0450 DDRE=0x00;
	OUT  0xD,R30
; 0001 0451 
; 0001 0452 // Timer/Counter 0 initialization
; 0001 0453 // Clock source: System Clock
; 0001 0454 // Clock value: 230.400 kHz
; 0001 0455 // Mode: CTC top=OCR0A
; 0001 0456 // OC0A output: Disconnected
; 0001 0457 // OC0B output: Disconnected
; 0001 0458 TCCR0A=0x02;
	LDI  R30,LOW(2)
	OUT  0x24,R30
; 0001 0459 TCCR0B=0x03;
	LDI  R30,LOW(3)
	OUT  0x25,R30
; 0001 045A TCNT0=0x00;
	LDI  R30,LOW(0)
	OUT  0x26,R30
; 0001 045B OCR0A=0x2F;
	LDI  R30,LOW(47)
	OUT  0x27,R30
; 0001 045C OCR0B=0x00;
	LDI  R30,LOW(0)
	OUT  0x28,R30
; 0001 045D 
; 0001 045E // Timer/Counter 1 initialization
; 0001 045F // Clock source: System Clock
; 0001 0460 // Clock value: 14745.600 kHz
; 0001 0461 // Mode: CTC top=OCR1A
; 0001 0462 // OC1A output: Discon.
; 0001 0463 // OC1B output: Discon.
; 0001 0464 // Noise Canceler: Off
; 0001 0465 // Input Capture on Falling Edge
; 0001 0466 // Timer1 Overflow Interrupt: Off
; 0001 0467 // Input Capture Interrupt: Off
; 0001 0468 // Compare A Match Interrupt: On
; 0001 0469 // Compare B Match Interrupt: Off
; 0001 046A TCCR1A=0x00;
	STS  128,R30
; 0001 046B TCCR1B=0x09;
	LDI  R30,LOW(9)
	STS  129,R30
; 0001 046C TCNT1H=0x00;
	LDI  R30,LOW(0)
	STS  133,R30
; 0001 046D TCNT1L=0x00;
	STS  132,R30
; 0001 046E ICR1H=0x00;
	STS  135,R30
; 0001 046F ICR1L=0x00;
	STS  134,R30
; 0001 0470 OCR1AH=0x00;
	STS  137,R30
; 0001 0471 OCR1AL=0xBF;
	LDI  R30,LOW(191)
	STS  136,R30
; 0001 0472 OCR1BH=0x00;
	LDI  R30,LOW(0)
	STS  139,R30
; 0001 0473 OCR1BL=0x00;
	STS  138,R30
; 0001 0474 
; 0001 0475 
; 0001 0476 // External Interrupt(s) initialization
; 0001 0477 // INT0: Off
; 0001 0478 // INT1: Off
; 0001 0479 // INT2: Off
; 0001 047A MCUCR=0x00;
	OUT  0x35,R30
; 0001 047B MCUSR=0x00;
	OUT  0x34,R30
; 0001 047C 
; 0001 047D 
; 0001 047E 
; 0001 047F // Timer(s)/Counter(s) Interrupt(s) initialization
; 0001 0480 // Timer/Counter 0 Interrupt(s) initialization
; 0001 0481 TIMSK0=0x02;
	LDI  R30,LOW(2)
	STS  110,R30
; 0001 0482 TIMSK1=0x02;
	STS  111,R30
; 0001 0483 
; 0001 0484 
; 0001 0485 // CAN Controller initialization
; 0001 0486 // CAN disabled
; 0001 0487 CANGCON=0x00;
	LDI  R30,LOW(0)
	STS  216,R30
; 0001 0488 
; 0001 0489 
; 0001 048A 
; 0001 048B // ADC initialization
; 0001 048C // ADC Clock frequency: 921.600 kHz
; 0001 048D // ADC Bandgap Voltage Reference: Off
; 0001 048E // ADC High Speed Mode: Off
; 0001 048F // ADC Auto Trigger Source: ADC Stopped
; 0001 0490 // Digital input buffers on ADC0: On, ADC1: On, ADC2: On, ADC3: On
; 0001 0491 // ADC4: On, ADC5: On, ADC6: On, ADC7: On
; 0001 0492 DIDR0=0x00;
	STS  126,R30
; 0001 0493 ADMUX=ADC_VREF_TYPE & 0xff;
	STS  124,R30
; 0001 0494 ADCSRA=0x84;
	LDI  R30,LOW(132)
	STS  122,R30
; 0001 0495 ADCSRB=0x20;
	LDI  R30,LOW(32)
	STS  123,R30
; 0001 0496 
; 0001 0497 
; 0001 0498 
; 0001 0499 // SPI initialization
; 0001 049A // SPI disabled
; 0001 049B SPCR=0x00;
	LDI  R30,LOW(0)
	OUT  0x2C,R30
; 0001 049C 
; 0001 049D 
; 0001 049E // Watchdog Timer initialization
; 0001 049F // Watchdog Timer Prescaler: OSC/512k
; 0001 04A0 // Watchdog Timer interrupt: Off
; 0001 04A1 #asm("wdr")
	wdr
; 0001 04A2 WDTCSR=0x38;
	LDI  R30,LOW(56)
	STS  96,R30
; 0001 04A3 WDTCSR=0x28;
	LDI  R30,LOW(40)
	STS  96,R30
; 0001 04A4 
; 0001 04A5 
; 0001 04A6 RS485.Tx_Size=20;
	LDI  R30,LOW(20)
	__PUTB1MN _RS485,2
; 0001 04A7 
; 0001 04A8 
; 0001 04A9 Reset:
_0x200F0:
; 0001 04AA 
; 0001 04AB Refresh_Bit=0;
	CBI  0x1E,3
; 0001 04AC 
; 0001 04AD if (Send_Mode) LINCR=0x00; //Disable Uart
	SBIS 0x9,6
	RJMP _0x200F3
	LDI  R30,LOW(0)
	RJMP _0x20159
; 0001 04AE  else LINCR=0x0F; // Enable Uart
_0x200F3:
	LDI  R30,LOW(15)
_0x20159:
	STS  200,R30
; 0001 04AF     {
; 0001 04B0     // UART initialization
; 0001 04B1     // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0001 04B2     // UART Receiver: On
; 0001 04B3     // UART Transmitter: On
; 0001 04B4     // UART Baud Rate: 9600
; 0001 04B5     LINCR=0x0F;
	LDI  R30,LOW(15)
	STS  200,R30
; 0001 04B6     LINBTR=0x18;
	LDI  R30,LOW(24)
	STS  204,R30
; 0001 04B7     LINBRRH=0x00;
	LDI  R30,LOW(0)
	STS  206,R30
; 0001 04B8     LINBRRL=47;
	LDI  R30,LOW(47)
	STS  205,R30
; 0001 04B9     LINDAT=0xFF;
	LDI  R30,LOW(255)
	STS  210,R30
; 0001 04BA     }
; 0001 04BB 
; 0001 04BC 
; 0001 04BD Timer.Time=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	__PUTW1MN _Timer,1
; 0001 04BE 
; 0001 04BF #asm("sei") // Global enable interrupts
	sei
; 0001 04C0 
; 0001 04C1 #ifndef _Test_
; 0001 04C2 Sinc_Bit=0;
	CBI  0x1E,2
; 0001 04C3 Timer_Sinc=0;
	LDI  R30,LOW(0)
	STS  _Timer_Sinc,R30
	STS  _Timer_Sinc+1,R30
; 0001 04C4 #endif
; 0001 04C5 
; 0001 04C6 ClrScr();
	CALL _ClrScr
; 0001 04C7 
; 0001 04C8 Cursor_Hide();
	CALL _Cursor_Hide
; 0001 04C9 
; 0001 04CA Print(" Vault Debug Menu V1.00 ",31,0,7);
	__POINTW1FN _0x20000,0
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(31)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04CB 
; 0001 04CC 
; 0001 04CD for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
	LDI  R17,LOW(0)
_0x200F8:
	CPI  R17,8
	BRSH _0x200F9
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDI  R26,LOW(0)
	STD  Z+0,R26
	SUBI R17,-1
	RJMP _0x200F8
_0x200F9:
; 0001 04CE Print_Serial_No(20);
	LDI  R26,LOW(20)
	RCALL _Print_Serial_No
; 0001 04CF 
; 0001 04D0 Print("; Firmware Version:",24,20,1);
	__POINTW1FN _0x20000,25
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(24)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04D1 Debug_Putchar(Firmware_Ver[0]);
	LDI  R30,LOW(_Firmware_Ver*2)
	LDI  R31,HIGH(_Firmware_Ver*2)
	LPM  R26,Z
	CALL _Debug_Putchar
; 0001 04D2 Debug_Putchar(Firmware_Ver[1]);
	__POINTW1FN _Firmware_Ver,1
	LPM  R26,Z
	CALL _Debug_Putchar
; 0001 04D3 Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 04D4 Debug_Putchar(Firmware_Ver[2]);
	__POINTW1FN _Firmware_Ver,2
	LPM  R26,Z
	CALL _Debug_Putchar
; 0001 04D5 Debug_Putchar(Firmware_Ver[3]);
	__POINTW1FN _Firmware_Ver,3
	LPM  R26,Z
	CALL _Debug_Putchar
; 0001 04D6 
; 0001 04D7 
; 0001 04D8 
; 0001 04D9 Print("; ",49,20,1);
	__POINTW1FN _0x20000,45
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(49)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04DA Print("; ",66,20,1);
	__POINTW1FN _0x20000,45
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(66)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04DB 
; 0001 04DC Print(" INPUTS:",1,16,7);
	__POINTW1FN _0x20000,48
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04DD Print("CBX Sensor=",10,16,1);
	__POINTW1FN _0x20000,57
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04DE Print("; Position Sensor=",23,16,1);
	__POINTW1FN _0x20000,69
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04DF Print("; Motor Lock Sensor=",43,16,1);
	__POINTW1FN _0x20000,88
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(43)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E0 Print("; Motor Fault=",65,16,1);
	__POINTW1FN _0x20000,109
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(65)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E1 
; 0001 04E2 
; 0001 04E3 Print("OUTPUTS:",1,14,7);
	__POINTW1FN _0x20000,124
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 04E4 Print("[S]olenoid=",10,14,1);
	__POINTW1FN _0x20000,133
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E5 Print("; Lamp[1]=",23,14,1);
	__POINTW1FN _0x20000,145
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E6 Print("; Lamp[2]=",35,14,1);
	__POINTW1FN _0x20000,156
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(35)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E7 Print("; Motor [P]hase=",47,14,1);
	__POINTW1FN _0x20000,167
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(47)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E8 Print("; Motor [R]un=",65,14,1);
	__POINTW1FN _0x20000,184
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(65)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04E9 
; 0001 04EA 
; 0001 04EB Print("Vault Holding Time=",1,2,1);
	__POINTW1FN _0x20000,199
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(2)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 04EC Temp_Byte=0x30+Solenoid_Time/10;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 04ED Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04EE Calculated.Th[0]=Temp_Byte;
	__PUTBMRN _Calculated,6,17
; 0001 04EF Temp_Byte=0x30+Solenoid_Time%10;
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 04F0 Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 04F1 Calculated.Th[1]=Temp_Byte;
	__PUTBMRN _Calculated,7,17
; 0001 04F2 Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 04F3 Debug_Putchar('s');
	LDI  R26,LOW(115)
	CALL _Debug_Putchar
; 0001 04F4 Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 04F5 
; 0001 04F6 
; 0001 04F7 Key_Press=Rx3_Pointer_Buff;
	LDS  R11,_Rx3_Pointer_Buff
; 0001 04F8 
; 0001 04F9 while (1)
_0x200FA:
; 0001 04FA       {
; 0001 04FB        #asm("wdr");
	wdr
; 0001 04FC 
; 0001 04FD 
; 0001 04FE        Out_Lig1=Status_Lights&1;
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x1)
	BRNE _0x200FD
	CBI  0xB,7
	RJMP _0x200FE
_0x200FD:
	SBI  0xB,7
_0x200FE:
; 0001 04FF        Out_Lig2=(Status_Lights>>1)&1;
	LDS  R30,_Status_Lights
	LSR  R30
	ANDI R30,LOW(0x1)
	BRNE _0x200FF
	CBI  0xB,2
	RJMP _0x20100
_0x200FF:
	SBI  0xB,2
_0x20100:
; 0001 0500 
; 0001 0501 
; 0001 0502        if (Key_Press!=Rx3_Pointer_Buff)
	LDS  R30,_Rx3_Pointer_Buff
	CP   R30,R11
	BRNE PC+3
	JMP _0x20101
; 0001 0503             {
; 0001 0504             Temp_Byte=Rx3_Buff[(Rx3_Pointer_Buff-1)&0x07];
	SUBI R30,LOW(1)
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R17,Z
; 0001 0505 
; 0001 0506             Key_Press=Rx3_Pointer_Buff;
	LDS  R11,_Rx3_Pointer_Buff
; 0001 0507 
; 0001 0508             if (Temp_Byte=='1') //On/Off Lamp 1
	CPI  R17,49
	BRNE _0x20102
; 0001 0509                 {
; 0001 050A                 Status_Lights=(Status_Lights&2)|(!Status_Lights&1)&0x01;
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x2)
	MOV  R26,R30
	LDS  R30,_Status_Lights
	CALL __LNEGB1
	ANDI R30,LOW(0x1)
	OR   R30,R26
	STS  _Status_Lights,R30
; 0001 050B                 Out_Lig1=Status_Lights&1;
	ANDI R30,LOW(0x1)
	BRNE _0x20103
	CBI  0xB,7
	RJMP _0x20104
_0x20103:
	SBI  0xB,7
_0x20104:
; 0001 050C                 Print("",33,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(33)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 050D                 Debug_Putchar(0x30+Out_Lig1);
	LDI  R30,0
	SBIC 0xB,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 050E                 }
; 0001 050F 
; 0001 0510             if (Temp_Byte=='2') //On/Off Lamp 2
_0x20102:
	CPI  R17,50
	BRNE _0x20105
; 0001 0511                 {
; 0001 0512                 if ((Status_Lights&2)==2) Status_Lights&=1;
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x2)
	CPI  R30,LOW(0x2)
	BRNE _0x20106
	LDS  R30,_Status_Lights
	ANDI R30,LOW(0x1)
	RJMP _0x2015A
; 0001 0513                  else Status_Lights|=2;
_0x20106:
	LDS  R30,_Status_Lights
	ORI  R30,2
_0x2015A:
	STS  _Status_Lights,R30
; 0001 0514                 Out_Lig2=(Status_Lights>>1)&1;
	LSR  R30
	ANDI R30,LOW(0x1)
	BRNE _0x20108
	CBI  0xB,2
	RJMP _0x20109
_0x20108:
	SBI  0xB,2
_0x20109:
; 0001 0515                 Print("",45,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(45)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0516                 Debug_Putchar(0x30+Out_Lig2);
	LDI  R30,0
	SBIC 0xB,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0517                 }
; 0001 0518 
; 0001 0519 
; 0001 051A             if ((Temp_Byte|32)=='s')  //On/Off valve solenoid On/Off
_0x20105:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x73)
	BRNE _0x2010A
; 0001 051B                     {
; 0001 051C                     Out_Valve=!Out_Valve;
	SBIS 0x8,1
	RJMP _0x2010B
	CBI  0x8,1
	RJMP _0x2010C
_0x2010B:
	SBI  0x8,1
_0x2010C:
; 0001 051D                     Print("",21,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 051E                     Debug_Putchar(0x30+Out_Valve);
	LDI  R30,0
	SBIC 0x8,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 051F                     }
; 0001 0520 
; 0001 0521             if ((Temp_Byte|32)=='p') // Motor Phase Left/Right
_0x2010A:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x70)
	BRNE _0x2010D
; 0001 0522                     {
; 0001 0523                     Out_Motor_Ph=!Out_Motor_Ph;
	SBIS 0xB,1
	RJMP _0x2010E
	CBI  0xB,1
	RJMP _0x2010F
_0x2010E:
	SBI  0xB,1
_0x2010F:
; 0001 0524                     Print("",63,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0525                     Debug_Putchar(0x30+Out_Motor_Ph);
	LDI  R30,0
	SBIC 0xB,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0526                     }
; 0001 0527 
; 0001 0528             if ((Temp_Byte|32)=='r') // sMotor Run/Stop
_0x2010D:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x72)
	BRNE _0x20110
; 0001 0529                     {
; 0001 052A                     Out_Motor_Run=!Out_Motor_Run;
	SBIS 0xB,0
	RJMP _0x20111
	CBI  0xB,0
	RJMP _0x20112
_0x20111:
	SBI  0xB,0
_0x20112:
; 0001 052B                     Print("",79,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 052C                     Debug_Putchar(0x30+!Out_Motor_Run);
	LDI  R30,0
	SBIS 0xB,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 052D                     }
; 0001 052E             }
_0x20110:
; 0001 052F 
; 0001 0530 
; 0001 0531        if ((Sinc_Bit)&&(RS485.Tx_Buffer[1]==NORMAL))
_0x20101:
	SBIS 0x1E,2
	RJMP _0x20114
	__GETB2MN _RS485,4
	CPI  R26,LOW(0x4E)
	BREQ _0x20115
_0x20114:
	RJMP _0x20113
_0x20115:
; 0001 0532        {
; 0001 0533        Sinc_Bit=0;
	CBI  0x1E,2
; 0001 0534        if (Refresh_Bit) goto Reset;
	SBIC 0x1E,3
	RJMP _0x200F0
; 0001 0535 
; 0001 0536        U_12V=read_adc(8)/3.1;
	LDI  R26,LOW(8)
	RCALL _read_adc
	CLR  R22
	CLR  R23
	CALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x40466666
	CALL __DIVF21
	LDI  R26,LOW(_U_12V)
	LDI  R27,HIGH(_U_12V)
	CALL __CFD1U
	ST   X+,R30
	ST   X,R31
; 0001 0537        U_5V=read_adc(9)/1.25;
	LDI  R26,LOW(9)
	RCALL _read_adc
	CLR  R22
	CLR  R23
	CALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x3FA00000
	CALL __DIVF21
	LDI  R26,LOW(_U_5V)
	LDI  R27,HIGH(_U_5V)
	CALL __CFD1U
	ST   X+,R30
	ST   X,R31
; 0001 0538 
; 0001 0539 
; 0001 053A        Temp_Byte=0x30;
	LDI  R17,LOW(48)
; 0001 053B        if ((U_5V<Min_5V)||(U_5V>Max_5V)) Temp_Byte|=1;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	CPI  R26,LOW(0x1C2)
	LDI  R30,HIGH(0x1C2)
	CPC  R27,R30
	BRLO _0x2011A
	CPI  R26,LOW(0x227)
	LDI  R30,HIGH(0x227)
	CPC  R27,R30
	BRLO _0x20119
_0x2011A:
	ORI  R17,LOW(1)
; 0001 053C        if ((U_12V<Min_12V)||(U_12V>Max_12V)) Temp_Byte|=2;
_0x20119:
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	CPI  R26,LOW(0x6E)
	LDI  R30,HIGH(0x6E)
	CPC  R27,R30
	BRLO _0x2011D
	CPI  R26,LOW(0x83)
	LDI  R30,HIGH(0x83)
	CPC  R27,R30
	BRLO _0x2011C
_0x2011D:
	ORI  R17,LOW(2)
; 0001 053D        if (!Motor_Fault) Temp_Byte|=4;
_0x2011C:
	SBIS 0x3,7
	ORI  R17,LOW(4)
; 0001 053E        Alarm_Status=Temp_Byte;
	STS  _Alarm_Status,R17
; 0001 053F 
; 0001 0540        if ((Alarm_Status&3)) Led_Power=1; //Alarm Light On/Off
	LDS  R30,_Alarm_Status
	ANDI R30,LOW(0x3)
	BREQ _0x20120
	SBI  0xB,5
; 0001 0541         else Led_Power=0;
	RJMP _0x20123
_0x20120:
	CBI  0xB,5
; 0001 0542 
; 0001 0543        //Print("_ U(12V)=",49,18,1);
; 0001 0544        Print("U(12V)=",51,20,1+6*((Alarm_Status>>1)&1));
_0x20123:
	__POINTW1FN _0x20000,219
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(51)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDS  R30,_Alarm_Status
	LSR  R30
	ANDI R30,LOW(0x1)
	LDI  R26,LOW(6)
	MUL  R30,R26
	MOVW R30,R0
	SUBI R30,-LOW(1)
	MOV  R26,R30
	CALL _Print
; 0001 0545        Temp_Byte=0x30+U_12V/100;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0546        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0547        Calculated.U12V[0]=Temp_Byte;
	__PUTBMRN _Calculated,3,17
; 0001 0548        Temp_Byte=0x30+(U_12V%100)/10;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0549        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 054A        Calculated.U12V[1]=Temp_Byte;
	__PUTBMRN _Calculated,4,17
; 0001 054B        Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 054C        Temp_Byte=0x30+U_12V%10;
	LDS  R26,_U_12V
	LDS  R27,_U_12V+1
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __MODW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 054D        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 054E        Calculated.U12V[2]=Temp_Byte;
	__PUTBMRN _Calculated,5,17
; 0001 054F        Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 0550        Debug_Putchar('V');
	LDI  R26,LOW(86)
	CALL _Debug_Putchar
; 0001 0551        Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 0552 
; 0001 0553 
; 0001 0554        //Print("_ U(5V)=",65,18,1);
; 0001 0555        Print("U(5V)=",68,20,1+6*Alarm_Status&1);
	__POINTW1FN _0x20000,227
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(68)
	ST   -Y,R30
	LDI  R30,LOW(20)
	ST   -Y,R30
	LDS  R30,_Alarm_Status
	LDI  R26,LOW(6)
	MUL  R30,R26
	MOVW R30,R0
	SUBI R30,-LOW(1)
	ANDI R30,LOW(0x1)
	MOV  R26,R30
	CALL _Print
; 0001 0556        Temp_Byte=0x30+U_5V/100;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 0557        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 0558        Calculated.U5V[0]=Temp_Byte;
	STS  _Calculated,R17
; 0001 0559        Debug_Putchar('.');
	LDI  R26,LOW(46)
	CALL _Debug_Putchar
; 0001 055A        Temp_Byte=0x30+(U_5V%100)/10;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	CALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 055B        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 055C        Calculated.U5V[1]=Temp_Byte;
	__PUTBMRN _Calculated,1,17
; 0001 055D        Temp_Byte=0x30+U_5V%10;
	LDS  R26,_U_5V
	LDS  R27,_U_5V+1
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __MODW21U
	SUBI R30,-LOW(48)
	MOV  R17,R30
; 0001 055E        Debug_Putchar(Temp_Byte);
	MOV  R26,R17
	CALL _Debug_Putchar
; 0001 055F        Calculated.U5V[2]=Temp_Byte;
	__PUTBMRN _Calculated,2,17
; 0001 0560        Debug_Putchar('[');
	LDI  R26,LOW(91)
	CALL _Debug_Putchar
; 0001 0561        Debug_Putchar('V');
	LDI  R26,LOW(86)
	CALL _Debug_Putchar
; 0001 0562        Debug_Putchar(']');
	LDI  R26,LOW(93)
	CALL _Debug_Putchar
; 0001 0563 
; 0001 0564 
; 0001 0565 
; 0001 0566        //Print("[S]olenoid=",11,14,1);
; 0001 0567        Print("",21,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0568        Debug_Putchar(0x30+Out_Valve);
	LDI  R30,0
	SBIC 0x8,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0569 
; 0001 056A 
; 0001 056B        //Print("; Lamp[1]=",24,14,1);
; 0001 056C        Print("",33,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(33)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 056D        Debug_Putchar(0x30+Out_Lig1);
	LDI  R30,0
	SBIC 0xB,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 056E 
; 0001 056F 
; 0001 0570        //Print("; Lamp[2]=",36,14,1);
; 0001 0571        Print("",45,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(45)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0572        Debug_Putchar(0x30+Out_Lig2);
	LDI  R30,0
	SBIC 0xB,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0573 
; 0001 0574 
; 0001 0575 
; 0001 0576        //Print("; Motor Phase=",48,14,1);
; 0001 0577        Print("",63,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0578        Debug_Putchar(0x30+Out_Motor_Ph);
	LDI  R30,0
	SBIC 0xB,1
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0579 
; 0001 057A 
; 0001 057B 
; 0001 057C        //Print("; Motor Run=",64,14,1);
; 0001 057D        Print("",79,14,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(14)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 057E        Debug_Putchar(0x30+!Out_Motor_Run);
	LDI  R30,0
	SBIS 0xB,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 057F 
; 0001 0580 
; 0001 0581        //Print("CBX Sensor=",11,16,1);
; 0001 0582        Print("",21,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(21)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0583        Debug_Putchar(0x30+!CbxPS_Sensor);
	LDI  R30,0
	SBIS 0x3,3
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0584 
; 0001 0585 
; 0001 0586 
; 0001 0587        //Print("; Position Sensor=",29,16,1);
; 0001 0588        Print("",41,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(41)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0589        Debug_Putchar(0x30+!RecPS_Sensor);
	LDI  R30,0
	SBIS 0x6,0
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 058A 
; 0001 058B 
; 0001 058C 
; 0001 058D        //Print("; Motor Lock Sensor=",49,16,1);
; 0001 058E        Print("",63,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(63)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 058F        Debug_Putchar(0x30+!LockPS_Sensor);
	LDI  R30,0
	SBIS 0x3,2
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0590 
; 0001 0591 
; 0001 0592 
; 0001 0593        //Print("; Motor Fault=",66,16,1);
; 0001 0594        Print("",79,16,1);
	__POINTW1FN _0x20000,24
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(79)
	ST   -Y,R30
	LDI  R30,LOW(16)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0595        Debug_Putchar(0x30+!Motor_Fault);
	LDI  R30,0
	SBIS 0x3,7
	LDI  R30,1
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 0596 
; 0001 0597 
; 0001 0598        Print_Vault_No(10);
	LDI  R26,LOW(10)
	RCALL _Print_Vault_No
; 0001 0599 
; 0001 059A 
; 0001 059B 
; 0001 059C 
; 0001 059D        Print("Bin Number:",1,8,1);
	__POINTW1FN _0x20000,234
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 059E        for (Temp_Byte=7+Mode_1;Temp_Byte<12;Temp_Byte++) Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
	LDI  R30,0
	SBIC 0x1E,0
	LDI  R30,1
	SUBI R30,-LOW(7)
	MOV  R17,R30
_0x20127:
	CPI  R17,12
	BRSH _0x20128
	__POINTW2MN _RS485,3
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CALL _Debug_Putchar
	SUBI R17,-1
	RJMP _0x20127
_0x20128:
; 0001 059F Mode_Print(!Mode_1);
	LDI  R26,0
	SBIS 0x1E,0
	LDI  R26,1
	CALL _Mode_Print
; 0001 05A0 
; 0001 05A1 
; 0001 05A2 
; 0001 05A3 #ifdef _Test_
; 0001 05A4        if (Cmp1!=RS485.Tx_Buffer[8]) {Cmp1=RS485.Tx_Buffer[8];Err1++;}
; 0001 05A5        Debug_Putchar(0x30+Err1);
; 0001 05A6 #endif
; 0001 05A7 
; 0001 05A8 
; 0001 05A9        Print("Cash Box Number:",1,6,1);
	__POINTW1FN _0x20000,246
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(6)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05AA        for (Temp_Byte=12+Mode_2;Temp_Byte<17;Temp_Byte++) if (RS485.Tx_Buffer[Temp_Byte]!=':') Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
	LDI  R30,0
	SBIC 0x1E,1
	LDI  R30,1
	SUBI R30,-LOW(12)
	MOV  R17,R30
_0x2012A:
	CPI  R17,17
	BRSH _0x2012B
	__POINTW2MN _RS485,3
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CPI  R26,LOW(0x3A)
	BREQ _0x2012C
	__POINTW2MN _RS485,3
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R26,X
	CALL _Debug_Putchar
; 0001 05AB        Mode_Print(RS485.Tx_Buffer[12]==':'?0:!Mode_2);
_0x2012C:
	SUBI R17,-1
	RJMP _0x2012A
_0x2012B:
	__GETB2MN _RS485,15
	CPI  R26,LOW(0x3A)
	BRNE _0x2012D
	LDI  R30,LOW(0)
	RJMP _0x2012E
_0x2012D:
	LDI  R30,0
	SBIS 0x1E,1
	LDI  R30,1
_0x2012E:
	MOV  R26,R30
	CALL _Mode_Print
; 0001 05AC 
; 0001 05AD 
; 0001 05AE 
; 0001 05AF #ifdef _Test_
; 0001 05B0        if (Cmp2!=RS485.Tx_Buffer[14]) {Cmp2=RS485.Tx_Buffer[14];Err2++;}
; 0001 05B1        Debug_Putchar(0x30+Err2);
; 0001 05B2 #endif
; 0001 05B3 
; 0001 05B4 
; 0001 05B5 
; 0001 05B6        Print("Transmission Data Format",1,4,1);
	__POINTW1FN _0x20000,263
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(4)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05B7        Mode_Print(!Send_Mode);
	LDI  R26,0
	SBIS 0x9,6
	LDI  R26,1
	CALL _Mode_Print
; 0001 05B8        }
; 0001 05B9 
; 0001 05BA 
; 0001 05BB 
; 0001 05BC for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++)
_0x20113:
	LDI  R17,LOW(0)
_0x20131:
	CPI  R17,8
	BRLO PC+3
	JMP _0x20132
; 0001 05BD    if ((Rx3_Buff[Temp_Byte]==13)||(Rx3_Buff[Temp_Byte]==27))
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R26,Z
	CPI  R26,LOW(0xD)
	BREQ _0x20134
	CPI  R26,LOW(0x1B)
	BREQ _0x20134
	RJMP _0x20133
_0x20134:
; 0001 05BE             {
; 0001 05BF              if (Rx3_Buff[Temp_Byte]==27) goto Reset;
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R26,Z
	CPI  R26,LOW(0x1B)
	BRNE _0x20136
	RJMP _0x200F0
; 0001 05C0 
; 0001 05C1              Temp_Byte=(Temp_Byte-6)&0x07;
_0x20136:
	MOV  R30,R17
	SUBI R30,LOW(6)
	ANDI R30,LOW(0x7)
	MOV  R17,R30
; 0001 05C2 
; 0001 05C3              for (Temp_Byte2=0;Temp_Byte2<6;Temp_Byte2++) if (Password[Temp_Byte2]!=Rx3_Buff[(Temp_Byte2+Temp_Byte)&0x07]) goto Bad_Psw;
	LDI  R16,LOW(0)
_0x20138:
	CPI  R16,6
	BRSH _0x20139
	MOV  R30,R16
	LDI  R31,0
	SUBI R30,LOW(-_Password*2)
	SBCI R31,HIGH(-_Password*2)
	LPM  R26,Z
	MOV  R30,R17
	ADD  R30,R16
	ANDI R30,LOW(0x7)
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LD   R30,Z
	CP   R30,R26
	BREQ _0x2013A
	RJMP _0x2013B
; 0001 05C4 
; 0001 05C5 
; 0001 05C6 #ifdef _Test_
; 0001 05C7 
; 0001 05C8             Err1=0;
; 0001 05C9             Err2=0;
; 0001 05CA #endif
; 0001 05CB 
; 0001 05CC 
; 0001 05CD              //Password Corect ,enter in Menu for changing Serial , Vault number and valt holding time
; 0001 05CE 
; 0001 05CF Loop_Change: ClrScr();
_0x2013A:
	SUBI R16,-1
	RJMP _0x20138
_0x20139:
_0x2013C:
	CALL _ClrScr
; 0001 05D0 
; 0001 05D1              Print(" [S]erial Number,[V]ault Number,Holding [T]ime [Esc]-Main Menu ",10,0,7);
	__POINTW1FN _0x20000,288
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 05D2 
; 0001 05D3              Print_Vault_No(4);
	LDI  R26,LOW(4)
	RCALL _Print_Vault_No
; 0001 05D4              Print("V",1,4,4);
	__POINTW1FN _0x20000,352
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(4)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 05D5              Print_Serial_No(6);
	LDI  R26,LOW(6)
	RCALL _Print_Serial_No
; 0001 05D6              Print("S",1,6,4);
	__POINTW1FN _0x20000,354
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(6)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 05D7              Print("Time Vault Holding=",1,8,1);
	__POINTW1FN _0x20000,356
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05D8              Debug_Putchar(0x30+(Solenoid_Time/10));
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05D9              Debug_Putchar(0x30+Solenoid_Time%10);
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMRDB
	MOV  R26,R30
	LDI  R30,LOW(10)
	CALL __MODB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	CALL _Debug_Putchar
; 0001 05DA              Print("T",1,8,4);
	__POINTW1FN _0x20000,376
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(4)
	CALL _Print
; 0001 05DB              Print("[s]",23,8,1);
	__POINTW1FN _0x20000,378
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(23)
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 05DC              Temp_Byte=Debug_Getchar();
	CALL _Debug_Getchar
	MOV  R17,R30
; 0001 05DD 
; 0001 05DE              if (Temp_Byte==27) goto Reset;
	CPI  R17,27
	BRNE _0x2013D
	RJMP _0x200F0
; 0001 05DF 
; 0001 05E0              if ((Temp_Byte|32)=='s')
_0x2013D:
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x73)
	BRNE _0x2013E
; 0001 05E1                     {
; 0001 05E2                     Print("New Serial Number:",1,10,7);
	__POINTW1FN _0x20000,382
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 05E3                     if (Input(Buff_Temp,8,0)==8) for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Serial_No[Temp_Byte]=Buff_Temp[Temp_Byte];
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x8)
	BRNE _0x2013F
	LDI  R17,LOW(0)
_0x20141:
	CPI  R17,8
	BRSH _0x20142
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Serial_No)
	SBCI R27,HIGH(-_Serial_No)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Buff_Temp)
	SBCI R31,HIGH(-_Buff_Temp)
	LD   R30,Z
	CALL __EEPROMWRB
	SUBI R17,-1
	RJMP _0x20141
_0x20142:
; 0001 05E4 };
_0x2013F:
_0x2013E:
; 0001 05E5 
; 0001 05E6 
; 0001 05E7              if ((Temp_Byte|32)=='v')
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x76)
	BRNE _0x20143
; 0001 05E8                     {
; 0001 05E9                     Print("New Vault Number:",1,10,7);
	__POINTW1FN _0x20000,401
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 05EA                     if (Input(Buff_Temp,5,0)==5) for (Temp_Byte=0;Temp_Byte<5;Temp_Byte++) Vault[Temp_Byte]=Buff_Temp[Temp_Byte];
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(5)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x5)
	BRNE _0x20144
	LDI  R17,LOW(0)
_0x20146:
	CPI  R17,5
	BRSH _0x20147
	MOV  R26,R17
	LDI  R27,0
	SUBI R26,LOW(-_Vault)
	SBCI R27,HIGH(-_Vault)
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Buff_Temp)
	SBCI R31,HIGH(-_Buff_Temp)
	LD   R30,Z
	CALL __EEPROMWRB
	SUBI R17,-1
	RJMP _0x20146
_0x20147:
; 0001 05EB };
_0x20144:
_0x20143:
; 0001 05EC 
; 0001 05ED 
; 0001 05EE              if ((Temp_Byte|32)=='t')
	MOV  R30,R17
	ORI  R30,0x20
	CPI  R30,LOW(0x74)
	BRNE _0x20148
; 0001 05EF                     {
; 0001 05F0                     Print("New Valt Holding Time:",1,10,7);
	__POINTW1FN _0x20000,419
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(10)
	ST   -Y,R30
	LDI  R26,LOW(7)
	CALL _Print
; 0001 05F1                     if (Input(Buff_Temp,2,0)==2)
	LDI  R30,LOW(_Buff_Temp)
	LDI  R31,HIGH(_Buff_Temp)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(2)
	ST   -Y,R30
	LDI  R26,LOW(0)
	CALL _Input
	CPI  R30,LOW(0x2)
	BRNE _0x20149
; 0001 05F2                             {
; 0001 05F3                             Temp_Byte=(10*(Buff_Temp[0]-0x30))+Buff_Temp[1]-0x30;
	LDS  R30,_Buff_Temp
	SUBI R30,LOW(48)
	LDI  R26,LOW(10)
	MUL  R30,R26
	MOVW R30,R0
	MOV  R26,R30
	__GETB1MN _Buff_Temp,1
	ADD  R26,R30
	SUBI R26,LOW(48)
	MOV  R17,R26
; 0001 05F4                             if ((Temp_Byte<=25)&&(Temp_Byte>=4)) Solenoid_Time=Temp_Byte;
	CPI  R17,26
	BRSH _0x2014B
	CPI  R17,4
	BRSH _0x2014C
_0x2014B:
	RJMP _0x2014A
_0x2014C:
	MOV  R30,R17
	LDI  R26,LOW(_Solenoid_Time)
	LDI  R27,HIGH(_Solenoid_Time)
	CALL __EEPROMWRB
; 0001 05F5                             Temp_Byte=0;
_0x2014A:
	LDI  R17,LOW(0)
; 0001 05F6                             }
; 0001 05F7                     };
_0x20149:
_0x20148:
; 0001 05F8 
; 0001 05F9              goto Loop_Change;
	RJMP _0x2013C
; 0001 05FA 
; 0001 05FB 
; 0001 05FC     Bad_Psw: for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
_0x2013B:
	LDI  R17,LOW(0)
_0x2014E:
	CPI  R17,8
	BRSH _0x2014F
	MOV  R30,R17
	LDI  R31,0
	SUBI R30,LOW(-_Rx3_Buff)
	SBCI R31,HIGH(-_Rx3_Buff)
	LDI  R26,LOW(0)
	STD  Z+0,R26
	SUBI R17,-1
	RJMP _0x2014E
_0x2014F:
; 0001 05FD break;
	RJMP _0x20132
; 0001 05FE             };
_0x20133:
	SUBI R17,-1
	RJMP _0x20131
_0x20132:
; 0001 05FF     };
	RJMP _0x200FA
; 0001 0600 };
_0x20150:
	RJMP _0x20150
;
;
;
;
;
;
;void Print_Serial_No(unsigned char position)
; 0001 0608 {
_Print_Serial_No:
; 0001 0609  unsigned counter;
; 0001 060A  Print("Serial Number:",1,position,1);
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	position -> Y+2
;	counter -> R16,R17
	__POINTW1FN _0x20000,386
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDD  R30,Y+5
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 060B  for (counter=0;counter<8;counter++) Debug_Putchar(Serial_No[counter]);
	__GETWRN 16,17,0
_0x20152:
	__CPWRN 16,17,8
	BRSH _0x20153
	LDI  R26,LOW(_Serial_No)
	LDI  R27,HIGH(_Serial_No)
	ADD  R26,R16
	ADC  R27,R17
	CALL __EEPROMRDB
	MOV  R26,R30
	CALL _Debug_Putchar
	__ADDWRN 16,17,1
	RJMP _0x20152
_0x20153:
; 0001 060C }
	RJMP _0x2060001
;
;
;
;void Print_Vault_No(unsigned char position)
; 0001 0611 {
_Print_Vault_No:
; 0001 0612  unsigned counter;
; 0001 0613  Print("Vault Number:",1,position,1);
	ST   -Y,R26
	ST   -Y,R17
	ST   -Y,R16
;	position -> Y+2
;	counter -> R16,R17
	__POINTW1FN _0x20000,405
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDD  R30,Y+5
	ST   -Y,R30
	LDI  R26,LOW(1)
	CALL _Print
; 0001 0614  for (counter=0;counter<5;counter++) Debug_Putchar(Vault[counter]);
	__GETWRN 16,17,0
_0x20155:
	__CPWRN 16,17,5
	BRSH _0x20156
	LDI  R26,LOW(_Vault)
	LDI  R27,HIGH(_Vault)
	ADD  R26,R16
	ADC  R27,R17
	CALL __EEPROMRDB
	MOV  R26,R30
	CALL _Debug_Putchar
	__ADDWRN 16,17,1
	RJMP _0x20155
_0x20156:
; 0001 0615 }
_0x2060001:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,3
	RET
;
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_standby=0x0C
	.SET power_ctrl_reg=smcr
	#endif

	.CSEG

	.CSEG

	.CSEG

	.DSEG

	.CSEG

	.DSEG
_Rx3_Counter:
	.BYTE 0x1
_Rx3_Pointer_Buff:
	.BYTE 0x1
_Tmp_Rx3:
	.BYTE 0x1
_Rx3_Buff:
	.BYTE 0x8
_Tx1_Counter:
	.BYTE 0x1
_TxD_1:
	.BYTE 0x1

	.ESEG
_Serial_No:
	.DB  0x53,0x4E,0x31,0x32
	.DB  0x33,0x34,0x35,0x36
	.DB  0x0
_Vault:
	.DB  0x56,0x41,0x55,0x4C
	.DB  0x54,0x0
_Solenoid_Time:
	.DB  0x5

	.DSEG
_Rx1_Buff:
	.BYTE 0x8
_Rx2_Buff:
	.BYTE 0x8
_Compare_Buffer_1:
	.BYTE 0x5
_Compare_Buffer_2:
	.BYTE 0x5
_Rx1_ID_Buff:
	.BYTE 0x5
_Rx2_ID_Buff:
	.BYTE 0x5
_CB1_Pointer:
	.BYTE 0x1
_CB2_Time:
	.BYTE 0x1
_CB2_Pointer:
	.BYTE 0x1
_CB1_Tmp:
	.BYTE 0x2
_CB2_Tmp:
	.BYTE 0x2
_Timer_Sinc:
	.BYTE 0x2
_Solenoid_Td:
	.BYTE 0x2
_Buff_Temp:
	.BYTE 0xA
_Led_Blink:
	.BYTE 0x1
_Status_Lights:
	.BYTE 0x1
_Alarm_Status:
	.BYTE 0x1
_U_12V:
	.BYTE 0x2
_U_5V:
	.BYTE 0x2
_Timer:
	.BYTE 0x3
_RS485:
	.BYTE 0x43
_Calculated:
	.BYTE 0x8

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x0:
	LDI  R26,LOW(27)
	CALL _Debug_Putchar
	LDI  R26,LOW(91)
	JMP  _Debug_Putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(10)
	CALL __DIVB21U
	SUBI R30,-LOW(48)
	MOV  R26,R30
	JMP  _Debug_Putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2:
	LDI  R30,LOW(255)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R26,LOW(0)
	JMP  _Print

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x3:
	LDI  R26,LOW(63)
	CALL _Debug_Putchar
	LDI  R26,LOW(50)
	CALL _Debug_Putchar
	LDI  R26,LOW(53)
	JMP  _Debug_Putchar


	.CSEG
__ROUND_REPACK:
	TST  R21
	BRPL __REPACK
	CPI  R21,0x80
	BRNE __ROUND_REPACK0
	SBRS R30,0
	RJMP __REPACK
__ROUND_REPACK0:
	ADIW R30,1
	ADC  R22,R25
	ADC  R23,R25
	BRVS __REPACK1

__REPACK:
	LDI  R21,0x80
	EOR  R21,R23
	BRNE __REPACK0
	PUSH R21
	RJMP __ZERORES
__REPACK0:
	CPI  R21,0xFF
	BREQ __REPACK1
	LSL  R22
	LSL  R0
	ROR  R21
	ROR  R22
	MOV  R23,R21
	RET
__REPACK1:
	PUSH R21
	TST  R0
	BRMI __REPACK2
	RJMP __MAXRES
__REPACK2:
	RJMP __MINRES

__UNPACK:
	LDI  R21,0x80
	MOV  R1,R25
	AND  R1,R21
	LSL  R24
	ROL  R25
	EOR  R25,R21
	LSL  R21
	ROR  R24

__UNPACK1:
	LDI  R21,0x80
	MOV  R0,R23
	AND  R0,R21
	LSL  R22
	ROL  R23
	EOR  R23,R21
	LSL  R21
	ROR  R22
	RET

__CFD1U:
	SET
	RJMP __CFD1U0
__CFD1:
	CLT
__CFD1U0:
	PUSH R21
	RCALL __UNPACK1
	CPI  R23,0x80
	BRLO __CFD10
	CPI  R23,0xFF
	BRCC __CFD10
	RJMP __ZERORES
__CFD10:
	LDI  R21,22
	SUB  R21,R23
	BRPL __CFD11
	NEG  R21
	CPI  R21,8
	BRTC __CFD19
	CPI  R21,9
__CFD19:
	BRLO __CFD17
	SER  R30
	SER  R31
	SER  R22
	LDI  R23,0x7F
	BLD  R23,7
	RJMP __CFD15
__CFD17:
	CLR  R23
	TST  R21
	BREQ __CFD15
__CFD18:
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R23
	DEC  R21
	BRNE __CFD18
	RJMP __CFD15
__CFD11:
	CLR  R23
__CFD12:
	CPI  R21,8
	BRLO __CFD13
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R23
	SUBI R21,8
	RJMP __CFD12
__CFD13:
	TST  R21
	BREQ __CFD15
__CFD14:
	LSR  R23
	ROR  R22
	ROR  R31
	ROR  R30
	DEC  R21
	BRNE __CFD14
__CFD15:
	TST  R0
	BRPL __CFD16
	RCALL __ANEGD1
__CFD16:
	POP  R21
	RET

__CDF1U:
	SET
	RJMP __CDF1U0
__CDF1:
	CLT
__CDF1U0:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __CDF10
	CLR  R0
	BRTS __CDF11
	TST  R23
	BRPL __CDF11
	COM  R0
	RCALL __ANEGD1
__CDF11:
	MOV  R1,R23
	LDI  R23,30
	TST  R1
__CDF12:
	BRMI __CDF13
	DEC  R23
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R1
	RJMP __CDF12
__CDF13:
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R1
	PUSH R21
	RCALL __REPACK
	POP  R21
__CDF10:
	RET

__ZERORES:
	CLR  R30
	CLR  R31
	CLR  R22
	CLR  R23
	POP  R21
	RET

__MINRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	SER  R23
	POP  R21
	RET

__MAXRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	LDI  R23,0x7F
	POP  R21
	RET

__DIVF21:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BRNE __DIVF210
	TST  R1
__DIVF211:
	BRPL __DIVF219
	RJMP __MINRES
__DIVF219:
	RJMP __MAXRES
__DIVF210:
	CPI  R25,0x80
	BRNE __DIVF218
__DIVF217:
	RJMP __ZERORES
__DIVF218:
	EOR  R0,R1
	SEC
	SBC  R25,R23
	BRVC __DIVF216
	BRLT __DIVF217
	TST  R0
	RJMP __DIVF211
__DIVF216:
	MOV  R23,R25
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R1
	CLR  R17
	CLR  R18
	CLR  R19
	CLR  R20
	CLR  R21
	LDI  R25,32
__DIVF212:
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	CPC  R20,R17
	BRLO __DIVF213
	SUB  R26,R30
	SBC  R27,R31
	SBC  R24,R22
	SBC  R20,R17
	SEC
	RJMP __DIVF214
__DIVF213:
	CLC
__DIVF214:
	ROL  R21
	ROL  R18
	ROL  R19
	ROL  R1
	ROL  R26
	ROL  R27
	ROL  R24
	ROL  R20
	DEC  R25
	BRNE __DIVF212
	MOVW R30,R18
	MOV  R22,R1
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	TST  R22
	BRMI __DIVF215
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	DEC  R23
	BRVS __DIVF217
__DIVF215:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__ANEGD1:
	COM  R31
	COM  R22
	COM  R23
	NEG  R30
	SBCI R31,-1
	SBCI R22,-1
	SBCI R23,-1
	RET

__LSLB12:
	TST  R30
	MOV  R0,R30
	MOV  R30,R26
	BREQ __LSLB12R
__LSLB12L:
	LSL  R30
	DEC  R0
	BRNE __LSLB12L
__LSLB12R:
	RET

__CWD1:
	MOV  R22,R31
	ADD  R22,R22
	SBC  R22,R22
	MOV  R23,R22
	RET

__LNEGB1:
	TST  R30
	LDI  R30,1
	BREQ __LNEGB1F
	CLR  R30
__LNEGB1F:
	RET

__MULB1W2U:
	MOV  R22,R30
	MUL  R22,R26
	MOVW R30,R0
	MUL  R22,R27
	ADD  R31,R0
	RET

__DIVB21U:
	CLR  R0
	LDI  R25,8
__DIVB21U1:
	LSL  R26
	ROL  R0
	SUB  R0,R30
	BRCC __DIVB21U2
	ADD  R0,R30
	RJMP __DIVB21U3
__DIVB21U2:
	SBR  R26,1
__DIVB21U3:
	DEC  R25
	BRNE __DIVB21U1
	MOV  R30,R26
	MOV  R26,R0
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__MODB21U:
	RCALL __DIVB21U
	MOV  R30,R26
	RET

__MODW21U:
	RCALL __DIVW21U
	MOVW R30,R26
	RET

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
