/*****************************************************
This program was produced by the
CodeWizardAVR V2.04.9 Standard
Automatic Program Generator
� Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : Vault Interface Board

Version : 1.04
Date    : 02/08/2018
Comments: V1.04
-Add logic to blink Out_Lig2  (PORTD.2) if CBX ID is detected.

Version : 1.03
Date    : 11/05/2014
Comments: V1.03
- Add Mode Auto NODE , input LockPS_Sensor-(PB.2) is used for define node 0 or 1.
-Swaped Motor [P]hase and Motor [R]un keys in Debug Menu.

V1.02 Fixed info data send no define node address
V10.1 Fixed Bin and CBX ID in old mode



Chip type               : ATMega16M1 / ATMega32M1
AVR Core Clock frequency: 14.745600 MHz
Memory model            : Tiny
External RAM size       : 0
Data Stack size         : 64
*****************************************************/

#include <mega16m1.h>
#include <delay.h>
#include "debug.h"






#define STX    2
#define ETX    3
#define EOT    4


#define Min_5V   450
#define Max_5V   550

#define Min_12V   110
#define Max_12V   130


//#define _Test_

#define Time_Base_CBXID   20



//******** INPUTS *************
#define CbxPS_Sensor      PINB.3
#define RecPS_Sensor      PINC.0
#define LockPS_Sensor     PINB.2
#define Motor_Fault       PINB.7


#define In_1            PINB.6
#define In_2            PINC.7
#define In_3            PINC.3


#define Send_Mode       PIND.6




//***** debug txd port *******
#define Soft_TxD    PORTC.2

#asm
.EQU _Soft_TxD=2
.EQU _PORTC=0x08
#endasm



//**** old mode outputs ******
#define Out_ID      PORTD.3
#define Out_ID2     PORTB.4


#define RS485_Dir   PORTB.5




#define Led_Power     PORTD.5
#define Led_Link      PORTB.1
#define Led_Sys       PORTC.6
#define Led_Service   PORTB.0




//***** OUTPUTS *********
#define Out_Light1   PORTD.7
#define Out_Light2   PORTD.2
#define Out_Valve  PORTC.1

#define Out_Motor_Ph   PORTD.0
#define Out_Motor_Run  PORTD.1

#define Power_Light DDRC.7

#define Data_Offset     30

eeprom unsigned char Serial_No[]="SN123456";
eeprom unsigned char Vault[]="VAULT0";
eeprom unsigned char Solenoid_Time=18;
eeprom unsigned char Node_Address_='a';

flash unsigned char Firmware_Ver[]="0104";
flash unsigned char Password[]="GFI847";



bit Mode_1,Mode_2,Sinc_Bit,Refresh_Bit,Read_CBX_No=0;
bit CBX1_Done_bit,CBX2_Done_bit,CBX1_bit=0,Old_CBX1,CBX2_bit=0,Old_CBX2;

unsigned char Rx1_Counter,Rx1_Pointer_Buff,Tmp_Rx1,Rx1_Buff[8],Time_Out_1;
unsigned char Rx2_Counter,Rx2_Pointer_Buff,Tmp_Rx2,Rx2_Buff[8],Time_Out_2;
unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8],Key_Press;

unsigned char Bin_Remove=0,Compare_Buffer_1[6],Compare_Buffer_2[6];


unsigned char Rx1_ID_Buff[6],Rx2_ID_Buff[6];

unsigned char CB_Tx_Counter1=0,CB_Tx_Counter2=0;

unsigned char Tx1_Counter,TxD_1,Debounce_CBX=0;

unsigned char CB1_Time,CB1_Pointer=0,CB2_Time,CB2_Pointer=0;

unsigned int CB1_Tmp,CB2_Tmp,Timer_Sinc,Solenoid_Td;

unsigned char Buff_Temp[10];

unsigned char Node_Address,Led_Blink,Status_Lights=0,Alarm_Status=0x30;

unsigned int U_12V=120,U_5V=500;



struct _Timer{
		  	 unsigned char Time_P,Link_Timer,Counting,Time_Old;
			 unsigned int Time;
		  	 }Timer;


struct _RS485{
		  	 unsigned char Rx_Counter,Tx_Size,LRC;
             signed char Tx_Counter;
			 unsigned char Tx_Buffer[60],Rx_Buffer[32],Get_All_Command[32];
		  	 }RS485;



void Print_Serial_No(unsigned char position);
void Print_Vault_No(unsigned char position);



//************************************* Timer 0 output compare A interrupt service routine *****************************
interrupt [TIM0_COMPA] void timer0_compa_isr(void)
{

#pragma optsize-

unsigned char Dat,Tmp1,Tmp2,Tmp3,Tmp4;

#asm("sei");

if(Mode_1)
          {
           if (++CB1_Time<250)
            {
            if (CBX1_bit)
              {
               CBX1_bit=0;

               if (CB1_Time>(Time_Base_CBXID*6))
                                  {
                                   CB1_Time=0;
                                   CB1_Pointer=0;
                                   CB1_Tmp=0;
                                   goto Exit_CB1;
                                  }


               if (CB1_Time>(Time_Base_CBXID*3))
                 {
                 //CB1_Tmp<<=4;
                 #asm
                 LDS       R30,_CB1_Tmp   ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1  ;Load direct from data space
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry


                 ;CB1_Tmp|=7
                 ORI       R30,0x07
                 STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp+1,R31

                 #endasm

                  CB1_Pointer+=4;

                  goto Exit_CB1;
                  };


               if (CB1_Time>(Time_Base_CBXID*2))
                 {
                 //CB1_Tmp<<=3;
                 #asm
                 LDS       R30,_CB1_Tmp     ;Load direct from data space
                 LDS       R31,_CB1_Tmp+1    ;Load direct from data space
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry

                 ;CB1_Tmp|=3;
                 ORI       R30,0x03
                 STS       _CB1_Tmp,R30
                 STS       _CB1_Tmp+1,R31
                 #endasm

                 CB1_Pointer+=3;
                 goto Exit_CB1;
                 };


               if (CB1_Time<(Time_Base_CBXID))
                 {
                 CB1_Tmp<<=1;
                 CB1_Pointer++;
                 goto Exit_CB1;
                 };


               //CB1_Tmp<<=2;
               #asm
               LDS       R30,_CB1_Tmp     ;Load direct from data space
               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
               LSL       R30              ;Logical Shift Left
               ROL       R31              ;Rotate Left Through Carry
               LSL       R30              ;Logical Shift Left
               ROL       R31              ;Rotate Left Through Carry

               ;CB1_Tmp|=1;

               ORI       R30,1
               STS       _CB1_Tmp,R30
               STS       _CB1_Tmp+1,R31
               #endasm

               CB1_Pointer+=2;

Exit_CB1:      CB1_Time=0;
                };
            }
             else
                {
                 if (CB1_Time==250)
                        {
                         if (CB1_Pointer==13) {
                                               //CB1_Tmp<<=3;
                                               #asm
                                               LDS       R30,_CB1_Tmp     ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry

                                               ;CB1_Tmp|=3;
                                               ORI       R30,3
                                               STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp+1,R31
                                               #endasm
                                               }


                         if (CB1_Pointer==14) {
                                              //CB1_Tmp<<=2;
                                              #asm
                                               LDS       R30,_CB1_Tmp    ;Load direct from data space
                                               LDS       R31,_CB1_Tmp+1   ;Load direct from data space
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry

                                               ;CB1_Tmp|=1;
                                               ORI       R30,1
                                               STS       _CB1_Tmp,R30
                                               STS       _CB1_Tmp+1,R31
                                               #endasm
                                              }

                         if (CB1_Pointer==15) CB1_Tmp<<=1;

                         CB1_Pointer=0;

                         Rx1_Buff[0]='0';
                         Rx1_Buff[1]='0';

                         //Rx1_Buff[1]=(CB1_Tmp>>12)&0x0F;
                         #asm
                         LDS       R30,_CB1_Tmp+1 ;       ;Load immediate
                         SWAP      R30
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK1
                         LDI       R30,0x37
                    OK1: STS       _Rx1_Buff+2,R30
                         #endasm

                         //Rx1_Buff[2]=(CB1_Tmp>>8)&0x0F;
                         #asm
                         LDS       R30,_CB1_Tmp+1       ;Load immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK2
                         LDI       R30,0x37
                    OK2: STS       _Rx1_Buff+3,R30
                         #endasm

                         //Rx1_Buff[3]=(CB1_Tmp>>4)&0x0F;
                         #asm
                         LDS       R30,_CB1_Tmp       ;Load immediate
                         SWAP      R30
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK3
                         LDI       R30,0x37
                    OK3: STS       _Rx1_Buff+4,R30
                         #endasm

                         //Rx1_Buff[4]=CB1_Tmp&0x0F;
                         #asm
                         LDS       R30,_CB1_Tmp       ;Load immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK4
                         LDI       R30,0x37
                    OK4: STS       _Rx1_Buff+5,R30
                         #endasm

                         CBX1_Done_bit=1;
                         }
                          else CB1_Time=250;
                };

          };



if(Mode_2)
          {
           if (++CB2_Time<250)
            {
            if (CBX2_bit)
              {
               CBX2_bit=0;

               if (CB2_Time>(Time_Base_CBXID*8))
                                  {
                                   CB2_Time=0;
                                   CB2_Pointer=0;
                                   CB2_Tmp=0;
                                   goto Exit_CB2;
                                  }



               if (CB2_Time>(Time_Base_CBXID*3))
                 {

                 //CB2_Tmp<<=4;
                 #asm
                 LDS       R30,_CB2_Tmp   ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1  ;Load direct from data space
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry

                 ;CB2_Tmp|=7
                 ORI       R30,0x07
                 STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp+1,R31

                 #endasm

                  CB2_Pointer+=4;

                  goto Exit_CB2;
                  };


               if (CB2_Time>(Time_Base_CBXID*2))
                 {
                 //CB2_Tmp<<=3;
                 #asm
                 LDS       R30,_CB2_Tmp     ;Load direct from data space
                 LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry
                 LSL       R30            ;Logical Shift Left
                 ROL       R31            ;Rotate Left Through Carry

                 ;CB2_Tmp|=3;
                 ORI       R30,0x03
                 STS       _CB2_Tmp,R30
                 STS       _CB2_Tmp+1,R31
                 #endasm

                 CB2_Pointer+=3;
                 goto Exit_CB2;
                 };


               if (CB2_Time<(Time_Base_CBXID))
                 {
                 CB2_Tmp<<=1;
                 CB2_Pointer++;
                 goto Exit_CB2;
                 };


               //CB2_Tmp<<=2;
               #asm
               LDS       R30,_CB2_Tmp     ;Load direct from data space
               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
               LSL       R30            ;Logical Shift Left
               ROL       R31            ;Rotate Left Through Carry
               LSL       R30            ;Logical Shift Left
               ROL       R31            ;Rotate Left Through Carry

               ;CB2_Tmp|=1;

               ORI       R30,1
               STS       _CB2_Tmp,R30
               STS       _CB2_Tmp+1,R31
               #endasm

               CB2_Pointer+=2;

Exit_CB2:      CB2_Time=0;
                };
            }
             else
                {
                 if (CB2_Time==250)
                        {
                         if (CB2_Pointer==13) {
                                               //CB2_Tmp<<=3;
                                               #asm
                                               LDS       R30,_CB2_Tmp     ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1    ;Load direct from data space
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry

                                               ;CB2_Tmp|=3;
                                               ORI       R30,3
                                               STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp+1,R31
                                               #endasm
                                               }


                         if (CB2_Pointer==14) {
                                              //CB2_Tmp<<=2;
                                              #asm
                                               LDS       R30,_CB2_Tmp    ;Load direct from data space
                                               LDS       R31,_CB2_Tmp+1   ;Load direct from data space
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry
                                               LSL       R30            ;Logical Shift Left
                                               ROL       R31            ;Rotate Left Through Carry

                                               ;CB2_Tmp|=1;
                                               ORI       R30,1
                                               STS       _CB2_Tmp,R30
                                               STS       _CB2_Tmp+1,R31
                                               #endasm
                                              }

                         if (CB2_Pointer==15) CB2_Tmp<<=1;

                         CB2_Pointer=0;

                         Rx2_Buff[0]='0';
                         Rx2_Buff[1]='0';

                         //Rx2_Buff[1]=(CB2_Tmp>>12)&0x0F;
                         #asm
                         LDS       R30,_CB2_Tmp+1 ;       ;Load immediate
                         SWAP      R30
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK11
                         LDI       R30,0x37
                 OK11:   STS       _Rx2_Buff+2,R30
                         #endasm

                         //Rx2_Buff[2]=(CB2_Tmp>>8)&0x0F;
                         #asm
                         LDS       R30,_CB2_Tmp+1       ;Load immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK22
                         LDI       R30,0x37
                 OK22:   STS       _Rx2_Buff+3,R30
                         #endasm

                         //Rx2_Buff[3]=(CB2_Tmp>>4)&0x0F;
                         #asm
                         LDS       R30,_CB2_Tmp       ;Load immediate
                         SWAP      R30
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK33
                         LDI       R30,0x37
                 OK33:   STS       _Rx2_Buff+4,R30

                         OR        R8,R26
                         #endasm

                         //Rx2_Buff[4]=CB2_Tmp&0x0F;
                         #asm
                         LDS       R30,_CB2_Tmp       ;Load immediate
                         ANDI      R30,0x0F       ;Logical AND with immediate
                         ORI       R30,0x30
                         CPI       R30,0x3A
                         BRNE      OK44
                         LDI       R30,0x37
                 OK44:   STS       _Rx2_Buff+5,R30
                         #endasm

                         CBX2_Done_bit=1;
                         }
                          else CB2_Time=250;
                };

          };


  if (Time_Out_1<250)
                     {
                      Time_Out_1++;
                      if (Time_Out_1==250) {
                                            Rx1_Pointer_Buff=8;
                                            Mode_1=0;
                                            }
                     };


  if (Time_Out_2<250)
                     {
                      Time_Out_2++;
                      if (Time_Out_2==250) {
                                            Rx2_Pointer_Buff=8;
                                            Mode_2=0;
                                            }
                     };


  if (++Timer_Sinc>7250)       //~1.5[s]
                    {
                    #asm("wdr");

                    Sinc_Bit=1;
                    Timer_Sinc=0; //reset 1.5[s] timer


                    CBX1_Done_bit=0;
                    CBX2_Done_bit=0;

                    if (++Timer.Link_Timer>3) Led_Link=1;
                     else Led_Link=0;


                    Tmp2=0;
                    Tmp3=0;
                    if (Rx1_Buff[0]==0) Bin_Remove++;
                     else Bin_Remove=0;

                    for (Dat=0;Dat<6;Dat++) //Compare Old & New Data Rec. Data
                                {
                                Tmp1=Rx1_Buff[Dat];
                                if (Compare_Buffer_1[Dat]!=Tmp1) Tmp2=1;   //Bin
                                Compare_Buffer_1[Dat]=Tmp1;


                                Tmp1=Rx2_Buff[Dat];
                                if (Compare_Buffer_2[Dat]!=Tmp1) Tmp3=1;   //CBX
                                Compare_Buffer_2[Dat]=Tmp1;
                                }

                    Tmp4=0;
                    for (Dat=0;Dat<6;Dat++)
                                {
                                RS485.Tx_Buffer[3+Dat]=Vault[Dat]; //Vault Number

                                if (Tmp3) Rx2_Buff[Dat]='0';        //CBX
                                if (Tmp2) Rx1_Buff[Dat]='0';        //Bin

                                RS485.Tx_Buffer[9+Dat]=Rx1_Buff[Dat]; //BIN Number
                                Tmp1=Rx2_Buff[Dat];
                                if (Tmp1!='0') Tmp4++;
                                RS485.Tx_Buffer[15+Dat]=Tmp1;       //CBX Number
                                }
                                                 
                    //Debounce CBX ID 3 times for status LED
                    if ((Tmp4>0)&&(!Tmp3)) {if (++Debounce_CBX>3) Read_CBX_No=1;} 
                      else {
                            Read_CBX_No=0;
                            Debounce_CBX=0;
                            }


                    if (Send_Mode)
                        {
                            // ********************* Send Old Mode *********************
                            //Bin Number
                            for (Dat=0;Dat<5;Dat++)
                                {
                                if (Rx1_Buff[Dat+2]==0x37) Rx1_ID_Buff[Dat+1]=0x0A;
                                 else Rx1_ID_Buff[Dat+1]=Rx1_Buff[Dat+2];
                                };


                            //CBX Number
                            for (Dat=0;Dat<5;Dat++)
                                {
                                if (Rx2_Buff[Dat+2]==0x37) Rx2_ID_Buff[Dat+1]=0x0A;
                                 else Rx2_ID_Buff[Dat+1]=Rx2_Buff[Dat+2];
                                }




                            // if data >3999
                            if ((!CBX1_Done_bit)&&((Rx1_ID_Buff[0]=!'0')||((Rx1_ID_Buff[1]&0x0F)>3)))
                                {
                                Rx1_ID_Buff[1]='3';
                                Rx1_ID_Buff[2]='9';
                                Rx1_ID_Buff[3]='9';
                                Rx1_ID_Buff[4]='9';
                                };

                            // if data >3999
                            if ((Rx2_ID_Buff[0]=!'0')||((Rx2_ID_Buff[1]&0x0F)>3)) goto Bad_Value;

                            if (!CBX2_Done_bit)
                                {
                                if (!CbxPS_Sensor)
                                    {
                                    Bad_Value:
                                            Rx2_ID_Buff[1]='3';
                                            Rx2_ID_Buff[2]='9';
                                            Rx2_ID_Buff[3]='9';
                                            Rx2_ID_Buff[4]='9';
                                    };
                                };

                            CB_Tx_Counter1=8;
                            CB_Tx_Counter2=8;


                            //Flush  Buffers CBX & BIN
                            for (Dat=0;Dat<6;Dat++)
                                {
                                Rx1_Buff[Dat]='0';
                                Rx2_Buff[Dat]='0';
                                };

                        goto Exit_InT0;
                        } //End send Old Mode





                    //Flush  Buffers CBX & BIN
                    for (Dat=0;Dat<6;Dat++)
                            {
                            Rx1_Buff[Dat]='0';
                            Rx2_Buff[Dat]='0';
                            };
                    }
                     // *** Send New Mode Data to CBX Computer ***
                     else  if (RS485.Tx_Counter<RS485.Tx_Size)
                               {
                                RS485_Dir=1;
                                if (RS485.Tx_Counter<0) RS485.Tx_Counter++;
                                 else if (!(LINSIR&0x10))
                                        {
                                        Dat=RS485.Tx_Buffer[RS485.Tx_Counter++];
                                        if (RS485.Tx_Counter>RS485.Tx_Size-2) //Prepare LRC_H and LRC_L
                                            {
                                            if (RS485.Tx_Counter==RS485.Tx_Size-1) Dat=0x30|((RS485.LRC&0xF0)>>4);
                                              else Dat=0x30|(RS485.LRC&0x0F);
                                            }
                                         else RS485.LRC=RS485.LRC^Dat; //Calculate LRC in fly

                                         LINDAT=Dat;
                                        }
                                }
                                 else
                                     {
                                      if (!(LINSIR&0x10))
                                           {
                                            RS485_Dir=0;
                                            if (LINSIR&1)
                                                {
                                                Dat=LINDAT;
                                                RS485.Rx_Counter=RS485.Rx_Counter&0x1F;

                                                if (Dat==STX) //Begin command
                                                    {
                                                    RS485.Rx_Counter=0;
                                                    RS485.LRC=STX;
                                                    }

                                                RS485.Rx_Buffer[(RS485.Rx_Counter++)&0x1F]=Dat;
                                                RS485.Rx_Buffer[(RS485.Rx_Counter)&0x1F]=0xFF;
                                                RS485.LRC^=Dat; //Calculate LRC

                                                if (((RS485.LRC&0x7F)==0x00)&&(RS485.Rx_Buffer[(RS485.Rx_Counter-2)&0x1F])==ETX) //End of transfer
                                                    {
                                                    if ((RS485.Rx_Buffer[0]==STX)&&(RS485.Rx_Buffer[1]==Node_Address))
                                                        {
                                                        RS485.Tx_Buffer[1]=Node_Address;
                                                        Dat=RS485.Rx_Buffer[2];
                                                        Timer.Link_Timer=0;

                                                        //Command Poll
                                                        if ((Dat=='P')&&((RS485.Rx_Counter==5)||(RS485.Rx_Counter==6)))
                                                            {

                                                                RS485.Tx_Buffer[22]=0x30|Status_Lights; //Status Light

                                                            if (RS485.Rx_Counter==6)
                                                                 {
                                                                  Status_Lights=RS485.Rx_Buffer[3]&0x33;
                                                                  Out_Light1=Status_Lights&1;                                                                 
                                                                 }

                                                                RS485.Tx_Buffer[0]=STX;
                                                                RS485.Tx_Buffer[2]='P';
                                                                RS485.Tx_Buffer[21]=(0x30+!RecPS_Sensor|(!CbxPS_Sensor<<1)|(Out_Valve<<2)); //Sensor Status
                                                                RS485.Tx_Buffer[23]=Alarm_Status; //Alarm Status
                                                                RS485.Tx_Buffer[24]=ETX;
                                                                RS485.Tx_Buffer[25]=EOT;
                                                                RS485.Tx_Counter=-5;
                                                                //RS485.Tx_Buffer[26]=LRC_H;
                                                                //RS485.Tx_Buffer[27]=LRC_L;
                                                                RS485.Tx_Size=28;
                                                                RS485.LRC=STX;
                                                                goto Exit_InT00;
                                                            }


                                                        //Get All
                                                        if ((Dat=='A')&&((RS485.Rx_Counter==5)||(RS485.Rx_Counter==6)))
                                                            {
                                                            Tmp1=(RS485.Rx_Buffer[3]-0x30);
                                                            if ((RS485.Rx_Counter==6)&&(Tmp1>=5)&&(Tmp1<=25))
                                                              {
                                                               Solenoid_Time=Tmp1;
                                                               RS485.Get_All_Command[0]=(Tmp1/10)+0x30;
                                                               RS485.Get_All_Command[1]=(Tmp1%10)+0x30;
                                                               Refresh_Bit=1;
                                                              }

                                                                   /*
                                                            RS485.Tx_Buffer[0]=STX;
                                                            RS485.Tx_Buffer[2]='A';
                                                            RS485.Tx_Buffer[25]=ETX;
                                                            RS485.Tx_Buffer[26]=EOT;
                                                            for (Dat=0;Dat<22;Dat++) RS485.Tx_Buffer[Dat+3]=RS485.Get_All_Command[Dat];
                                                            RS485.Tx_Counter=-5;
                                                            RS485.Tx_Size=27;
                                                            */

                                                            RS485.Tx_Buffer[0+Data_Offset]=STX;
                                                            RS485.Tx_Buffer[1+Data_Offset]=Node_Address;
                                                            RS485.Tx_Buffer[2+Data_Offset]='A';
                                                            RS485.Tx_Buffer[25+Data_Offset]=ETX;
                                                            RS485.Tx_Buffer[26+Data_Offset]=EOT;
                                                            for (Dat=0;Dat<22;Dat++) RS485.Tx_Buffer[Dat+3+Data_Offset]=RS485.Get_All_Command[Dat];
                                                            RS485.Tx_Counter=Data_Offset;
                                                            RS485.Tx_Size=29+Data_Offset;

                                                            goto Exit_InT0;
                                                            }
                                                        }
                                                    }
                                                }
                                           }
                                     };//End Send New Mode Data Format

        if (Send_Mode)
                {

                LINCR=0x00; //Disable Uart

                if (CB_Tx_Counter1<255)   //Send CBX1 Old Mode
                  {
                    if (!(CB_Tx_Counter1&8))
                      {
                       Dat=Rx2_ID_Buff[1+((CB_Tx_Counter1>>6)&3)];
                       if (!((Dat<<((CB_Tx_Counter1>>4)&3))&8)) Out_ID=1;
                      }
                       else Out_ID=0;

                  CB_Tx_Counter1++;
                  }
                   else Out_ID=0;



                if (CB_Tx_Counter2<255)   //Send CBX2 Old Mode
                  {
                   if (!(CB_Tx_Counter2&8))
                     {
                     Dat=Rx1_ID_Buff[1+((CB_Tx_Counter2>>6)&3)];
                     if (!((Dat<<((CB_Tx_Counter2>>4)&3))&8)) Out_ID2=1;
                     }
                      else Out_ID2=0;

                  CB_Tx_Counter2++;
                  }
                   else Out_ID2=0;
                }//End Send  Old Mode Data Format


Exit_InT00:
		 if (!Timer.Time_P--)
                {
                Timer.Time_P=239;     //50ms

                //Sys LED Blink
                if (++Led_Blink&0x08) Led_Sys=0;
                 else Led_Sys=1;

                if (RecPS_Sensor&CbxPS_Sensor)
                {
                Solenoid_Td=0;
                Timer.Time=0;
                }
                
                //Show Vault Status LED
                Out_Light2=Read_CBX_No & (Led_Blink>>4) | (Status_Lights>>1)&1; //If CBX ID is detected, blinking Out_Lig2, Data System can keep this output active.
         
                //***** Vault Holding Timer *********
                if (Timer.Time>30) Timer.Time--;
                 else
                    {
                     if (Timer.Time>1)
                        {
                        Timer.Time--;
                        if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Out_Valve=1;      //Solenoid On
                          else {
                               Out_Valve=0; //Solenoid Off
                               }
                        }
                        else
                            {
                              Out_Valve=0; //Solenoid Off  , Time Out
                              if ((!RecPS_Sensor)||(!CbxPS_Sensor))
                                {
                                 if (++Solenoid_Td>=30)
                                    {
                                     Timer.Time=Solenoid_Time;
                                     Timer.Time*=20;
                                     Timer.Counting=1;
                                     }
                                }
                            }
                    }
                }

Exit_InT0:

  if ((!RecPS_Sensor)||(!CbxPS_Sensor)) Power_Light=0; //Power Up Led Light
   else Power_Light=1;

  #asm("cli");

#pragma optsize+
};







#include "look_up_tbl.h"

#pragma optsize-
//***********************  Timer1 output compare A interrupt service routine ***********************
interrupt [TIM1_COMPA] void timer1_compa_isr(void)
{

unsigned char Tmp_;



  //******** Software Trasmit for Debuging TxD ***************

                    #asm
            ;if (++Tx1_Counter<72)
                    LDS     R31,_Tx1_Counter
                    INC     R31
                    CPI     R31,72
                    BRCC    Exit_Else_Tx1

            ;if (Tx1_Counter<8) _Soft_TxD=0;
                    CPI     R31,8
                    BRCC    Else_Tx1
                    CBI     _PORTC,_Soft_TxD      ;_Soft_TxD=0
                    RJMP    Exit_Tx1

            ;if (!(Tx1_Counter&7))
Else_Tx1:           MOV     R30,R31
                    ANDI    R30,0x07
                    BRNE    Exit_Tx1

            ;if (TxD_1&0x80) _TxD_Dbg=0;
                    LDS     R30,_TxD_1

                    SBRS    R30,0
                    CBI     _PORTC,_Soft_TxD     ;_Soft_TxD=0
                    SBRC    R30,0

            ;else
                    SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
                    LSR     R30                   ;TxD_1>>=1;
                    STS     _TxD_1,R30
                    RJMP    Exit_Tx1

Exit_Else_Tx1:      SBI     _PORTC,_Soft_TxD      ;_Soft_TxD=1
                    CPI     R31,80
                    BRCS    Exit_Tx1
                    LDI     R31,80

Exit_Tx1:           STS     _Tx1_Counter,R31
                    #endasm



//if(!Mode_1)
           {
            if (++Rx1_Counter<78) //Software RxD_1  for CBX_1
                    {
                    if (!In_1) Tmp_Rx1&=Bit_Table[Rx1_Counter];
                    }
                    else
                        {
                         if (Rx1_Counter==78)
                            {
                            if (!Tmp_Rx1) Mode_1=1;
                            if (!Mode_1) Rx1_Buff[(Rx1_Pointer_Buff++)&0x07]=Tmp_Rx1;
                            Time_Out_1=0;
                            CBX1_Done_bit=1;
                            }
                            else
                                {
                                Rx1_Counter=78;
                                Tmp_Rx1=0xFF;
                                if (In_1==0) Rx1_Counter=0;
                                };
                        };
            }
//             else
                 {
                  Tmp_=In_1;
                  if ((Tmp_)&&(!Old_CBX1)) {
                                              CBX1_bit=1;
                                              if (CB1_Time>=250) CB1_Time=0;
                                              }
                  Old_CBX1=Tmp_;
                 };



//if(!Mode_2)
          {
            if (++Rx2_Counter<78) //Software RxD_2  for CBX_2
                    {
                    if (!In_2) Tmp_Rx2=Tmp_Rx2&Bit_Table[Rx2_Counter];
                    }
                    else
                        {
                         if (Rx2_Counter==78)
                            {
                            if (!Tmp_Rx2) Mode_2=1;
                            if (!Mode_2) Rx2_Buff[(Rx2_Pointer_Buff++)&0x07]=Tmp_Rx2;
                             Time_Out_2=0;
                             CBX2_Done_bit=1;
                             }
                             else
                                {
                                Rx2_Counter=78;
                                Tmp_Rx2=0xFF;
                                if (!In_2) Rx2_Counter=0;
                                };
                        }
            }
  //            else
                  {
                   Tmp_=In_2;
                   if ((Tmp_)&&(!Old_CBX2))
                                           {
                                            CBX2_bit=1;
                                            if (CB2_Time>=250) CB2_Time=0;
                                            }
                    Old_CBX2=Tmp_;
                   };



 if (++Rx3_Counter<78)   //Software Receive for Debuging RxD
                    {
                    if (!In_3) Tmp_Rx3=Tmp_Rx3&Bit_Table[Rx3_Counter];
                    }
                    else
                        {
                         if (Rx3_Counter==78) Rx3_Buff[(Rx3_Pointer_Buff++)&0x07]=Tmp_Rx3;
                             else
                                {
                                Rx3_Counter=78;
                                Tmp_Rx3=0xFF;
                                if (!In_3) Rx3_Counter=0;
                                };
                        };

#pragma optsize+
};




#define ADC_VREF_TYPE 0x00
// Read the AD conversion result

unsigned int read_adc(unsigned char adc_input)
{
ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
// Delay needed for the stabilization of the ADC input voltage
delay_us(10);
// Start the AD conversion
ADCSRA|=0x40;
// Wait for the AD conversion to complete
while ((ADCSRA & 0x10)==0);
ADCSRA|=0x10;
return ADCW;
}



//*************************************** MAIN ***********************************************

void main(void)
{

#pragma optsize+

unsigned char Temp_Byte,Temp_Byte2;

#ifdef _Test_
unsigned char Cmp1,Cmp2,Err1,Err2;
#endif



// Crystal Oscillator division factor: 1
#pragma optsize-
CLKPR=0x80;
CLKPR=0x00;
#ifdef _OPTIMIZE_SIZE_
#pragma optsize+
#endif



// Input/Output Ports initialization
// Port B initialization
// Func7=In Func6=In Func5=Out Func4=Out Func3=In Func2=In Func1=Out Func0=Out
// State7=P State6=P State5=1 State4=1 State3=P State2=P State1=1 State0=0
PORTB=0xFE;
DDRB=0x33;

// Port C initialization
// Func7=In Func6=Out Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=In
// State7=T State6=0 State5=T State4=T State3=P State2=1 State1=0 State0=P
PORTC=0x0D;
DDRC=0x46;

// Port D initialization
// Func7=Out Func6=In Func5=Out Func4=In Func3=Out Func2=Out Func1=Out Func0=Out
// State7=T State6=P State5=1 State4=P State3=1 State2=0 State1=0 State0=1
PORTD=0x79;
DDRD=0xAF;


// Port E initialization
// Func2=In Func1=In Func0=In
// State2=T State1=T State0=T
PORTE=0x00;
DDRE=0x00;

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: 230.400 kHz
// Mode: CTC top=OCR0A
// OC0A output: Disconnected
// OC0B output: Disconnected
TCCR0A=0x02;
TCCR0B=0x03;
TCNT0=0x00;
OCR0A=0x2F;
OCR0B=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 14745.600 kHz
// Mode: CTC top=OCR1A
// OC1A output: Discon.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: On
// Compare B Match Interrupt: Off
TCCR1A=0x00;
TCCR1B=0x09;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0xBF;
OCR1BH=0x00;
OCR1BL=0x00;


// External Interrupt(s) initialization
// INT0: Off
// INT1: Off
// INT2: Off
MCUCR=0x00;
MCUSR=0x00;



// Timer(s)/Counter(s) Interrupt(s) initialization
// Timer/Counter 0 Interrupt(s) initialization
TIMSK0=0x02;
TIMSK1=0x02;


// CAN Controller initialization
// CAN disabled
CANGCON=0x00;



// ADC initialization
// ADC Clock frequency: 921.600 kHz
// ADC Bandgap Voltage Reference: Off
// ADC High Speed Mode: Off
// ADC Auto Trigger Source: ADC Stopped
// Digital input buffers on ADC0: On, ADC1: On, ADC2: On, ADC3: On
// ADC4: On, ADC5: On, ADC6: On, ADC7: On
DIDR0=0x00;
ADMUX=ADC_VREF_TYPE & 0xff;
ADCSRA=0x84;
ADCSRB=0x20;



// SPI initialization
// SPI disabled
SPCR=0x00;


// Watchdog Timer initialization
// Watchdog Timer Prescaler: OSC/512k
// Watchdog Timer interrupt: Off
#asm("wdr")
WDTCSR=0x38;
WDTCSR=0x28;


RS485.Tx_Size=20;
Timer.Link_Timer=4;
Timer.Time=0;


Reset:

Timer.Counting=1;
Refresh_Bit=0;

if (Send_Mode) LINCR=0x00; //Disable Uart
 else LINCR=0x0F; // Enable Uart
    {
    // UART initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // UART Receiver: On
    // UART Transmitter: On
    // UART Baud Rate: 9600
    LINCR=0x0F;
    LINBTR=0x18;
    LINBRRH=0x00;
    LINBRRL=47;
    LINDAT=0xFF;
    }


#asm("sei") // Global enable interrupts

#ifndef _Test_
Sinc_Bit=0;
Timer_Sinc=0;
#endif

ClrScr();

Cursor_Hide();

Print(" Vault Debug Menu V1.00 ",30,0,7);


if ((Node_Address_|0x20)=='a') //AUTO External Switch at J6
    {
     Print("Node Address (Auto):0x0",1,12,1);
     Debug_Putchar(0x30+(!LockPS_Sensor&1));
    }
    else
        {
        Print("Node Address:",1,12,1);
        Debug_Putchar('0');
        Debug_Putchar('x');
        Debug_Putchar('0');
        Debug_Putchar(Node_Address_);
        }

for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
Print_Serial_No(20);


Print("; Firmware Version:",24,20,1);
Temp_Byte=Firmware_Ver[0];
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[16]=Temp_Byte;

Temp_Byte=Firmware_Ver[1];
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[17]=Temp_Byte;

Debug_Putchar('.');
Temp_Byte=Firmware_Ver[2];
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[18]=Temp_Byte;

Temp_Byte=Firmware_Ver[3];
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[19]=Temp_Byte;


Print("; ",49,20,1);
Print("; ",66,20,1);

Print(" INPUTS:",1,16,7);
Print("CBX Sensor=",10,16,1);
Print("; Position Sensor=",23,16,1);
Print("; Motor Lock Sensor=",43,16,1);
Print("; Motor Fault=",65,16,1);


Print("OUTPUTS:",1,14,7);
Print("[S]olenoid=",10,14,1);
Print("; Lamp[1]=",23,14,1);
Print("; Lamp[2]=",35,14,1);
Print("; Motor [P]hase=",47,14,1);
Print("; Motor [R]un=",65,14,1);


Print("Vault Holding Time=",1,18,1);
Temp_Byte=0x30+Solenoid_Time/10;
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[0]=Temp_Byte;
Temp_Byte=0x30+Solenoid_Time%10;
Debug_Putchar(Temp_Byte);
RS485.Get_All_Command[1]=Temp_Byte;
Debug_Putchar('[');
Debug_Putchar('s');
Debug_Putchar(']');


Key_Press=Rx3_Pointer_Buff;
Out_Light1=Status_Lights&1; 

while (1)
      {
       #asm("wdr");
       if ((Node_Address_|0x20)=='a')
           {
           Temp_Byte=Node_Address;
           Node_Address=0x30+!(LockPS_Sensor&1);
           if (0x30+!(LockPS_Sensor&1)!=Temp_Byte) goto Reset;
           }
           else Node_Address=Node_Address_;

       if ((LINSIR&8)) LINSIR=0xFF;

                                      


       //*************** Holding time Progress Bar
       Temp_Byte=Timer.Time/20;
       if (Timer.Time_Old!=Temp_Byte)
                {
                Timer.Time_Old=Temp_Byte;
                if (Timer.Counting==1)
                    {
                    Print("",27,18,1);
                    for (Temp_Byte=0;Temp_Byte<Timer.Time_Old;Temp_Byte++) Debug_Putchar(176); //Show Progres Bar
                    Timer.Counting=0;
                    }
                    else Print("   ",26+Solenoid_Time-(Solenoid_Time-Temp_Byte),18,1);
                }

       if ((Timer.Time==0)&&(Timer.Counting!=2))
            {
            Print("",27,18,1);
            for (Temp_Byte=0;Temp_Byte<Solenoid_Time-1;Temp_Byte++) Debug_Putchar(' '); //Erase Progress Bar
            Timer.Counting=2;
            }




       if (Key_Press!=Rx3_Pointer_Buff)
            {
            Temp_Byte=Rx3_Buff[(Rx3_Pointer_Buff-1)&0x07];

            Key_Press=Rx3_Pointer_Buff;


            if (Temp_Byte=='1') //On/Off Lamp 1
                {
                if ((Status_Lights&1)==1) Status_Lights&=2;
                 else Status_Lights|=1;
                Print("",33,14,1);
                Debug_Putchar(0x30+Out_Light1);
                }


            if (Temp_Byte=='2') //On/Off Lamp 2
                {
                if ((Status_Lights&2)==2) Status_Lights&=1;
                 else Status_Lights|=2;
                Print("",45,14,1);
                Debug_Putchar(0x30+Out_Light2);
                }



            if ((Temp_Byte|32)=='s')  //On/Off valve solenoid On/Off
                    {
                    Out_Valve=!Out_Valve;
                    Print("",21,14,1);
                    Debug_Putchar(0x30+Out_Valve);
                    }

            if ((Temp_Byte|32)=='p') // Motor Phase Left/Right
                    {
                    Out_Motor_Ph=!Out_Motor_Ph;
                    Print("",63,14,1);
                    Debug_Putchar(0x30+Out_Motor_Ph);
                    }

            if ((Temp_Byte|32)=='r') // Motor Run/Stop
                    {
                    Out_Motor_Run=!Out_Motor_Run;
                    Print("",79,14,1);
                    Debug_Putchar(0x30+!Out_Motor_Run);
                    }

             Temp_Byte=0;
            }      

    if ((Sinc_Bit)&&(RS485.Tx_Buffer[2]!='A'))
       {
       Sinc_Bit=0;
       if (Refresh_Bit) goto Reset;

       U_12V=read_adc(8)/3.1;
       U_5V=read_adc(9)/1.25;


       Temp_Byte=0x30;
       if ((U_5V<Min_5V)||(U_5V>Max_5V)) Temp_Byte|=1;
       if ((U_12V<Min_12V)||(U_12V>Max_12V)) Temp_Byte|=2;
       if (!Motor_Fault) Temp_Byte|=4;
       Alarm_Status=Temp_Byte;

       if ((Alarm_Status&3)) Led_Power=1; //Alarm Light On/Off
        else Led_Power=0;

       //Print("_ U(12V)=",49,18,1);
       Print("U(12V)=",51,20,1+6*((Alarm_Status>>1)&1));
       Temp_Byte=0x30+U_12V/100;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[5]=Temp_Byte;
       Temp_Byte=0x30+(U_12V%100)/10;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[6]=Temp_Byte;
       Debug_Putchar('.');
       Temp_Byte=0x30+U_12V%10;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[7]=Temp_Byte;
       Debug_Putchar('[');
       Debug_Putchar('V');
       Debug_Putchar(']');


       //Print("_ U(5V)=",65,18,1);
       Print("U(5V)=",68,20,1+6*Alarm_Status&1);
       Temp_Byte=0x30+U_5V/100;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[2]=Temp_Byte;
       Debug_Putchar('.');
       Temp_Byte=0x30+(U_5V%100)/10;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[3]=Temp_Byte;
       Temp_Byte=0x30+U_5V%10;
       Debug_Putchar(Temp_Byte);
       RS485.Get_All_Command[4]=Temp_Byte;
       Debug_Putchar('[');
       Debug_Putchar('V');
       Debug_Putchar(']');



       //Print("[S]olenoid=",11,14,1);
       Print("",21,14,1);
       Debug_Putchar(0x30+Out_Valve);


       //Print("; Lamp[1]=",24,14,1);
       Print("",33,14,1);
       Debug_Putchar(0x30+Out_Light1);


       //Print("; Lamp[2]=",36,14,1);
       Print("",45,14,1);
       Debug_Putchar(0x30+Out_Light2);



       //Print("; Motor Phase=",48,14,1);
       Print("",63,14,1);
       Debug_Putchar(0x30+Out_Motor_Ph);



       //Print("; Motor Run=",64,14,1);
       Print("",79,14,1);
       Debug_Putchar(0x30+!Out_Motor_Run);


       //Print("CBX Sensor=",11,16,1);
       Print("",21,16,1);
       Debug_Putchar(0x30+!CbxPS_Sensor);



       //Print("; Position Sensor=",29,16,1);
       Print("",41,16,1);
       Debug_Putchar(0x30+!RecPS_Sensor);



       //Print("; Motor Lock Sensor=",49,16,1);
       Print("",63,16,1);
       Debug_Putchar(0x30+!LockPS_Sensor);



       //Print("; Motor Fault=",66,16,1);
       Print("",79,16,1);
       Debug_Putchar(0x30+!Motor_Fault);


       Print_Vault_No(10);




       Print("Bin Number:",1,8,1);
       for (Temp_Byte=9+Mode_1;Temp_Byte<15;Temp_Byte++) Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
       Mode_Print(!Mode_1);



#ifdef _Test_
       if (Cmp1!=RS485.Tx_Buffer[8]) {Cmp1=RS485.Tx_Buffer[8];Err1++;}
       Debug_Putchar(0x30+Err1);
#endif


       Print("Cash Box Number:",1,6,1);
       if (Power_Light==0)
        {
         for (Temp_Byte=15+Mode_2;Temp_Byte<21;Temp_Byte++)
            {
             if (RS485.Tx_Buffer[Temp_Byte]!=':') Debug_Putchar(RS485.Tx_Buffer[Temp_Byte]);
              else Debug_Putchar('0');
            }
         if (Send_Mode) Mode_Print(RS485.Tx_Buffer[12]==':'?0:!Mode_2);
        }
         else Print(" ...   ",255,0,0);




#ifdef _Test_
       if (Cmp2!=RS485.Tx_Buffer[14]) {Cmp2=RS485.Tx_Buffer[14];Err2++;}
       Debug_Putchar(0x30+Err2);
#endif



       Print("Transmission Data Format",1,4,1);
       Mode_Print(!Send_Mode);
       if (Led_Link==0) Print("(Online) ",255,0,0);
        else Print("(Offline)",255,0,0);
       }



for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++)
   if ((Rx3_Buff[Temp_Byte]==13)||(Rx3_Buff[Temp_Byte]==27))
            {
             if (Rx3_Buff[Temp_Byte]==27) goto Reset;

             Temp_Byte=(Temp_Byte-6)&0x07;

             for (Temp_Byte2=0;Temp_Byte2<6;Temp_Byte2++) if (Password[Temp_Byte2]!=Rx3_Buff[(Temp_Byte2+Temp_Byte)&0x07]) goto Bad_Psw;


#ifdef _Test_

            Err1=0;
            Err2=0;
#endif


             //Password Corect ,enter in Menu for changing Serial , Vault number and valt holding time

Loop_Change: ClrScr();

             Print(" [S]erial Number|[V]ault Number|Holding [T]ime|[N]-Node Address|[Esc]-Main Menu ",1,0,7);

             Print_Vault_No(4);
             Print("V",1,4,4);

             Print_Serial_No(6);
             Print("S",1,6,4);

             Print("Time Vault Holding=",1,8,1);
             Debug_Putchar(0x30+(Solenoid_Time/10));
             Debug_Putchar(0x30+Solenoid_Time%10);
             Print("T",1,8,4);
             Print("[s]",23,8,1);

             if ((Node_Address_|0x20)=='a')
                {
                Print("Node Address (Auto)=0x0",1,10,1);
                Debug_Putchar(0x30+(!LockPS_Sensor&1));
                Print("N",1,10,4);
                }
                else
                    {
                    Print("Node Address=",1,10,1);
                    Debug_Putchar('0');
                    Debug_Putchar('x');
                    Debug_Putchar('0');
                    Debug_Putchar(Node_Address_);
                    Print("N",1,10,4);
                    }


             Temp_Byte=Debug_Getchar();

             if (Temp_Byte==27) goto Reset;

             if ((Temp_Byte|32)=='s')
                    {
                    Print("New Serial Number:",1,12,7);
                    if (Input(Buff_Temp,8,0)==8) for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Serial_No[Temp_Byte]=Buff_Temp[Temp_Byte];
                    };

            if ((Temp_Byte|32)=='n')
                    {
                    Print("New Node Address [0-7] or [A]-Auto:",1,12,7);
                    if (Input(Buff_Temp,1,0)==1) if (((Buff_Temp[0]>=0x30)&&(Buff_Temp[0]<=0x38))||((Buff_Temp[0]|0x20)=='a')) Node_Address_=Buff_Temp[0];
                    };


             if ((Temp_Byte|32)=='v')
                    {
                    Print("New Vault Number:",1,12,7);
                    if (Input(Buff_Temp,6,0)==6) for (Temp_Byte=0;Temp_Byte<6;Temp_Byte++) Vault[Temp_Byte]=Buff_Temp[Temp_Byte];
                    };


             if ((Temp_Byte|32)=='t')
                    {
                    Print("New Vault Holding Time:",1,12,7);
                    if (Input(Buff_Temp,2,0)==2)
                            {
                            Temp_Byte=(10*(Buff_Temp[0]-0x30))+Buff_Temp[1]-0x30;
                            if ((Temp_Byte<=25)&&(Temp_Byte>=4)) Solenoid_Time=Temp_Byte;
                            Temp_Byte=0;
                            }
                    };

             goto Loop_Change;


    Bad_Psw: for (Temp_Byte=0;Temp_Byte<8;Temp_Byte++) Rx3_Buff[Temp_Byte]=0;
             break;
            };
    };
};






void Print_Serial_No(unsigned char position)
{
 unsigned counter,Tmp;

 Print("Serial Number:",1,position,1);

 for (counter=0;counter<8;counter++)
    {
    Tmp=Serial_No[counter];
    RS485.Get_All_Command[counter+8]=Tmp;
    Debug_Putchar(Tmp);
    }
}




void Print_Vault_No(unsigned char position)
{
 unsigned counter;
 Print("Vault Number:",1,position,1);
 for (counter=0;counter<6;counter++) Debug_Putchar(Vault[counter]);
}

