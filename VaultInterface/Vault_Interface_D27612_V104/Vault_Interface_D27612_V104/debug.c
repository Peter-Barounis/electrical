
#include "debug.h"



extern unsigned char Rx3_Counter,Rx3_Pointer_Buff,Tmp_Rx3,Rx3_Buff[8];
extern unsigned char Tx1_Counter,TxD_1;


#pragma optsize+

//*********************************** Debug putchar *******************************
void Debug_Putchar(unsigned char Data)
{
 TxD_1=Data;
 Tx1_Counter=0;
 while (Tx1_Counter<80);
}




//*********************************** Debug Getchar *******************************
unsigned char Debug_Getchar(void)
{
 Rx3_Pointer_Buff=0;
 while (Rx3_Pointer_Buff==0) #asm("wdr");
 return(Rx3_Buff[0]);
}




//********************************* DEBUG PUTSTRING **************************************
void Print(char flash *text,unsigned char X,unsigned char Y,unsigned char color)
 {

            #asm ("wdr");


    if (color)
             {
              Debug_Putchar(0x1B);
              Debug_Putchar(0x5B);
              Debug_Putchar(0x30+color);
              Debug_Putchar(0x6D);
             };

   if (X<200)
            {
              Debug_Putchar(0x1B);
              Debug_Putchar(0x5B);
              if (Y<10) Debug_Putchar(0x30+Y);
               else
                    {
                    Debug_Putchar(0x30+Y/10);
                    Debug_Putchar(0x30+(Y%10));
                    };

              Debug_Putchar(0x03B);
              Debug_Putchar(0x30+(X/10));
              Debug_Putchar(0x30+(X%10));
              Debug_Putchar(0x48);

            }

              while (*text!=0) Debug_Putchar(*text++);

              if (color)
                          {
                           Debug_Putchar(0x1B);
                           Debug_Putchar(0x5B);
                           Debug_Putchar(0x30);
                           Debug_Putchar(0x6D);
                          }

  };






//*************************************** Input Data ****************************************
unsigned char Input(unsigned char *Out_Pointer,unsigned char Size,unsigned char Password)
 {
  unsigned char Tmp,Tmp2;

    Debug_Putchar(' ');
    Cursor_Show();
    Tmp=0;

Loop_input:

    Tmp2=Debug_Getchar();

    if (Tmp2==27)
                {
                Tmp=0;
                goto Exit_Input;
                };


    if (Tmp2==13)
                {
                 if (Tmp==Size) goto Exit_Input;

                 Tmp=0;
                 goto Exit_Input;
                };

    if (Tmp2==8)
               {
               if (Tmp>0)
                        {
                        Tmp--;
                        Debug_Putchar(8);
                        Debug_Putchar(' ');
                        Debug_Putchar(8);
                        }
                        goto Loop_input;
               }


    if (Tmp<Size)
                {
                Out_Pointer[Tmp]=Tmp2;

                if (Password) Debug_Putchar('*');
                 else Debug_Putchar(Tmp2);
                Tmp++;
                };

                goto Loop_input;
  Exit_Input:
  Cursor_Hide();
  return(Tmp);
 }






//************************************************** Print Mode New/Old *****************************************
void Mode_Print(unsigned char mode)
{

 if (mode) Print(" => New ",255,0,0);
        else Print(" => Old ",255,0,0);

 Print("Mode ",255,0,0);
};




     //********************************************** Clear Screen ***********************************************************
void ClrScr(void)
{
 Debug_Putchar(12);
 Debug_Putchar(0x1B);
 Debug_Putchar('[');
 Debug_Putchar('2');
 Debug_Putchar('J');
}



//************************************************************ Cursor Hide ********************************************************
void Cursor_Hide()
{
Debug_Putchar(0x1B);
Debug_Putchar('[');
Debug_Putchar('?');
Debug_Putchar('2');
Debug_Putchar('5');
Debug_Putchar('l');
}




//************************************************************ Cursor Show ********************************************************
void Cursor_Show()
{
Debug_Putchar(0x1B);
Debug_Putchar('[');
Debug_Putchar('?');
Debug_Putchar('2');
Debug_Putchar('5');
Debug_Putchar('h');
}
