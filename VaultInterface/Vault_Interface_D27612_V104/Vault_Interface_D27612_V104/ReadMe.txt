*****V1.04
-Add logic to blink Out_Lig2  (PORTD.2) if CBX ID is detected.



*****V1.03
- Added Auto NODE mode, input LockPS_Sensor-(PB.2) is used for set NODE 0 or 1. Default after programming NODE is set to Auto mode.Node is change by jumper at J6 (PB.2) (pins 2-3) Open NODE-0 ,Short NODE-1.(Still is possible to set node 0 or 1 thru        Diagnostic Menu without jumper)
-Default Vaulting time after programming is set to 12 sec.
-(Change CKOUT Fuse to support Motor Circuit)
-Swapped Motor [P]hase and Motor [R]un keys in Diagnostic Menu.



***** V1.02
- Fixed info data send no define node address

**** V10.1
- Fixed Bin and CBX ID in old mode
